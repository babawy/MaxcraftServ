package fr.maxcraft;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Animals;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.material.Sign;

public class ClassListener implements Listener {
	private LockMain plugin;

	public ClassListener(LockMain plugin) {
		this.plugin = plugin;
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@EventHandler
	public void onclic(PlayerInteractEvent e) {
		if ((e.getAction().equals(Action.RIGHT_CLICK_BLOCK))
				&& (LockMain.isLock(e.getClickedBlock()))) {
			if (LockMain.isAllow(e.getPlayer(), e.getClickedBlock()))
				return;
			e.setCancelled(true);
			e.getPlayer().sendMessage(
					ChatColor.BLUE + "Ceci ne vous appartient pas!");
		}
		if (e.getAction().equals(Action.RIGHT_CLICK_AIR)
				&& e.getPlayer().getItemInHand().getType()
						.equals(Material.SADDLE))
			LockMain.setSaddleOwner(e.getPlayer());
	}

	@EventHandler
	public void onbreak(BlockBreakEvent e) {
		if (e.getBlock().getType().equals(Material.WALL_SIGN)) {
			Sign s = (Sign) e.getBlock().getState().getData();
			if (LockMain.isLock(e.getBlock().getRelative(s.getAttachedFace()))) {
				if (LockMain.isAllow(e.getPlayer(),
						e.getBlock().getRelative(s.getAttachedFace())))
					return;
				e.setCancelled(true);
				e.getPlayer().sendMessage(
						ChatColor.BLUE + "Ceci ne vous appartient pas!");
			}
		}
			if (LockMain.isLock(e.getBlock())) {
				if (LockMain.isAllow(e.getPlayer(), e.getBlock()))
					return;
				e.setCancelled(true);
				e.getPlayer().sendMessage(
						ChatColor.BLUE + "Ceci ne vous appartient pas!");
			}
	}

	@EventHandler
	public void onclic(PlayerInteractAtEntityEvent e) {
		if ((e.getRightClicked() instanceof ArmorStand)) {
			if (LockMain.MZ
					.getZone(e.getRightClicked().getLocation())
					.getOwner()
					.equals(LockMain.MM.getMaxcraftien(e.getPlayer().getName())))
				return;
			if (LockMain.MZ
					.getZone(e.getRightClicked().getLocation())
					.getBuilders()
					.contains(
							LockMain.MM.getMaxcraftien(e.getPlayer().getName())))
				return;
			if (e.getPlayer().hasPermission("maxcraft.lock.bypass"))
				return;
			e.setCancelled(true);
			e.getPlayer().sendMessage(
					ChatColor.BLUE + "Ceci ne vous appartient pas!");
		}
	}

	@EventHandler
	public void ondamage(EntityDamageByEntityEvent e) {
		if ((e.getEntity() instanceof Animals))
			if (e.getDamager() instanceof Player){
				if(LockMain.MZ.getZone(e.getEntity().getLocation())==null)
					return;
				if (LockMain.MZ.getZone(e.getEntity().getLocation()).canBuild(
						(Player) e.getDamager()))
					return;
				if (e.getDamager().hasPermission("maxcraft.lock.bypass"))
					return;
					e.setCancelled(true);
					e.getDamager().sendMessage(
							ChatColor.BLUE + "Ceci ne vous appartient pas!");
				}
		if ((e.getEntity() instanceof ArmorStand)) {
			if ((e.getDamager() instanceof Player)) {
				if(LockMain.MZ.getZone(e.getEntity().getLocation())==null)
					return;
				if (LockMain.MZ
						.getZone(e.getEntity().getLocation())
						.getOwner()
						.equals(LockMain.MM.getMaxcraftien((Player) e
								.getDamager())))
					return;
				if (e.getDamager()
						.hasPermission("maxcraft.lock.bypass"))
					return;
				if (LockMain.MZ
						.getZone(e.getEntity().getLocation())
						.getBuilders()
						.contains(
								LockMain.MM.getMaxcraftien((Player) e
										.getDamager())))
					return;
			}
			e.setCancelled(true);
			e.getDamager().sendMessage(
					ChatColor.BLUE + "Ceci ne vous appartient pas!");
		}
	}

	@EventHandler
	public void ondamage(PlayerInteractEntityEvent e) {
		if (e.getRightClicked() instanceof Horse) {
			Horse c = (Horse) e.getRightClicked();
			if (!e.getPlayer().getItemInHand().equals(Material.NAME_TAG))
				if (c.getInventory().getSaddle() != null)
					if (c.getInventory().getSaddle().getItemMeta().hasLore())
						if (!e.getPlayer()
								.hasPermission("maxcraft.lock.bypass"))
							if (!c.getInventory()
									.getSaddle()
									.getItemMeta()
									.getLore()
									.contains(
											ChatColor.MAGIC
													+ e.getPlayer()
															.getUniqueId()
															.toString()))
								e.setCancelled(true);
		}
	}
	@EventHandler
	public void onsign(SignChangeEvent e){
		if (e.getLine(0).equalsIgnoreCase("[private]")){
			if (!e.getPlayer().hasPermission("maxcraft.lock.bypass"))
				e.setLine(1,e.getPlayer().getName());
			e.setLine(0, "[private]");
		}
	}
}