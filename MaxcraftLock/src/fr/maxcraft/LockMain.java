package fr.maxcraft;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;
import org.bukkit.plugin.java.JavaPlugin;

public class LockMain extends JavaPlugin {
	public static MaxcraftManager MM = null;
	public static MaxcraftZones MZ = null;
	public static List<String> protect;

	public void onEnable() {
		Listener l = new ClassListener(this);
		MM = getMM();
		MZ = getMZ();
		Bukkit.getLogger().info("Plugins charg�s");
		saveDefaultConfig();
		protect = getConfig().getStringList("protected");
	}

	public void onDisable() {
	}

	public MaxcraftManager getMM() {
		MaxcraftManager mm = (MaxcraftManager) getServer().getPluginManager()
				.getPlugin("MaxcraftManager");
		Bukkit.getLogger().info("MaxcraftManager charg� !");
		return mm;
	}

	public MaxcraftZones getMZ() {
		MaxcraftZones mz = (MaxcraftZones) getServer().getPluginManager()
				.getPlugin("MaxcraftZones");
		Bukkit.getLogger().info("MaxcraftZones charg� !");
		return mz;
	}

	public static List<String> getSignArg(Block b) {
		List<String> arg = getSignText(b);
		if (getDouble(b) != null)
			arg.addAll(getSignText(getDouble(b)));
		if (b.getType().toString().contains("DOOR")) {
			arg.addAll(getSignText(b.getRelative(BlockFace.NORTH)));
			arg.addAll(getSignText(b.getRelative(BlockFace.EAST)));
			arg.addAll(getSignText(b.getRelative(BlockFace.SOUTH)));
			arg.addAll(getSignText(b.getRelative(BlockFace.WEST)));
			arg.addAll(getSignText(b.getRelative(BlockFace.UP)));
			arg.addAll(getSignText(b.getRelative(BlockFace.DOWN)));
		}
		return arg;
	}

	private static List<String> getSignText(Block b) {
		List<String> arg = new ArrayList();
		List<BlockFace> bf = Arrays.asList(BlockFace.NORTH,BlockFace.EAST,BlockFace.SOUTH,BlockFace.WEST);
		Sign s = null;
		for (BlockFace f:bf)
			if (b.getRelative(f).getType().equals(Material.WALL_SIGN))
			{
				s = (Sign) b.getRelative(f).getState();
				org.bukkit.material.Sign si = (org.bukkit.material.Sign) s.getData();
				if (s.getBlock().getRelative(si.getAttachedFace()).equals(b))
				{
					if (s != null) {
						arg.add(s.getLine(0));
						arg.add(s.getLine(1));
						arg.add(s.getLine(2));
						arg.add(s.getLine(3));
				}
			}
		}
		return arg;
	}

	public static Block getDouble(Block b) {
		Block bl = null;
		if (b.getRelative(BlockFace.NORTH).getType() == b.getType())
			bl = b.getRelative(BlockFace.NORTH);
		if (b.getRelative(BlockFace.EAST).getType() == b.getType())
			bl = b.getRelative(BlockFace.EAST);
		if (b.getRelative(BlockFace.SOUTH).getType() == b.getType())
			bl = b.getRelative(BlockFace.SOUTH);
		if (b.getRelative(BlockFace.WEST).getType() == b.getType())
			bl = b.getRelative(BlockFace.WEST);
		return bl;
	}

	public static boolean isAllow(Player p, Block b) {
		Maxcraftien m = MM.getMaxcraftien(p);
		Zone z = MZ.getZone(b.getLocation());
		if (m == null)
			return false;
		if ((z == null) && (!getSignArg(b).contains("[private]")))
			return true;
		if (z != null) {
			if (z.getTags().contains("lock-public")&& (!getSignArg(b).contains("[private]")))
				return true;
			if (z.hasOwner())
			if ((z.getOwner().equals(m))&& (!getSignArg(b).contains("[private]")))
				return true;
			if ((z.getBuilders().contains(m))
					&& (!getSignArg(b).contains("[private]")))
				return true;
			if (m.haveFaction())
					if(z.getTags().contains(m.getFaction().getTag()))
						return true;	
		}
		if (p.hasPermission("maxcraft.lock.bypass"))
			return true;
		if (p.hasPermission("maxcraft.lock."+b.getType().toString().toLowerCase()))
			return true;
		if ((m.haveFaction())
				&& (getSignArg(b).contains("[" + m.getFaction().getTag() + "]")))
			return true;
		if (getSignArg(b).contains("[public]"))
			return true;
		if (getSignArg(b).contains("[publique]"))
			return true;
		if (getSignArg(b).contains("[" + p.getName() + "]"))
			return true;
		if (getSignArg(b).contains(p.getName())) 
			return true;
		if(getSignArg(b).contains("[SHOP]"))
			return true;
		return false;
		
	}
	

	public static boolean isLock(Block b) {
		if (b.getType().toString().contains("DOOR")) {
			return true;
		}
		return protect.contains(b.getType().toString());
	}

	public static void setSaddleOwner(Player p) {
		List<String> lore = new ArrayList();
		lore.add(ChatColor.MAGIC+p.getUniqueId().toString());
		ItemMeta meta = p.getItemInHand().getItemMeta();
		meta.setDisplayName("Selle de "+ChatColor.RESET+p.getName());
		meta.setLore(lore);
		p.getItemInHand().setItemMeta(meta);
		p.updateInventory();
		
	}
}