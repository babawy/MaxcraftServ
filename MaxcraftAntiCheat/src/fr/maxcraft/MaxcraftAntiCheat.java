package fr.maxcraft;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;


import fr.maxcraft.MaxcraftManager;


public class MaxcraftAntiCheat extends JavaPlugin{


	//PLUGINS
	public static MaxcraftManager MM = null;


	//LISTES
	public static ArrayList<OreStats> listOreStats = new ArrayList<OreStats>();

	//LISTENERS
	private TrackListener Listener;

	
	@Override
	public void onDisable() {
		
	}

	@Override
	public void onEnable() {
		
		// PLUGINS
		this.MM = this.getMM();
		
		//LISTENERS
		this.Listener = new TrackListener(this);
		this.getServer().getPluginManager().registerEvents(this.Listener, this);

		//LOAD ENTITIES 
		try {
			this.loadEntities();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void log(String message, ChatColor color)
	{
		ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
		console.sendMessage(color + "[MAXCRAFT] "+ ChatColor.WHITE +"[MaxcraftAntiCheat] " + message);
		
	}
	public void log(String message)
	{
		ChatColor color = ChatColor.RED;
		log(message, color);
		
	}
	
	public String message(String message)
	{
		return ChatColor.RED + "[AntiCheat] " + ChatColor.GRAY + message;
	}
	
	public MaxcraftManager getMM()
	{
		MaxcraftManager mm = (MaxcraftManager) this.getServer().getPluginManager().getPlugin("MaxcraftManager");
        log("MaxcraftManager charg� !");
		return mm;
	}
	

	public void loadEntities() throws SQLException
	{
		//Zones
				ResultSet result;
				
				result = this.MM.mysql_query("SELECT * FROM orestats", false);
				
				while(result.next())
				{
					Maxcraftien m = this.MM.getMaxcraftien(result.getInt("user"));
					OreStats os = new OreStats(this, result.getInt("id"), m, result.getInt("totalStone"), result.getInt("totalDiamond") ,result.getInt("fastDiamond"), result.getInt("totalGold"), result.getInt("fastGold"), result.getInt("totalIron"), result.getInt("fastIron"));
					log("OreStats <"+ m.getUserName() +">", ChatColor.GREEN);
					
				}
	}
	
	public OreStats getOreStats(Maxcraftien m)
	{
		for(OreStats os : this.listOreStats)
		{
			if(os.getUser() == m)
			{
				return os;
			}
		}
		return null;
		
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		//MAXCRAFT
		if(cmd.getName().equalsIgnoreCase("xray")){ 
		this.commandXray(sender, args);
		}
	
		return true;
	}
	
	public void commandXray(CommandSender sender,  String[] args)
	{
		if(args == null)
		{
			this.message("Il manque des param�tres !");
		}
		
		switch(args[0])
		{
		case "list": case "l": case "liste":
			this.commandList(sender, args);
			break;
		case "track": case "t": case "traque":
			this.commandTrack(sender, args);
			break;
			default:
				this.message("Il manque des param�tres !");
				
		}
	}
	
	public void commandList(CommandSender sender,  String[] args)
	{
		sender.sendMessage(this.message("LISTE DES RATIOS"));
		double min;
		int stone;
		
		if(args.length >= 2)
		{
		min = Double.parseDouble(args[1]);
		}
		else
		{
			min = -0;
		}
		
		if(args.length >= 3)
		{
		stone = Integer.parseInt(args[2]);
		}
		else
		{
			stone = 0;
		}
	
		for(OreStats os : this.listOreStats)
		{
			if(os.ratioD() > min && os.getTotalStone() > stone)
			{
				sender.sendMessage("- "+ os.getUser().getUserName() + " : "+ ChatColor.AQUA + os.ratioD() + " " + ChatColor.YELLOW + os.ratioG() + " " + ChatColor.GRAY + os.ratioI() + " "+ ChatColor.GRAY+ "("+os.getTotalStone()+") F:"+os.getFastDiamond());
			}
		}
	}
	
	public void commandTrack(CommandSender sender,  String[] args)
	{
		if(args.length != 2)
		{
			sender.sendMessage(this.message("Indiquez un joueur � traquer"));
			return;
		}
		
		Maxcraftien m = this.MM.getMaxcraftien(args[1]);
		
		if(m == null)
		{
			sender.sendMessage(this.message("Ce n'est pas un joueur !"));
			return;
		}
		
		OreStats os = this.getOreStats(m);
		
		if(os == null)
		{
			sender.sendMessage(this.message("Le joueur n'as pas encore de stats !"));
			return;
		}
		
		sender.sendMessage(this.message("STATS DE <"+os.getUser().getUserName()+">"));
		sender.sendMessage(ChatColor.AQUA + "Diamant : " + os.ratioD() + " ["+os.getTotalDiamond()+"/"+os.getTotalStone()+"] F:"+ os.getFastDiamond());
		sender.sendMessage(ChatColor.YELLOW + "Or : " + os.ratioG() + " ["+os.getTotalGold()+"/"+os.getTotalStone()+"] F:"+ os.getFastGold());
		sender.sendMessage(ChatColor.GRAY + "Fer : " + os.ratioI() + " ["+os.getTotalIron()+"/"+os.getTotalStone()+"] F:"+ os.getFastIron());
		
		
		
	}

}
