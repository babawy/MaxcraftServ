package fr.maxcraft;

import java.sql.SQLException;
import java.util.Date;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class TrackListener implements Listener{

	//VARIABLES
		private MaxcraftAntiCheat plugin;
					
				
		//CONSTRUCTOR
		public TrackListener(MaxcraftAntiCheat p)
		{
		this.plugin = p;
		}
		
		//QUIT
		@EventHandler(priority = EventPriority.NORMAL)
		public void onQuit(PlayerQuitEvent e){
		
			Maxcraftien m = this.plugin.getMM().getMaxcraftien(e.getPlayer().getName());
			if(m == null)
			{
				return;
			}
			
			OreStats os = this.plugin.getOreStats(m);
			
			if(os == null)
			{
				return;
			}
			
			try {
				os.saveInSql();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
		
		@EventHandler(priority = EventPriority.NORMAL)
		public void onMine(BlockBreakEvent e){
			
			Maxcraftien m = this.plugin.MM.getMaxcraftien(e.getPlayer().getName());
			if(m == null)
			{
				return;
			}
			
			OreStats os = this.plugin.getOreStats(m);
			
			//OS inexistant
			if(os == null)
			{
			
				os = new OreStats(this.plugin, 0, m, 0,0,0,0,0,0,0);
				try {
					os.saveInSql();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			Material mat = e.getBlock().getType();
			
			//Fast Mining
			Date time = new Date();
			
			if(mat.equals(Material.DIAMOND_ORE) && os.getLocLastDiamond() != null && os.getLocLastDiamond().distance(e.getBlock().getLocation()) >= 5 && ((time.getTime()-os.getLastDiamond().getTime())) < 120*1000)
			{
				os.setFastDiamond(os.getFastDiamond()+1);
				
			}
			
			if(mat.equals(Material.GOLD_ORE) && os.getLocLastGold() != null && os.getLocLastGold().distance(e.getBlock().getLocation()) >= 5 && ((time.getTime()-os.getLastGold().getTime())) < 120*1000)
			{
				os.setFastGold(os.getFastGold()+1);
				
			}
			
			if(mat.equals(Material.IRON_ORE) && os.getLocLastIron() != null && os.getLocLastIron().distance(e.getBlock().getLocation()) >= 5 && ((time.getTime()-os.getLastIron().getTime())) < 120*1000)
			{
				os.setFastIron(os.getFastIron()+1);
				
			}
			
			//Enregistrement
		
			
			if(mat.equals(Material.STONE))
			{
				os.setTotalStone(os.getTotalStone()+1);
				
			}
			
			if(mat.equals(Material.DIAMOND_ORE))
			{
				os.setTotalDiamond(os.getTotalDiamond()+1);
				os.setLastDiamond(new Date());
				os.setLocLastDiamond(e.getBlock().getLocation());
			}
			
			if(mat.equals(Material.GOLD_ORE))
			{
				os.setTotalGold(os.getTotalGold()+1);
				os.setLastGold(new Date());
				os.setLocLastGold(e.getBlock().getLocation());
			}
			
			if(mat.equals(Material.IRON_ORE))
			{
				os.setTotalIron(os.getTotalIron()+1);
				os.setLastIron(new Date());
				os.setLocLastIron(e.getBlock().getLocation());
			}
		
			
			
			
			
		}
}
