package fr.maxcraft;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class OreStats {

	//VARIABLES
			private int id;
			private MaxcraftAntiCheat plugin;
			private Maxcraftien user;
			private boolean isCheating;
			
			private int totalStone;
			
			private Date lastDiamond;
			private int totalDiamond;
			private int fastDiamond;
			
			private Date lastGold;
			private int totalGold;
			private int fastGold;
			
			private Date lastIron;
			private int totalIron;
			private int fastIron;
			
			private Location locLastDiamond;
			private Location locLastGold;
			private Location locLastIron;
					
			//CONSTRUCTOR
			public OreStats(MaxcraftAntiCheat p, int id, Maxcraftien user, int totalStone, int totalDiamond,int fastDiamond, int totalGold, int fastGold,int totalIron, int fastIron)
			{
			this.id = id;
			this.plugin = p;
			
			this.user = user;
			this.totalStone = totalStone;
			this.totalDiamond = totalDiamond;
			this.fastDiamond = fastDiamond;
			this.totalGold = totalGold;
			this.fastGold = fastGold;
			this.totalIron = totalIron;
			this.fastIron = fastIron;
			
			this.lastDiamond = new Date();
			this.lastGold = new Date();
			this.lastIron = new Date();
			
			this.plugin.listOreStats.add(this);
			}
			
			public void saveInSql() throws SQLException
			{
				
					//Existe d�j�, on delete
					this.plugin.MM.mysql_update("DELETE FROM `orestats` WHERE `orestats`.`id` ="+ this.id);
			
					this.id = this.plugin.MM.getNextId("orestats");
					
					//On cr�e
					this.plugin.MM.mysql_update("INSERT INTO `orestats` (`id`, `user`, `totalStone`, `totalDiamond`, `totalGold`, `totalIron`, `fastDiamond`, `fastGold`, `fastIron`) VALUES ("
							+ "NULL,"
							+ " '"+this.user.getId()+"',"
							+ " '"+this.totalStone+"',"
							+ " '"+this.totalDiamond+"',"
							+ " '"+this.totalGold+"',"
							+ " '"+this.totalIron+"',"
							+ " '"+this.fastDiamond+"',"
							+ " '"+this.fastGold+"',"
							+ " '"+this.fastIron+"'"
							+ ");");
			}

			

			public Maxcraftien getUser() {
				return user;
			}

			public int getId() {
				return id;
			}

			public void setId(int id) {
				this.id = id;
			}

			public boolean isCheating() {
				return isCheating;
			}

			public void setCheating(boolean isCheating) {
				this.isCheating = isCheating;
			}

			public Location getLocLastDiamond() {
				return locLastDiamond;
			}

			public void setLocLastDiamond(Location locLastDiamond) {
				this.locLastDiamond = locLastDiamond;
			}

			public Location getLocLastGold() {
				return locLastGold;
			}

			public void setLocLastGold(Location locLastGold) {
				this.locLastGold = locLastGold;
			}

			public Location getLocLastIron() {
				return locLastIron;
			}

			public void setLocLastIron(Location locLastIron) {
				this.locLastIron = locLastIron;
			}

			public int getTotalStone() {
				return totalStone;
			}

			public void setTotalStone(int totalStone) {
				this.totalStone = totalStone;
			}

			public Date getLastDiamond() {
				return lastDiamond;
			}

			public void setLastDiamond(Date lastDiamond) {
				this.lastDiamond = lastDiamond;
			}

			public int getTotalDiamond() {
				return totalDiamond;
			}

			public void setTotalDiamond(int totalDiamond) {
				this.totalDiamond = totalDiamond;
			}

			public int getFastDiamond() {
				return fastDiamond;
			}

			public void setFastDiamond(int fastDiamond) {
				this.fastDiamond = fastDiamond;
			}

			public Date getLastGold() {
				return lastGold;
			}

			public void setLastGold(Date lastGold) {
				this.lastGold = lastGold;
			}

			public int getTotalGold() {
				return totalGold;
			}

			public void setTotalGold(int totalGold) {
				this.totalGold = totalGold;
			}

			public int getFastGold() {
				return fastGold;
			}

			public void setFastGold(int fastGold) {
				this.fastGold = fastGold;
			}

			public Date getLastIron() {
				return lastIron;
			}

			public void setLastIron(Date lastIron) {
				this.lastIron = lastIron;
			}

			public int getTotalIron() {
				return totalIron;
			}

			public void setTotalIron(int totalIron) {
				this.totalIron = totalIron;
			}

			public int getFastIron() {
				return fastIron;
			}

			public void setFastIron(int fastIron) {
				this.fastIron = fastIron;
			}

			public void setUser(Maxcraftien user) {
				this.user = user;
			}
			
			public double ratioD()
			{
				return (double) Math.round(((double)this.totalDiamond/(double)this.totalStone)*1000*100)/100;
				
			}
			
			public double ratioG()
			{
				return (double) Math.round(((double)this.totalGold/(double)this.totalStone)*1000 *100)/100;
				
			}
			
			public double ratioI()
			{
				return (double) Math.round(((double)this.totalIron/(double)this.totalStone)*1000*100)/100;
				
			}
			
}
