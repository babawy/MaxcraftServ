package fr.maxcraft;


import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.maxcraft.event.ShopInfoEvent;
import fr.maxcraft.event.ShopTransactionEvent;

public class Shop {

	private MaxcraftMarket plugin;
	private Sign sign;
	private Maxcraftien owner;
	private String type;
	private int amount;
	private float price;
	private boolean admin;
	private int id;
	
	public Shop(MaxcraftMarket plugin, int id, Location location, Maxcraftien m, String type, int amount, float price, boolean admin)
	{
		this.plugin = plugin;
		
		this.id = id;
		
		try
		{
		this.sign = (Sign) location.getBlock().getState();
		}
		catch(Exception e){
		this.removeFromConfig();
		return;
		}
		
		this.owner = m;
		this.type = type;
		this.amount = amount;
		this.price = price;
		this.admin = admin;
		
		this.plugin.ShopManager.shops.add(this);

		this.loadSign();
	}
	
	public void remove()
	{
		
		this.plugin.ShopManager.shops.remove(this);
		this.removeFromConfig();
	}
	
	public void removeFromConfig()
	{
		this.plugin.getConfig().set("shops."+this.id, null);
		this.plugin.saveConfig();
	}
	

	
	public Chest getChest()
	{
		return this.plugin.getRelativeChest(this.sign);
	}
	
	
	public void saveInConfig()
	{
		ConfigurationSection section = this.plugin.getConfig().createSection("shops."+ this.id);

		section.set("id", this.id);
		section.set("owner", this.owner.getId());
		section.set("type", this.type);
		section.set("price", this.price);
		section.set("admin", this.admin);
		section.set("amount", this.amount);
	
		section.set("x", this.sign.getLocation().getX());
		section.set("y", this.sign.getLocation().getY());
		section.set("z", this.sign.getLocation().getZ());
		section.set("world", this.sign.getLocation().getWorld().getName());	
		
		this.plugin.saveConfig();
	}
	
	public ItemFrame getItemFrame()
	{
		for(Entity e : this.sign.getLocation().getWorld().getEntities())
		{
			if(! (e instanceof ItemFrame))
			{
				continue;
			}
			
			if(e.getLocation().getBlock().equals(this.getChest().getBlock().getRelative(0, 1, 0)))
			{
				return (ItemFrame) e;
			}
			
		}
		
		return null;
	}
	
	public ItemStack getItemStack()
	{
		ItemStack is = null;
		
		if(this.getItemFrame() != null)
		{
		is = this.getItemFrame().getItem();
		}
		
		return is;
	}
	
	public void reloadSign()
	{
		this.sign.update();
		this.sign.getChunk().unload();
		this.sign.getChunk().load();
	}
	
	public void loadSign()
	{
		
		ChatColor color = ChatColor.GREEN;
		
		if(!this.canWork())
		{
			color = ChatColor.GREEN;
		}
		
		this.sign.setLine(0, ChatColor.BOLD + "[SHOP]");
		this.sign.setLine(1, ChatColor.DARK_AQUA +""+ ChatColor.BOLD + this.amount + ChatColor.BLACK+ChatColor.RESET+ " pour");
		this.sign.setLine(2, ChatColor.DARK_AQUA +" "+ ChatColor.BOLD + this.plugin.MM.economy.format((double) this.price));
		
		
		
		if(this.type.equals("SELL"))
		{
			this.sign.setLine(3, color + "" + ChatColor.BOLD + "- ACHETER -");
		}
		else if(this.type.equals("BUY"))
		{
			this.sign.setLine(3,  color + "" + ChatColor.BOLD + "- VENDRE -");
		}
		
		this.reloadSign();
	}
	
	public boolean canWork()
	{
		if(this.getSign() == null)
		{
			return false;
		}
		
		if(this.getChest() == null)
		{
			return false;
		}
		
		if(this.getItemFrame() == null)
		{
			return false;
			
		}
		
		
		
		if(this.getItemFrame().getItem() == null || this.getItemFrame().getItem().getType().equals(Material.AIR))
		{
			return false;
		}
		
		return true;
	}
	
	

	// GETTERS SETTERS
	public Sign getSign() {
		return sign;
	}

	public void setSign(Sign sign) {
		this.sign = sign;
	}

	public Maxcraftien getOwner() {
		return owner;
	}

	public void setOwner(Maxcraftien owner) {
		this.owner = owner;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public boolean hasAccess(Player p)
	{
		
		if(p.hasPermission("maxcraft.market.admin")) return true;
		Maxcraftien m = this.plugin.MM.getMaxcraftien(p);
		if(m == null) return false;
		if(this.owner.getUserName().equals(p.getName())) return true;
		
		return false;
		
	}
	
	public int getStock(Inventory inventory)
	{
		if(this.getItemStack() == null) return 0;
		
		int stock = 0;
		
		for(ItemStack is : inventory.getContents())
		{
			if(is == null) continue;
			if(this.plugin.sameItem(this.getItemStack(), is))
			{
				stock = stock + is.getAmount();
			}
		}
		
		return stock;
	}
	
	public int getStock()
	{
		return this.getStock(this.getChest().getInventory());
	}
	
	public void transaction(Player p)
	{
		if(!this.canWork())
		{
			p.sendMessage(this.plugin.message("Ce shop n'est pas activé !"));
			return;	
		}
		
		if(this.type.equals("SELL"))
		{	
			if(this.getStock() < this.getAmount() && !this.admin)
			{
				p.sendMessage(this.plugin.message("Il n'y a pas assez de stock dans ce shop pour cet achat ("+this.getStock()+") !"));
				return;
			}
			
			if(!(this.plugin.MM.economy.hasAccount(p.getName()) && this.plugin.MM.economy.getBalance(p.getName()) >= this.price))
			{
				p.sendMessage(this.plugin.message("Vous n'avez pas assez d'argent pour cet achat ! Il vous faut au moins "+this.plugin.MM.economy.format(this.price)+"."));
				return;
			}
			
			ItemStack is = this.getItemStack().clone();
			is.setAmount(this.getAmount());
			
			if(!this.canGiveItem(p.getInventory(), is))
			{
				p.sendMessage(this.plugin.message("Vous n'avez pas assez de place dans votre inventaire pour cet achat ("+this.getAmount()+" slots requis) !"));
				return;
			}
			
			//TRANSACTION
			Bukkit.getServer().getPluginManager().callEvent(new ShopTransactionEvent(this, p));
			
			this.giveItem(p.getInventory(), is);
			p.updateInventory();
			
			if(!this.admin)
			{
			this.removeFromChest(this.getAmount());
			}
		
			this.plugin.MM.economy.withdrawPlayer(p.getName(), this.price);
			
			//Economie
			if(!this.admin)
			{
			if(!this.plugin.MM.economy.hasAccount(this.owner.getUserName())) return;
			this.plugin.MM.economy.depositPlayer(this.owner.getUserName(), this.price);
			}
			
			//Messages
			if(!this.admin)
			{
			p.sendMessage(this.plugin.message("Vous avez acheté "+this.amount+" "+this.getItemStack().getType().name()+" pour "+this.plugin.MM.economy.format(this.price)+" � "+this.owner.getUserName()+"."));
			}
			else
			{
				p.sendMessage(this.plugin.message("Vous avez acheté "+this.amount+" "+this.getItemStack().getType().name()+" pour "+this.plugin.MM.economy.format(this.price)+" � l'administration."));
			}
			
			if(this.owner.isOnline() && !this.admin )
			{
				this.owner.getPlayer().sendMessage(this.plugin.message("Vous avez vendu "+this.amount+" "+this.getItemStack().getType().name()+" pour "+this.plugin.MM.economy.format(this.price)+" � "+p.getName()+"."));
			}
			return;
			
		}
		
		if(this.type.equals("BUY"))
		{
			if(this.getStock(p.getInventory()) < this.getAmount())
			{
				p.sendMessage(this.plugin.message("Vous n'avez pas "+this.amount+" item(s) de ce type dans votre inventaire."));
				return;
			}
			
			if(!this.admin && !(this.plugin.MM.economy.hasAccount(this.owner.getUserName()) && this.plugin.MM.economy.getBalance(this.owner.getUserName()) >= this.price))
			{
				p.sendMessage(this.plugin.message("Le propriétaire de ce shop d'achat ("+this.owner.getUserName()+") n'a pas assez d'argent pour vous acheter ces items !"));
				return;
			}
			
			ItemStack is = this.getItemStack().clone();
			is.setAmount(this.getAmount());
			
			//Ajout dans le coffre
			if(!this.admin && !this.canGiveItem(this.getChest().getInventory(), is))
			{
				p.sendMessage(this.plugin.message("Il n'y a plus de place dans le coffre pour stocker vos items ("+this.getFreeSpace(is, this.getChest().getInventory())+" places restantes) !"));
				return;
			}
			
			//TRANSACTION
			ShopTransactionEvent ste = new ShopTransactionEvent(this, p);
			Bukkit.getServer().getPluginManager().callEvent(ste);
			
			if(ste.isCancelled())
			{
				p.sendMessage(this.plugin.message(ste.getReason()));
				return;
			}
			
			
			if(!this.admin)
			{
			this.giveItem(this.getChest().getInventory(), is);
			}
		
			//Remove de l'inventaire
			this.remove(p.getInventory(), this.getAmount());
			p.updateInventory();
			
			//Economie
			
			if(!this.plugin.MM.economy.hasAccount(p.getName())) return;
			this.plugin.MM.economy.depositPlayer(p.getName(), this.price);
			
			if(!this.admin)
			{
				this.plugin.MM.economy.withdrawPlayer(this.owner.getUserName(), this.price);
			}
			
			//Messages
			if(!this.admin)
			{
				p.sendMessage(this.plugin.message("Vous avez vendu "+this.amount+" "+this.getItemStack().getType().name()+" pour "+this.plugin.MM.economy.format(this.price)+" � "+this.owner.getUserName()+"."));
			}
			else
			{
				p.sendMessage(this.plugin.message("Vous avez vendu "+this.amount+" "+this.getItemStack().getType().name()+" pour "+this.plugin.MM.economy.format(this.price)+" � l'administration."));	
			}
			
			if(this.owner.isOnline() && !this.admin )
			{
				this.owner.getPlayer().sendMessage(this.plugin.message(p.getName()+" vous a vendu "+this.amount+" "+this.getItemStack().getType().name()+" pour "+this.plugin.MM.economy.format(this.price)+"."));
			}
			return;
			
		}
	}
	
	public boolean canGiveItem(Inventory inventory, ItemStack is)
	{
		if(this.getFreeSpace(is, inventory) >= is.getAmount())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void giveItem(Inventory inventory, ItemStack is)
	{
		int amount = is.getAmount();
		
			while(amount > 0)
			{
				if(amount >= is.getMaxStackSize())
				{
				ItemStack istack = is.clone();
				istack.setAmount(is.getMaxStackSize());
				inventory.addItem(istack);
				amount -= is.getMaxStackSize();
				}
				else
				{
					ItemStack istack = is.clone();
					istack.setAmount(amount);
					inventory.addItem(is);
					amount = 0;
				}
				
			}
			
			return;
	}
	
	public int getFreeSpace(ItemStack is, Inventory inventory)
	{
		int free = 0;
		
		for(ItemStack slot : inventory.getContents())
		{
			if(slot == null)
			{
				free += is.getMaxStackSize();
			}
			else if(slot.isSimilar(is))
			{
				free += is.getMaxStackSize() - slot.getAmount();
			}
		}
		
		return free;
	}
	
	public void removeFromChest(int amount)
	{
		this.remove(this.getChest().getInventory(), amount);
	}
	
	public void remove(Inventory inventory, int amount)
	{
		if(amount > this.getStock(inventory)) return;
		
		for(ItemStack is : inventory.getContents())
		{
			if(is == null) continue;
			if(!(this.plugin.sameItem(is, this.getItemStack())))
			{
				continue;
			}
			
			if(amount > 0)
			{
				if(is.getAmount() <= amount)
				{
					inventory.removeItem(is);
					amount -= is.getAmount();
				}
				else
				{
					is.setAmount(is.getAmount()-amount);
					amount = 0;
				}
			}
		}
		
	}
	
	public void saveInSql() throws SQLException
	{
		if(!this.canWork()) return;
		
		this.removeFromSql();
		
		Zone z = this.plugin.MZ.getZone(this.getSign().getLocation());
		
		String zoneId;
		
		if(z == null) zoneId = "NULL";
		else
		{
			zoneId = ""+z.getId();
		}
		
		
		String query = "INSERT INTO `shop` "
				+ "(`id`, `zone_id`, `type`, `x`, `y`, `z`, `price`, `world`, `owner`, `stock`, `mcid`, `amount`, `itemId`, `itemDamage`) "
				+ "VALUES (NULL, '"+zoneId+"', '"+this.type+"', '"+this.getSign().getLocation().getBlockX()+"', '"+this.getSign().getLocation().getBlockY()+"', '"+this.getSign().getLocation().getBlockZ()+"', '"+this.price+"', '"+this.getSign().getLocation().getWorld().getName()+"', '"+this.owner.getId()+"', '"+this.getStock()+"', '"+this.id+"', '"+this.amount+"', '"+this.getItemStack().getTypeId()+"', '"+this.getItemStack().getDurability()+"')";
		
		this.plugin.MM.mysql_update(query);
	}
	
	public void removeFromSql() throws SQLException
	{
		
		ResultSet result = this.plugin.MM.mysql_query("SELECT COUNT(*) AS nb FROM shop WHERE mcid = "+this.id, true);
		int nb = result.getInt("nb");
		
		if(nb > 0)
		{
			this.plugin.MM.mysql_update("DELETE FROM shop WHERE mcid = "+this.id);
		}
	}
	
	
}
