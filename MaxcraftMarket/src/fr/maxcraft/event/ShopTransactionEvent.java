package fr.maxcraft.event;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

import fr.maxcraft.Maxcraftien;
import fr.maxcraft.Shop;

public class ShopTransactionEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    
    private Shop shop;
    private Player player;
    private boolean cancelled;
    private String reason;

 
    public ShopTransactionEvent(Shop shop, Player p) {
    	this.shop = shop;
    	this.player = p;
    	this.cancelled = false;
    	this.reason = "Ce shop ne peux pas vendre.";

    }
 


	public String getReason() {
		return reason;
	}



	public void setReason(String reason) {
		this.reason = reason;
	}



	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Shop getShop() {
		return shop;
	}


	public void setShop(Shop shop) {
		this.shop = shop;
	}




	public boolean isCancelled() {
		return cancelled;
	}



	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}



	public HandlerList getHandlers() {
        return handlers;
    }
 
    public static HandlerList getHandlerList() {
        return handlers;
    }
    
}