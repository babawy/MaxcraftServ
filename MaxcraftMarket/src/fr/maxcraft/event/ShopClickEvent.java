package fr.maxcraft.event;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

import fr.maxcraft.Maxcraftien;
import fr.maxcraft.Shop;

public class ShopClickEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    
    private Shop shop;
    private Player player;

 
    public ShopClickEvent(Shop shop, Player p) {
    	this.shop = shop;
    	this.player = p;

    }
 


	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Shop getShop() {
		return shop;
	}


	public void setShop(Shop shop) {
		this.shop = shop;
	}




	public HandlerList getHandlers() {
        return handlers;
    }
 
    public static HandlerList getHandlerList() {
        return handlers;
    }
    
}