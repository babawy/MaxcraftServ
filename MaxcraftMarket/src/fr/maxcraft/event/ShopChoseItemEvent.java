package fr.maxcraft.event;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

import fr.maxcraft.Maxcraftien;
import fr.maxcraft.Shop;

public class ShopChoseItemEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    
    private Shop shop;
    private Player player;
    private ItemStack item;
 
    public ShopChoseItemEvent(Shop shop, Player p, ItemStack is) {
    	this.shop = shop;
    	this.player = p;
    	this.item = is;
    }
 
 




	public ItemStack getItem() {
		return item;
	}






	public void setItem(ItemStack item) {
		this.item = item;
	}






	public Player getPlayer() {
		return player;
	}






	public void setPlayer(Player player) {
		this.player = player;
	}






	public Shop getShop() {
		return shop;
	}


	public void setShop(Shop shop) {
		this.shop = shop;
	}




	public HandlerList getHandlers() {
        return handlers;
    }
 
    public static HandlerList getHandlerList() {
        return handlers;
    }
    
}