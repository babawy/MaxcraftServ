package fr.maxcraft.event;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import fr.maxcraft.Maxcraftien;
import fr.maxcraft.Shop;

public class ShopRemoveEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    
    private Shop shop;
    private boolean cancelled;
    private Player player;
 
    public ShopRemoveEvent(Shop shop, Player player) {
    	this.shop = shop;
        this.cancelled = false;
        this.player = player;
    }
 

	public Player getPlayer() {
		return player;
	}


	public void setPlayer(Player player) {
		this.player = player;
	}


	public Shop getShop() {
		return shop;
	}


	public void setShop(Shop shop) {
		this.shop = shop;
	}


	public boolean isCancelled() {
		return cancelled;
	}


	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}


	public HandlerList getHandlers() {
        return handlers;
    }
 
    public static HandlerList getHandlerList() {
        return handlers;
    }
    
}