package fr.maxcraft.event;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import fr.maxcraft.Maxcraftien;
import fr.maxcraft.Shop;

public class ShopCreatedEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    
    private Shop shop;
    private Player player;
 
    public ShopCreatedEvent(Shop shop, Player p) {
    	this.shop = shop;
    	this.player = p;
    }
 




	public Shop getShop() {
		return shop;
	}


	public void setShop(Shop shop) {
		this.shop = shop;
	}




	public Player getPlayer() {
		return player;
	}





	public void setPlayer(Player player) {
		this.player = player;
	}





	public HandlerList getHandlers() {
        return handlers;
    }
 
    public static HandlerList getHandlerList() {
        return handlers;
    }
    
}