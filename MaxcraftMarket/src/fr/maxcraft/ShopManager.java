package fr.maxcraft;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Sign;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

public class ShopManager {

	private MaxcraftMarket plugin;
	public static ArrayList<Shop> shops = new ArrayList<Shop>();
	
	public ShopManager(MaxcraftMarket plugin)
	{
		this.plugin = plugin;
		
		//Chargement depuis config
		
		FileConfiguration config = this.plugin.getConfig();
		Location sign;
		String path;
		Maxcraftien m;
		Sign s;
		
		ConfigurationSection shopconf = config.getConfigurationSection("shops");
		if(shopconf == null) return;
		
		for(String key : shopconf.getKeys(false))
		{
			path = "shops."+key+".";
			
			sign = new Location(Bukkit.getWorld(config.getString(path + "world")), config.getInt(path + "x"), config.getInt(path + "y"), config.getInt(path + "z"));
			m = this.plugin.MM.getMaxcraftien(config.getInt(path + "owner"));
			
			Shop shop = new Shop(this.plugin, config.getInt(path + "id"), sign, m, config.getString(path + "type"),  config.getInt(path + "amount"),  Float.parseFloat(config.getString(path + "price")), config.getBoolean(path + "admin"));
			
		}
		
	}
}
