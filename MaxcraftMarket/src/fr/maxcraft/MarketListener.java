package fr.maxcraft;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;





import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import fr.maxcraft.event.ShopChoseItemEvent;
import fr.maxcraft.event.ShopClickEvent;
import fr.maxcraft.event.ShopCreateEvent;
import fr.maxcraft.event.ShopCreatedEvent;
import fr.maxcraft.event.ShopInfoEvent;
import fr.maxcraft.event.ShopRemoveEvent;
import fr.maxcraft.event.ShopRemoveItemEvent;

public class MarketListener  implements Listener{
	
	private MaxcraftMarket plugin;
	
	public MarketListener(MaxcraftMarket plugin)
	{
		this.plugin = plugin;
		
	}
	

	@EventHandler(priority = EventPriority.NORMAL)
	public void onSignChange(SignChangeEvent e){
		
		if(! e.getPlayer().hasPermission("maxcraft.market.use"))
		{
			return;	
		}
		
		Maxcraftien m = this.plugin.MM.getMaxcraftien(e.getPlayer());
		if(m == null)
		{
			return;
		}
		
		if(! (e.getLine(0).equalsIgnoreCase("shop") || e.getLine(0).equalsIgnoreCase("[shop]") ))
		{
			return;
		}
		
		
		//Position du sign
		if(!e.getBlock().getType().equals(Material.WALL_SIGN))
		{
			e.getPlayer().sendMessage(this.plugin.message("Le panneau doit être posé contre un coffre pour créer un shop !"));
			return;
		}
		
		Sign s = (Sign) e.getBlock().getState();
		Chest chest = this.plugin.getRelativeChest(s);
		if(chest == null)
		{
			e.getPlayer().sendMessage(this.plugin.message("Le panneau doit être posé contre un coffre pour créer un shop !"));
			return;
		}
		
		//D�j� un shop
		for(Shop shop : this.plugin.ShopManager.shops)
		{
			if(shop.getChest().equals(chest))
			{
				e.getPlayer().sendMessage(this.plugin.message("Ce coffre est déjà utilisé pour un autre shop !"));
				return;
			}
		}
		
		//Param�tres
		int amount;
		float price;
		boolean admin = false;
		String type = "SELL";
		
		try
		{
			amount =  Integer.parseInt(e.getLine(1));
			
			if(amount <= 0 || amount > 64*9)
			{
				e.getPlayer().sendMessage(this.plugin.message("Le nombre d'item à vendre par clic (ligne 2) n'est pas valide, il doit être compris entre 1 et 576 !"));
				return;
			}
		}
		catch(Exception ex)
		{
			e.getPlayer().sendMessage(this.plugin.message("La deuxième ligne doit comporter le nombre d'items à vendre à chaque clic."));
			return;
		}
		
		try
		{
			price =  Float.parseFloat(e.getLine(2));
			
			if(price <= 0 || price > 10000)
			{
				e.getPlayer().sendMessage(this.plugin.message("Le prix (ligne 3) n'est pas valide, il doit être compris entre 0 et 10 000 Po !"));
				return;
			}
		}
		catch(Exception ex)
		{
			e.getPlayer().sendMessage(this.plugin.message("La troisième ligne doit comporter le prix !"));
			return;
		}
		
		if(e.getLine(3).contains("admin") && e.getPlayer().hasPermission("maxcraft.market.admin"))
		{
			admin = true;
		}
		
		if(e.getLine(3).contains("achat"))
		{
			type = "BUY";
		}
	
		ShopCreateEvent sce = new ShopCreateEvent(m, price, amount, e.getBlock().getLocation(), type, admin);
		Bukkit.getServer().getPluginManager().callEvent(sce);
		
		if(!sce.isCancelled())
		{
			
			
			Shop shop = new Shop(this.plugin, this.plugin.ramdomInt(1, 1000000000), sce.getLocation(), sce.getOwner(), sce.getType(), sce.getAmount(), sce.getPrice(), sce.isAdmin());
			shop.saveInConfig();
			e.getPlayer().sendMessage(this.plugin.message("Vous avez créé un shop !"));
			
			Bukkit.getServer().getPluginManager().callEvent(new ShopCreatedEvent(shop, e.getPlayer()));
			
			if(!shop.canWork())
			{
				e.getPlayer().sendMessage(this.plugin.message("Le shop ne peut pas encore fonctionner, vous devez placer un coffre et un item frame (cadre à item) !"));
				
			}
		}
		
		return;
	}
	
	

	@EventHandler(priority = EventPriority.NORMAL)
	public void onSignBreak(BlockBreakEvent e){
		
		Shop s = null;
		for(Shop shop : this.plugin.ShopManager.shops)
		{
			if(e.getBlock().equals(shop.getSign().getBlock()))
			{
				s = shop;
			}
		}
		
		if(s == null) return;
		
		ShopRemoveEvent sre = new ShopRemoveEvent(s, e.getPlayer());
		
		Bukkit.getServer().getPluginManager().callEvent(sre);
		
		if(!sre.isCancelled())
		{
			sre.getShop().remove();
			e.getPlayer().sendMessage(this.plugin.message("Vous avez retiré le shop !"));
			
		}
		else
		{
			e.setCancelled(true);
		}
	}
	

	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onBlockBreak(BlockBreakEvent e){
		
		for(Shop shop : this.plugin.ShopManager.shops)
		{
			if(shop.getChest() != null && shop.getChest().getBlock().equals(e.getBlock()))
			{
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onItemFrameRightClick(PlayerInteractEntityEvent e){
		
		if(!( e.getRightClicked() instanceof ItemFrame))
		{
			return;
		}
		
		for(Shop shop : this.plugin.ShopManager.shops)
		{
			if(shop.getItemFrame() != null && shop.getItemFrame().equals(e.getRightClicked()))
			{
				if(!shop.hasAccess(e.getPlayer()))
				{
				//Pas le droit
				e.setCancelled(true);
				Bukkit.getServer().getPluginManager().callEvent(new ShopInfoEvent(shop, e.getPlayer()));
				
				}
				
				else
				{
					// On a le droit

					ItemFrame itemf = (ItemFrame) e.getRightClicked();
					
					// Nouvel item !
					
					if((itemf.getItem() == null || itemf.getItem().getType().equals(Material.AIR)) && e.getPlayer().getItemInHand() != null)
					{
						e.setCancelled(true);
						ItemStack itemshowcase = e.getPlayer().getItemInHand().clone();
						itemshowcase.setAmount(1);
						
						itemf.setItem(itemshowcase);
						
						Bukkit.getServer().getPluginManager().callEvent(new ShopChoseItemEvent(shop, e.getPlayer(), e.getPlayer().getItemInHand()));
					}
				}
				
				return;
			}
		}
		
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onItemFrameLeftClick(EntityDamageByEntityEvent e)
	{
		if(!(e.getEntity() instanceof ItemFrame))
		{
			return;
		}
		
		ItemFrame itemf = (ItemFrame) e.getEntity();
		
		Shop s = null;
		for(Shop shop : this.plugin.ShopManager.shops)
		{
			if(shop.getItemFrame() != null && shop.getItemFrame().equals(itemf))
			{
				s = shop;
			}
		}
		
		if(s == null) return;
		
		if(!(e.getDamager() instanceof Player))
		{
			e.setCancelled(true);
			return;
		}
		
		Player p = (Player) e.getDamager();
		
		if(!s.hasAccess(p))
		{
			e.setCancelled(true);
			return;
		}
		
		// Item retir� !
		if(itemf.getItem() != null)
		{
			itemf.setItem(new ItemStack(Material.AIR,1));
			e.setCancelled(true);
			Bukkit.getServer().getPluginManager().callEvent(new ShopRemoveItemEvent(s, p));
		}
		
		return;
		
		
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onItemFrameBreak(HangingBreakEvent e)
	{
		if(!(e.getEntity() instanceof ItemFrame))
		{
			return;
		}
		
		
		ItemFrame itemf = (ItemFrame) e.getEntity();
		
		Shop s = null;
		for(Shop shop : this.plugin.ShopManager.shops)
		{
			if(shop.getItemFrame() != null && shop.getItemFrame().equals(itemf))
			{
				s = shop;
			}
		}
		
		if(s == null) return;
		
		e.setCancelled(true);
	}
	
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onChestOpen(InventoryOpenEvent e)
	{
		if(!(e.getInventory().getHolder() instanceof Chest)) return;
		if(! (e.getPlayer() instanceof Player)) return;
		Player p = (Player) e.getPlayer();
		
		Chest chest = (Chest) e.getInventory().getHolder();
		
		Shop s = null;
		for(Shop shop : this.plugin.ShopManager.shops)
		{
			if(shop.getChest() != null && shop.getChest().equals(chest))
			{
				s = shop;
			}
		}
		
		if(s == null) return;
		
		
		if(!s.hasAccess(p))
		{
			p.sendMessage(this.plugin.message("Vous n'avez pas accès au contenu de ce shop !"));
			e.setCancelled(true);
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onChestOpen(PlayerInteractEvent e)
	{
	
		if(!e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) return;
		
		Shop s = null;
		for(Shop shop : this.plugin.ShopManager.shops)
		{
			if(shop.getChest() != null && shop.getChest().getBlock().equals(e.getClickedBlock()))
			{
				s = shop;
			}
		}
		
		if(s == null) return;
		
		
		if(!s.hasAccess(e.getPlayer()))
		{
			e.getPlayer().sendMessage(this.plugin.message("Vous n'avez pas accès au contenu de ce shop !"));
			e.setCancelled(true);
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onSignClicked(PlayerInteractEvent e)
	{
		if(!e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) return;
		
		Shop s = null;
		for(Shop shop : this.plugin.ShopManager.shops)
		{
			if(shop.getSign() != null && shop.getSign().getBlock().equals(e.getClickedBlock()))
			{
				s = shop;
			}
		}
		
		if(s == null) return;
		
		Bukkit.getServer().getPluginManager().callEvent(new ShopClickEvent(s, e.getPlayer()));
		e.setCancelled(true);
	}
	
	
}
