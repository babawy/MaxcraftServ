package fr.maxcraft;

import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import fr.maxcraft.event.ShopChoseItemEvent;
import fr.maxcraft.event.ShopClickEvent;
import fr.maxcraft.event.ShopCreatedEvent;
import fr.maxcraft.event.ShopInfoEvent;
import fr.maxcraft.event.ShopRemoveEvent;
import fr.maxcraft.event.ShopRemoveItemEvent;

public class ShopListener  implements Listener{
	
	private MaxcraftMarket plugin;
	
	public ShopListener(MaxcraftMarket plugin)
	{
		this.plugin = plugin;
		
	}
	

	@EventHandler(priority = EventPriority.NORMAL)
	public void onShopCreated(ShopCreatedEvent e){
		
		try {
			e.getShop().saveInSql();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onShopRemoved(ShopRemoveEvent e){
		
		if(!e.getShop().hasAccess(e.getPlayer()))
		{
			e.getPlayer().sendMessage(this.plugin.message("Vous n'avez pas la permission de détruire ce shop ! (owner ou maxcraft.market.admin)"));
			e.setCancelled(true);
		}
		
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onShopRemovedLast(ShopRemoveEvent e){
		
		if(!e.isCancelled())
		{
			
			if(e.getShop().canWork())
			{
				e.getShop().getItemFrame().setItem(null);
			}
			
			try {
				e.getShop().removeFromSql();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onShopChoseItem(ShopChoseItemEvent e){
		
		e.getPlayer().sendMessage(this.plugin.message("Ce shop est désormais activé avec l'item <"+ this.plugin.MM.Essentials.getItemDb().name(e.getItem())+">."));
		
		//SQL
		try {
			e.getShop().saveInSql();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	

	@EventHandler(priority = EventPriority.NORMAL)
	public void onShopRemoveItem(ShopRemoveItemEvent e){
		
		e.getPlayer().sendMessage(this.plugin.message("Ce shop est désormais désactivé."));
		
		
		//SQL
		try {
			e.getShop().removeFromSql();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onShopClick(ShopClickEvent e){
		

		if(e.getShop().getOwner().getUserName().equals(e.getPlayer().getName()))
		{
			//INFO
			Bukkit.getServer().getPluginManager().callEvent(new ShopInfoEvent(e.getShop(), e.getPlayer()));
			return;
		}
		else
		{
			
			e.getShop().transaction(e.getPlayer());
		}
		
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onShopInfo(ShopInfoEvent e){
		
		e.getShop().loadSign();
		String type = "Vente";
		
		if(e.getShop().getType() == "BUY") type = "Achat";
		
		e.getPlayer().sendMessage(this.plugin.message("*** INFORMATION SHOP ("+type+") ***"));
		e.getPlayer().sendMessage(this.plugin.message("Stock : "+ ChatColor.GOLD + e.getShop().getStock()));
		e.getPlayer().sendMessage(this.plugin.message("Propriétaire : "+ ChatColor.GOLD + e.getShop().getOwner().getUserName()));
		
		if(e.getShop().canWork())
		{
			e.getPlayer().sendMessage(this.plugin.message("Etat : "+ ChatColor.GREEN + "En marche"));
			e.getPlayer().sendMessage(this.plugin.message("Item : "+ ChatColor.GOLD + this.plugin.MM.Essentials.getItemDb().name(e.getShop().getItemStack())));
			e.getPlayer().sendMessage(this.plugin.message("Prix : "+ ChatColor.GOLD + this.plugin.MM.economy.format(e.getShop().getPrice())+" pour "+e.getShop().getAmount() +" unité(s)"));
			
		}
		else
		{
			e.getPlayer().sendMessage(this.plugin.message("Etat : "+ ChatColor.RED + "Shop non activé (manque item frame ou item)"));
		}
		
	
	}
	

	
	
}
