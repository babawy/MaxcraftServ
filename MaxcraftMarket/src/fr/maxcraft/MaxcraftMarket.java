package fr.maxcraft;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class MaxcraftMarket extends JavaPlugin{

		//VARIABLES
		public static MaxcraftManager MM = null;
		public static MaxcraftZones MZ = null;
		public static ShopManager ShopManager;
		
		//LISTES
		
		//LISTENERS
		private MarketListener ML;
		private ShopListener SL;
		
	
	@Override
	public void onDisable() {
		
	}

	@Override
	public void onEnable() {
	
		super.onEnable();
		
		//Configuration
		saveDefaultConfig();
		
		//PLUGINS
		this.MM = this.getMM();
		this.MZ = this.getMZ();
		this.ShopManager = new ShopManager(this);
		this.log("Plugins chargés !");
			
		//Listeners
		this.ML = new MarketListener(this);
		this.SL = new ShopListener(this);
		this.getServer().getPluginManager().registerEvents(this.ML, this);
		this.getServer().getPluginManager().registerEvents(this.SL, this);	
				
		
	}

	public MaxcraftManager getMM()
	{
		MaxcraftManager mm = (MaxcraftManager) this.getServer().getPluginManager().getPlugin("MaxcraftManager");
        log("MaxcraftManager chargé !");
		return mm;
	}
	
	public MaxcraftZones getMZ()
	{
		MaxcraftZones mz = (MaxcraftZones) this.getServer().getPluginManager().getPlugin("MaxcraftZones");
        log("MaxcraftZones chargé !");
		return mz;
	}

	
	public void log(String message, ChatColor color)
	{
		ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
		console.sendMessage(color + "[MAXCRAFT] "+ ChatColor.WHITE +"[MaxcraftMarket] " + message);
		
	}
	public void log(String message)
	{
		ChatColor color = ChatColor.RED;
		log(message, color);
		
	}
	
	public String message(String message)
	{
		return ChatColor.AQUA + "[Commerce] " + ChatColor.GRAY + message;
	}
	
	public Chest getRelativeChest(Sign sign)
	{
		Chest chest = null;
		org.bukkit.material.Sign s = (org.bukkit.material.Sign) sign.getData();
		Block chestBlock = sign.getBlock().getRelative(s.getAttachedFace());
		
		try
		{
			chest = (Chest) chestBlock.getState();
		}
		catch(Exception e)
		{
		}
		
		return chest;
	}
	
	public int ramdomInt(int min, int max)
	{
		return min + (int)(Math.random() * ((max - min) + 1));
	}
	
	public boolean sameItem(ItemStack item1, ItemStack item2)
	{
		//Type
		if(! item1.getType().equals(item2.getType())) return false;
		if(item1.getDurability() != item2.getDurability()) return false;
		if(!item1.getItemMeta().equals(item2.getItemMeta())) return false;
		if(!item1.getData().equals(item2.getData())) return false;
		return true;
	}
}
