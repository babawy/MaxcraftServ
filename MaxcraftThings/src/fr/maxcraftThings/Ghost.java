package fr.maxcraftThings;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Ghost {

	public static void Command(CommandSender sender) {
		Player p = Bukkit.getPlayer(sender.getName());
		ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
		SkullMeta meta = (SkullMeta) skull.getItemMeta();
		meta.setOwner("tnt");
		skull.setItemMeta(meta);
		if (p.getInventory().getHelmet()== null &&p.getInventory().getChestplate()== null) {
			p.getInventory().setHelmet(skull);
			p.getInventory().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
			p.updateInventory();
			p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY,9999999,1));
			p.sendMessage("Vous etes devenu un fantome!");
		}
		else{
			p.getInventory().setHelmet(null);
			p.getInventory().setChestplate(null);
			p.removePotionEffect(PotionEffectType.INVISIBILITY);
			p.sendMessage("Vous n'etes plus un fantome!");
		}
	}

}
