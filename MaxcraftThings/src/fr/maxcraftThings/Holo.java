package fr.maxcraftThings;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Holo {

	public static void create(String[] args, CommandSender sender) {
		String holo = "";
		for (String s : args)
		{
			holo = holo+" "+s;
		}
		Location l = Bukkit.getPlayer(sender.getName()).getLocation();
		Entity n = l.getWorld().spawnEntity(l,EntityType.ARMOR_STAND);
		ArmorStand a = (ArmorStand) n;
		a.setGravity(false);
		a.setCustomNameVisible(true);
		a.setCustomName(holo);
		a.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY,9999999,1));
	}

}
