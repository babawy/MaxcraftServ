package fr.maxcraftThings;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.command.CommandSender;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class ModeratorDataStorage {
	
	static ArrayList<ModeratorDataStorage> listmode = new ArrayList<ModeratorDataStorage>();

	private String date;
	private String modo;
	private String joueur;
	private String type;
	private String dur�e;
	private String raison;

	public ModeratorDataStorage(String date, String modo, String joueur,String type,String dur�e,String raison) {
		this.date = date;
		this.modo = modo;
		this.joueur = joueur;
		this.type = type;
		this.dur�e = dur�e;
		this.raison = raison;
	} 

	public String getDate() {
		return date;
	}

	public String getModo() {
		return modo;
	}

	public String getJoueur() {
		return joueur;
	}

	public String getType() {
		return type;
	}

	public String getDur�e() {
		return dur�e;
	}

	public String getRaison() {
		return raison;
	}
	
	
	
	public static void onCommand(PlayerCommandPreprocessEvent e) {
		if (e.getMessage().contains("/ban")||e.getMessage().contains("/banip")||e.getMessage().contains("/jail")||e.getMessage().contains("/mute")){
		Date now = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yy");
		String[] args = e.getMessage().split(" ");
		if (!(args.length>3)){
			e.getPlayer().sendMessage(ChatColor.RED+"Vous devais indiquez une dur�e et une raison");
			e.setCancelled(true);
		}
		if ( args.length>4){
			for (int i=4;i<args.length;i++)
				args[3] = args[3]+" "+args[i];
		}
		ModeratorDataStorage m = new ModeratorDataStorage(format.format(now),e.getPlayer().getName(),args[1],args[0],args[2],args[3]);
		listmode.add(m);
		try {
		Main.MM.mysql_update("INSERT INTO moderator (`date`, `modo`, `joueur`, `type`, `dur�e`, `raison`) VALUES ('"+format.format(now)+"','"+e.getPlayer().getName()+"','"+args[1]+"','"+args[0]+"','"+args[2]+"','"+args[3]+"')");
		}catch(Exception ex){
			ex.printStackTrace();
			e.getPlayer().sendMessage("Merci de prevenir crevebedaine de cette ereur");
		}
		
		}
	}


	public static void onCommand(CommandSender sender, String[] args) {
		if (args.length==0){
			for (int i = listmode.size()-1; i<listmode.size()-21;){
				i--;
				ModeratorDataStorage m = listmode.get(i);
				sender.sendMessage(ChatColor.RED+m.getDate()+" : "+m.getModo()+" "+m.getType()+" "+m.getJoueur()+" pour une dur�e de "+m.getDur�e()+" pour "+m.getRaison());
			}
		}
		else{
			sender.sendMessage(ChatColor.RED+"Casier de "+args[0]+" :");
			for (ModeratorDataStorage m : listmode)
				if (m.getJoueur().equals(args[0]))
					sender.sendMessage(ChatColor.GRAY+m.getDate()+" : "+ChatColor.RED+m.getModo()+" "+m.getType()+" "+m.getJoueur()+ChatColor.GRAY+" pour une dur�e de "+ChatColor.RED+m.getDur�e()+" : "+ChatColor.GRAY+m.getRaison());
		}
		
	}

	public static void onEnable() {
		ResultSet r = Main.MM.mysql_query("SELECT * FROM moderator", false);
		try {
		while (r.next())
		{
			ModeratorDataStorage m = new ModeratorDataStorage(r.getString("date"),r.getString("modo"),r.getString("joueur"),r.getString("type"),r.getString("dur�e"),r.getString("raison"));
			listmode.add(m);
		}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

}
