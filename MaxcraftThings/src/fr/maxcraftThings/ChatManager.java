package fr.maxcraftThings;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatManager {

	static ArrayList<String> espion = new ArrayList<String>();
	static boolean mute = false;

	public static void chat(AsyncPlayerChatEvent e) {
		if (mute)
			if(!e.getPlayer().hasPermission("maxcraft.things.admin"))
			{
			e.setCancelled(true);
			e.getPlayer().sendMessage(ChatColor.RED+"Chat fermé");
			return;
			}
		if (e.getMessage().startsWith("*"))
			if (Main.MM.getMaxcraftien(e.getPlayer()).haveFaction())
			{
				String m = e.getMessage();
				e.setCancelled(true);
				for(Player p : Bukkit.getServer().getOnlinePlayers())
				{ 
					if ((Main.MM.getMaxcraftien(e.getPlayer()).getFaction()==Main.MM.getMaxcraftien(p).getFaction())||espion.contains(p.getName()))
						p.sendMessage(ChatColor.BLUE+"[FACTION] "+e.getPlayer().getName()+ChatColor.WHITE+" : "+m.substring(1,m.length()));
				}
			}
	}

	public static void muteAll(Main main) {
		String str = "fermé";
		if(mute)
			str = "ouvert";
		mute=!mute;
		for (Player p : main.getServer().getOnlinePlayers())
			Annonce.sendTitle(p, 200, "&4[!]", "&9Chat "+str+"!", Sound.NOTE_PIANO, 2);
	}

	public static void espion(CommandSender sender) {

		if (!ChatManager.espion.contains(sender.getName()))
		{
		ChatManager.espion.add(sender.getName());
		sender.sendMessage("Vous etes desormais un espion!");
		}
		else
		{
			ChatManager.espion.remove(sender.getName());
			sender.sendMessage("Vous n'etes plus un espion!");
		}
		
	}

}
