package fr.maxcraftThings;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Joueur {

	public static void change(String[] args, CommandSender sender, Main plugin) {
		Player p = (Player)sender;
		if (plugin.permission.playerInGroup(p, "joueur")){
			if (p.hasPermission("maxcraft.things.admin"))
			setGroup(p,"admin",plugin);
			if (p.hasPermission("maxcraft.things.logicien"))
				setGroup(p,"logicien",plugin);
			if (p.hasPermission("maxcraft.things.modo"))
				setGroup(p,"modoe",plugin);
			if (p.hasPermission("maxcraft.things.helper"))
				setGroup(p,"helper",plugin);
			if (p.hasPermission("maxcraft.things.builder"))
				setGroup(p,"builder",plugin);
			if (p.hasPermission("maxcraft.things.guide"))
				setGroup(p,"guide",plugin);
	}
		else{
			if (p.hasPermission("maxcraft.things.admin"))
			setGroup(p,"joueur",plugin);
			if (p.hasPermission("maxcraft.things.logicien"))
				setGroup(p,"joueur",plugin);
			if (p.hasPermission("maxcraft.things.modo"))
				setGroup(p,"joueur",plugin);
			if (p.hasPermission("maxcraft.things.helper"))
				setGroup(p,"joueur",plugin);
			if (p.hasPermission("maxcraft.things.builder"))
				setGroup(p,"joueur",plugin);
			if (p.hasPermission("maxcraft.things.guide"))
				setGroup(p,"joueur",plugin);
		}
	}

	private static void setGroup(Player p, String group, Main plugin) {
		for (String g :plugin.permission.getPlayerGroups(p)){
			plugin.permission.playerRemoveGroup(p, g);
			plugin.permission.playerRemoveGroup(p.getWorld().getName(), p, g);
		}
		plugin.permission.playerAddGroup(p,group);
		
	}
}
