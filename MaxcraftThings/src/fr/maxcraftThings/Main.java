package fr.maxcraftThings;

import java.sql.SQLException;
import java.util.HashMap;

import net.citizensnpcs.Citizens;
import net.milkbowl.vault.permission.Permission;
import net.nathem.script.core.NSCore;
import net.nathem.script.core.NathemWorld;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import fr.maxcraft.MaxcraftManager;
import fr.maxcraft.MaxcraftZones;

public class Main extends JavaPlugin {

	public static MaxcraftManager MM = null;
	public static MaxcraftZones MZ = null;
	public static Citizens C = null;
	public static Plugin P = null;
	public static NSCore NS = null;
    Permission permission = null;
	public static HashMap<String, Boolean> things = new HashMap<String, Boolean>();
	

	@SuppressWarnings("unused")
	public void onEnable() {
		Listener l = new ClassListener(this);
		Listener l1 = new fr.maxcraftThings.Game.ClassListener(this);
		MM = getMM();
		MZ = getMZ();
		C = getC();
		NS = getNS();
		P = getPlugin();
		this.setupPermissions();
		Bukkit.getLogger().info("Plugins charg�s");
		saveDefaultConfig();
		getEnable();
		try {
			fr.maxcraftThings.Jobs.Main.onEnable();
			fr.maxcraftThings.Game.Game.onEnable();
			fr.maxcraftThings.ModeratorDataStorage.onEnable();
				WarZone.onEnable(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// BackPack.onEnable();
		//
		if(getServer().getPluginManager().getPlugin("Citizens") == null || getServer().getPluginManager().getPlugin("Citizens").isEnabled() == false) {
			getLogger().warning("Citizens 2.0 not found or not enabled");
			return;
		}	

		net.citizensnpcs.api.CitizensAPI.getTraitFactory().registerTrait(net.citizensnpcs.api.trait.TraitInfo.create(fr.maxcraftThings.Quest.Sabine.class).withName("Sabine"));
		net.citizensnpcs.api.CitizensAPI.getTraitFactory().registerTrait(net.citizensnpcs.api.trait.TraitInfo.create(fr.maxcraftThings.Quest.Notch.class).withName("Notch"));
		net.citizensnpcs.api.CitizensAPI.getTraitFactory().registerTrait(net.citizensnpcs.api.trait.TraitInfo.create(fr.maxcraftThings.Quest.Speaker.class).withName("Speaker"));
		net.citizensnpcs.api.CitizensAPI.getTraitFactory().registerTrait(net.citizensnpcs.api.trait.TraitInfo.create(fr.maxcraftThings.Quest.Franky.class).withName("Franky"));
		net.citizensnpcs.api.CitizensAPI.getTraitFactory().registerTrait(net.citizensnpcs.api.trait.TraitInfo.create(fr.maxcraftThings.Quest.Natacha.class).withName("Natacha"));
		net.citizensnpcs.api.CitizensAPI.getTraitFactory().registerTrait(net.citizensnpcs.api.trait.TraitInfo.create(fr.maxcraftThings.Quest.Palefrenier.class).withName("Palefrenier"));
		net.citizensnpcs.api.CitizensAPI.getTraitFactory().registerTrait(net.citizensnpcs.api.trait.TraitInfo.create(fr.maxcraftThings.Quest.GardienTour.class).withName("Gardien tour"));


	}

	private void getEnable() {
		reloadConfig();
		things.clear();
		for (String str : getConfig().getKeys(true)) {
			things.put(str, getConfig().getBoolean(str));
		}
		Bukkit.getLogger().info("" + things);
	}

	public void onDisable() {
		fr.maxcraftThings.Game.Game.onDisable();
		fr.maxcraftThings.Jobs.Main.onDisable();
		// try {
		// BackPack.onDisable();
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("things")){
			if (args.length == 0)
				for (String str : getConfig().getKeys(true))
					sender.sendMessage("things: " + str + " : "
							+ getConfig().getString(str));
			if (args.length == 2) {
				getConfig().set(args[0], Boolean.valueOf(args[1]));
				saveConfig();
				getEnable();
				sender.sendMessage("Changement pris en compte");
				return true;
			}
		}
		if (CommandManager.onCommand(sender,cmd,label,args,this))
			return true;
		return false;
	}
	public MaxcraftManager getMM() {
		MaxcraftManager mm = (MaxcraftManager) getServer().getPluginManager()
				.getPlugin("MaxcraftManager");
		Bukkit.getLogger().info("MaxcraftManager charg� !");
		return mm;
	}
	public NSCore getNS() {
		NSCore ns = (NSCore) getServer().getPluginManager()
				.getPlugin("NathemScript-Core");
		Bukkit.getLogger().info("NathemScript-Core charg� !");
		return ns;
	}

	public MaxcraftZones getMZ() {
		MaxcraftZones mz = (MaxcraftZones) getServer().getPluginManager()
				.getPlugin("MaxcraftZones");
		Bukkit.getLogger().info("MaxcraftZones charg� !");
		return mz;
	}

	public Citizens getC() {
		Citizens c = (Citizens) getServer().getPluginManager()
				.getPlugin("Citizens");
		Bukkit.getLogger().info("Citizens charg� !");
		return c;
	}

	public boolean isEnable(String thing) {
		return true;
	}
	public Plugin getPlugin(){
		return this;
		}
    private boolean setupPermissions()
    {
        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
        if (permissionProvider != null) {
            permission = permissionProvider.getProvider();
        }
        return (permission != null);
    }
}