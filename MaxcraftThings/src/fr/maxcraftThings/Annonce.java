package fr.maxcraftThings;

import net.minecraft.server.v1_8_R1.ChatSerializer;
import net.minecraft.server.v1_8_R1.EnumTitleAction;
import net.minecraft.server.v1_8_R1.IChatBaseComponent;
import net.minecraft.server.v1_8_R1.PacketPlayOutTitle;
import net.minecraft.server.v1_8_R1.PlayerConnection;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class Annonce {


	public static void onCommand(CommandSender sender, Command cmd,
			String label, String[] args, Main main) {
		String title = "";
		String subtitle = "";
		int a = 1;
		if(Main.MZ.isInt(args[0]))
			a = 0;
		for (int i = 1-a ; i<args.length;i++)
			title=title+" "+args[i];
		if (title.contains("|")){
			subtitle = title.substring(title.indexOf("|")+1);
			title = title.substring(0,title.indexOf("|"));
		}
		else{
			subtitle = title;
			title="";
		}
		a = 10;
		if(Main.MZ.isInt(args[0]))
			a = Integer.parseInt(args[0]);
		for (Player p : Bukkit.getServer().getOnlinePlayers())
			sendTitle(p,a* 20,title, subtitle,null,1);
	}
	
    public static void sendTitle(Player player, Integer dur�e, String title, String subtitle,Sound sound,float pitch) {
        PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
        PacketPlayOutTitle packetPlayOutTimes = new PacketPlayOutTitle(EnumTitleAction.TIMES, null, 5, dur�e, 5);
        connection.sendPacket(packetPlayOutTimes);
        if (sound!=null)
        	player.playSound(player.getLocation(), sound, 10, pitch);
        if (subtitle != null) {
            subtitle = subtitle.replaceAll("%player%", player.getDisplayName());
            subtitle = ChatColor.translateAlternateColorCodes('&', subtitle);
            IChatBaseComponent titleSub = ChatSerializer.a("{\"text\": \"" + subtitle + "\"}");
            PacketPlayOutTitle packetPlayOutSubTitle = new PacketPlayOutTitle(EnumTitleAction.SUBTITLE, titleSub);
            connection.sendPacket(packetPlayOutSubTitle);
        }
        if (title != null) {
            title = title.replaceAll("%player%", player.getDisplayName());
            title = ChatColor.translateAlternateColorCodes('&', title);
            IChatBaseComponent titleMain = ChatSerializer.a("{\"text\": \"" + title + "\"}");
            PacketPlayOutTitle packetPlayOutTitle = new PacketPlayOutTitle(EnumTitleAction.TITLE, titleMain);
            connection.sendPacket(packetPlayOutTitle);
        }
    }
}
