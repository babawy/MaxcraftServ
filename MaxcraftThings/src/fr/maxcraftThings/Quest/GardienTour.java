package fr.maxcraftThings.Quest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.citizensnpcs.api.trait.Trait;
import net.citizensnpcs.api.util.DataKey;

public class GardienTour extends Trait {
	public GardienTour() {
		super("GardienTour");
	}
	@EventHandler
	public void onclic(PlayerInteractEntityEvent e) {
		if (e.getRightClicked().equals(this.getNPC().getBukkitEntity())){
			Player p = e.getPlayer();
			if (!quester.containsKey(p.getUniqueId())){
				Outils.NPCChat(p, npc, "Mais qu'en ais-je fait ? O� est cette foutu cl�f ? Tiens ! Qui �tes vous ? Oh et puisque vous �tes l�, souhaiteriez vous m'aider ? J'ai perdu la clef permettant l'ouverture de ce coffre !");
				Outils.ask("Bien sur.", 1, p, npc, "blue");
				Outils.ask("J'ai autres chose � faire.", 2, p, npc, "red");
			}
			if (quester.containsKey(p.getUniqueId())){
				Outils.NPCChat(p, npc, "Vous revoila ! Avez vous trouv� ?");
				Outils.ask("Oui !", 3, p, npc, "blue");
				Outils.ask("Toujours pas ...", 4, p, npc, "red");
			}
		}
	}
	@EventHandler
	public void onchoice(PlayerCommandPreprocessEvent e) {
		if (e.getMessage().contains("quest")&&e.getMessage().contains(npc.getName())){
			int i = Integer.parseInt(e.getMessage().replaceAll("[\\D]", ""));
			if (i==1) oui(e.getPlayer());
			if (i==2) non(e.getPlayer());
			if (i==3) oui2(e.getPlayer());
			if (i==4) non2(e.getPlayer());
		}
	}	
	private void non2(Player p) {
		Outils.NPCChat(p, npc, "Retournez y ! Je vous en supplie !");
		
	}
	private void oui2(Player p) {
		ItemStack c = new ItemStack(Material.IRON_INGOT);
		ItemMeta clef = new ItemStack(Material.IRON_INGOT).getItemMeta();
		clef.setDisplayName(ChatColor.ITALIC+"Clef Rouill�e");
		c.setAmount(1);
		c.setItemMeta(clef);
		ItemStack r = new ItemStack(Material.IRON_SWORD);
		r.setDurability((short) 10);
		r.setAmount(1);
		
		if (p.getInventory().contains(c)){
			p.getInventory().removeItem(c);
			Outils.NPCChat(p, npc, "Parfait ! AAaaah comment puis-je vous remercier ? Ah ! Tenez !");
			p.getInventory().addItem(r);
		}
		
	}
	public void oui(Player p){
		Outils.NPCChat(p, npc, "Merci beaucoup ! Je me souviens que la derni�re fois que je l'ai vu je me rendait aux �gouts pour parler � mon vielle amis qui s'en occupe ...");
		quester.put(p.getUniqueId(), 1);
	}
	public void non(Player p){
		Outils.NPCChat(p, npc, "Dans ce cas allez faire ce que vous avez � faire au lieu de vous balader !");
	}
	
	
	
	
	
	private DataKey key;
	static HashMap<UUID,Integer> quester = new HashMap<UUID,Integer>();
	
	@Override
	public void load(DataKey key) {
		this.key = key;
		Object derp =  key.getRaw("player");
		if (derp !=null) quester= (HashMap<UUID, Integer>) key.getRaw("player");
	}
	public void save(DataKey key) {
		key.setRaw("player",quester);
	}
}