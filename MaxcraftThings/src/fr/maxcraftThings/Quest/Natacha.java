package fr.maxcraftThings.Quest;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import fr.maxcraftThings.Main;
import net.citizensnpcs.api.trait.Trait;
import net.citizensnpcs.api.util.DataKey;

public class Natacha extends Trait {
	static ArrayList<String> quester = new ArrayList<String>();
	private DataKey key;
	private Location l = new Location(Bukkit.getWorld("maxcraft"), 30, 71, -111);
	private Location l1= new Location(Bukkit.getWorld("maxcraft"), 62, 72, -85);
	public Natacha() {
		super("Natacha");
	}	

	@EventHandler
	public void onclic(PlayerInteractEntityEvent e) {
		if (e.getRightClicked().equals(this.getNPC().getBukkitEntity())){
		if (!e.getPlayer().getItemInHand().getType().equals(Material.MILK_BUCKET)&&!quester.contains(e.getPlayer().getName()))
			Outils.NPCChat(e.getPlayer(), npc,"Bonsoir mon p'tit chou ! J'aurais besoin d'une substance lubrifiante pour entretenir mes relations commerciales ...t'aurais pas un sceau de lait ?");
		if (e.getPlayer().getItemInHand().getType().equals(Material.MILK_BUCKET)){
			Outils.NPCChat(e.getPlayer(), npc, "Merci mon chevalier ! �a va faciliter certaines n�gociations plus serr�es. N'h�site pas � revenir pour en profiter.");
			e.getPlayer().setItemInHand(new ItemStack(Material.BUCKET,1));
			quester.add(e.getPlayer().getName());
		}
		if (quester.contains(e.getPlayer().getName())&&npc.getBukkitEntity().getLocation().distance(l1)>3){
			Outils.NPCChat(e.getPlayer(), npc,"Je suis libre et fra�che, as-tu besoin d'un moment de repos ? Suis-moi si �a te plairait !");
			npc.getNavigator().setTarget(l1);
			new Task(e.getPlayer()).runTaskLater(Main.P, 800);
			
		}
		if (quester.contains(e.getPlayer().getName())&&npc.getBukkitEntity().getLocation().distance(l1)<3){
			e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 800, 800));
			npc.getNavigator().setTarget(e.getPlayer(), false);
			Player p = (Player)npc.getBukkitEntity();
			p.setSneaking(true);
		}
		}
	}
	@Override
	public void load(DataKey key) {
		this.key = key;
		Object derp =  key.getRaw("player");
		if (derp !=null) quester= (ArrayList<String>) key.getRaw("player");
	}
	public void save(DataKey key) {
		key.setRaw("player",quester);
	}
	public class Task extends BukkitRunnable {
		private Player player;

		public Task(Player player) {
			this.player = player;
		}

		@Override
		public void run() {
			if (player.hasPotionEffect(PotionEffectType.BLINDNESS)){
			Outils.NPCChat(player, npc,"Merci pour cet �change, je te saisis 15 PO !");
			player.setHealth(20);
			player.removePotionEffect(PotionEffectType.BLINDNESS);
			Outils.pay(player, 15);
			}
			Player p = (Player)npc.getBukkitEntity();
			p.setSneaking(false);
			npc.getNavigator().setTarget(l);
		}
	}
}
