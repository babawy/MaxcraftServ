package fr.maxcraftThings.Quest;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import fr.maxcraftThings.Main;
import net.citizensnpcs.api.trait.Trait;
import net.citizensnpcs.api.util.DataKey;

public class Speaker extends Trait {
	public Speaker() {
		super("Speaker");
	}	
	private String message;
	private DataKey key;
	@EventHandler
	public void onset(PlayerCommandPreprocessEvent e) {
		if (e.getMessage().contains("quest")&&e.getMessage().contains("text"))
		if (this.getNPC().equals(Main.C.getNPCSelector().getSelected(e.getPlayer()))){
			message = e.getMessage().replaceAll("/quest text ", "");
			save(key);
			Outils.NPCChat(e.getPlayer(),this.getNPC(),"Texte modifi�");
		}

	}
	@Override
	public void load(DataKey key) {
		this.key = key;
		message = key.getString("message", "Salut!");
	}
	public void save(DataKey key) {
		key.setString("message",message);
	}
	@EventHandler
	public void onmove(PlayerMoveEvent e) {
		if (Outils.ProximityEnter(e,this.getNPC(),5))
				Outils.NPCChat(e.getPlayer(),this.getNPC(),message);
	}
}