package fr.maxcraftThings.Quest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import fr.maxcraft.Zone;
import fr.maxcraftThings.Main;
import net.citizensnpcs.api.trait.Trait;
import net.citizensnpcs.trait.Anchors;
import net.citizensnpcs.util.Anchor;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class Palefrenier extends Trait {
	public Palefrenier() {
		super("Palefrenier");
	}
	HashMap<String,String> choice = new HashMap<String,String> ();

	@EventHandler
	public void onclic(PlayerInteractEntityEvent e) throws ParseException {
		if (e.getRightClicked().equals(this.getNPC().getBukkitEntity())){
			if (!choice.containsKey(e.getPlayer().getName())){
				Outils.NPCChat(e.getPlayer(), this.getNPC(), "Que puis-je faire pour vous?");
				TextComponent message = new TextComponent("[Reprendre un cheval.]");
				message.setColor(ChatColor.BLUE);
				message.setClickEvent( new ClickEvent( ClickEvent.Action.RUN_COMMAND, "/quest palefrenier 1" ));
				e.getPlayer().spigot().sendMessage(message);
				TextComponent message2 = new TextComponent("[Deposer un cheval.]");
				message2.setColor(ChatColor.RED);
				message2.setClickEvent( new ClickEvent( ClickEvent.Action.RUN_COMMAND, "/quest palefrenier 2" ));
				e.getPlayer().spigot().sendMessage(message2);
				return;
				}
			if (choice.get(e.getPlayer().getName())=="1")
				if (e.getPlayer().getItemInHand().getType().equals(Material.PAPER)){
					ItemMeta meta = e.getPlayer().getItemInHand().getItemMeta();
					if (meta.getDisplayName().equals(ChatColor.GRAY+"Certificat de paturage")){
						Anchor n = npc.getTrait(Anchors.class).getAnchor("enclo");
						Zone z = Main.MZ.getZone(n.getLocation());
						for (Entity en : Main.MZ.getEntities(z))
							if (en.getUniqueId().toString().equals(ChatColor.stripColor(meta.getLore().get(1)))){
								Date aujourdhui = new Date();
								SimpleDateFormat format = new SimpleDateFormat("dd-MM-yy");
								Date date = format.parse(ChatColor.stripColor(meta.getLore().get(3)));
								en.teleport(e.getPlayer().getLocation());
								choice.remove(e.getPlayer().getName());
								Outils.pay(e.getPlayer(),aujourdhui.getDay()-date.getDay());
								en.setPassenger(e.getPlayer());
								e.getPlayer().setItemInHand(null);
						}
					}
				}
			if (choice.get(e.getPlayer().getName())=="2" && e.getPlayer().isInsideVehicle()){
				Date aujourdhui = new Date();
				SimpleDateFormat format = new SimpleDateFormat("dd-MM-yy");
				Horse h = (Horse) e.getPlayer().getVehicle();
				Anchor n = npc.getTrait(Anchors.class).getAnchor("enclo");
				Zone zone = Main.MZ.getZone(n.getLocation());
				int x=0;
				int z=0;
				for(Location l : zone.getSelection().points){
					x=x+(l.getBlockX());
					z=z+(l.getBlockZ());
				}
				e.getPlayer().leaveVehicle();
				h.teleport(n.getLocation());
				ItemStack i = new ItemStack(Material.PAPER,1);
				ItemMeta meta = i.getItemMeta();
				meta.setLore(Arrays.asList(ChatColor.AQUA+zone.getName()+", cheval numero :",ChatColor.GRAY+h.getUniqueId().toString(),ChatColor.AQUA+"Date de mise en paturage : ",ChatColor.GRAY+format.format(aujourdhui)));
				meta.setDisplayName(ChatColor.GRAY+"Certificat de paturage");
				i.setItemMeta(meta);
				e.getPlayer().getInventory().addItem(i);
				e.getPlayer().updateInventory();
				choice.remove(e.getPlayer().getName());
				Outils.pay(e.getPlayer(),10);
			}
		}
	}
	@EventHandler
	public void onchoice(PlayerCommandPreprocessEvent e) {
		if (e.getMessage().contains("quest")&&e.getMessage().contains("palefrenier")){
			int i = Integer.parseInt(e.getMessage().replaceAll("[\\D]", ""));
			if (i==1) reprendre(e.getPlayer());
			if (i==2) deposer(e.getPlayer());
	}
}
	private void reprendre(Player player) {
		choice.put(player.getName(),"1");
		Outils.NPCChat(player, this.getNPC(), "Donner moi votre certificat de paturage.");
		Outils.NPCChat(player, this.getNPC(), "Vous devrez payer 1 POs par jours de garde.");
	}
	private void deposer(Player player) {
		choice.put(player.getName(),"2");
		Outils.NPCChat(player, this.getNPC(), "Amener moi votre cheval.");
		Outils.NPCChat(player, this.getNPC(), "Vous devrez payer 10 POs de garde.");
	}
}
