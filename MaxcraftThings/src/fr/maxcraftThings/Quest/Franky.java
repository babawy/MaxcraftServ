package fr.maxcraftThings.Quest;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;

import net.citizensnpcs.api.trait.Trait;

public class Franky extends Trait {
	public Franky() {
		super("Franky");
	}
	@EventHandler
	public void onclic(PlayerInteractEntityEvent e) {
		if (e.getRightClicked().equals(this.getNPC().getBukkitEntity()))
			if (e.getPlayer().getItemInHand().getType().equals(Material.APPLE)){
				e.getPlayer().getInventory().removeItem(new ItemStack(Material.APPLE,1));
				this.getNPC().getNavigator().setTarget(e.getPlayer(), false);
				Outils.Narrate(e.getPlayer(), "On dirrais que Franky vous apprecie, il vas sans doute vous suivre un petit peu!");
			}
	}
	@EventHandler
	public void onclic(EntityDamageByEntityEvent e) {
		if (e.getEntity().equals(this.getNPC()))
			if (e.getDamager()instanceof Player){
				Player p = (Player)e.getDamager();
				if (p.getItemInHand().getType().equals(Material.STICK)){
					this.getNPC().getNavigator().cancelNavigation();
					
				}
			}
	}
    @Override
    public void run() {
    	Outils.Follow(this.npc);
    }
}
