package fr.maxcraftThings.Quest;

import java.util.HashMap;
import java.util.UUID;

import net.citizensnpcs.api.trait.Trait;
import net.citizensnpcs.api.util.DataKey;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerEditBookEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

public class Sabine extends Trait {
		private HashMap<UUID, ItemStack> writer = new HashMap<UUID, ItemStack> ();
		public Sabine() {
			super("Sabine");
		}	

	@EventHandler
	public void onclic(PlayerInteractEntityEvent e) {
		if (e.getRightClicked().equals(this.getNPC().getBukkitEntity())){
			if (writer.containsKey(e.getPlayer().getUniqueId()))
				return;
			Outils.NPCChat(e.getPlayer(), this.getNPC(),"Pour que votre suggestion reste anonyme, ne pas signer le livre.");
			ItemStack si = e.getPlayer().getItemInHand();
			e.getPlayer().setItemInHand(new ItemStack(Material.BOOK_AND_QUILL,1));
			writer.put(e.getPlayer().getUniqueId(), si);
		}
	}
	@EventHandler
	public void onsign(PlayerEditBookEvent e) {
		if (writer.containsKey(e.getPlayer().getUniqueId()))
		{
			Outils.NPCChat(e.getPlayer(), this.getNPC(),"Merci pour votre suggestion, elle sera rapidement lue par un admin.");
			e.getPlayer().setItemInHand(writer.get(e.getPlayer().getUniqueId()));
			Location loc = e.getPlayer().getLocation();
			loc.setX(30);
			loc.setY(77);
			loc.setZ(29);
			ItemStack item = new ItemStack(Material.WRITTEN_BOOK,1);
			item.setItemMeta(e.getNewBookMeta());
			Chest c = (Chest)loc.getWorld().getBlockAt(loc).getState();
			c.getBlockInventory().addItem(item);
			writer.remove(e.getPlayer().getUniqueId());
		}
	}
	@EventHandler
	public void onmove(PlayerMoveEvent e) {
		if (Outils.ProximityEnter(e,this.getNPC(),5))
				Outils.NPCChat(e.getPlayer(),this.getNPC(),"Bienvenue au bureau des suggestions!");
		if (writer.containsKey(e.getPlayer().getUniqueId())&&Outils.ProximityExit(e, this.getNPC(), 5)){
			e.setCancelled(true);
			Outils.NPCChat(e.getPlayer(), this.getNPC(), "Vous devais finir votre suggestion avant de partir!");
		}
	}
}

