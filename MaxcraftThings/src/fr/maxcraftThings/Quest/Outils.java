package fr.maxcraftThings.Quest;

import net.citizensnpcs.api.npc.NPC;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

import fr.maxcraftThings.Main;

public class Outils {

	public static void NPCChat(Player player, NPC npc, String string) {
		player.sendMessage(ChatColor.GREEN+"[NPC] "+npc.getFullName()+ChatColor.WHITE+" : "+string);
	}
	public static void Narrate(Player player, String string) {
		player.sendMessage(ChatColor.GRAY+string);
	}

	public static boolean ProximityEnter(PlayerMoveEvent e,NPC npc, int i) {
		if (npc.isSpawned())
		if (e.getTo().getWorld().equals(npc.getBukkitEntity().getWorld()))
		if ((int)e.getTo().distance(npc.getBukkitEntity().getLocation())==i)
			if ((int)e.getTo().distance(npc.getBukkitEntity().getLocation())<(int)e.getFrom().distance(npc.getBukkitEntity().getLocation()))
				return true;
		return false;
	}
	public static boolean ProximityExit(PlayerMoveEvent e,NPC npc, int i) {
		if (npc.isSpawned())
		if (e.getTo().getWorld().equals(npc.getBukkitEntity().getWorld()))
		if ((int)e.getTo().distance(npc.getBukkitEntity().getLocation())==i)
			if ((int)e.getTo().distance(npc.getBukkitEntity().getLocation())>(int)e.getFrom().distance(npc.getBukkitEntity().getLocation()))
				return true;
		return false;
	}
	public static void Follow(NPC npc){
		if (npc.isSpawned()){
			Location to = npc.getNavigator().getTargetAsLocation();
			Location from = npc.getBukkitEntity().getLocation();
			if (!to.getWorld().equals(from.getWorld()))
				npc.teleport(to, TeleportCause.PLUGIN);
			else if(to.distance(from)<=4&&!npc.getNavigator().isPaused())
				npc.getNavigator().setPaused(true);
			else if(to.distance(from)>=5&&npc.getNavigator().isPaused())
				npc.getNavigator().setPaused(false);
			else if(to.distance(from)>=50)
				npc.teleport(to, TeleportCause.PLUGIN);
			
		}
	}
	public static void pay(Player player, int i) {
		Main.MM.economy.bankWithdraw(player.getName(), (float)i);
		Narrate(player,"Vous avez payer "+i+" POs");
	}
	public static void earn(Player player, int i) {
		Main.MM.economy.bankDeposit(player.getName(), (float)i);
		Narrate(player,"Vous avez gagner "+i+" POs");
		
	}
	public static void ask(String string,int id,Player p,NPC npc,String color){
		TextComponent message = new TextComponent("["+string+"]");
		message.setColor(ChatColor.valueOf(color.toUpperCase()));
		message.setClickEvent( new ClickEvent( ClickEvent.Action.RUN_COMMAND, "/quest "+npc.getName()+" "+Integer.toString(id)));
		p.spigot().sendMessage(message);
	}
}
