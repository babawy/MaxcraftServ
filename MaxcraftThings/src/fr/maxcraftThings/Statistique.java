package fr.maxcraftThings;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.command.CommandSender;

import fr.maxcraft.Zone;


public class Statistique {
	static void update(CommandSender sender){
		ResultSet result = Main.MM.mysql_query("SELECT * FROM user", false);
		try {
			Main.MM.mysql_update("TRUNCATE TABLE `stats`");
			while (result.next())
			{
				if (result.getInt("actif")>0)
					if (result.getInt("gametime")>600){
						String username = result.getString("username");
						Double money = Main.MM.economy.getBalance(username);
						int terrain = 0;
						for (Zone z : Main.MZ.ZoneManager.listZones)
							if (z.hasOwner())
								if (z.getOwner().getUserName().equals(username)&&z.getSelection().getWorld().getName().equals("maxcraft"))
									terrain = terrain+z.getSelection().getArea();
						Main.MM.mysql_update("INSERT INTO stats (`nom`, `argent`,`terrain`,`a venir`) VALUES ('"+username+"', "+money+", "+terrain+", null);");
						sender.sendMessage("Stats update for "+username);
					}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
