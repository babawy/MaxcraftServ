package fr.maxcraftThings;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Sign;
import org.bukkit.scheduler.BukkitRunnable;

import fr.maxcraft.Faction;
import fr.maxcraft.Maxcraftien;
import fr.maxcraft.Zone;

public class WarZone {
	public static void onclic(PlayerInteractEntityEvent e) {
		if (e.getRightClicked() instanceof ItemFrame) {
			ItemFrame f = (ItemFrame) e.getRightClicked();
			if (f.getItem()==null)
				return;
			ItemStack b = f.getItem();
			if (b.hasItemMeta())
			if (b.getItemMeta().hasLore())
				if (b.getItemMeta().getLore().contains("warzone")) {
					Zone z = Main.MZ.getZone(e.getRightClicked().getLocation());
					Maxcraftien m = Main.MM.getMaxcraftien(e.getPlayer().getName());
					e.setCancelled(false);
					int i = Integer.parseInt(b.getItemMeta().getDisplayName().substring(0,b.getItemMeta().getDisplayName().length() - 1));
					ItemMeta n = b.getItemMeta();
					
					
					if (m.haveFaction()) {
						if (i == 1){
							message(z.getTags().get(0),"&6"+z.getParent().getName()+" ","&cPoint de conquete perdu!");
							n.setLore(Arrays.asList("warzone", m.getFaction()
									.getTag()));
						}
						if (b.getItemMeta().getLore()
								.contains(m.getFaction().getTag())
								&& i < 100)
							++i;
						if (!b.getItemMeta().getLore()
								.contains(m.getFaction().getTag())
								&& i > 0)
							--i;
						if (i == 100
								&& b.getItemMeta().getLore()
										.contains(m.getFaction().getTag()))
							conquest(e, m);
						if (i == 99
								&& !b.getItemMeta().getLore()
										.contains(m.getFaction().getTag()))
							message(b.getItemMeta().getLore().get(1),"&6"+Main.MZ.getZone(e.getRightClicked().getLocation()).getParent().getName()+" ","&cPoint de conquete attaqu�!");
						n.setDisplayName(i + "%");
						b.setItemMeta(n);
						f.setItem(b);
					}
				}
		}

	}

	// conqui l'altar et eventuellement la mere
	private static void conquest(PlayerInteractEntityEvent e, Maxcraftien m) {
		Zone z = Main.MZ.getZone(e.getRightClicked().getLocation());
		ArrayList<String> tags = z.getTags();
		tags.set(0, m.getFaction().getTag());
		message(z.getTags().get(0),"&6"+z.getParent().getName()+" ","&cPoint de conquete pris par l'ennemi!");
		z.setTags(tags);
		z.saveInSQL();
		z.reloadDynmap();
		message(m.getFaction().getTag(),"&6"+z.getParent().getName()+" ","&cPoint de conquete pris!");
		e.setCancelled(true);
		Bukkit.getLogger().info(z.getChildren()+"");
		for (Zone zo : z.getParent().getChildren())
			if (!zo.getTags().contains(m.getFaction().getTag()))
				return;
		message(z.getParent().getTags().get(0),"&6"+z.getParent().getName()+" ","&cZone perdue!");
		message(m.getFaction().getTag(),"&6"+z.getParent().getName()+" ","&cZone conquie!");
		z.getParent().setTags(tags);
		z.saveInSQL();
		z.reloadDynmap();
	}

	private static void message(String faction, String title, String subtitle) {
		for (Player p : Bukkit.getServer().getOnlinePlayers())
			if (Main.MM.isMaxcraftien(p.getName()))
				if (Main.MM.getMaxcraftien(p).haveFaction())
				if (Main.MM.getMaxcraftien(p).getFaction().getTag()== faction)
					Annonce.sendTitle(p, 200, title, subtitle, Sound.ANVIL_LAND, 1);
			
		
	}

	public static void onEnable(Main main) throws SQLException {
		ResultSet result = Main.MM.mysql_query("SELECT * FROM warzone", false);
		while (result.next())
		{
			Block b = Bukkit.getWorld(result.getString("world")).getBlockAt(result.getInt("x"), result.getInt("y"), result.getInt("z"));
			new WarzoneTask(b,Material.getMaterial(result.getInt("item_id")),result.getInt("id")).runTaskTimer(main, 0L, (long)result.getInt("sec")*20);
		}
	}

	public static void sign(SignChangeEvent e, Main plugin) {
		Sign s = (Sign) e.getBlock().getState().getData();
		Location l = e.getBlock().getLocation();
		if (e.getBlock().getRelative(s.getAttachedFace()).getType().equals(Material.CHEST))
			if (e.getLine(0).equalsIgnoreCase("warzone"))
				if (Main.MM.isInt(e.getLine(1)) && Main.MM.isInt(e.getLine(2)))
					if(e.getPlayer().hasPermission("maxcraft.things.admin"))
					{
						l = e.getBlock().getRelative(s.getAttachedFace()).getLocation();
						Main.MM.mysql_update("INSERT INTO warzone (`id`, `item_id`, `world`, `x`, `y`, `z`, `sec`) VALUES (NULL, "+Integer.parseInt(e.getLine(1))+", '"+l.getWorld().getName()+"', "+l.getBlockX()+", "+l.getBlockY()+", "+l.getBlockZ()+", "+Integer.parseInt(e.getLine(2))+");");
						e.getPlayer().sendMessage("Remplissage enclench� : 1 "+Material.getMaterial(Integer.parseInt(e.getLine(1)))+" toute les "+e.getLine(2)+" secondes");
						new WarzoneTask(l.getBlock(),Material.getMaterial(Integer.parseInt(e.getLine(1))),0).runTaskTimer(plugin, 0L,20*Integer.parseInt(e.getLine(2)));
						e.setCancelled(true);
					}
		
	}
}

