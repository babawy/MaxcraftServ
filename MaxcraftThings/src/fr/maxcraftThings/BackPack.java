package fr.maxcraftThings;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.meta.ItemMeta;

public class BackPack {
	private static File file;
	private static YamlConfiguration config;
	private static ArrayList<Inventory> invlist = new ArrayList<Inventory>();

	@SuppressWarnings("unchecked")
	public static void onEnable() {
		file = new File("plugins/MaxcraftThings/backpack.yml");
		config = YamlConfiguration.loadConfiguration(file);
		if (!file.exists())
			try {
				file.createNewFile();
			} catch (IOException e) {
			}
		invlist.addAll((Collection<? extends Inventory>) config.getItemStack("inv"));
	}

	public static void invclick(InventoryClickEvent e) {
		if (e.getClick().isRightClick()||(e.getClick().isCreativeAction()&&e.getClick().isRightClick()))
			if (e.getCurrentItem().getItemMeta().getLore()
					.contains(ChatColor.YELLOW+"Sac")) {
				if (e.getCurrentItem().getItemMeta().getLore().size() < 2) {
					Inventory inv = Bukkit.createInventory(e.getWhoClicked(), 9,"Sac");
					invlist.add(inv);
					ItemMeta meta = e.getCurrentItem().getItemMeta();
					meta.setLore(Arrays.asList(ChatColor.YELLOW+"Sac",
							Integer.toString(invlist.size()-1)));
					e.getCurrentItem().setItemMeta(meta);
				}
				e.setCancelled(true);
				e.getWhoClicked().openInventory(
						invlist.get(Integer.parseInt(e.getCurrentItem()
								.getItemMeta().getLore().get(1))));
			}
	}
	public static void onDisable() throws IOException{
		config.set("inv", invlist);
		config.save(file);
	}
}
