package fr.maxcraftThings;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

public class WarzoneTask extends BukkitRunnable
{

	private Block b;
	private Material item;
	private int id;

	public WarzoneTask(Block b, Material item, int id) {
		this.b = b;
		this.item = item;
		this.id = id;
	}


	@Override
	public void run() {
		if (b.getType().equals(Material.CHEST))
		{
			b.getChunk().load(true);
			Chest c = (Chest) b.getState();
			c.getBlockInventory().addItem(new ItemStack(item));
		}
		else
		{
			Main.MM.mysql_update("DELETE FROM `warzone` WHERE `warzone`.`id` = "+id);
			this.cancel();
		}
	}
}