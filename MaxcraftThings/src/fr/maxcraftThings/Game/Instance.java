package fr.maxcraftThings.Game;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javafx.concurrent.Task;
import net.md_5.bungee.api.ChatColor;
import net.nathem.script.core.Map;
import net.nathem.script.core.NSCore;
import net.nathem.script.core.NathemWorld;

import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.block.Sign;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import fr.maxcraftThings.Main;
import fr.maxcraftThings.WarzoneTask;

public class Instance {
	
	private String sourceWorldName;
	private String instanceWorldName;
	private World instanceWorld;
	private NathemWorld nathemWorld;
	private HashMap<Player, Location> backLocations;
	private HashMap<Player, String> entrance;
	static ArrayList<String> dispatcher = new ArrayList<String>();

	private int max;
	private String marker;
	private boolean dispatch;
	
	public Instance(String sourceWorldName,StartSign s) {
		this.sourceWorldName = sourceWorldName;
		this.instanceWorld = null;
		this.nathemWorld = null;
		this.backLocations = new HashMap<Player, Location>();
		this.entrance = new HashMap<Player, String>();
		this.instanceWorldName = "NS"+sourceWorldName;
		this.max = s.getMax();
		this.dispatch = s.isDispatch();
		if (dispatch)
			for (int i =1;i<=max;i++)
				dispatcher.add("libre");
	}
	
	public NathemWorld build()
	{
		File sourceWorldFile = new File(this.sourceWorldName);
		File instanceFile = new File(this.instanceWorldName);
		if(!sourceWorldFile.exists())
		{
			NSCore.log("World <"+instanceWorldName+"> could not be loaded because the source world <"+sourceWorldName+"> not exists in files");
			return null;
			
		}
		
		World oldInstance = Bukkit.getWorld(this.instanceWorldName);
		if(oldInstance != null)
		{
			NSCore.log("World <"+instanceWorldName+"> is already loaded, unloading world...");
			Bukkit.unloadWorld(oldInstance, false);
		}
		
		if(instanceFile.exists())
		{
			NSCore.log("World <"+instanceWorldName+"> already exists in files, removing files...");
			instanceFile.delete();
		}
		
		// Copy
		
		try {
			FileUtils.copyDirectory(sourceWorldFile, instanceFile);
		} catch (IOException e) {
		
			NSCore.log("Error copying file to create instance <"+instanceWorldName+">");
			return null;
		}
		
		// Removing uid.dat

		File uidFile = new File(instanceFile, "uid.dat");
        uidFile.delete();
        
        // World loading
        this.instanceWorld = Bukkit.createWorld(WorldCreator.name(this.instanceWorldName));
        
        if(this.instanceWorld == null)
        {
        	
			NSCore.log("World <"+instanceWorldName+"> cannot be loaded...");
        	return null;
        }
        
        
        // Map loading
        Map map = null;
        
        try {
        	map = new Map(this.sourceWorldName);
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        if(map == null){
        	NSCore.log("No Map");
        	return null;
        }
        
        // NathemWorld loading
        this.nathemWorld = new NathemWorld(Main.NS.Editor.getPlugin(), this.instanceWorldName, map);
        
        //auto destroy
		new Task(this).runTaskTimer(Main.P, 200L, 200L);
		
		for (Entity e:this.getInstanceWorld().getEntities())
			e.remove();
		Game.listinstance.add(this);
		
        return this.nathemWorld;

        
	}
	
	public void destroy()
	{
		// Teleport back
		for(Player p : this.getInstanceWorld().getPlayers())
		{
			this.teleportBack(p);
		}
		
		boolean unload = Bukkit.unloadWorld(this.getInstanceWorld(), true);
		
		if(unload == false)
		{
			NSCore.log("Error unloading world <"+instanceWorldName+">...");
		}
		
		World sourceWorld = Bukkit.getWorld(this.sourceWorldName);
		File testWorldFile = new File(this.instanceWorldName);

		try {
			FileUtils.deleteDirectory(testWorldFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void teleport(Player p, String entrance)
	{
		if (this.inGame()!=null)
		if (this.inGame().size()>=this.max)
			p.sendMessage("Plus de place dans cette partie");
		else {
			if (dispatch)
				for (int i = 0;i<this.max;i++)
					if (this.dispatcher.get(i).equals("libre")){
						entrance = entrance+":"+Integer.toString(i+1);
						this.dispatcher.set(i,p.getName());
						break;
				}
					
			if (this.nathemWorld.getMarkers().containsKey(entrance)){
				this.backLocations.put(p, p.getLocation());
				this.entrance.put(p, entrance);
				p.teleport(this.nathemWorld.getMarkers().get(entrance));
				p.setGameMode(GameMode.ADVENTURE);
				}
			else{
				p.sendMessage(entrance+" not found!");
				return;
			}
		for (Player player : this.instanceWorld.getPlayers())
			player.sendMessage(ChatColor.GREEN+p.getName()+" a rejoint la partie "+ChatColor.RED+"("+inGame().size()+"/"+this.max+")");
			this.nathemWorld.sendGlobalSignal(0, true, p);
		if (inGame().size()==(int) 0.25*this.max)
			this.nathemWorld.sendGlobalSignal(1, true, p);
		if (inGame().size()==(int) 0.50*this.max)
			this.nathemWorld.sendGlobalSignal(2, true, p);
		if (inGame().size()==(int) 0.75*this.max)
			this.nathemWorld.sendGlobalSignal(3, true, p);
		if (inGame().size()==this.max)
			this.nathemWorld.sendGlobalSignal(4, true, p);

	}
			
	}
	public HashMap<Player, String> getEntrance() {
		return entrance;
	}

	public ArrayList<Player> inGame()
	{
			ArrayList<Player> ps= new ArrayList<Player>();
			for (Player p : this.instanceWorld.getPlayers())
				if (p.getGameMode().equals(GameMode.ADVENTURE)||p.getGameMode().equals(GameMode.SURVIVAL))
					ps.add(p);
		return ps;
	}
	public ArrayList<Player> ModoinGame()
	{
			ArrayList<Player> ps= new ArrayList<Player>();
			for (Player p : this.instanceWorld.getPlayers())
				if (!p.getGameMode().equals(GameMode.ADVENTURE)||p.getGameMode().equals(GameMode.SURVIVAL))
					if (p.hasPermission("maxcraft.modo"))
					ps.add(p);
		return ps;
	}

	public void teleportBack(Player p)
	{
		Location backLocation = this.getBackLocations().get(p);
		if(backLocation == null) backLocation = Bukkit.getServer().getWorlds().get(0).getSpawnLocation();
		p.teleport(backLocation);
		p.setGameMode(GameMode.SURVIVAL);
		if (this.dispatch)
			for (int i = 0;i<this.max;i++)
				if (this.dispatcher.get(i).equals(p.getName())){
					this.dispatcher.set(i,"libre");
					break;
			}
			
		for (Player player : this.instanceWorld.getPlayers())
			player.sendMessage(ChatColor.GREEN+p.getName()+" a quitt� la partie "+ChatColor.RED+"("+inGame().size()+"/"+this.max+")");
		
	}
	
	@Override
	public String toString() {
		return "Instance [sourceWorldName=" + sourceWorldName
				+ ", instanceWorldName=" + instanceWorldName + "]";
	}

	public String getSourceWorldName() {
		return sourceWorldName;
	}

	public World getInstanceWorld() {
		return instanceWorld;
	}

	public NathemWorld getNathemWorld() {
		return nathemWorld;
	}
	
	public World getSourceWorld()
	{
		return Bukkit.getWorld(this.sourceWorldName);
	}

	public HashMap<Player, Location> getBackLocations() {
		return backLocations;
	}
	public class Task extends BukkitRunnable {
		private Instance instance;

		public Task(Instance instance) {
			this.instance = instance;
		}

		@Override
		public void run() {
			if (instance.inGame().isEmpty()&&instance.ModoinGame().isEmpty()){
				NSCore.log("Destroy instance");
				instance.destroy();
				Game.listinstance.remove(instance);
				this.cancel();
				
				
			}
		}
	}
}