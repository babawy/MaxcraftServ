package fr.maxcraftThings.Game;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Sign;
import org.bukkit.configuration.file.YamlConfiguration;

public class Game {
	static ArrayList<Instance> listinstance = new ArrayList<Instance>();
	static ArrayList<StartSign> listss = new ArrayList<StartSign>();
	private static File file;
	private static YamlConfiguration config;
	
	public static void onEnable() {
		file = new File("plugins/MaxcraftThings/StartSigns.yml");
		config = YamlConfiguration.loadConfiguration(file);
		if (!file.exists())
			try {
				file.createNewFile();
			} catch (IOException e) {
			}
		for (String c:config.getKeys(false)){
			Location l = (Location) config.get(c+".location");
			String name = config.getString(c+".name");
			String map = config.getString(c+".map");
			int max = config.getInt(c+".max");
			String entrance = config.getString(c+".entrance");
			Sign s = (Sign) l.getBlock().getState();
			int status = config.getInt(c+".status");
			boolean dispatch = config.getBoolean(c+".dispatch");
			new StartSign(l, name, map, max,entrance, s, status,dispatch);
		}
	}
	
	
	
	
	public static StartSign getSS(Location location) {
		for (StartSign s : listss)
			if (s.getLocation().equals(location))
				return s;
		return null;
		
	}




	public static void onDisable() {
		int i=0;
		for (StartSign s : listss){
			++i;
			config.set(Integer.toString(i)+".location",s.getLocation());
			config.set(Integer.toString(i)+".name",s.getName());
			config.set(Integer.toString(i)+".map",s.getMap());
			config.set(Integer.toString(i)+".max",s.getMax());
			config.set(Integer.toString(i)+".entrance",s.getEntrance());
			config.set(Integer.toString(i)+".status",s.getStatus());
			config.set(Integer.toString(i)+".dispatch",s.isDispatch());
		}
		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
