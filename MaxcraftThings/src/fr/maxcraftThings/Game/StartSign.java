package fr.maxcraftThings.Game;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

public class StartSign {
	private String entrance;
	private Object name;
	private String map;
	private int max;
	private int status;
	private Location location;
	private Sign sign;
	private boolean dispatch;

	public StartSign(Location location, String Name,String map,int max,String entrance,Sign sign,int status, boolean dispatch){
		this.name = Name;
		this.map=map;
		this.max=max;
		this.entrance=entrance;
		this.location=location;
		this.status=status;
		this.sign=sign;
		this.dispatch = dispatch;
		Game.listss.add(this);
		}

	public boolean isDispatch() {
		return dispatch;
	}

	public void open() {
		this.sign = (Sign) this.location.getBlock().getState();
		this.sign.setLine(0,""+ChatColor.GREEN + ChatColor.BOLD + "MaxcraftGames");
		this.sign.setLine(1,""+ChatColor.DARK_RED + ChatColor.BOLD + name);
		if (!instanceIsOpen())
			this.sign.setLine(2,""+ChatColor.GREEN + ChatColor.BOLD + "Nouveau");
		else
			this.sign.setLine(2,""+ChatColor.GREEN + ChatColor.BOLD + "("+getInstance().inGame().size()+"/"+max+")");
		this.sign.setLine(3, " ");
		this.sign.update();	
		this.sign.getChunk().unload();
		this.sign.getChunk().load();	
		this.status = 1;
	}

	public Location getLocation() {
		return location;
	}

	public Sign getSign() {
		return sign;
	}

	private Instance getInstance() {
		for (Instance i:Game.listinstance)
			if (i.getSourceWorldName().equals(map))
				return i;
		return null;
	}

	private boolean instanceIsOpen() {
		for (Instance i:Game.listinstance)
			if (i.getSourceWorldName().equals(map))
				return true;
		return false;
	}

	public void switchState() {
		if (this.status==4)
			this.open();
		else if (this.status==3)
			this.destroy();
		else if (this.status==2)
			this.close();
		else if (this.status==0)
			this.open();
		else if (this.status==1)
			this.event();
	}

	public String getEntrance() {
		return entrance;
	}

	public Object getName() {
		return name;
	}

	public String getMap() {
		return map;
	}

	public int getMax() {
		return max;
	}

	public int getStatus() {
		return status;
	}

	private void event() {
		this.sign = (Sign) this.location.getBlock().getState();
		this.sign.setLine(0,""+ChatColor.YELLOW + ChatColor.BOLD + "MaxcraftGames");
		this.sign.setLine(1,""+ChatColor.DARK_RED + ChatColor.BOLD + name);
		if (!instanceIsOpen())
			this.sign.setLine(2,""+ChatColor.YELLOW + ChatColor.BOLD + "Nouvel event");
		else
			this.sign.setLine(2,""+ChatColor.YELLOW+ ChatColor.BOLD + "Partie en cours.");
		this.sign.setLine(3, " ");
		this.sign.update();	
		this.sign.getChunk().unload();
		this.sign.getChunk().load();		
		this.status=2;
	}

	private void destroy() {
		this.sign = (Sign) this.location.getBlock().getState();
		this.sign.setLine(0, ""+ChatColor.GRAY + ChatColor.BOLD + "***********");
		this.sign.setLine(1, ""+ChatColor.DARK_RED + ChatColor.BOLD + "Supprimer ?");
		this.sign.setLine(2, ""+ChatColor.DARK_RED + ChatColor.BOLD + " ");
		this.sign.setLine(3, ""+ChatColor.GRAY + ChatColor.BOLD + "***********");
		this.sign.update();
		this.sign.getChunk().unload();
		this.sign.getChunk().load();	
		this.status=4;
		
	}

	private void close() {
		this.sign = (Sign) this.location.getBlock().getState();
		this.sign.setLine(2, ""+ChatColor.DARK_RED + ChatColor.BOLD + "Jeu ferm� !");
		this.sign.update();
		this.sign.getChunk().unload();
		this.sign.getChunk().load();		
		this.status=3;
		
	}

	public void click(Player player) {
		if (status==1)
			if(!instanceIsOpen()){
				Instance i = new Instance(this.map, this);
				i.build();
				i.teleport(player,this.entrance);
				this.open();
			}
			else{
			getInstance().teleport(player,this.entrance);
			this.open();
			}
		else if (status==3)
			player.sendMessage(ChatColor.GREEN+"Ce jeu est actuellement ferm�.");
		else if (status==4&&player.hasPermission("maxcraft.modo"))
			this.remove();
		else if (status==0)
			this.open();
		else if (status == 2)
			this.eventcall(player);
			
	}

	private void eventcall(Player player) {
		if (!instanceIsOpen()){
			Instance i = new Instance(this.map, this);
			i.build();
			if (i.getNathemWorld().getMarkers().containsKey(entrance+"admin"))
				player.teleport(i.getNathemWorld().getMarkers().get(entrance+"admin"));
			else 
				player.teleport(i.getInstanceWorld().getSpawnLocation());
			player.setGameMode(GameMode.SPECTATOR);
			for (Player p : Bukkit.getOnlinePlayers()){
				TextComponent message = new TextComponent("[Event] : Une partie de "+this.name+" vas commencer, cliquez sur ce message pour la rejoindre!");
				message.setColor(ChatColor.YELLOW);
				message.setClickEvent( new ClickEvent( ClickEvent.Action.RUN_COMMAND, "/party join "+player.getName()+" "+this.entrance));
				p.spigot().sendMessage(message);
			}
		}
		else {
			getInstance().teleport(player,"admin");
			player.setGameMode(GameMode.SPECTATOR);
		}
	}

	private void remove() {
		this.sign.getBlock().breakNaturally();
		Game.listss.remove(this);
	}
	
}
