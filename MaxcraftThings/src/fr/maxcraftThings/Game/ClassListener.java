package fr.maxcraftThings.Game;


import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import fr.maxcraftThings.Main;

public class ClassListener implements Listener {

	private Main plugin;
	public ClassListener(Main plugin) {
		this.plugin = plugin;
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	@EventHandler
	public void onInstanceJoin(PlayerInteractEvent e){
		if (e.getClickedBlock()==null)
			return;
			if (e.getClickedBlock().getType().equals(Material.WALL_SIGN)||e.getClickedBlock().getType().equals(Material.SIGN_POST))
				if (Game.getSS(e.getClickedBlock().getLocation())!=null){
					if (e.getAction().equals(Action.LEFT_CLICK_BLOCK))
						if (e.getPlayer().hasPermission("maxcraft.modo")){
							e.setCancelled(true);
							Game.getSS(e.getClickedBlock().getLocation()).switchState();
						}
					if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
						Game.getSS(e.getClickedBlock().getLocation()).click(e.getPlayer());
						}
					}
			}
	@EventHandler
	public void onSignPlaced(SignChangeEvent e){
		if (e.getPlayer().hasPermission("maxcraft.modo"))
			if (e.getLine(0).contains("=ss")){
				StartSign s = new StartSign(e.getBlock().getLocation(),e.getLine(1),e.getLine(0).substring(4,e.getLine(0).length()),Integer.parseInt(e.getLine(2)),e.getLine(3), null, 0,false);
			}
	}
	@EventHandler
	public void onDeco(PlayerQuitEvent e){
		for (Instance i :Game.listinstance)
			if (i.inGame().contains(e.getPlayer()))
				i.teleportBack(e.getPlayer());
	}
	@EventHandler
	public void onDeath(PlayerDeathEvent e){
		for (Instance i :Game.listinstance)
			if (i.inGame().contains(e.getEntity())){
				i.teleportBack(e.getEntity());
			}
	}
	@EventHandler
	public void onUseItem(PlayerInteractEvent e){
		for (Instance i :Game.listinstance)
			if (i.inGame().contains(e.getPlayer())&&e.getItem()!=null){
				if (e.getItem().getType().equals(Material.BED))
					if (e.getItem().getItemMeta().getDisplayName().equals(ChatColor.GREEN+"HUB"))
						i.teleportBack((Player) e.getPlayer());
				
				if (e.getItem().getType().equals(Material.FIREWORK))
					if (e.getItem().getItemMeta().getDisplayName().equals(ChatColor.DARK_RED+"Fus�e de d�tresse"))
						for (Player p : Bukkit.getOnlinePlayers()){
							TextComponent message = new TextComponent("Le joueur "+e.getPlayer().getName()+" vous invite dans "+i.getSourceWorldName());
							message.setColor(ChatColor.YELLOW);
							message.setClickEvent( new ClickEvent( ClickEvent.Action.RUN_COMMAND, "/party join "+e.getPlayer().getName()));
							p.spigot().sendMessage(message);
						}
							
	}
	}
	public static void onCommand(CommandSender sender, String[] args) {
		if (args[0].equals("?"))
			sender.sendMessage("a venir");
		if (args[0].equals("join")){
			Player p = Bukkit.getPlayer(args[1]);
			for (Instance i :Game.listinstance)
				if (i.inGame().contains(p)||i.ModoinGame().contains(p))
					if (!i.inGame().contains((Player) sender))	
						if (args.length==2)
						i.teleport((Player) sender,i.getEntrance().get(p));
						else
							i.teleport((Player) sender, args[2]);
		}
		if (args[0].equals("leave"))
			for (Instance i :Game.listinstance)
				if (i.inGame().contains((Player)sender)||i.ModoinGame().contains((Player)sender))
					i.teleportBack((Player) sender);
		if (args[0].equals("end")&&sender.hasPermission("maxcraft.modo"))
			for (Instance i :Game.listinstance)
				if (i.inGame().contains((Player)sender)||i.ModoinGame().contains((Player)sender))
					i.destroy();		
	}
	
}
