package fr.maxcraftThings;

import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Chicken;
import org.bukkit.entity.IronGolem;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.entity.EntityDeathEvent;

public class AntiAutoFarm {
	private static ArrayList<Material> protect = new ArrayList<Material>(
			Arrays.asList(Material.PUMPKIN, Material.MELON_BLOCK, Material.SOIL, Material.SUGAR_CANE_BLOCK, Material.WHEAT));

	public static void ondeath(EntityDeathEvent e) {
		if ((e.getEntity() instanceof IronGolem)
				|| (e.getEntity() instanceof Chicken))
			if (null == e.getEntity().getKiller())
				e.getDrops().clear();
	}

	public static void onbreak(BlockPistonExtendEvent e) {
		for (Block b : e.getBlocks())
			if (protect.contains(b.getType()))
				e.setCancelled(true);

	}
}
