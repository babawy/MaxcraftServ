package fr.maxcraftThings;


import java.io.IOException;
import java.text.ParseException;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerAchievementAwardedEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ClassListener implements Listener {

	private Main plugin;
	public ClassListener(Main plugin) {
		this.plugin = plugin;
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	@EventHandler
	public void onclic(PlayerInteractEntityEvent e) throws IOException {
		if (Main.things.get("jobs"))
			fr.maxcraftThings.Jobs.ClassListener.onclic(e);
		if (Main.things.get("warzone"))
			WarZone.onclic(e);
	}
	@EventHandler
	public void onbreak(BlockBreakEvent e) {
		if (Main.things.get("jobs"))
			fr.maxcraftThings.Jobs.ClassListener.onbreak(e);
	}
	@EventHandler
	public void onPlace(BlockPlaceEvent e) {
		if (Main.things.get("jobs"))
			fr.maxcraftThings.Jobs.ClassListener.onPlace(e,plugin);
	}
	@EventHandler
	public void onextend(BlockPistonExtendEvent e){
		if (Main.things.get("antiautofarm"))
			AntiAutoFarm.onbreak(e);
		if (Main.things.get("jobs"))
			fr.maxcraftThings.Jobs.ClassListener.onextend(e);
	}
	@EventHandler
	public void ondeath(EntityDeathEvent e) {
		if (Main.things.get("antiautofarm"))
			AntiAutoFarm.ondeath(e);
	}
	@EventHandler
	public void ondamage(EntityDamageEvent e) {
		if (Main.things.get("horsekeep"))
			HorseKeep.onEntityDamage(e);
	}
	@EventHandler
	public void onco(PlayerJoinEvent e) {
		if (Main.things.get("jobs"))
			fr.maxcraftThings.Jobs.Main.Readsave(e.getPlayer().getUniqueId());
		if (Main.things.get("menu"))
			Menu.deleteachievement(e);
		
	}

	@EventHandler
	public void ondeco(PlayerQuitEvent e) throws IOException {
		if (Main.things.get("jobs"))
			fr.maxcraftThings.Jobs.Main.Savesave(e.getPlayer().getUniqueId());
		if (Main.things.get("horsekeep"))
			HorseKeep.ondeco(e);
	}
	@EventHandler
	public void invclick(InventoryClickEvent e){
		if (Main.things.get("backpack"))
			BackPack.invclick(e);
		if (Main.things.get("menu"))
			Menu.invclick(e);
	}
	@EventHandler
	public void invopen(PlayerAchievementAwardedEvent e){
		if (Main.things.get("menu"))
			Menu.invopen(e);
	}
	@EventHandler
	public void invopen(PlayerInteractEvent e) throws ParseException{
	}

	@EventHandler
	public void onChatAdmin(AsyncPlayerChatEvent e){
		if (Main.things.get("chat"))
			ChatManager.chat(e);
	}
	@EventHandler
	public void onSignPlaced(SignChangeEvent e){
		if (Main.things.get("warzone"))
			WarZone.sign(e,plugin);
	}
	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e){
		if (Main.things.get("sundaynopvp"))
			SundayNoPvp.onDamage(e);
	}
	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent e){
			ModeratorDataStorage.onCommand(e);
	}
}
