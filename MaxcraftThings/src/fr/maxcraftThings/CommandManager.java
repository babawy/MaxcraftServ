package fr.maxcraftThings;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class CommandManager {

	public static boolean onCommand(CommandSender sender, Command cmd,
			String label, String[] args, Main plugin) {
		if (cmd.getName().equalsIgnoreCase("an"))
			Annonce.onCommand(sender,cmd,label,args,plugin);
		
		if (cmd.getName().equalsIgnoreCase("mute-all"))
			ChatManager.muteAll(plugin);
		
		if (cmd.getName().equalsIgnoreCase("espion"))
			ChatManager.espion(sender);
		
		if (cmd.getName().equalsIgnoreCase("ghost"))
			Ghost.Command(sender);
		
		if (cmd.getName().equalsIgnoreCase("holo"))
			Holo.create(args,sender);

		if (Main.things.get("joueur"))
		if (cmd.getName().equalsIgnoreCase("Joueur"))
			Joueur.change(args,sender,plugin);
		
		if (cmd.getName().equalsIgnoreCase("things"))
			if (args[0].equals("stats"))
			Statistique.update(sender);

		if (Main.things.get("jobs"))
			if (cmd.getName().equalsIgnoreCase("Joueur"))
				fr.maxcraftThings.Jobs.ClassListener.command(sender,cmd,label,args,plugin);
		
		if (cmd.getName().equalsIgnoreCase("party"))
			fr.maxcraftThings.Game.ClassListener.onCommand(sender,args);
		
		if (cmd.getName().equalsIgnoreCase("journal"))
			fr.maxcraftThings.ModeratorDataStorage.onCommand(sender, args);
		
		return true;
	}
}
