package fr.maxcraftThings.Jobs;

import java.util.UUID;


public class Worker {

	private UUID Name;
	private String metier;
	private double xp;
	private boolean confirm;
	@SuppressWarnings("unused")
	private int lvl;

	public Worker(UUID name2, String metier, double d,boolean confirm) {
		this.Name=name2;
		this.metier=metier;
		this.xp=d;
		this.confirm=confirm;
	}

	public UUID getName() {
		return Name;
	}

	public void setName(UUID Name) {
		this.Name = Name;
	}

	public String getMetier() {
		return metier;
	}

	public void setMetier(String metier) {
		this.metier = metier;
	}

	public double getXp() {
		return xp;
	}

	public void setXp(double d) {
		this.xp = d;
	}

	public boolean isConfirm() {
		return confirm;
	}

	public void setConfirm(boolean confirm) {
		this.confirm = confirm;
	}

	public int getLvl() {
		return (int)(Math.sqrt(xp)/30);
	}

	public void setLvl(int lvl) {
		this.lvl = lvl;
	}

}
