package fr.maxcraftThings.Jobs;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import fr.maxcraftThings.Jobs.Worker;

public class Main {
	static HashMap<UUID, Worker> workerlist = new HashMap<UUID, Worker>();
	private static FileConfiguration config;
	private static File file;

	public static void onEnable() {
		file = new File("plugins/MaxcraftThings/player.yml");
		config = YamlConfiguration.loadConfiguration(file);
		if (!file.exists())
			try {
				file.createNewFile();
			} catch (IOException e) {
			}
		Collection<? extends Player> co = Bukkit.getServer().getOnlinePlayers();
		for (Player o : co)
			Readsave(o.getUniqueId());
	}

	public static void onDisable() {
		Collection<? extends Player> co = Bukkit.getServer().getOnlinePlayers();
		for (Player o : co)
			try {
				Savesave(o.getUniqueId());
			} catch (IOException e) {
			}
	}

	public static void Savesave(UUID uuid) throws IOException {
		String name = uuid.toString();
		config.set(name + ".metier", workerlist.get(uuid).getMetier());
		config.set(name + ".xp", workerlist.get(uuid).getXp());
		config.set(name + ".name",Bukkit.getServer().getPlayer(uuid).getName());
		config.save(file);
	}

	public static void Readsave(UUID uuid) {
		String name = uuid.toString();
		if (config.contains(name)) {
			Worker worker = new Worker(UUID.fromString(name),config.getString(
					name + ".metier"), config.getDouble(name + ".xp"), false);
			workerlist.put(UUID.fromString(name), worker);
		} else {
			Worker worker = new Worker(UUID.fromString(name), "citoyen", 0, false);
			workerlist.put(UUID.fromString(name), worker);
		}
	}
}