package fr.maxcraftThings.Jobs;


import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.ItemFrame;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import fr.maxcraftThings.Main;


public class ClassListener{
	public static void onbreak(BlockBreakEvent e) {
		if (e.isCancelled())
			return;
		if (!fr.maxcraftThings.Main.MM.isMaxcraftien(e.getPlayer().getName()))
			return;
		if (fr.maxcraftThings.Main.MZ.getZone(e.getBlock().getLocation())!=null)
		if (!fr.maxcraftThings.Main.MZ.getZone(e.getBlock().getLocation()).canBuild(e.getPlayer()))
			return;
		if (e.getBlock().hasMetadata("player"))
			return;
		if (!e.getPlayer().getGameMode().equals(GameMode.SURVIVAL))
			return;
		Worker player = fr.maxcraftThings.Jobs.Main.workerlist.get(e.getPlayer().getUniqueId());
		int exlvl = player.getLvl();
		double xp = 0;
		// Mineur
		if (e.getBlock().getType().equals(Material.DIAMOND_ORE)
				|| e.getBlock().getType().equals(Material.IRON_ORE)
				|| e.getBlock().getType().equals(Material.LAPIS_ORE)
				|| e.getBlock().getType().equals(Material.COAL_ORE)
				|| e.getBlock().getType().equals(Material.REDSTONE_ORE)
				|| e.getBlock().getType().equals(Material.QUARTZ_ORE)
				|| e.getBlock().getType().equals(Material.GOLD_ORE)) {
			if (player.getMetier().equalsIgnoreCase("Mineur")) {
				player.setXp(player.getXp() + 20);
				xp = (player.getLvl() / 10);
			}
			double rate = Math.random();
			if (rate <= (xp)) {
				for (ItemStack i:e.getBlock().getDrops())
				{
					e.getBlock().getWorld().dropItemNaturally(e.getBlock().getLocation(),new ItemStack(i.getType(),i.getAmount()));
				}
			}
		}
		// Bucheron
		if (e.getBlock().getType().equals(Material.LOG)
				||e.getBlock().getType().equals(Material.LEAVES)
				||e.getBlock().getType().equals(Material.LOG_2)
				||e.getBlock().getType().equals(Material.VINE)) {
			if (player.getMetier().equalsIgnoreCase("Bucheron")) {
				player.setXp(player.getXp() + 3);
				xp = (player.getLvl() / 10);
			}
			double rate = Math.random();
			if (xp>0.5 && (xp-0.5)>rate)
				for (ItemStack i:e.getBlock().getDrops())
				{
					e.getBlock().getWorld().dropItemNaturally(e.getBlock().getLocation(),new ItemStack(i.getType(),i.getAmount()));
				}
			else if (rate >= (0.5 + xp)) {
				e.setCancelled(true);
				e.getBlock().setType(Material.AIR);
			}
		}
		// Excavateur
		if (e.getBlock().getType().equals(Material.STONE)
				|| e.getBlock().getType().equals(Material.DIRT)
				|| e.getBlock().getType().equals(Material.GRASS)
				|| e.getBlock().getType().equals(Material.GRAVEL)
				|| e.getBlock().getType().equals(Material.CLAY)
				|| e.getBlock().getType().equals(Material.SAND)
				|| e.getBlock().getType().equals(Material.RED_SANDSTONE)
				|| e.getBlock().getType().equals(Material.MYCEL)
				|| e.getBlock().getType().equals(Material.SNOW_BLOCK)
				|| e.getBlock().getType().equals(Material.PRISMARINE)
				|| e.getBlock().getType().equals(Material.SANDSTONE)
				|| e.getBlock().getType().equals(Material.HARD_CLAY)) {
			if (player.getMetier().equalsIgnoreCase("Excavateur")) {
				player.setXp(player.getXp() + 0.5);
				xp = (player.getLvl() / 10);
			}
			double rate = Math.random();
			if (xp>0.5 && (xp-0.5)>rate)
				for (ItemStack i:e.getBlock().getDrops())
				{
					e.getBlock().getWorld().dropItemNaturally(e.getBlock().getLocation(),new ItemStack(i.getType(),i.getAmount()));
				}
			else if (rate >= (0.5 + xp)) {
					e.setCancelled(true);
					e.getBlock().setType(Material.AIR);
				
			}
		}
		// Fermier
		if (e.getBlock().getType().equals(Material.WHEAT)
				|| e.getBlock().getType().equals(Material.PUMPKIN)
				|| e.getBlock().getType().equals(Material.MELON_BLOCK)
				|| e.getBlock().getType().equals(Material.POTATO_ITEM)
				|| e.getBlock().getType().equals(Material.CARROT_ITEM)
				|| e.getBlock().getType().equals(Material.CROPS)
				|| e.getBlock().getType().equals(Material.NETHER_WARTS)
				|| e.getBlock().getType().equals(Material.SUGAR_CANE)
				|| e.getBlock().getType().equals(Material.SEEDS)
				|| e.getBlock().getType().equals(Material.BROWN_MUSHROOM)
				|| e.getBlock().getType().equals(Material.RED_MUSHROOM)) {
			if (player.getMetier().equalsIgnoreCase("Fermier")) {
				player.setXp(player.getXp() + 3);
				xp = (player.getLvl() / 10);
			}
			double rate = Math.random();
			if (xp>0.5 && (xp-0.5)>rate)
				for (ItemStack i:e.getBlock().getDrops())
				{
					e.getBlock().getWorld().dropItemNaturally(e.getBlock().getLocation(),new ItemStack(i.getType(),i.getAmount()));
				}
			else if (rate >= (0.5 + xp)) {
				e.setCancelled(true);
				e.getBlock().setType(Material.AIR);
			}
		}
		if (exlvl < player.getLvl())
			e.getPlayer().sendMessage(
					ChatColor.RED + "Vous etes maintenant un "
							+ player.getMetier() + " de niveau "
							+ player.getLvl());
		if (e.isCancelled())
			if (e.getPlayer().getItemInHand().getMaxStackSize()==1)
				e.getPlayer().getItemInHand().setDurability((short) (e.getPlayer().getItemInHand().getDurability()-1));

	}
	

	public static void onclic(PlayerInteractEntityEvent e) {
		try {
			Worker player = fr.maxcraftThings.Jobs.Main.workerlist.get(e.getPlayer().getUniqueId());
			String percent = ""
					+ ((Math.sqrt(player.getXp()) / 30) - player.getLvl())
					* 100 + "00000"; // Tronquature
			percent = percent.substring(0, 5);//
			if (e.getRightClicked() instanceof ItemFrame) {
				ItemFrame f = (ItemFrame) e.getRightClicked();
				if (f.getItem().getItemMeta().getLore().get(0)
						.contains("maxcraftjobs")) {
					if (player.getMetier().equalsIgnoreCase(
							f.getItem().getItemMeta().getDisplayName())) {

						e.getPlayer().sendMessage(
								ChatColor.BLUE + "Vous etes un "
										+ ChatColor.RED + player.getMetier()
										+ ChatColor.BLUE + " De niveau "
										+ ChatColor.RED + (player.getLvl()));
						e.getPlayer().sendMessage(
								ChatColor.BLUE + "Vous etes � " + ChatColor.RED
										+ (percent) + "%" + ChatColor.BLUE
										+ " du prochain niveau");
						e.setCancelled(true);
						return;
					}
					if (player.isConfirm()) {
						e.getPlayer().sendMessage(
								ChatColor.BLUE
										+ "Vous etes maintenant "
										+ ChatColor.RED
										+ f.getItem().getItemMeta()
												.getDisplayName());
						player.setMetier(f.getItem().getItemMeta()
								.getDisplayName());
						player.setConfirm(false);
						player.setXp(0);
						e.setCancelled(true);
						return;
					}
					e.getPlayer()
							.sendMessage(
									ChatColor.BLUE
											+ "Attention vous allez perdre la progression de votre metier actuel. Cliquez a nouveau pour confirmer votre choix!");
					player.setConfirm(true);
					e.setCancelled(true);
					return;
				}
			}
		} catch (NullPointerException ex) {}
	}


	public static void onPlace(BlockPlaceEvent e, fr.maxcraftThings.Main plugin) {
		
		e.getBlock().setMetadata("player",new FixedMetadataValue(plugin,e.getPlayer().getName()));
		
	}


	public static void onextend(BlockPistonExtendEvent e) {
		for(Block b : e.getBlocks()){
			Block nb = b.getRelative(e.getDirection());
			nb.setMetadata("player",new FixedMetadataValue(Main.P,"piston"));
		}
			
		
	}


	public static void command(CommandSender sender, Command cmd, String label,
			String[] args, Main plugin) {
		if (args[0].equals("lvl")&&args[1]!=null){
			Worker player = fr.maxcraftThings.Jobs.Main.workerlist.get(Main.MM.getMaxcraftien(args[1]).getPlayer());
			String percent = ""
					+ ((Math.sqrt(player.getXp()) / 30) - player.getLvl())
					* 100 + "00000"; // Tronquature
			percent = percent.substring(0, 5);//	
			sender.sendMessage(args[1]+" est lvl :"+percent);
		}
		
	}


	//public static void onBreed(PlayerInteractEntityEvent e) {
	//	if (e.getRightClicked()instanceof Animals)
	//	if (e.getPlayer().getItemInHand().getType()!=null){
	//		Material i = e.getPlayer().getItemInHand().getType();
	//		if(i.equals(Material.WHEAT)
	//			||i.equals(Material.CARROT)
	//			||i.equals(Material.SEEDS)){
	//			Worker player = Main.workerlist.get(e.getPlayer().getUniqueId());
	//			double xp = 0;
	//			if (player.getMetier().equalsIgnoreCase("Fermier")) {
	//				xp = (player.getLvl() / 10);
	//			}
	//			double rate = Math.random();
	//			if (rate >= (0.4 + xp)) {
	//				e.setCancelled(true);
	//			}
	//		}
	//	}
	//}
}