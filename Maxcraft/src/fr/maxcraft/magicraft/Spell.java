package fr.maxcraft.magicraft;

public abstract class Spell {
	
	public abstract boolean isCastable();
	public abstract void cast();
	
	public Spell(){
		
	}
	
	
}
