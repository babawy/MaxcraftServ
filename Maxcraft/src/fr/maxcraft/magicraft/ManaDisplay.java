package fr.maxcraft.magicraft;

import java.lang.reflect.Field;

import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_8_R1.DataWatcher;
import net.minecraft.server.v1_8_R1.EntityPlayer;
import net.minecraft.server.v1_8_R1.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_8_R1.PacketPlayOutSpawnEntityLiving;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

public class ManaDisplay {

	public static void sendPacket(Player player, Mage mage){
		EntityPlayer entityPlayer = ((CraftPlayer) player).getHandle();
		entityPlayer.playerConnection.sendPacket(getMobPacket(player,mage));
		entityPlayer.playerConnection.sendPacket(destroy());
	}
		@SuppressWarnings("deprecation")
		public static PacketPlayOutSpawnEntityLiving getMobPacket(Player p, Mage mage){
			Location loc = p.getLocation().add(p.getLocation().getDirection().multiply(20));
			
			PacketPlayOutSpawnEntityLiving mobPacket = new PacketPlayOutSpawnEntityLiving();
			try {
				Field a = getField(mobPacket.getClass(), "a");
		        a.setAccessible(true);
				a.set(mobPacket, (int) 63);
				
				Field b = getField(mobPacket.getClass(), "b");
		        b.setAccessible(true);
				b.set(mobPacket, (byte) EntityType.WITHER.getTypeId());
				
				Field c = getField(mobPacket.getClass(), "c");
		        c.setAccessible(true);
				c.set(mobPacket, (int) Math.floor(loc.getBlockX() * 32.0D));
				
				Field d = getField(mobPacket.getClass(), "d");
		        d.setAccessible(true);
				d.set(mobPacket, (int) Math.floor(loc.getBlockY() * 32.0D));
				
				Field e = getField(mobPacket.getClass(), "e");
		        e.setAccessible(true);
				e.set(mobPacket, (int) Math.floor(loc.getBlockZ() * 32.0D));
				
				Field f = getField(mobPacket.getClass(), "f");
		        f.setAccessible(true);
				f.set(mobPacket, (byte) 0);
				
				Field g = getField(mobPacket.getClass(), "g");
		        g.setAccessible(true);
				g.set(mobPacket, (byte) 0);
				
				Field h = getField(mobPacket.getClass(), "h");
		        h.setAccessible(true);
				h.set(mobPacket, (byte) 0);
				
				Field i = getField(mobPacket.getClass(), "i");
		        i.setAccessible(true);
				i.set(mobPacket, (byte) 0);
				
				Field j = getField(mobPacket.getClass(), "j");
		        j.setAccessible(true);
				j.set(mobPacket, (byte) 0);
				
				Field k = getField(mobPacket.getClass(), "k");
		        k.setAccessible(true);
				k.set(mobPacket, (byte) 0);
			
			} catch (IllegalArgumentException e1) {
				e1.printStackTrace();
			} catch (IllegalAccessException e1) {
				e1.printStackTrace();
			}
			
			DataWatcher watcher = getWatcher(mage);
			
			try{
				Field t = PacketPlayOutSpawnEntityLiving.class.getDeclaredField("l");
				t.setAccessible(true);
				t.set(mobPacket, watcher);
			} catch(Exception ex){
				ex.printStackTrace();
			}
			
			return mobPacket;
		}
		public static Field getField(Class<?> cl, String field_name){
	        try {
	            Field field = cl.getDeclaredField(field_name);
	            return field;
	        } catch (SecurityException e) {
	            e.printStackTrace();
	        } catch (NoSuchFieldException e) {
	            e.printStackTrace();
	        }
	        return null;
	    }	
		public static DataWatcher getWatcher(Mage mage){
		
			DataWatcher watcher = new DataWatcher(null);
			watcher.a(0, (byte) 0x20);
			watcher.a(2, ChatColor.DARK_PURPLE+"Mana :");
			watcher.a(3, (byte) 1);
			watcher.a(6, (float)mage.getMana()/mage.getMaxMana()*300);
			watcher.a(20, (int) 1000);
			return watcher;
		}
		public static PacketPlayOutEntityDestroy destroy(){
			return new PacketPlayOutEntityDestroy(new int[] {63});
		}
}
