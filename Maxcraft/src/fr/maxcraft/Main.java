package fr.maxcraft;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

//import com.comphenix.protocol.ProtocolLibrary;
//import com.comphenix.protocol.ProtocolManager;

import fr.maxcraft.Menu.MenuListener;
import fr.maxcraft.chatmanager.ChatListener;
import fr.maxcraft.economy.EcoListener;
import fr.maxcraft.faction.Faction;
import fr.maxcraft.faction.FactionListener;
import fr.maxcraft.game.GameListener;
import fr.maxcraft.jobs.JobsListener;
import fr.maxcraft.magicraft.MagicListener;
import fr.maxcraft.magicraft.ManaTask;
import fr.maxcraft.moderation.ModeratorCommand;
import fr.maxcraft.protect.ProtectListener;
import fr.maxcraft.utils.MySQLSaver;
import fr.maxcraft.world.Marker;
import fr.maxcraft.world.Travel;
import fr.maxcraft.world.World;
import fr.maxcraft.world.WorldListener;
import fr.maxcraft.zone.Zone;
import fr.maxcraft.zone.ZoneCommand;
import fr.maxcraft.zone.ZoneListener;

public class Main extends JavaPlugin {


	private static Plugin plugin;
	//private ProtocolManager protocolManager;
	public void onEnable() {
		plugin = this;
		
		try {
		MySQLSaver.connect();
		//protocolManager = ProtocolLibrary.getProtocolManager();
		//new PacketsUtil(protocolManager);
		
		//Data convert
		//MaxcraftDataConverter.convertUser();
		//MaxcraftDataConverter.convertZone();

		Faction.load();
		User.loadActive();
		Zone.load();
		Marker.load();
		//ZoneVente.load();
		
		new ManaTask().runTaskTimer(this, 0, 10);
		
		
		//CommandRegister
		new ModeratorCommand();
		new HelpManager();
		new ZoneCommand();
		World.register(this);
		Marker.register(this);
		Travel.register(this);
		ChatListener.register(this);
		
		
		//Listeners
		new JobsListener(this);
		new ZoneListener(this);
		new WorldListener(this);
		new EcoListener(this);
		new ChatListener(this);
		new ProtectListener(this);
		new FactionListener(this);
		new MenuListener(this);
		new MagicListener(this);
		new GameListener(this);
		
		
		
		Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.DARK_RED+"===================");
		Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.DARK_RED+"= Maxcraft charg� =");
		Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.DARK_RED+"===================");
		} catch (Exception e) {
			e.printStackTrace();
			logError("Erreur au chargement!");
		}
		
	}
	public static void log(String s){
		Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.BLUE+"[Maxcraft]"+s);
	}
	public static void logError(String s){
		Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.DARK_RED+"[Maxcraft]"+s);
	}
	public static Main getPlugin() {
		return (Main)plugin ;
	}
}
