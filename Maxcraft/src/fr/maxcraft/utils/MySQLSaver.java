package fr.maxcraft.utils;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import fr.maxcraft.Main;

public class MySQLSaver {
	public static java.sql.Connection sql;
	
	/*
	 * Connexion MySQL
	 */
	public static void connect(){
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		try {

			sql = DriverManager.getConnection("jdbc:mysql://localhost:3306/maxcraft","root", "");
			Main.log("Connexion sql etablie !");
		} catch (SQLException e) {
			e.printStackTrace();
			Main.log("Connexion sql echou�e !");
		}
	}
	
	
	/*
	 * Requete MySQL
	 */
	public static ResultSet mysql_query(String query, boolean singleResult)
	{
		try {
			if(sql.isClosed())
			{
				connect();
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			Statement state = (Statement) MySQLSaver.sql.createStatement();
			ResultSet result = state.executeQuery(query);
	
		if(singleResult)
		{
			result.next();
		}
		return result;
				
		} catch (SQLException e) {
			return null;
		}
		
	}
	
	public static void mysql_update(String query)
	{
		try {
			if(sql.isClosed())
			{
				connect();
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			Statement state = (Statement) MySQLSaver.sql.createStatement();
		
			state.executeUpdate(query);
			state.close();
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}	
	}
}
