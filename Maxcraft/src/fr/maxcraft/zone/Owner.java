package fr.maxcraft.zone;

import fr.maxcraft.economy.Account;

public interface Owner extends Account{

	String getName();

	Object getUuid();

	boolean isActive();

}
