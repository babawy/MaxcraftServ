package fr.maxcraft.faction;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.scoreboard.NameTagVisibility;
import org.bukkit.scoreboard.Team;

import fr.maxcraft.utils.MySQLSaver;
import fr.maxcraft.utils.Serialize;
import fr.maxcraft.Main;
import fr.maxcraft.User;

public class Faction {

	private static ArrayList<Faction> factionlist = new ArrayList<Faction>();
	private String name;
	private int id;
	private String TAG;
	private User owner;
	private ArrayList<User> members = new ArrayList<User>();
	private Location sapwn;
	private Location jail;
	private double balance;
	private Team team;

	public Faction(int id,String name,String TAG,double balance,Location spawn,Location jail){
		this.setName(name);
		this.id = id;
		this.TAG = TAG;
		this.setBalance(balance);
		this.sapwn = spawn;
		this.jail = jail;
		if (Bukkit.getScoreboardManager().getMainScoreboard().getTeam(TAG)!=null)
			this.team = Bukkit.getScoreboardManager().getMainScoreboard().getTeam(TAG);
		else{
			this.team = Bukkit.getScoreboardManager().getMainScoreboard().registerNewTeam(TAG);
			this.team.setAllowFriendlyFire(false);
			this.team.setCanSeeFriendlyInvisibles(true);
			this.team.setNameTagVisibility(NameTagVisibility.ALWAYS);
		}
		factionlist.add(this);
	}
	public static void load(){
		ResultSet r = MySQLSaver.mysql_query("SELECT * FROM `faction`", false);
		try {
		while (r.next()){
		Location spawn = Serialize.locationFromString(r.getString("spawn"));
		Location jail = Serialize.locationFromString(r.getString("jail"));
		new Faction(r.getInt("id"),r.getString("name"),r.getString("tag"),r.getDouble("capital"),spawn,jail);

		}
		} catch (SQLException e) {
			e.printStackTrace();
			Main.logError("Erreur au chargement des factions");
		}
		Main.log("Faction chargees. ("+factionlist.size()+")");
	}
	public String getTAG() {
		return TAG;
	}
	public User getOwner() {
		return owner;
	}
	public ArrayList<User> getMembers() {
		return members;
	}
	public Location getSapwn() {
		return sapwn;
	}
	public Location getJail() {
		return jail;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	public void connect(User u) {
		this.team.addPlayer(u.getPlayer());
		u.getPlayer().setDisplayName(ChatColor.AQUA+"["+this.TAG+"] "+u.getPlayer().getName());
		this.members.add(u);
	}
	public static Faction get(int factionid) {
		for (Faction f : factionlist)
			if (f.id == factionid)
				return f;
		return null;
	}
}
