package fr.maxcraft.faction;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import fr.maxcraft.Main;
import fr.maxcraft.User;
import fr.maxcraft.permissions.Perms;

public class FactionListener implements Listener {
	
	public FactionListener(Main main) {
		main.getServer().getPluginManager().registerEvents(this, main);
	}
	
	@EventHandler
	public void onco(PlayerJoinEvent e){
		Perms.load(e.getPlayer().getUniqueId());
		User u = User.get(e.getPlayer());
		if (u.getFaction()==null)
			return;
		u.getFaction().connect(u);
		
	}
}
