package fr.maxcraft.economy;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import fr.maxcraft.Main;

public class EcoListener implements Listener {

	public EcoListener(Main main) {
		main.getServer().getPluginManager().registerEvents(this, main);
	}
	
	@EventHandler
	public void onSignPlaced(SignChangeEvent e){
		e.getPlayer().sendMessage("Ca marche!");
		if (!e.getLine(0).contains("vendre"))
			return;
		e.getPlayer().sendMessage(ZoneVente.create(e));
	}
	@EventHandler
	public void onSignBreak(BlockBreakEvent e){
		for(ZoneVente vente : ZoneVente.ventelist){
			Block down = vente.getSign().getBlock().getRelative(BlockFace.DOWN);
			if(e.getBlock().equals(vente.getSign().getBlock()) || e.getBlock().equals(down)){		
				e.setCancelled(true);
				return;
			}
		}
	}
	@EventHandler
	public void onBuyBySign(PlayerInteractEvent e){
		if(e.getAction().equals(Action.LEFT_CLICK_BLOCK))
			for(ZoneVente vente : ZoneVente.ventelist)
				if(e.getClickedBlock().equals(vente.getSign().getBlock())){
					vente.onClick(e.getPlayer());
					return;
				}
			
		if(e.getAction().equals(Action.RIGHT_CLICK_BLOCK))
			for(ZoneVente vente : ZoneVente.ventelist)
				if(e.getClickedBlock().equals(vente.getSign().getBlock())){
					vente.onRightClick(e.getPlayer());
					return;
				}
	}
}
