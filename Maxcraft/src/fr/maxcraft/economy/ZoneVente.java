package fr.maxcraft.economy;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.block.SignChangeEvent;

import fr.maxcraft.Main;
import fr.maxcraft.User;
import fr.maxcraft.utils.IsInt;
import fr.maxcraft.utils.MySQLSaver;
import fr.maxcraft.zone.Zone;

public class ZoneVente {

	public static ArrayList<ZoneVente> ventelist = new ArrayList<ZoneVente>();
	private int prix;
	private Zone zone;
	private Sign sign;
	private int choiceMenu = 0;

	
	public ZoneVente(int prix, Zone zone,Sign sign) {
		this.prix = prix;
		this.zone = zone;
		this.sign = sign;
		ventelist.add(this);
		loadSign();
	}

	public Sign getSign() {
		return sign;
	}

	public static void load() {
		ResultSet r = MySQLSaver.mysql_query("SELECT * FROM `vente`", false);
		try {
			while (r.next()){
			Location l = new Location(Bukkit.getWorld(r.getString("world")),r.getInt("x"),r.getInt("y"),r.getInt("z"));
			new ZoneVente(r.getInt("price"),Zone.getZone(r.getInt("zone")),(Sign) l.getBlock().getState());
			}
		} catch (SQLException e) {
			e.printStackTrace();
			Main.logError("Erreur au chargement des ventes.");
		}
		
	}
	public void loadSign()
	{
		sign.setLine(0, ChatColor.AQUA + "A VENDRE");
		if (zone.getOwner()!=null)
		sign.setLine(1,""+zone.getOwner().getName());
		else 
			sign.setLine(1,"Maxcraft");
		sign.setLine(2, "clique = achat");
		if(this.choiceMenu == 0)
			sign.setLine(3, ChatColor.DARK_RED +""+ this.prix + " POs");
		else if(this.choiceMenu == 1)
			sign.setLine(3, ChatColor.DARK_RED + "SUPPRIMER");
		reloadSign(sign);
	}

	private void reloadSign(Sign sign2) {
		sign.update();
		sign.getChunk().unload();
		sign.getChunk().load();
	}
	public static String create(SignChangeEvent e){
		if (Zone.getZone(e.getBlock().getLocation())==null)
			return "Vous n'etes pas sur une zone.";
		Zone z = Zone.getZone(e.getBlock().getLocation());
		if (z.getOwner()==null)
			if (!e.getPlayer().hasPermission("maxcraft.guide"))
				return "Vous ne pouvez pas vendre cette parcelle";
		if (z.getOwner()!=null)
			if (!z.getOwner().equals(User.get(e.getPlayer()))&&!e.getPlayer().hasPermission("maxcraft.guide"))
				return "Vous ne pouvez pas vendre cette parcelle";
		if (!IsInt.isInt((e.getLine(1))))
			return "Ce prix n'est pas un nombre, le nombre doit etre un entier positif.";
		int i = Integer.parseInt(e.getLine(1));
		if (i<1)
			return "Le prix doit etre un entier positif";
		ZoneVente v = new ZoneVente(i, z, (Sign) e.getBlock().getState());
		v.insert();
		return "La zone a �t� mise en vent.";
	}

	private void insert() {
		MySQLSaver.mysql_update("INSERT INTO `vente` (`prix`, `zone`,`x`, `y`, `z`,`world`) VALUES"
				+ " ("+this.prix+", "+this.zone.getId()+", "+this.sign.getLocation().getBlockX()+", "+this.sign.getLocation().getBlockY()+", "+this.sign.getLocation().getBlockZ()+", '"+this.sign.getLocation().getWorld().getName()+"');");
		
	}

	public void onClick(Player player) {
		if (player.hasPermission("maxcraft.guide")&& this.choiceMenu==1){
			this.remove();
			return;
		}
		if (this.zone.getOwner()!=null)
			if (!User.get(player).pay(this.prix, this.zone.getOwner()))
				return;
		if (this.zone.getOwner()==null)
			if (!User.get(player).take(this.prix))
				return;
		this.zone.reset();
		this.zone.setOwner(User.get(player));
		this.remove();
		
	}

	private void remove() {
		MySQLSaver.mysql_update("DELETE FROM `vente` WHERE `zone`= "+this.zone.getId());
		ventelist.remove(this);
		this.sign.getBlock().setType(Material.AIR);
	}

	public void onRightClick(Player player) {
		if (player.hasPermission("maxcraft.guide")&& this.choiceMenu==0)
			this.choiceMenu=1;
		if (player.hasPermission("maxcraft.guide")&& this.choiceMenu==1)
			this.choiceMenu=0;
		this.loadSign();
		
	}

}
