package fr.maxcraft.Menu;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import fr.maxcraft.Main;
import fr.maxcraft.Menu.faction.FactionMenu;
import fr.maxcraft.utils.ItemStackCreator;

public class MainMenu extends Menu{
	
	public MainMenu(){
		super();
		new Task(getItem(null)).runTaskTimer(Main.getPlugin(), 1, 1);

	}

	@SuppressWarnings("static-access")
	@Override
	public void execute(Player p) {
		super.playermenulist.remove(p);
		Inventory inv = super.getInventory("Menu");
		inv.setItem(5, new FactionMenu(p).getItem(p));
		
		p.openInventory(inv);
		
	}

	@Override
	public ItemStack getItem(Player p) {
		return new ItemStackCreator(Material.EXP_BOTTLE, "Menu", Arrays.asList("Cliquez pour acceder au menu"), 1, 0);
	}	
	
	public class Task extends BukkitRunnable {

		private ItemStack i;

		public Task(ItemStack i) {
			this.i = i;
		}

		@Override
		public void run() {
			for (Player p : Bukkit.getServer().getOnlinePlayers())
				if (p.getOpenInventory().getTopInventory().getType().equals(InventoryType.CRAFTING))
					if (p.getOpenInventory().getTopInventory().getItem(0)==null){
						p.getOpenInventory ().getTopInventory ().setItem (0,i);
						p.updateInventory();
					
		}
	}
	}
}
	
