package fr.maxcraft.Menu;

import java.util.ArrayList;
import java.util.HashMap;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public abstract class Menu {
	
	static HashMap<Player,ArrayList<Menu>> playermenulist = new HashMap<Player,ArrayList<Menu>>();
	static ArrayList<Menu> menulist = new ArrayList<Menu>();
	static Menu voidMenu = new VoidMenu();
	
	public Menu(){
		menulist.add(this);
	}
	public Menu(Player p){
		if (!playermenulist.containsKey(p))
			playermenulist.put(p, new ArrayList<Menu>());
		playermenulist.get(p).add(this);
	}
	
	public abstract void execute(Player p);
	public abstract ItemStack getItem(Player p);
	


	public static Inventory getInventory(String string) {
		Inventory inv = Bukkit.createInventory(null, 9, ChatColor.GOLD+string);
		for (int s = 0 ; s<inv.getSize();s++)
			inv.setItem(s, voidMenu.getItem(null));
		return inv;
	}
	
	public static boolean clic(ItemStack item, Player p) {
		for (Menu m : menulist)
			if (m.getItem(p).getItemMeta().equals(item.getItemMeta())){
				m.execute(p);
				return true;
			}
		if (!playermenulist.containsKey(p))
			return false;
		
		for (Menu m : playermenulist.get(p))
			if (m.getItem(p).getItemMeta().equals(item.getItemMeta())){
				m.execute(p);
				return true;
			}
		return false;
		
	}
}
