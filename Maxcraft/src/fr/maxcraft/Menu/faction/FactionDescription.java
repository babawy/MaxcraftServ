package fr.maxcraft.Menu.faction;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import fr.maxcraft.User;
import fr.maxcraft.Menu.Menu;
import fr.maxcraft.faction.Faction;
import fr.maxcraft.utils.ItemStackCreator;

public class FactionDescription extends Menu {


	public FactionDescription(Player p) {
		super(p);
	}

	@Override
	public void execute(Player p) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ItemStack getItem(Player p) {
		Faction f = User.get(p).getFaction();
		return new ItemStackCreator(Material.PAPER,f.getName(),Arrays.asList("Nom : "+f.getName(),"Tag : "+f.getTAG(),
				"Fondateur : "+f.getOwner().getName(),"Argent : "+f.getBalance(),"Joueurs : "+f.getMembers().size()),1,0);
	}

}
