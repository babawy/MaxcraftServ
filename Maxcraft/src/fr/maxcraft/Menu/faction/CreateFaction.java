package fr.maxcraft.Menu.faction;

import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import fr.maxcraft.User;
import fr.maxcraft.Menu.Menu;
import fr.maxcraft.utils.InputSign;
import fr.maxcraft.utils.ItemStackCreator;
import fr.maxcraft.utils.PacketsUtil;

public class CreateFaction extends Menu implements InputSign{

	public CreateFaction(Player p) {
		super(p);
	}

	@Override
	public void execute(Player p) {
		User u = User.get(p);
		if (u.getBalance()<=15000)
			return;
		p.closeInventory();
		//PacketsUtil.openSignGUI(p,this,"[Nom]","[TAG]","de_votre","faction");
		
	}

	@Override	
	public ItemStack getItem(Player p) {
		User u = User.get(p);
		if (u.getBalance()>=15000)
			return new ItemStackCreator(Material.BRICK,"Cr�er votre faction",Arrays.asList("Cr�er votre faction vous coutera 15.000 POs"),0,0);
		return new ItemStackCreator(Material.BRICK,"Cr�er votre faction",Arrays.asList("Vous n'avez pas assez d'argent pour creer une faction."),0,0);

	}

	@Override
	public void input(ArrayList<String> s,Player p) {
		p.sendMessage(""+s);
		
	}
	
	

}
