package fr.maxcraft.Menu.faction;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.maxcraft.User;
import fr.maxcraft.Menu.Menu;
import fr.maxcraft.utils.ItemStackCreator;

public class FactionMenu extends Menu{
	

	public FactionMenu(Player p){
		super(p);
	}

	@Override
	public void execute(Player p) {
		Inventory inv = super.getInventory("Menu");
		if (User.get(p).getFaction()!=null){
			inv.setItem(0,new FactionDescription(p).getItem(p));
		}
		else {
			inv.setItem(0, new CreateFaction(p).getItem(p));
		}
		
		p.openInventory(inv);
		
	}

	@Override
	public ItemStack getItem(Player p) {
		User u = User.get(p);
		if (u.getFaction()==null)
			return new ItemStackCreator(Material.IRON_SWORD,"Faction",Arrays.asList("Integrer une faction existante","ou creer la votre pour 15.000POs"),1,0);
		return new ItemStackCreator(Material.IRON_SWORD,"Faction",Arrays.asList(u.getFaction().getName()),1,0);
	}

}
