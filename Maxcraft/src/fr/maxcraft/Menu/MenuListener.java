package fr.maxcraft.Menu;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;



import fr.maxcraft.Main;

public class MenuListener implements Listener {
	public MenuListener(Main main) {
		main.getServer().getPluginManager().registerEvents(this, main);
		new MainMenu();
	}
	
	static HashMap<Player,ArrayList<Menu>> menulist = new HashMap<Player,ArrayList<Menu>>();

	@EventHandler
	public void onclic(InventoryClickEvent e){
		e.setCancelled(Menu.clic(e.getCurrentItem(),(Player)e.getWhoClicked()));
	}
}
