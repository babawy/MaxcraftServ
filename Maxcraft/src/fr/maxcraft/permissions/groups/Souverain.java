package fr.maxcraft.permissions.groups;


import java.util.Arrays;
import java.util.List;

import org.bukkit.ChatColor;

import fr.maxcraft.permissions.Group;

public class Souverain implements Group{
	
	public Souverain(){
		
	}

	@Override
	public String getPrefix() {
		return "Souverain";
	}

	@Override
	public List<String> getPermissions() {
		return Arrays.asList("*","maxcraft.guide","maxcraft.modo","maxcraft.admin");
	}

	@Override
	public ChatColor getColor() {
		return ChatColor.RED;
	}

}
