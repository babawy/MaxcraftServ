package fr.maxcraft.permissions;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.permissions.PermissionAttachment;

import fr.maxcraft.Main;
import fr.maxcraft.User;
import fr.maxcraft.utils.MySQLSaver;
import fr.maxcraft.utils.Serialize;

public class Perms {

	
	private Group group;
	PermissionAttachment attachment;
	private User user;

	public Perms(UUID uuid,String group, ArrayList<String> arrayList){
		try {
		this.user = User.get(uuid);
		this.attachment = this.user.getPlayer().addAttachment(Main.getPlugin());
		this.group = (Group) Class.forName("fr.maxcraft.permissions.groups."+group).getConstructor().newInstance();
		for (String p : this.group.getPermissions()){
			if (p.startsWith("!"))
				this.attachment.setPermission(p.substring(1), false);
			else
				this.attachment.setPermission(p, true);
			}
		for (String p : arrayList){
			if (p.startsWith("!"))
				this.attachment.setPermission(p.substring(1), false);
			else
				this.attachment.setPermission(p, true);
			}
		this.user.getPlayer().setDisplayName(ChatColor.BOLD+""+this.group.getColor()+this.group.getPrefix()+ChatColor.RESET+" "+this.group.getColor()+this.user.getPlayer().getName());
		this.user.getPlayer().setPlayerListName(this.group.getColor()+this.user.getPlayer().getName());
		
		
	} catch (Exception e) {
		e.printStackTrace();
		Main.logError("Erreur au chargement des groupes!");
	}
	}

	public static Perms load(UUID uuid) {
		ResultSet r = MySQLSaver.mysql_query("SELECT * FROM `perms` WHERE `perms`.`id` = '"+uuid.toString()+"'",true);
		try {
			if (!r.isFirst())
				return new Perms(uuid,"Citoyen",Serialize.ArrayStringFromString(""));
		return new Perms(uuid,r.getString("group"),Serialize.ArrayStringFromString(r.getString("perms")));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
