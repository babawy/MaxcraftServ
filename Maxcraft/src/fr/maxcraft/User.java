package fr.maxcraft;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

import net.md_5.bungee.api.chat.TextComponent;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import fr.maxcraft.economy.Account;
import fr.maxcraft.faction.Faction;
import fr.maxcraft.jobs.Jobs;
import fr.maxcraft.magicraft.Mage;
import fr.maxcraft.moderation.Moderation;
import fr.maxcraft.utils.MySQLSaver;
import fr.maxcraft.utils.PacketsUtil;
import fr.maxcraft.zone.Owner;

public class User implements Owner {

	private static ArrayList<User> playerlist = new ArrayList<User>();
	private UUID uuid;
	private String name;
	private Moderation moderation;
	private Jobs jobs;
	private boolean active;
	private double balance;
	private Mage mage;
	private Faction faction;
	private int id;

	public Faction getFaction() {
		return faction;
	}

	public void setFaction(Faction faction) {
		this.faction = faction;
	}

	public User(int id, UUID uuid,String name,double balance, boolean active,int factionid){
		this.id = id;
		this.uuid = uuid;
		this.name = name;
		this.balance=balance;
		this.active=active;
		this.moderation = Moderation.load(uuid);
		this.jobs = Jobs.load(uuid);
		this.mage = Mage.load(uuid);
		this.faction = Faction.get(factionid);
		playerlist.add(this);
	}
	// My SQL statements
	
		public Mage getMage() {
		return mage;
	}

		@SuppressWarnings("unused")
		private static User load(UUID uuid) {
			ResultSet r = MySQLSaver.mysql_query("SELECT * FROM `user` WHERE `user`.`id` = '"+uuid.toString()+"';",true);
			if (r==null){
				Main.log("Player not found, creating new instance");
				return null;
			}
			try {
				return new User(r.getInt("id"),UUID.fromString(r.getString("uuid")),r.getString("username"),r.getInt("balance"),r.getBoolean("actif"),r.getInt("faction"));
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return null;
		}

		public void sqlInsert() {
			MySQLSaver.mysql_update("INSERT INTO `maxcraft`.`player` (`id`, `name`, `balance`, `actif`) VALUES ('"+this.uuid+"', '"+this.name+"', '"+this.balance+"', "+this.active+");");
			
		}

		public static void loadActive() {
			ResultSet r = MySQLSaver.mysql_query("SELECT * FROM `user` WHERE `user`.`actif` = '1';",false);
			try {
			while (r.next()){
				UUID uuid = UUID.fromString(r.getString("uuid"));
				new User(r.getInt("id"),uuid,r.getString("username"),r.getInt("balance"),r.getBoolean("actif"),r.getInt("faction"));
			}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			Main.log("Joueurs charg�s ("+playerlist.size()+" joueurs).");
		}
		public void sendMessage(String string){
			if (this.getPlayer()!=null)
				this.getPlayer().sendMessage(string);
		}
		public void sendNotifMessage(String string){
			if (this.getPlayer()!=null){
				PacketsUtil.sendActionBar(this.getPlayer(), string);
				this.getPlayer().playSound(this.getPlayer().getLocation(), Sound.NOTE_PIANO, 20f , 1.5f);
				this.getPlayer().sendMessage(string);
			}
		}

		public void sendNotifMessage(TextComponent message) {
			if (this.getPlayer()!=null){
				PacketsUtil.sendActionBar(this.getPlayer(), message.getText());
				this.getPlayer().playSound(this.getPlayer().getLocation(), Sound.NOTE_PIANO, 20f , 1.5f);
				this.getPlayer().spigot().sendMessage(message);
			}
		}

		public Player getPlayer() {
			for (Player p : Bukkit.getOnlinePlayers())
				if (p.getUniqueId().equals(this.getUuid()))
					return p;
			return null;
		}

		public static User get(String string) {
			for (User p : playerlist)
				if (p.name.equals(string))
					return p;
			return null;
		}

		public static User get(UUID uuid) {
			if (uuid!=null)
				for (User p : playerlist)
					if (p.uuid.equals(uuid))
						return p;
			return null;
		}

		public static User get(Player pl) {
			for (User p : playerlist)
				if (p.getUuid().equals(pl.getUniqueId()))
					return p;
			return null;
		}

		public static User get(int id) {
			for (User p : playerlist)
				if (p.id==id)
					return p;
			return null;
		}
		public Jobs getJob() {
			return jobs;
		}

		public void setJob(Jobs jobs) {
			this.jobs = jobs;
		}

		public UUID getUuid() {
			return uuid;
		}

		public String getName() {
			return name;
		}

		public Moderation getModeration() {
			return moderation;
		}

		public boolean isActive() {
			return active;
		}
		
		public boolean isConnect(){
			if (this.getPlayer()==null)
				return false;
			return true;
		}

		@Override
		public boolean take(double d) {
			if (this.balance<d){
				this.sendNotifMessage("Vous n'avez pas assez d'argent.");
				return false;
			}
			this.balance-=d;
			this.sendNotifMessage("Vous avez payer "+d+" POs.");
			return true;
		}

		@Override
		public double getBalance() {
			return this.balance;
		}

		@Override
		public void give(double d) {
			this.balance+=d;
			this.sendNotifMessage("Vous avez recu "+d+" POs.");
			
		}

		@Override
		public boolean pay(double d, Account to) {
			if (this.take(d))
				return false;
			to.give(d);
			return true;
		}
}
