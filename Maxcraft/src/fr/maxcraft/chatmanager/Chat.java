package fr.maxcraft.chatmanager;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class Chat {

	public Chat(AsyncPlayerChatEvent e) {
		for (Player p : Bukkit.getOnlinePlayers()){
			TextComponent player = new TextComponent(e.getPlayer().getDisplayName() +ChatColor.WHITE+" : ");
			player.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Cliquez pour ajouter ce joueur en ami.").color(ChatColor.BLUE).create() ));
			player.setClickEvent( new ClickEvent( ClickEvent.Action.RUN_COMMAND, "/friends add "+e.getPlayer().getName()));
			TextComponent message = new TextComponent(e.getMessage());
			message.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Cliquez pour repondre en priv�.").color(ChatColor.BLUE).create() ));
			message.setClickEvent( new ClickEvent( ClickEvent.Action.SUGGEST_COMMAND, "/msg "+e.getPlayer().getName()));
			player.addExtra(message);
			p.spigot().sendMessage(player);
		}
	}

}
