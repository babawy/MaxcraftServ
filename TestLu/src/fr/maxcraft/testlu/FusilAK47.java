package fr.maxcraft.testlu;

import org.bukkit.ChatColor;

import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Projectile;


public class FusilAK47 extends Fusil{

	public FusilAK47(int id, int ammoAmount) {
		super(id, ammoAmount);
		
	}

	@Override
	public int getBulletMax() {
		return 31;
	}

	@Override
	public int getAmmoMax() {
		return 5;
	}

	@Override
	public int getReloadDelay() {
		return 3*20;
	}

	@Override
	public Class<? extends Projectile> getProjectile() {
		return Arrow.class;
	}

	@Override
	public double getPower() {
		return 2.0;
	}

	@Override
	public int getFireRate() {
		return 2;
	}

	

	@Override
	public ChatColor getNameColor() {
		return ChatColor.RED;
	}

	@Override
	public String getName() {
		return "AK47";
	}

	@Override
	public Material getItemMaterial() {
		return Material.STICK;
	}

}
