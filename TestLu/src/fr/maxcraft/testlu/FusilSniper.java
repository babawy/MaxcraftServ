package fr.maxcraft.testlu;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Projectile;

public class FusilSniper extends Fusil{

	public FusilSniper(int id, int ammoAmount) {
		super(id, ammoAmount);
	}

	@Override
	public int getBulletMax() {
		return 1;
	}

	@Override
	public int getAmmoMax() {
		return 30;
	}

	@Override
	public int getReloadDelay() {
		return 2*20;
	}

	@Override
	public Class<? extends Projectile> getProjectile() {
		return Arrow.class;
	}

	@Override
	public double getPower() {
		return 20.0;
	}

	@Override
	public int getFireRate() {
		return 20;
	}

	@Override
	public ChatColor getNameColor() {
		return ChatColor.YELLOW;
	}

	@Override
	public String getName() {
		return "Sniper";
	}

	@Override
	public Material getItemMaterial() {
		return Material.FISHING_ROD;
	}

}
