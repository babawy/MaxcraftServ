package fr.maxcraft.testlu;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerEditBookEvent;

public class TestLuListener implements Listener{
	

	@EventHandler(priority= EventPriority.NORMAL)
	public void onPlayerEditBook(PlayerEditBookEvent event){
		if (!(event.getPlayer() instanceof Player)) return;
		if (event.isSigning()){
			if (event.getPreviousBookMeta().getDisplayName().equals(NewsManager.givenEditableTitle) && event.getPreviousBookMeta().getLore().equals(NewsManager.getInstance().returnGivenLore())) {
				if (event.getPlayer().hasPermission("testlu.news.admin")){
					NewsManager.getInstance().setNewsText(event.getNewBookMeta().getPages());
					TestLu.getInstance().getLogger().info(NewsManager.getInstance().getNewsText().toString());
					event.getPlayer().getInventory().remove(event.getPlayer().getItemInHand());
					event.getPlayer().sendMessage(ChatColor.GRAY + "" + ChatColor.ITALIC + "Infos modifiées !");
					Bukkit.broadcastMessage(ChatColor.GREEN + "Nouvelles infos du Serveur disponibles !");
					return;
				}
				else {
					event.setCancelled(true);
					event.getPlayer().sendMessage(ChatColor.RED + "Vous ne pouvez pas faire ça !");
					return;
				}											
		    }
		}
	}
}
