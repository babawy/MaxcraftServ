package fr.maxcraft.testlu;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.ItemMeta;


public class NewsManager {
	
	
//VARIABLES	
	private static NewsManager instance;
	public static NewsManager getInstance(){
		return NewsManager.instance;
	}
	
	public static String givenEditableTitle = new String(ChatColor.AQUA + "" + ChatColor.ITALIC + "News du Staff, éditer");
	public static String givenLore = new String(ChatColor.GREEN + "" + ChatColor.ITALIC + "News du Staff");
	
	
	
	
	private String newsTitle;
	private String newsAuthor;
	private List<String> newsText;      
	private ArrayList<String> newsBookLore;
	
	private BookMeta newsBookMeta;
	
	
	
//CONTRUCTEUR	
	public NewsManager() {	
		NewsManager.instance = this;
		newsTitle = ChatColor.AQUA + "Infos Serveur";
		newsAuthor = ChatColor.YELLOW + "Maxcraft";
		this.newsText = new ArrayList<>();
	}



//METHODES	
	
	
	
	public List<String> getNewsText() {
		return newsText;
	}



	public BookMeta getNewsBookMeta() {
		return newsBookMeta;
	}



	public void setNewsBookMeta(BookMeta newsBookMeta) {
		this.newsBookMeta = newsBookMeta;
	}



	public void setNewsText(List<String> newsText) {
		this.newsText = newsText;
	}
	
	public ArrayList<String> returnGivenLore(){
		ArrayList<String> lore = new ArrayList<>();
		lore.add(NewsManager.givenLore);
		return lore;		
	}
	
	/*public String returnNewsText(){
		for (int i=0; i < this.newsText.size(); i ++){
			
		}
		return text;
		
	}*/
	
	



	public void onNewsCommand(CommandSender sender, String[] args){
		if (args.length < 1){
			sender.sendMessage(ChatColor.GRAY + "Il manque des paramètres...");
			return;
		}
		if (!(sender instanceof Player)){
			sender.sendMessage("Vous devez être un joueur pour exécuter cette commande");
			return;
		}
		Player p = (Player)sender;
		switch(args[0]) {
		case "read" :
				if (p.getInventory().firstEmpty() == -1) {
					p.sendMessage(ChatColor.GRAY + "Inventaire plein");
					return;
				}
				p.sendMessage(this.getNewsText().toString());
				p.getInventory().addItem(this.getReadableBook());
				break;
				
		case "create" :
			if (!(p.hasPermission("testlu.news.admin"))) {
				p.sendMessage(ChatColor.RED + "Vous n'avez pas la permission d'utiliser cette commande !");
				return;
			}
			if (p.getInventory().firstEmpty() == -1) {
				p.sendMessage(ChatColor.AQUA + "Inventaire plein...");
				return;
			}
			p.getInventory().addItem(this.getEditableBook());
			break;
		}
		
	}
	

	



	private ItemStack getEditableBook() {
		ItemStack item = new ItemStack(Material.BOOK_AND_QUILL, 1);
		BookMeta itemMeta = (BookMeta) item.getItemMeta();
		String title = NewsManager.givenEditableTitle;;
		ArrayList<String> lore = new ArrayList<>();
		lore.add(NewsManager.givenLore);
		itemMeta.setDisplayName(title);
		itemMeta.setLore(lore);
		itemMeta.addPage("");
		item.setItemMeta(itemMeta);
		return item;		
	}
	
	private ItemStack getReadableBook() {	
		ItemStack item = new ItemStack(Material.WRITTEN_BOOK, 1);
		BookMeta itemMeta = (BookMeta) item.getItemMeta();
		String title = this.newsTitle;
		ArrayList<String> lore = new ArrayList<>();
		lore.addAll(this.returnGivenLore()); 
		String author = this.newsAuthor;
		itemMeta.setTitle(title);
		itemMeta.setLore(lore);  
		itemMeta.setAuthor(author);
		/*ArrayList<String> test = new ArrayList<>();
		test.add("Bonjour un deux trois, test!! 637657");
		test.add("autre truc ®ê‘Ú®†î");
		itemMeta.setPages(test);*/
		itemMeta.setPages(this.getNewsText());	
		item.setItemMeta(itemMeta);
		return item;
	}
	
	
	
	public void saveConfig() {
		File config = new File("plugins/"+ TestLu.getInstance().getName() +"/news.yml"); 
	}
	
	
}