package fr.maxcraft.testlu;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class TestLu extends JavaPlugin{
	
	private static TestLu instance;
	
	public static TestLu getInstance() {
		return TestLu.instance;
	}
	
	

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		switch(command.getName()){
		
		case "fusil": 
			this.onFusilCommand(sender, args);
			break;
		
		case "news":
			NewsManager.getInstance().onNewsCommand(sender, args);
			break;
		}
		return true;
	}

	@Override
	public void onDisable() {
		
	}

	@Override
	public void onEnable() {
		TestLu.instance = this;
		NewsManager newsManager = new NewsManager();
		this.getServer().getPluginManager().registerEvents(new FusilListener(), this);
		this.getServer().getPluginManager().registerEvents(new TestLuListener(), this);
		this.getLogger().info("Chargé !");
	}

	
	
	
	public void onFusilCommand(CommandSender sender, String[] args){
		if (args.length < 1) {
			sender.sendMessage(ChatColor.GRAY+"Veuillez préciser le type de Fusil souhaité !");
			return;
		}
		if (!(sender instanceof Player)) {
			sender.sendMessage("Vous devez être un joueur pour exécuter cette commande!");
			return;
		}
		Player p = (Player)sender;
		switch(args[0]) {
				
		case "ak47":
			FusilAK47 ak47 = new FusilAK47(Fusil.getNextId(), 5);
			ak47.giveWeapon(p);
			break;
			
		case "sniper":
			FusilSniper sniper = new FusilSniper(Fusil.getNextId(), 30);
			sniper.giveWeapon(p);
			break;
			
		case "uzi":
			FusilUZI uzi = new FusilUZI(Fusil.getNextId(), 19);
			uzi.giveWeapon(p);
			break;
		
		default : 
			p.sendMessage(ChatColor.RED+"Veuillez spécifier le type de fusil souhaité.");
			break;
		} 
	}
	
}
