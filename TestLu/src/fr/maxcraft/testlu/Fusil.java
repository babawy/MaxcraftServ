package fr.maxcraft.testlu;

import java.util.ArrayList;
import java.util.Date;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public abstract class Fusil {
	
	public static ArrayList<Fusil> fusils = new ArrayList<>();
	private static int lastId = 1;
	
	public static int getNextId(){
		Fusil.lastId ++;
		return Fusil.lastId;
	}
	
	
	protected int id;
	protected int bulletAmount;
	protected int ammoAmount;
	protected boolean isFiring;
	protected Date lastFire;
	
	
	public Fusil(int id, int ammoAmount) {
		this.id = id;
		this.ammoAmount = ammoAmount;
		this.bulletAmount = this.getBulletMax();
		this.isFiring = false;
		Fusil.fusils.add(this);
	}
	
	public abstract int getBulletMax();
	public abstract int getAmmoMax();
	public abstract int getReloadDelay();
	public abstract Class<? extends Projectile> getProjectile();
	public abstract double getPower();
	public abstract int getFireRate();
	public abstract ChatColor getNameColor();
	public abstract String getName();
	public abstract Material getItemMaterial();
	
	

	public int getBulletAmount() {
		return bulletAmount;
	}

	public int getAmmoAmount() {
		return ammoAmount;
	}

	public boolean isFiring() {
		return isFiring;
	}

	public Date getLastFire() {
		return lastFire;
	}
	
	public void setFiring(boolean isFiring) {
		this.isFiring = isFiring;
	}

	public void reloadWeapon(){
		if (this.getAmmoAmount() < 1) return;
		this.bulletAmount = this.getBulletMax();
		this.ammoAmount--;
	}
	
	
	public void startReloading(Player p){
		new ReloadTask(this, p).runTaskLater(TestLu.getInstance(), this.getReloadDelay());
	}
	
	
	
	public boolean hasWeaponInHand(Player p){  
		ItemStack inHand = p.getItemInHand();
		if (inHand == null) return false;
		if (!inHand.equals(this.getItem())) return false;
		return true; 
	}
	
	
	public void startFiring(Player p){
		new FireTask(this, p).runTask(TestLu.getInstance());
		
	}
	
	public void consumeAmmo(){
		if (this.bulletAmount >= 1) this.bulletAmount --;
	}
	
	public void fire(Player p){
		if (!this.hasWeaponInHand(p)) return;
		if (this.getBulletAmount() < 1) return;
		Vector v =  p.getLocation().getDirection();
		v.multiply(this.getPower());
		this.consumeAmmo();
		p.launchProjectile(this.getProjectile(), v);
	}
	
	public ItemStack getItem() {
		ItemStack item = new ItemStack(this.getItemMaterial(), 1);
		ItemMeta itemMeta = item.getItemMeta();
		itemMeta.setDisplayName(this.getNameColor() + this.getName());
		ArrayList<String> lore = new ArrayList<>();
		lore.add(""+this.id);
		itemMeta.setLore(lore);
		item.setItemMeta(itemMeta);
		return item;
	}
	
	public void giveWeapon(Player p){
		p.getInventory().addItem(this.getItem());
	}
	
	
	
	
	
//classes	
	private class ReloadTask extends BukkitRunnable{
		
		private Fusil arme;
		
		private Player player;

		public ReloadTask(Fusil arme, Player p) {
			this.arme = arme;
			this.player = p;
		}



		@Override
		public void run() {
			this.arme.reloadWeapon();
			player.sendMessage(ChatColor.GRAY+"Arme rechargée !");
		}
		
		
		
	}
	
	private class FireTask extends BukkitRunnable{	//faire tirer par un joueur avec une arme
		
		private Fusil arme;
		private Player player;
		
		

		public FireTask(Fusil arme, Player player) {
			this.arme = arme;
			this.player = player;
		}



		@Override
		public void run() {	
			if (arme.getLastFire() != null && new Date().getTime() < arme.getLastFire().getTime() + (arme.getFireRate()/20)*1000) return;
			arme.fire(this.player);
			if (arme.isFiring()) {				
				new FireTask(this.arme, this.player).runTaskLater(TestLu.getInstance(), arme.getFireRate());
			}
			
		}
		
	}
	
}

