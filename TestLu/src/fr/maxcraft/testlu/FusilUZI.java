package fr.maxcraft.testlu;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Projectile;

public class FusilUZI extends Fusil{

	public FusilUZI(int id, int ammoAmount) {
		super(id, ammoAmount);
	}

	@Override
	public int getBulletMax() {
		return 10;
	}

	@Override
	public int getAmmoMax() {
		return 20;
	}

	@Override
	public int getReloadDelay() {
		return 20*5;
	}

	@Override
	public Class<? extends Projectile> getProjectile() {
		return Arrow.class;
	}

	@Override
	public double getPower() {
		return 6;
	}

	@Override
	public int getFireRate() {
		return 15;
	}

	@Override
	public ChatColor getNameColor() {
		return ChatColor.GREEN;
	}

	@Override
	public String getName() {
		return "UZI";
	}

	@Override
	public Material getItemMaterial() {
		return Material.FLINT;
	}

	
}
