package fr.maxcraft.testlu;

 
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;


public class FusilListener implements Listener{
	
	@EventHandler(priority= EventPriority.NORMAL)
	public void onToggleSneak(PlayerToggleSneakEvent event){
		for (Fusil fusil : Fusil.fusils){
			if (!fusil.hasWeaponInHand(event.getPlayer())) continue;
			if (event.isSneaking()){
				fusil.setFiring(true);
				fusil.startFiring(event.getPlayer());
			}
			else{
				fusil.setFiring(false);
			} return;
		}
		
	}
	
	@EventHandler(priority= EventPriority.NORMAL)
	public void onPlayerInteract(PlayerInteractEvent event){
		if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) return;
		for (Fusil fusil : Fusil.fusils){
			if (!fusil.hasWeaponInHand(event.getPlayer())) continue;
			if (fusil.getAmmoAmount() < 1) {
				event.getPlayer().sendMessage(ChatColor.GRAY+"Plus de munitions...");
				return;
			}
			fusil.startReloading(event.getPlayer());
			event.getPlayer().sendMessage(ChatColor.GRAY+"Rechargement en cours...");
			return;
		
		}    
	}

}
