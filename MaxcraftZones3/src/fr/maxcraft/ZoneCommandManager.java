package fr.maxcraft;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class ZoneCommandManager {
	private MaxcraftZones plugin;
	
	public ZoneCommandManager(MaxcraftZones plugin)
	{
		this.plugin = plugin;
	}
	
	public void command(CommandSender sender, String[] args)
	{
		if(args.length >= 1)
		{
		
			switch(args[0])
			{
		
			case "infos": case "info": case "i": case "information": case "informations":
				if(args.length == 1) zoneInfo(sender);
				else zoneInfo(sender, args[1]);
				return;
			case "tp":
				tp(sender, args[1]);
				return;
			case "inactive": case "in": case "dead": case "d":
				inactiveList(sender);
				return;
			case "new": case "n": case "add": case "create": case "nouvelle":
				this.createZone(sender, args);
				return;
			case "remove": case "supprimer": case "r": case "delete": case "del":
				this.removeZone(sender, args[1]);
				return;
			case "resize": case "rs": case "s": case "redimensionner":
				this.resizeZone(sender, args[1]);
				return;
			case "reload": case "load": case "l": case "rl": case "recharger":
				this.loadZone(sender, args[1]);
				return;
			case "set":
				this.set(sender, args);
				return;
			case "addtag": case "tag": case "newtag":
				this.addTag(sender, args[1], args[2]);
				return;
			case "removetag": case "deletetag":
				this.removeTag(sender, args[1], args[2]);
				return;
			case "reset": case "reinitialiser":
				this.reset(sender, args[1]);
				return;
			default:
				sender.sendMessage(this.plugin.message("Il manque des paramètres..."));
				return;
		
			}
		}
		
		sender.sendMessage(this.plugin.message("Il manque des paramètres..."));
	}
	
	
	public void createZone(CommandSender sender, String[] args)
	{
		Player p = (Player) sender;
		
		Maxcraftien m = this.plugin.MM.getMaxcraftien(p);
		
		if(m == null) return;
		
		
		if(!this.plugin.isSelectionValid(m, true))
		{
			return;
		}
		
		//Detection de zone parente 
		
		Zone parent = this.plugin.ZoneManager.findParentZone(this.plugin.ZoneManager.selections.get(m));
					
		if(args.length == 1)
		{
			return;
		}
		Zone zone = null;
		String name = this.plugin.multiArgs(args, 1);
		
		int id = this.plugin.MM.getNextId("zone");
		while(this.plugin.getZone(id) != null)
		{
			id++;
		}
		
		zone = new Zone(this.plugin, id, this.plugin.ZoneManager.selections.get(m).clone(), m, name,null);

		//parent set
		zone.setParent(parent);
		if (parent!=null)
			zone.setOwner(parent.getOwner());
		
		//SQL 
		
		Object ownerid = null;
		Object parentid = null;
		
		if(zone.hasOwner())
		{
			ownerid = zone.getOwner().getId();
		}
		else
		{
			ownerid = "NULL";
		}
		
		if(zone.hasParentZone())
		{
			parentid = zone.getParent().getId();
		}
		else
		{
			parentid = "NULL";
		}
		
	
		this.plugin.MM.mysql_update("INSERT INTO `zone` (`id`, `user`, `name`, `points`, `world`, `parent`, tags, shopDemand) VALUES (NULL, "+ownerid+", '"+ zone.getName() +"', 'WAITING', 'WAITING', "+parentid+", NULL, 0);");
		
		zone.saveInSQL();
		zone.createDynmap();
		this.plugin.ZoneManager.selections.get(m).points.clear();
		
		p.sendMessage(this.plugin.message("La zone <"+ ChatColor.GOLD+ zone.getName() + ChatColor.GRAY +"> à été créée !"));
		
	}
	
	public void zoneInfo(CommandSender sender, String idstr)
	{
		if(!this.plugin.MM.isInt(idstr))
		{
			sender.sendMessage(this.plugin.message("Ce n'est pas une zone !"));
			return;
		}
		
		int id = Integer.parseInt(idstr);
		
		Zone zone = this.plugin.ZoneManager.getZone(id);
		if(zone == null)
		{
			sender.sendMessage(this.plugin.message("Ce n'est pas une zone !"));
			return;
		}
		
		sender.sendMessage("");
		sender.sendMessage(this.plugin.message("*** <"+ChatColor.GOLD+ zone.getName() +ChatColor.GRAY +"> ***"));
		sender.sendMessage(this.plugin.message("Numéro : "+ ChatColor.GREEN + zone.getId()));
		sender.sendMessage(this.plugin.message("Surface : "+ ChatColor.GREEN + zone.getSelection().getArea()+ " m² (sans la moitié bordures)"));
		if (zone.getBailleur()==null)
			if(zone.hasOwner())
			{
				sender.sendMessage(this.plugin.message("Propriétaire : "+ ChatColor.GREEN + zone.getOwner().getUserName()));
			}
			else
			{
				sender.sendMessage(this.plugin.message("Propriétaire : "+ ChatColor.RED + "Aucun"));	
			}
		else
		{
			sender.sendMessage(this.plugin.message("Proprietaire : "+ ChatColor.GREEN + zone.getBailleur().getUserName()));
			sender.sendMessage(this.plugin.message("Locataire : "+ ChatColor.GREEN + zone.getOwner().getUserName()));
		}
		
		
		if(zone.getParent() != null)
		{
		sender.sendMessage(this.plugin.message("Localisation : "+ ChatColor.GREEN + zone.getLocalisation()));
		}
		
		sender.sendMessage(this.plugin.message("Cuboiders : "+ ChatColor.GREEN + zone.displayCuboiderList()));
		sender.sendMessage(this.plugin.message("Builders : "+ ChatColor.GREEN + zone.displayBuilderList()));
		
		if(zone.isToSell())
		{
		sender.sendMessage(this.plugin.message("A vendre : "+ ChatColor.GOLD + zone.getVente().getPrice() + " POs"));
		}
		
		if(zone.hasOwner() && zone.getOwner().isActif())
		{
			sender.sendMessage(this.plugin.message("Actif : "+ ChatColor.GREEN + "Oui"));
		}
		else if(zone.hasOwner() && ! zone.getOwner().isActif())
		{
			sender.sendMessage(this.plugin.message("Actif : "+ ChatColor.RED + "Non"));
		}
		
		if(!zone.getTags().isEmpty())
		{
		sender.sendMessage(this.plugin.message("Tags : "+ ChatColor.GREEN + zone.displayTags()));
		}
		
	}
	
	public void zoneInfo(CommandSender sender)
	{
		Player p = (Player) sender;
		Zone zone = this.plugin.ZoneManager.getZone(p.getLocation());
		if(zone == null)
		{
			sender.sendMessage(this.plugin.message("Vous n'êtes pas sur une zone !"));
			return;
		}
		
		this.zoneInfo(sender, ""+zone.getId());
	}
	
	public void loadZone(CommandSender sender, String idstr)
	{
		if(!this.plugin.MM.isInt(idstr))
		{
			sender.sendMessage(this.plugin.message("Ce n'est pas une zone !"));
			return;
		}
		
		int id = Integer.parseInt(idstr);
		
		Zone zone = this.plugin.ZoneManager.getZone(id);
		if(zone == null)
		{
			sender.sendMessage(this.plugin.message("Cette zone n'existe pas !"));
			return;
		}
		
		try {
			zone.reloadFromSql();
			zone.reloadZone();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.plugin.log("La zone <"+zone.getName()+"> à été rechargée !");
	}
	
	public void resizeZone(CommandSender sender, String idstr)
	{

		if(!this.plugin.MM.isInt(idstr))
		{
			sender.sendMessage(this.plugin.message("Ce n'est pas une zone !"));
			return;
		}
		
		int id = Integer.parseInt(idstr);
		
		Zone zone = this.plugin.ZoneManager.getZone(id);
		if(zone == null)
		{
			sender.sendMessage(this.plugin.message("Cette zone n'existe pas !"));
			return;
		}
		
		Maxcraftien m = this.plugin.MM.getMaxcraftien((Player) sender);
		Player p = (Player) sender;
		
		//Faire disparaitre la zone pour eviter superposition
		this.plugin.ZoneManager.listZones.remove(zone);
		
		if(!this.plugin.isSelectionValid(m, true))
		{
			this.plugin.ZoneManager.listZones.add(zone);
			return;
		}
		
	
		
		//Faire r�aparaitre la zone pour eviter superposition
		this.plugin.ZoneManager.listZones.add(zone);
		
		if(zone.canAdmin(p))
		{
		zone.setSelection(this.plugin.ZoneManager.selections.get(m).clone());
		zone.saveInSQL();
		zone.reloadZone();
		
		sender.sendMessage(this.plugin.message("La zone "+ ChatColor.GOLD +"<"+zone.getName()+">"+ChatColor.GRAY+" à été redimensionnée !"));
		return;
	}
		else
		{
			sender.sendMessage(this.plugin.message("Vous n'avez pas le droit de redimensionner cette zone !"));
			return;
		}
		
	}
	
	public void removeZone(CommandSender sender, String idstr)
	{

		if(!this.plugin.MM.isInt(idstr))
		{
			sender.sendMessage(this.plugin.message("Ce n'est pas une zone !"));
			return;
		}
		
		int id = Integer.parseInt(idstr);
		
		Zone zone = this.plugin.ZoneManager.getZone(id);
		if(zone == null)
		{
			sender.sendMessage(this.plugin.message("Cette zone n'existe pas !"));
			return;
		}
		
		Player p = (Player) sender;
		
		if(zone.canAdmin(p))
		{
			
		for(Zone fille : this.plugin.ZoneManager.listZones)
		{
			if(fille.getParent() == zone)
			{
				sender.sendMessage(this.plugin.message("Vous ne pouvez pas supprimer cette zone, la zone #"+ fille.getId()+" est une zone fille."));
				return;
			}
		}
		
		if(zone.isToSell())
		{
			zone.getVente().remove();
		}
	
		zone.remove();
		
		sender.sendMessage(this.plugin.message("La zone "+ ChatColor.GOLD +"<"+zone.getName()+">"+ChatColor.GRAY+" à été supprimée !"));
		return;
	}
		else
		{
			sender.sendMessage(this.plugin.message("Vous n'avez pas le droit de supprimer cette zone !"));
			return;
		}
		
	}
	
	public void set(CommandSender sender, String[] args)
	{
		if(!this.plugin.MM.isInt(args[1]))
		{
			sender.sendMessage(this.plugin.message("Ce n'est pas une zone !"));
			return;
		}
		
		int id = Integer.parseInt(args[1]);
		
		Zone zone = this.plugin.ZoneManager.getZone(id);
		if(zone == null)
		{
			sender.sendMessage(this.plugin.message("Cette zone n'existe pas !"));
			return;
		}
		
		Player p = (Player) sender;
		
		
		switch(args[2])
		{
		
	case "title": case "name": case "nom": case "titre": case "t":
			
		if(!p.hasPermission("maxcraft.admin.zone"))
		{
			sender.sendMessage(this.plugin.message("Vous ne pouvez pas changer le nom de cette zone !"));
			return;
		}
		
		zone.setName(this.plugin.multiArgs(args, 3));
		zone.saveInSQL();
		zone.reloadDynmap();
		sender.sendMessage(this.plugin.message("Le nom de la zone à été modifié."));
		return;
			
	case "id":
				
		if(!p.hasPermission("maxcraft.admin.zone"))
		{
			sender.sendMessage(this.plugin.message("Vous ne pouvez pas changer l'id de cette zone !"));
			return;
		}
		if(!this.plugin.MM.isInt(args[3]))
		{
			sender.sendMessage(this.plugin.message("Ce n'est pas une ID valide !"));
			return;
		}
		Zone zone2 = this.plugin.ZoneManager.getZone(Integer.parseInt(args[3]));
		if(zone2 != null)
		{
			sender.sendMessage(this.plugin.message("Cette zone existe deja !"));
			return;
		}
		zone.setId(Integer.parseInt(args[3]));
		zone.saveInSQL();
		zone.reloadDynmap();
		sender.sendMessage(this.plugin.message("L'ID de la zone � �t� modifi�."));
		return;
				
	case "parent": case "p": case "mere":
		
		if(! args[3].equalsIgnoreCase("null"))
		{
		if(!this.plugin.MM.isInt(args[3]))
		{
			sender.sendMessage(this.plugin.message("Ce n'est pas une zone !"));
			return;
		}
		
		int idparent = Integer.parseInt(args[3]);
		
		Zone parent = this.plugin.ZoneManager.getZone(idparent);
		if(parent == null)
		{
			sender.sendMessage(this.plugin.message("Cette zone n'existe pas !"));
			return;
		}
		
		
		
			if(!(zone.canAdmin(p) && parent.canAdmin(p)))
			{
				sender.sendMessage(this.plugin.message("Vous devez pouvoir administrer la zone parente et la zone fille pour cela !"));
				return;
			}
			
			if(zone == parent)
			{
				sender.sendMessage(this.plugin.message("C'est pas possible ca du con, on est pas dans inception !"));
				return;
			}
			
			zone.setParent(parent);
		}
		
		if(args[3].equalsIgnoreCase("null"))
		{
			zone.setParent(null);
		}

		zone.saveInSQL();
		zone.reloadDynmap();
		sender.sendMessage(this.plugin.message("La parentée de la zone a été modifiée."));
		return;
		
			
		default:
			sender.sendMessage(this.plugin.message("Ce set n'existe pas !"));
			return;
			
		}
	}
	
	public void tp(CommandSender sender, String idstr)
	{
		if(!this.plugin.MM.isInt(idstr))
		{
			sender.sendMessage(this.plugin.message("Ce n'est pas une zone !"));
			return;
		}
		
		int id = Integer.parseInt(idstr);
		
		Zone zone = this.plugin.ZoneManager.getZone(id);
		if(zone == null)
		{
			sender.sendMessage(this.plugin.message("Cette zone n'existe pas !"));
			return;
		}
		
		Player p = (Player) sender;
		if(!p.hasPermission("maxcraft.guide"))
		{
			sender.sendMessage(this.plugin.message("Vous n'avez pas accès à cette commande !"));
			return;
		}
		int x=0;
		int z=0;
		for(Location l : zone.getSelection().points){
			x=x+(l.getBlockX());
			z=z+(l.getBlockZ());
		}
		Location loc = p.getLocation();	
		loc.setX(x/zone.getSelection().points.size());
		loc.setZ(z/zone.getSelection().points.size());
		loc = loc.getWorld().getHighestBlockAt(loc).getLocation();
		
		p.teleport(loc);
		sender.sendMessage(this.plugin.message("Téléportation vers la zone <"+zone.getName()+"> ..."));
		return;
	}
	
	public void inactiveList(CommandSender sender)
	{
		Player p = (Player) sender;
		if(!p.hasPermission("maxcraft.guide"))
		{
			sender.sendMessage(this.plugin.message("Vous n'avez pas accès à cette commande !"));
			return;
		}
		Zone zone = this.plugin.ZoneManager.getZone(p.getLocation());
		sender.sendMessage(this.plugin.message("LISTE DES PARCELLES MORTES SUR "+ zone.getName()));
		for (Zone z : this.plugin.ZoneManager.listZones)
			if (z.hasOwner())
				if (z.isChildOf(zone) && !z.getOwner().isActif() && !z.isToSell())
						sender.sendMessage(this.plugin.message(ChatColor.GOLD + "#" +z.getId()+ ChatColor.WHITE + " " + z.getName()));

	}
	
	public void addTag(CommandSender sender,String idstr, String tag)
	{
	
		Player p = (Player) sender;
		if(!p.hasPermission("maxcraft.admin.zone"))
		{
			sender.sendMessage(this.plugin.message("Vous ne pouvez pas changer le nom de cette zone !"));
			return;
		}
		

		if(!this.plugin.MM.isInt(idstr))
		{
			sender.sendMessage(this.plugin.message("Ce n'est pas une zone !"));
			return;
		}
		
		int id = Integer.parseInt(idstr);
		
		Zone zone = this.plugin.ZoneManager.getZone(id);
		if(zone == null)
		{
			sender.sendMessage(this.plugin.message("Cette zone n'existe pas !"));
			return;
		}
		
		if(zone.hasTag(tag)&&!tag.equals("enter-message"))
		{
			sender.sendMessage(this.plugin.message("Cette zone possède déjà ce tag !"));
			return;
		}
		
		zone.getTags().add(tag);
		zone.saveInSQL();
		zone.reloadDynmap();
		
		sender.sendMessage(this.plugin.message("Le tag <"+tag+"> à été ajouté !"));
		return;
		
	}
	
	public void removeTag(CommandSender sender,String idstr, String tag)
	{
	
		Player p = (Player) sender;
		if(!p.hasPermission("maxcraft.admin.zone"))
		{
			sender.sendMessage(this.plugin.message("Vous ne pouvez pas changer le nom de cette zone !"));
			return;
		}
		

		if(!this.plugin.MM.isInt(idstr))
		{
			sender.sendMessage(this.plugin.message("Ce n'est pas une zone !"));
			return;
		}
		
		int id = Integer.parseInt(idstr);
		
		Zone zone = this.plugin.ZoneManager.getZone(id);
		if(zone == null)
		{
			sender.sendMessage(this.plugin.message("Cette zone n'existe pas !"));
			return;
		}
		
		if(! zone.hasTag(tag))
		{
			sender.sendMessage(this.plugin.message("Cette zone ne possède pas ce tag !"));
			return;
		}
		
		zone.getTags().remove(tag);
		zone.saveInSQL();
		zone.reloadDynmap();
		sender.sendMessage(this.plugin.message("Le tag <"+tag+"> à été retiré !"));
		return;
		
	}
	
	public void reset(CommandSender sender, String idstr)
	{
	
		Player p = (Player) sender;
		
		if(!this.plugin.MM.isInt(idstr))
		{
			sender.sendMessage(this.plugin.message("Ce n'est pas une zone !"));
			return;
		}
		
		int id = Integer.parseInt(idstr);
		
		Zone zone = this.plugin.ZoneManager.getZone(id);
		if(zone == null)
		{
			sender.sendMessage(this.plugin.message("Cette zone n'existe pas !"));
			return;
		}
		
		if(!zone.canAdmin(p))
		{
			sender.sendMessage(this.plugin.message("Vous ne pouvez pas reset cette zone !"));
			return;
		}
		
		
		if(zone.isToSell())
		{
			zone.getVente().remove();
		}
		
		zone.setOwner(null);
		zone.getBuilders().clear();
		zone.getCuboiders().clear();
		zone.removeUsersFromSql();
		zone.saveInSQL();
		zone.reloadDynmap();
		
		sender.sendMessage(this.plugin.message("La zone a été réinitialisée !"));
		return;
		
	}
}
