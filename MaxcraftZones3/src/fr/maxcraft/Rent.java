package fr.maxcraft;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.bukkit.Bukkit;

public class Rent {

	private MaxcraftZones plugin;
	private int id;
	private Zone zone;
	private Maxcraftien owner;
	private Maxcraftien locataire;
	private int prix;
	private String dur�e;
	private Date last;

	public Rent(MaxcraftZones plugin, int id, Zone zone, Maxcraftien owner,
			Maxcraftien locataire, int prix, String dur�e, Date date) {
		this.plugin = plugin;
		this.id = id;
		this.zone = zone;
		this.owner = owner;
		this.locataire = locataire;
		this.prix = prix;
		this.dur�e = dur�e;
		this.last = date;
	}




	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public Zone getZone() {
		return zone;
	}


	public void setZone(Zone zone) {
		this.zone = zone;
	}


	public Maxcraftien getOwner() {
		return owner;
	}


	public void setOwner(Maxcraftien owner) {
		this.owner = owner;
	}


	public Maxcraftien getLocataire() {
		return locataire;
	}


	public void setLocataire(Maxcraftien locataire) {
		this.locataire = locataire;
	}


	public int getPrix() {
		return prix;
	}


	public void setPrix(int prix) {
		this.prix = prix;
	}


	public String getDur�e() {
		return dur�e;
	}


	public void setDur�e(String dur�e) {
		this.dur�e = dur�e;
	}


	public String getLast() {	
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return df.format(last);
	}


	public void setLast(Date last) {
		this.last = last;
	}	
	public void remove()
	{
		this.plugin.MM.mysql_update("DELETE FROM `location` WHERE `location`.`id` = "+this.id);
		zone.setOwner(this.getOwner());
		zone.setBailleur(null);
		this.zone.saveInSQL();
		//this.zone.reloadDynmap();
		
	}


	public void create() {
		this.plugin.MM.mysql_update("INSERT INTO location (id, zone, owner, locataire, prix, dur�e, last) VALUES (NULL, "+this.zone.getId()+", "+this.getOwner().getId()+", "+this.getLocataire().getId()+", "+this.getPrix()+", '"+this.getDur�e()+"', '"+this.getLast()+"');");
		this.getZone().setBailleur(this.getOwner());
		this.getZone().setOwner(this.getLocataire());
		this.getZone().saveInSQL();
		//this.zone.reloadDynmap();
	}




	public void pay() {
		SimpleDateFormat df = new SimpleDateFormat("DD");
		if (!this.getLocataire().isActif())
			this.remove();
		else
		if (df.format(last)!=df.format(new Date()))
		{
			if(this.plugin.MM.economy.getBalance(this.getLocataire().getUserName())< (float) this.getPrix())
				this.remove();
			else{
				this.plugin.MM.economy.bankDeposit(this.getOwner().getUserName(), (float) this.getPrix());
				this.plugin.MM.economy.bankWithdraw(this.getLocataire().getUserName(), (float) this.getPrix());
			}
		}
		
	}
}
