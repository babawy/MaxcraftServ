package fr.maxcraft;

import java.awt.Point;
import java.awt.Polygon;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;


public class Selection {

	private MaxcraftZones plugin;
	public ArrayList<Location> points = null;
	public World world;
	
	public Selection(MaxcraftZones plugin)
	{
		this.plugin = plugin;
		this.points = new ArrayList<Location>();
	}
	public World getWorld(){
		return this.world;
	}
	public Polygon getPolygon()
	{
		Polygon p = new Polygon();
		
		for(Location l : this.points)
		{
			p.addPoint(l.getBlockX(), l.getBlockZ());
		}
		
		return p;
	}
	
	public boolean haveCollision()
	{
		
		for(Line2D line: this.getLines())
		{
			for(Zone z : this.plugin.ZoneManager.listZones)
			{
				if(!this.world.equals(z.getSelection().world)) continue;
				
				for(Line2D otherline : z.getSelection().getLines())
				{
					if(line.intersectsLine(otherline))
					{
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public boolean isInside(Location l)
	{
		boolean same =false;
		if (l.getWorld().getName().contains("instance"))
		{
			for (Instance ins :this.plugin.MG.listInstances )
				if (l.getWorld().getName().equals(ins.getWorldName()))
					if (this.world.getName().equals(ins.getSourceWorld()))
						same = true;
		}
		if(!l.getWorld().equals(this.world)&&same==false)
		{
			return false;
		}
		
		Point2D p = new Point();
		p.setLocation(l.getBlockX(), l.getBlockZ());
		
		if(this.getPolygon().contains(p))
		{
			return true;
		}
		
		//Test des arretes
		
		
		for(Line2D line : this.getLines())
		{
			if(line.ptSegDist(l.getBlockX(), l.getBlockZ()) == 0.0)
			{
				return true;
			}
		
		}
		
		return false;
		

		
	}
	
	public Selection clone()
	{
		Selection s = new Selection(this.plugin);
		s.world = world;
		s.points = (ArrayList<Location>) this.points.clone();
		return s;
	}
	
	public ArrayList<Line2D.Double> getLines()
	{
		ArrayList<Line2D.Double> lines = new ArrayList<Line2D.Double>();
		
		for(int i = 0 ; i < this.points.size()-1; i++)
		{
			Line2D.Double line = new Line2D.Double();
			line.setLine(this.points.get(i).getX(), this.points.get(i).getZ(), this.points.get(i+1).getX(), this.points.get(i+1).getZ());
			lines.add(line);
		}
		
		Line2D.Double line = new Line2D.Double();
		line.setLine(this.points.get(this.points.size()-1).getX(), this.points.get(this.points.size()-1).getZ(), this.points.get(0).getX(), this.points.get(0).getZ());
		lines.add(line);
		
		return lines;
	}
	
	public boolean isCroised()
	{
		//TODO
		return false;
	}
	
	public int getArea()
	{
		double somme = 0;
		
		for(int i = 0; i<this.points.size(); i++)
		{
			int next = i+1;
			if(i == this.getLines().size()-1) next = 0;
			
			somme += this.points.get(i).getBlockX()*this.points.get(next).getBlockZ()-this.points.get(next).getBlockX()*this.points.get(i).getBlockZ();
		}
		
		int area = (int) Math.round(somme/2);
		return Math.abs(area);
	}
	
	public Zone allPointsInAZone()
	{
		// null => OK
		// Zone => Tous les points doivent etre dans Zone
		
		if(this.points.isEmpty()) return null;
		Location first = this.points.get(0);
		
		for(Location l : this.points)
		{
			if(this.plugin.getZone(l) != this.plugin.getZone(first)) return this.plugin.getZone(l);
		}
		
		return null;
	}
	

	
	
}
