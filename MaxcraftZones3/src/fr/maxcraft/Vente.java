package fr.maxcraft;


import java.util.ArrayList;
import java.util.Date;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Beacon;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

public class Vente {

	//VARIABLES
	private MaxcraftZones plugin;
	private int id;
	private Zone zone;
	private int price;
	private String account;
	private Block signBlock;
	private String type;
	private int choiceMenu;

	
	//CONSTRUCTOR

	public Vente(MaxcraftZones plugin, int id, Zone zone, int price, Block signBlock, String type, String account)
	{
		this.id = id;
		this.plugin = plugin;
		this.zone = zone;
		this.price = price;
		this.signBlock = signBlock;
		this.type = type;
		this.account = account;
		this.plugin.ZoneManager.listVentes.add(this);
		this.choiceMenu = 0;

	}

	//GETTER SETTER
	
	
	public int getId() {
		return id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Zone getZone() {
		return zone;
	}

	public void setZone(Zone zone) {
		this.zone = zone;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Block getSignBlock() {
		return signBlock;
	}

	public void setSignBlock(Block signBlock) {
		this.signBlock = signBlock;
	}
	

	
	//METHODES
	


	public Sign getSign()
	{
		if(signBlock.getState() instanceof Sign)
		{
			return (Sign) signBlock.getState();
		}
		else
		{
			return null;
		}
	}
	
	public void loadSign()
	{
		if(this.getSign() == null)
		{
			return;
		}
		
		Sign sign = this.getSign();
		
		sign.setLine(0, ChatColor.AQUA + "A VENDRE");
		if (this.type!=null)
		if (this.getType().equals("L"))
			sign.setLine(0, ChatColor.AQUA + "A LOUER");
		sign.setLine(1, this.getEconomyAccount());
		
		if(this.displayType() == null)
		{
		sign.setLine(2, "clique = achat");
		}
		else
		{
		sign.setLine(2, this.displayType());
		}

		if(this.choiceMenu == 0)
		{
			sign.setLine(3, ChatColor.DARK_RED +""+ this.price + " POs");
		}
		else if(this.choiceMenu == 1)
		{
			sign.setLine(3, ChatColor.DARK_RED + "SUPPRIMER");
		}
		if (this.type!=null)
			if (this.getType().equals("L"))
				if(this.choiceMenu == 0)
				{
					sign.setLine(3, ChatColor.DARK_RED +""+ this.price + " POs par jours");
				}
				else if(this.choiceMenu == 1)
				{
					sign.setLine(3, ChatColor.DARK_RED + "SUPPRIMER");
				}
		
		this.plugin.reloadSign(sign);
		
		
	}
	
	public String getEconomyAccount()
	{
		if(this.account == null)
		{
			if(this.zone.hasOwner())
			{
			return this.zone.getOwner().getUserName();
			}
			else
			{
				return "maxcraft";
			}
		}
		else
		{
			return this.account;
		}
	}
	
	public void remove()
	{
		this.plugin.MM.mysql_update("DELETE FROM `vente` WHERE `vente`.`id` = "+this.id);
		this.plugin.ZoneManager.listVentes.remove(this);
		this.zone.reloadDynmap();
		
		
		//Deletation du sign
		this.signBlock.setType(Material.AIR);
		
	}
	
	public String displayType()
	{
		if(! this.zone.hasOwner() || !this.zone.getOwner().haveFaction())
		{
			return null;
		}
		
		if(this.type == null)
		{
			return null;
		}
		
		switch(this.type)
		{
		case "F":
			return this.zone.getOwner().getFaction().getTag();
		case "A":
			return "Alliés de "+ this.zone.getOwner().getFaction().getTag();
		case "F+A":
			return  this.zone.getOwner().getFaction().getTag()+" + Alliés";
		default: 
			return null;
		}
		
	}
	
	public void create()
	{
		this.loadSign();
		this.createInSql();
		this.zone.reloadDynmap();
	}
	
	public String getSqlStringType()
	{
		if(this.type == null)
		{
			return "NULL";
		}
		else
		{
			return "'"+this.type+"'";
		}
	}
	
	public String getSqlStringAccount()
	{
		if(this.account == null)
		{
			return "NULL";
		}
		else
		{
			return "'"+this.account+"'";
		}
	}
	
	public void createInSql()
	{
	this.plugin.MM.mysql_update("INSERT INTO `vente` (`id`, `zone_id`, `type`, `x`, `y`, `z`, `price`, `world`, account) VALUES (NULL, "+this.zone.getId()+", "+this.getSqlStringType()+", "+this.signBlock.getLocation().getBlockX()+", "+this.signBlock.getLocation().getBlockY()+", "+this.signBlock.getLocation().getBlockZ()+", "+this.price+", '"+this.signBlock.getLocation().getWorld().getName()+"', "+this.getSqlStringAccount()+");");
	}
	
	public boolean achatValidBy(Maxcraftien m)
	{
		if(this.type == null)
		{
			return true;
		}
		switch(this.type)
		{
		case "F":
			if(m.getFaction() == this.zone.getOwner().getFaction())
			{
				return true;
			}
			return false;
		case "A":
			if(m.getStateWith(this.zone.getOwner()).equals("FRIEND"))
			{
				return true;
			}
			return false;
		case "F+A":
			if(m.getFaction() == this.zone.getOwner().getFaction() || m.getStateWith(this.zone.getOwner()).equals("FRIEND"))
			{
				return true;
			}
			return false;
		default:
			return true;
		
		}
	}
	
	public void onClick(Player p)
	{
		if(this.choiceMenu == 0)
		{
			if(this.plugin.MM.isMaxcraftien(p.getName()))
			{
				Maxcraftien m = this.plugin.MM.getMaxcraftien(p);
				
				
				
			if(this.getZone().getOwner() != m)
			{
				if(! this.achatValidBy(m))
				{
					p.sendMessage(this.plugin.message("Ce terrain est réservé à certains joueurs : "+ this.displayType()));
					
					return;
				}
				
				if(this.plugin.MM.economy.getBalance(m.getUserName()) < (float) this.getPrice())
				{
				//Pas assez d'argent
					p.sendMessage(this.plugin.message("Vous n'avez pas assez de POs pour acheter cette parcelle !"));
					
					return;
				}
				
				//ACHAT POSSIBLE
				
				//Transaction
				this.plugin.MM.economy.bankDeposit(this.getEconomyAccount(), (float) this.getPrice());
				this.plugin.MM.economy.bankWithdraw(m.getUserName(), (float) this.getPrice());
				//Changement de proprio

				if (this.type!=null)
				if (this.getType().equals("L"))
					{
					Rent r =new Rent(this.plugin, this.plugin.MM.getNextId("location"), this.getZone(), this.getZone().getOwner(), m,this.getPrice(),"DAY",new Date());
					r.create();
					this.plugin.ZoneManager.listLocations.add(r);
					}
					this.getZone().setOwner(m);
					this.plugin.MM.mysql_update("UPDATE `zone` SET `user` = "+m.getId()+" WHERE `zone`.`id` = "+this.getZone().getId());
				
				//Suppression de la vente
				this.remove();
				
				//Notif a faire
				this.getZone().setBuilders(new ArrayList<Maxcraftien>());
				this.getZone().setCuboiders(new ArrayList<Maxcraftien>());
				this.getZone().removeUsersFromSql();
				
				//reload de la zone
				this.getZone().reloadZone();
				
				//Message
				p.sendMessage(this.plugin.message("Vous avez acheté le terrain <"+ ChatColor.GOLD +this.getZone().getName()+ ChatColor.GRAY +"> pour "+this.plugin.MM.economy.format((double) this.getPrice())));
				p.sendMessage(this.plugin.message("Pour modifier les droits: builders/cuboiders, rendez-vous sur le site."));
				return;
			}
			}
		}
		else if(this.choiceMenu == 1)
		{
			this.signBlock.setType(Material.AIR);
			this.remove();
			p.sendMessage(this.plugin.message("La vente a été supprimée."));
			return;
			
		}
	}
	
	public void onRightClick(Player p)
	{
		if(!(p.hasPermission("maxcraft.zone.admin") || p.getName().equals(this.getZone().getOwner().getUserName()) || this.getZone().canAdmin(p)))
		{
			return;
		}
		
		if(this.choiceMenu == 0) this.choiceMenu = 1;
		else this.choiceMenu = 0;
		
		this.loadSign();
	}
	

}
