package fr.maxcraft;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;
import org.dynmap.markers.MarkerSet;

public class MaxcraftZones extends JavaPlugin{

	//VARIABLES
	public static MaxcraftManager MM = null;
	public static MaxcraftGames MG = null;
	public ZoneManager ZoneManager = null;
	
	//LISTENERS
	private PlayerListener PL;
	private ProtectListener ProtectL;
	private ZoneCommandManager CommandManager = new ZoneCommandManager(this);
	
	//DYNMAP
	MarkerSet zoneMarker;
	MarkerSet toSellMarker;
	
	//METHODES
	@Override
    public void onEnable(){
		
		//PLUGINS
		this.MM = this.getMM();
		this.MG = this.getMG();
		this.log("Plugins chargés !");
	
		//Listeners
		this.PL = new PlayerListener(this);
		this.ProtectL = new ProtectListener(this);
		this.getServer().getPluginManager().registerEvents(this.PL, this);
		this.getServer().getPluginManager().registerEvents(this.ProtectL, this);		
		
		//Zone manager
		this.ZoneManager = new ZoneManager(this);
		try {
			this.ZoneManager.load();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//DynMap
		this.zoneMarker = this.MM.Dynmap.getMarkerAPI().createMarkerSet("MaxcraftZones Zones", "Zones", null, false);
		this.zoneMarker.setHideByDefault(true);
		this.toSellMarker = this.MM.Dynmap.getMarkerAPI().createMarkerSet("MaxcraftZones Zones à vendre", "Zones à vendre", null, false);
		this.toSellMarker.setHideByDefault(true);
		this.createDynmapZones();
		
	}
	
	@Override
    public void onDisable(){
		
	}
		

	
	public MaxcraftManager getMM()
	{
		MaxcraftManager mm = (MaxcraftManager) this.getServer().getPluginManager().getPlugin("MaxcraftManager");
        log("MaxcraftManager chargé !");
		return mm;
	}
	
	public MaxcraftGames getMG()
	{
		MaxcraftGames mg = (MaxcraftGames) this.getServer().getPluginManager().getPlugin("MaxcraftGames");
        log("MaxcraftGames chargé !");
		return mg;
	}
	
	/*
	 * Affichage dans console
	 */
	public void log(String message, ChatColor color)
	{
		ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
		console.sendMessage(color + "[MAXCRAFT] "+ ChatColor.WHITE +"[MaxcraftZones] " + message);
		
	}
	public void log(String message)
	{
		ChatColor color = ChatColor.RED;
		log(message, color);
		
	}
	
	
	
	public String message(String message)
	{
		return ChatColor.AQUA + "[Zones] " + ChatColor.GRAY + message;
	}

	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		//ZONE
		if(cmd.getName().equalsIgnoreCase("zone")){ 
			CommandManager.command(sender, args);
		}
		return true;
		
		}
	
	
	public String multiArgs(String[] args, int start)
	{
		
		int length = args.length;
		int nlength = length-start;
		String[] nametable = new String[nlength];
		int nid;
		for(int i = start; i < args.length; i++)
		{
			nid = i-start;
			nametable[nid] = args[i];
		}
		
		String name = StringUtils.join(nametable, " ");
		return name;
	}
	


public void createDynmapZones()
{
	for(Zone z : this.ZoneManager.listZones)
	{
		z.createDynmap();
	}
}

public Sign reloadSign(Sign sign)
{
	
	sign.update();
	sign.getChunk().unload();
	sign.getChunk().load();
	return sign;
}

public boolean isInt(String chaine) {
	try {
		Integer.parseInt(chaine);
	} catch (NumberFormatException e) {
		return false;
	}

	return true;
}

public boolean isSelectionValid(Maxcraftien m, boolean displayReason)
{
	if(!this.ZoneManager.selections.containsKey(m))
	{
		if(displayReason) m.getPlayer().sendMessage(this.message("Vous devez faire une séléction !"));
		return false;
		
	}
	
	Selection selection = this.ZoneManager.selections.get(m);
	
	if(selection.points.size() < 3)
	{
		if(displayReason) m.getPlayer().sendMessage(this.message("Vous devez faire une séléction d'au moins 3 points !"));
		return false;
	}
	
	Zone parent = selection.allPointsInAZone();
	if(parent != null)
	{
		Zone first = this.getZone(selection.points.get(0));
		if(first == null)
		{
			if(displayReason) m.getPlayer().sendMessage(this.message("Sélection impossible ! Le premier point se trouve en dehors d'une zone et un autre point se trouve dans une zone (#"+parent.getId()+")"));
			
		}
		else
		{
			if(displayReason) m.getPlayer().sendMessage(this.message("Sélection impossible ! Le premier point se trouve dans la zone #"+ first.getId()+" et un autre point se trouve dans une autre zone (#"+parent.getId()+")"));
			
		}
		return false;
	}
	
	if(selection.isCroised())
	{
		if(displayReason) m.getPlayer().sendMessage(this.message("Cette sélection se croise elle même !"));
		return false;
	}
	
	for(Location l : selection.points)
	{
		Zone z = this.getZone(l);
		
		if(z == null)
		{
			if(m.getPlayer().hasPermission("maxcraft.zone.admin"))
			{
				return true;
			}
			else
			{
				if(displayReason) m.getPlayer().sendMessage(this.message("Cette sélection n'est pas dans une zone, vous devez être admin."));
				return false;
			}
		}
		
		if(!z.canCuboid(m.getPlayer()))
		{
			if(displayReason) m.getPlayer().sendMessage(this.message("Cette sélection n'est pas dans une zone où vous pouvez cuboider !"));
			return false;
		}
	}
	
	//TODO
	
	return true;
}

public Zone getZone(Location l)
{
	return this.ZoneManager.getZone(l);
}


public Zone getZone(int id) {
	return this.ZoneManager.getZone(id);
}
public ArrayList<Entity> getEntities(Zone z) {
	return this.ZoneManager.getEntities(z);
}


}
