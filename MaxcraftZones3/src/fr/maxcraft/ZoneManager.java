package fr.maxcraft;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class ZoneManager {

	public ArrayList<Zone> listZones;
	public ArrayList<Vente> listVentes; 
	public ArrayList<Rent> listLocations = new ArrayList<Rent>(); 
	private MaxcraftZones plugin;
	public Map<Maxcraftien, Selection> selections;
	
	public ZoneManager(MaxcraftZones plugin)
	{
		this.listZones = new ArrayList<Zone>();
		this.listVentes = new ArrayList<Vente>();
		this.selections = new HashMap<Maxcraftien, Selection>();
		this.plugin = plugin;
		
		
	}

	public void loadZone(ResultSet result) throws SQLException
	{
		Maxcraftien owner;
		
		if(result.getObject("user") == null)
		{
			owner = null;
		}
		else
		{
			owner = this.plugin.MM.getMaxcraftien(result.getInt("user"));
		}
		
		
		Selection selection = new Selection(this.plugin);
		
		World world = this.plugin.getServer().getWorld(result.getString("world"));
	
		
		if(world == null) return;
		selection.world = world;
		
		for(String coord : result.getString("points").split(";"))
		{
			int x = Integer.parseInt((coord.split(":"))[0]);
			int y = Integer.parseInt((coord.split(":"))[1]);
			Location point = new Location(world, x, 0, y);
			selection.points.add(point);
		}
	
		Zone zone = new Zone(this.plugin, result.getInt("id"), selection, owner, result.getString("name"),null);
		
		//tags
		if(result.getString("tags") != null)
		{
			
			
			String[] tagstable = result.getString("tags").split(";");
			ArrayList<String> tags = new ArrayList<String>();
			for(String tag : tagstable)
			{
				tags.add(tag);
			}
			zone.setTags(tags);
		}
	}
	
	public void doParentLinks(ResultSet result) throws SQLException
	{
		Zone myzone = this.getZone(result.getInt("id"));
		if(myzone == null) return;
		
		if(this.getZone(result.getInt("parent")) != null)
		{
			myzone.setParent(this.getZone(result.getInt("parent")));
		}
	}
	
	public void loadRoles(ResultSet result) throws SQLException
	{
		Maxcraftien m = this.plugin.MM.getMaxcraftien(result.getInt("user"));
		Zone z = this.getZone(result.getInt("zone_id"));
		if(z ==null) return;
		if(m == null) return;
		if(result.getString("role").equals("CUBO"))
		{
			z.getBuilders().add(m);
			z.getCuboiders().add(m);
			
			
		}
		else if(result.getString("role").equals("BUILD"))
		{
			z.getBuilders().add(m);
			
		}
	}
	
	public void loadVente(ResultSet result) throws SQLException
	{
		Location l = new Location(this.plugin.getServer().getWorld(result.getString("world")), result.getInt("x"), result.getInt("y"), result.getInt("z"));
		
		Block b = l.getBlock();
		Zone z = this.getZone(result.getInt("zone_id"));
		Vente v = new Vente(this.plugin, result.getInt("id"), z, result.getInt("price"), b, result.getString("type"), result.getString("account"));
		this.plugin.log("Vente de zone <"+ v.getZone().getName()+">", ChatColor.GREEN);
	
	}
	
	private void loadRent(ResultSet result)throws SQLException
	{
		try {
		Zone z = this.getZone(result.getInt("zone"));
		Maxcraftien o = this.plugin.MM.getMaxcraftien(result.getInt("owner"));
		Maxcraftien l = this.plugin.MM.getMaxcraftien(result.getInt("locataire"));
		
		Rent r = new Rent(this.plugin,result.getInt("id"),z,o,l,result.getInt("prix"),result.getString("dur�e"),result.getDate("last"));
		r.pay();
		z.setBailleur(o);
		 	listLocations.add(r);
		}catch(NullPointerException e){
			this.plugin.log("Erreur au chargement de la location #"+ result.getInt("id") +" pour la zone #" + result.getInt("zone"));
			e.printStackTrace();
		}
	}
	
	
	public void load() throws SQLException 
	{
		//Zones
			
				ResultSet result = this.plugin.MM.mysql_query("SELECT * FROM zone", false);
				
				while(result.next())
				{
					try {
						this.loadZone(result);
					} catch (SQLException e) {
						Bukkit.getLogger().info(""+result.getInt("id"));
						e.printStackTrace();
						this.plugin.log("Erreur au chargement de la zone #" + result.getInt("id"));
						continue;
					}
					
				}
				
				Zone myzone;
				ResultSet result2 = this.plugin.MM.mysql_query("SELECT * FROM zone", false);
				
				//Chargement des parentées de zone
				while( result2.next())
				{
					try {
						this.doParentLinks(result2);
					} catch (SQLException e) {
						
						e.printStackTrace();
						this.plugin.log("Erreur au chargement des parentées sur une zone");
						continue;
					}
				}
				
				//Chargement des roles sur zones
				
				result = this.plugin.MM.mysql_query("SELECT * FROM zoneUser", false);
				
				while(result.next())
				{
					Maxcraftien m = this.plugin.MM.getMaxcraftien(result.getInt("user"));
					Zone z = this.getZone(result.getInt("zone_id"));
					try {
						this.loadRoles(result);
					} catch (SQLException e) {
						e.printStackTrace();
						this.plugin.log("Erreur au chargement du role #"+ result.getInt("id") +" pour la zone #" + result.getInt("zone_id"));
						continue;
					}
				}
				//Chargement des ventes
				
				result = this.plugin.MM.mysql_query("SELECT * FROM vente", false);
				while(result.next())
				{

					try {
						this.loadVente(result);
					} catch (SQLException e) {
						
						e.printStackTrace();
						this.plugin.log("Erreur au chargement de la vente #"+ result.getInt("id") +" pour la zone #" + result.getInt("zone_id"));
						
						continue;
					}
				}
				//Chargement des locations
				
				result = this.plugin.MM.mysql_query("SELECT * FROM location", false);
				while(result.next())
				{

					try {
						this.loadRent(result);
					} catch (SQLException e) {
						
						e.printStackTrace();
						this.plugin.log("Erreur au chargement de la location #"+ result.getInt("id") +" pour la zone #" + result.getInt("zone"));
						
						continue;
					}
				}
				
	}

	public Zone getZone(Location l)
	{
		Zone smallestZone = null;
		int level = 0;
		for (Zone zone : this.listZones) {

			if (zone.getSelection().isInside(l) && zone.getGeneration() >= level) {
				smallestZone = zone;
				level = smallestZone.getGeneration();
			}
		}
		
		return smallestZone;
	}
	
	
	public Zone getZone(int id) {
		for (Zone zone : this.listZones) {
			if (zone.getId() == id) {
				return zone;
			}
		}
		return null;

	}
	
	public boolean isInAZone(Location l)
	{
		if(this.getZone(l) == null)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public Zone findParentZone(Selection s)
	{
		return this.getZone(s.points.get(0));
	}
	
	public boolean hasParentZone(Selection s)
	{
		if(this.findParentZone(s) == null)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	public Rent getRent(Zone z)
	{
		for (Rent r : this.listLocations){
			Bukkit.getLogger().info("ListLocations : "+r.getZone() );
			if (r.getZone().equals(z))
				return r;
			}
		return null;
	}
	public ArrayList<Entity> getEntities(Zone z){
		ArrayList<Entity> entity = new ArrayList<Entity>();
		for (LivingEntity e : z.getSelection().getWorld().getLivingEntities())
			if (z.getSelection().isInside(e.getLocation()))
				entity.add(e);
	return entity;
	}
}
