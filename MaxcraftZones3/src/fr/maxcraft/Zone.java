package fr.maxcraft;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.math.BigInteger;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.dynmap.markers.AreaMarker;
import org.dynmap.markers.MarkerSet;


public class Zone {
	
	//VARIABLES
	private int id;
	private MaxcraftZones plugin;

	public Maxcraftien owner; 
	private ArrayList<Maxcraftien> builders = new ArrayList<Maxcraftien>();
	private ArrayList<Maxcraftien> cuboiders = new ArrayList<Maxcraftien>();
	private Zone parent;
	private String name;
	private AreaMarker marker;
	private ArrayList<String> tags;
	private Selection selection;
	private Maxcraftien bailleur;
	
	
	//CONSTRUCTOR
	
	public Zone(MaxcraftZones plugin, int id, Selection selection, Maxcraftien owner, String name,Maxcraftien bailleur)
	{
		this.id =id;
		this.plugin = plugin;
		this.selection = selection;
		this.owner = owner;
		this.name = name;
		this.tags = new ArrayList<String>();
		this.plugin.ZoneManager.listZones.add(this);
		this.bailleur = bailleur;
		
	}



	public Maxcraftien getBailleur() {
		return bailleur;
	}



	public void setBailleur(Maxcraftien bailleur) {
		this.bailleur = bailleur;
	}



	public ArrayList<Maxcraftien> getBuilders() {
		return builders;
	}



	public ArrayList<String> getTags() {
		return tags;
	}



	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}







	public void setBuilders(ArrayList<Maxcraftien> builders) {
		this.builders = builders;
	}



	public ArrayList<Maxcraftien> getCuboiders() {
		return cuboiders;
	}



	public void setCuboiders(ArrayList<Maxcraftien> cuboiders) {
		this.cuboiders = cuboiders;
	}



	public Zone getParent() {
		return parent;
	}

	

	public void setParent(Zone parent) {
		this.parent = parent;
		
	}

	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public Maxcraftien getOwner() {
			return owner;
	}



	public Selection getSelection() {
		return selection;
	}



	public void setSelection(Selection selection) {
		this.selection = selection;
	}



	public void setOwner(Maxcraftien owner) {
		this.owner = owner;
	}
	
	
	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}
	

	//GETTER SETTER
	
	




	//METHODES
	public int getGeneration()
	{
		int g = 0;
		Zone z = this;
		while(z.getParent() != null)
		{
			z = z.getParent();
			g++;
		}
		return g;
	}
	

	
	public String getLocalisation()
	{
		if(this.getParent() == null)
		{
			return "Maxcraft";
		}
		else
		{
			String loc = ChatColor.GOLD + this.name;
			Zone z = this;
			while(z.getParent() != null)
			{
				z = z.getParent();
				loc = z.getName() +" -> "+ loc;
				
			}
			return loc;
		
		}
	}
	
	public void reloadFromSql() throws SQLException
	{
		this.selection.points.clear();
		ResultSet result = this.plugin.MM.mysql_query("SELECT * FROM zone WHERE id = "+ this.id, true);
		this.name = result.getString("name");
		
		World world = this.plugin.getServer().getWorld(result.getString("world"));
		this.selection.world = world;
		
		if(world == null) return;
		
		for(String coord : result.getString("points").split(";"))
		{
			int x = Integer.parseInt((coord.split(":"))[0]);
			int y = Integer.parseInt((coord.split(":"))[1]);
			Location point = new Location(world, x, 0, y);
			this.selection.points.add(point);
		}
		
		Maxcraftien owner = this.plugin.MM.getMaxcraftien(result.getInt("user"));
		Zone parent = this.plugin.ZoneManager.getZone(result.getInt("parent"));

		 this.owner = owner;
		 this.parent = parent;
		 
		 this.builders.clear();
		 this.cuboiders.clear();
		 
		 this.tags = new ArrayList<String>();
		 if(result.getString("tags") != null)
		 {
			 
			String[] tagstable = result.getString("tags").split(";");
			for(String tag : tagstable)
			{
				this.tags.add(tag);
			}
		 
		 
		 }
		
		 
		 result = this.plugin.MM.mysql_query("SELECT * FROM zoneUser WHERE zone_id = "+this.id, false);
		
		 
			while(result.next())
			{
				Maxcraftien m = this.plugin.MM.getMaxcraftien(result.getInt("user"));
				Zone z = this;
				
				if(result.getString("role").equals("CUBO"))
				{
					z.cuboiders.add(m);
					z.builders.add(m);
					
					}
				else if(result.getString("role").equals("BUILD"))
				{
					z.builders.add(m);
					}
			}
		
	}
	

	
	

	
	public boolean hasOwner()
	{
		if(this.owner != null)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
	public Zone findParentZone()
	{
		this.getParent();
		return null;
	}
	
	public boolean hasParentZone()
	{
		if(this.getParent() == null)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public boolean isChildOf(Zone zone)
	{
		if(this.getParent() == null)
		{
			return false;
		}
		else
		{
		
			Zone z = this;
			while(z != null)
			{
				if(z == zone)
				{
					return true;
				}
				z = z.getParent();
				
			}
			return false;
			
		
		}
	}
	
	
	public boolean canCuboid(Player p)
	{
		
		if(p.hasPermission("maxcraft.admin.zone"))
		{
			return true;
		}
		
		Maxcraftien m = this.plugin.MM.getMaxcraftien(p);
		if(m == null)
		{
			return false;
		}
		
		
		//Parentées 
		
		Zone parent = this;
		while(parent != null)
		{
			if(parent.cuboiders.contains(m) || parent.owner == m)
			{
				return true;
			}
			
			parent = parent.getParent();
		}
		
		return false;
	}
	
	public boolean canBuild(Player p)
	{

		if(p.hasPermission("maxcraft.admin.zone"))
		{
			return true;
		}
		
		Maxcraftien m = this.plugin.MM.getMaxcraftien(p);
		if(m == null)
		{
			return false;
		}
		//Parentées 
		
		Zone parent = this;
		while(parent != null)
		{
			if(parent.builders.contains(m) || parent.owner == m)
			{
				return true;
			}
					
			parent = parent.getParent();
		}
				
		return false;
		
	}
	
	public boolean canAdmin(Player p)
	{
		if(p.hasPermission("maxcraft.admin.zone"))
		{
			return true;
		}
		
		
		if(this.parent == null)
		{
			return false;
					
		}
		if(this.parent.canCuboid(p))
		{
			return true;
		}
		
		return false;
		
	}
	

	
	public void createDynmap()
	{
	
		if(this.hasDirectlyTag("hide"))
		{
			return;
		}
		
	
		
		 double[] x = new double[this.selection.points.size()];
		 double[] y = new double[this.selection.points.size()];
		 int i = 0;
		 
		 for(Location l : this.selection.points)
		 {
			 x[i] = l.getX()+0.5;
			 y[i] = l.getZ()+0.5;
			 i++;
		 }
           
           String thisid = ""+this.id;
           
           MarkerSet set = this.plugin.zoneMarker;
           
           if(this.isToSell())
           {
        	   set = this.plugin.toSellMarker;
           }
           
           
           this.marker = set.createAreaMarker(thisid, this.name, true, this.selection.world.getName(), x, y, false);
	       
	       this.marker.setFillStyle(0.5, this.getDynmapColor());
	       
	       if(!this.isToSell())
	       {
			this.marker.setLineStyle(1, 0.8, this.getDynmapColor());
	       }
	       else
	       {
	    	   this.marker.setLineStyle(2, 1, 0xffff00);
	    	  
	       }
			
			if(this.isToSell())
			{
			this.marker.setDescription("<span style=\" text-align:center; width: 200px; height: 200px; color:red; \"><strong>"+ this.name +"</strong></span><br/>" +
					"<span style=\" color:orange; font-weight:bold;\">A vendre "+this.plugin.MM.economy.format((double) this.getVente().getPrice())+"</span>" +
					"<br/><br/>" +
					"Propriétaire : <span style=\" color:green; font-weight:bold;\">"+ this.displayOwner() +"</span><br/>" +
					"Localisation : <span style=\" color:black; font-weight:bold;\">"+ ChatColor.stripColor(this.getLocalisation()) +"</span><br/>" +
					"Numéro : <span style=\" color:black;\">"+ this.id +"</span><br/>" +
					"Surface : <span style=\" color:black;\">"+ this.getSelection().getArea() +"m² (sans la moitié des bordures)</span><br/>" +
					
					"Cuboiders : <span style=\" color:gray; font-weight:bold;\">"+ this.displayCuboiderList() +"</span><br/>" +
							"Builders : <span style=\" color:gray; font-weight:bold;\">"+ this.displayBuilderList() +"</span><br/>" +
					"");
			}
			else
			{
				this.marker.setDescription("<span style=\" text-align:center; width: 200px; height: 200px; color:red; \"><strong>"+ this.name +"</strong></span><br/>" +
						
						"<br/>" +
						"Propriétaire : <span style=\" color:green; font-weight:bold;\">"+ this.displayOwner() +"</span><br/>" +
						"Localisation : <span style=\" color:black; font-weight:bold;\">"+ ChatColor.stripColor(this.getLocalisation()) +"</span><br/>" +
						"Numéro : <span style=\" color:black;\">"+ this.id +"</span><br/>" +
						"Surface : <span style=\" color:black;\">"+ this.getSelection().getArea() +"m² (sans la moitié des bordures)</span><br/>" +
						
						
						"Cuboiders : <span style=\" color:gray; font-weight:bold;\">"+ this.displayCuboiderList() +"</span><br/>" +
								"Builders : <span style=\" color:gray; font-weight:bold;\">"+ this.displayBuilderList() +"</span><br/>" +
						"");
			}
			
	}
	
	public String displayCuboiderList()
	{
		ArrayList<Maxcraftien> list = this.getCuboiders();
		
		String namelist = null;
		int i = 0;
		for(Maxcraftien m : list)
		{
			if(i == 0)
			{
				namelist  = m.getUserName();
			}
			else
			{
				namelist  = namelist + ", " + m.getUserName();
			}
			i++;
		}

		if(namelist == null)
		{
			return "Pas de cuboider";
		}
		else
		{
			return namelist;
		}
	}
	
	public String displayBuilderList()
	{
		ArrayList<Maxcraftien> list = this.getBuilders();
		
		String namelist = null;
		int i = 0;
		for(Maxcraftien m : list)
		{
			if(i == 0)
			{
				namelist  = m.getUserName();
			}
			else
			{
				namelist  = namelist + ", " + m.getUserName();
			}
			i++;
		}

		if(namelist == null)
		{
			return "Pas de builder";
		}
		else
		{
			return namelist;
		}
	}
	
	public int getDynmapColor()
	{
		if(this.hasDirectlyTag("public"))
		{
			return 0x222222;
		}
		
		if(this.hasDirectlyTag("shop"))
		{
			return 0x00ffff;
		}
		
		if(this.hasDirectlyTag("region"))
		{
			return 0x1b7100;
		}
		
		if(this.hasDirectlyTag("farm"))
		{
			return 0xb60354;
		}
		
		return 0x28a600;
		
	}
	

	
	public void removeDynmap()
	{
		if(this.marker == null)
		{
			return;
		}
		marker.deleteMarker();
	}

	public void reloadZone()
	{
		this.removeDynmap();
		this.createDynmap();
	}
	
	public void reloadDynmap()
	{
		this.removeDynmap();
		this.createDynmap();
	}
	
	public String displayOwner()
	{
		if(this.hasOwner())
		{
			return this.owner.getUserName();
		}
		else
		{
			return "Aucun";
		}
	}
	
	public Vente getVente()
	{
		for(Vente v : this.plugin.ZoneManager.listVentes)
		{
			if(v.getZone() == this)
			{
				return v;
			}
		}
		return null;
	}
	
	public boolean isToSell()
	{
		if(this.getVente() != null)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void remove()
	{
		this.removeUsersFromSql();
		this.plugin.MM.mysql_update("DELETE FROM `zone` WHERE `zone`.`id` = "+this.id);
		
		if(this.isToSell())
		{
			this.getVente().remove();
		}
		if(this.getBailleur()!=null)
			this.plugin.MM.mysql_update("DELETE FROM `location` WHERE `location`.`zone` = "+this.id);
			
		this.removeDynmap();
		this.plugin.ZoneManager.listZones.remove(this);
	}
	
	
	public void saveInSQL()
	{
		String parent = "NULL";
		if(this.parent != null)
		{
			parent = ""+this.parent.getId();
		}
		
		String owner = "NULL";
		if(this.hasOwner())
		{
			owner = ""+this.getOwner().getId();
		}
		
		
		String points = "";
		for(Location l : this.selection.points)
		{
			points += l.getBlockX()+":"+l.getBlockZ()+";";
		}
		
		this.plugin.MM.mysql_update("UPDATE `zone` SET points = '"+ points +"', world = '"+this.selection.world.getName() + "', parent = "+parent+", user = "+owner+", name = '"+this.name+"', tags = "+this.formatTags()+" WHERE id = "+this.id);
		
	}
	
	public boolean hasTag(String tag)
	{
		
	
		Zone parent = this;
		
		while(parent != null)
		{
			
			if(parent.tags != null)
			{
				for(String t : parent.tags)
				{
					if(t.equalsIgnoreCase(tag))
					{
						return true;
					}
					
					if(t.equalsIgnoreCase("!"+tag))
					{
						return false;
					}
				}
			}
			
		
		
		parent = parent.getParent();
		}
		
		return false;
	}
	
	public boolean hasNegativeTag(String tag)
	{
		return this.hasTag("!"+tag);
	}
	
	public boolean hasDirectlyTag(String tag)
	{
		for(String t : this.tags)
		{
			if(t.equalsIgnoreCase(tag))
			{
				return true;
			}
			
			if(t.equalsIgnoreCase("!"+tag))
			{
				return false;
			}
		}
		return false;
	}
	
	public String displayTags()
	{
		String taglist = "";
		int i = 0;
		for(String tag : this.tags)
		{
			if(tag != "notag")
			{
			if(i == 0)
			{
			taglist = tag;	
			}
			else
			{
			taglist = taglist + ", " + tag;
			}
			}
			
			i++;
		}
		
		return taglist;
	}
	

	public String formatTags()
	{
		if(this.tags.size() == 0)
		{
			return "NULL";
		}
		String taglist = "";
		int i = 0;
		for(String tag : this.tags)
		{
			if(tag != "notag")
			{
			if(i == 0)
			{
			taglist = tag;	
			}
			else
			{
			taglist = taglist + ";" + tag;
			}
			}
			
			i++;
		}
		
		return "'"+taglist+"'";
	}
	
	public void removeUsersFromSql()
	{
		this.plugin.MM.mysql_update("DELETE FROM `zoneUser` WHERE `zone_id` = "+ this.getId());
	}
	
	public ArrayList<Zone> getChildren(){
		ArrayList<Zone> children = new ArrayList<Zone>();
		for (Zone zo : this.plugin.ZoneManager.listZones){
			if (zo.hasParentZone())
				if (zo.getParent().equals(this)){
					children.add(zo);
				}
			
		}
		return children;
	}
}
