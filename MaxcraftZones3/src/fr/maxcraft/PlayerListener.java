package fr.maxcraft;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import fr.maxcraft.event.ShopCreateEvent;
import fr.maxcraft.event.ShopCreatedEvent;
import fr.maxcraft.event.ShopTransactionEvent;



public class PlayerListener implements Listener{

	//VARIABLES
		private MaxcraftZones plugin;
					
				
		//CONSTRUCTOR
		public PlayerListener(MaxcraftZones p)
		{
		this.plugin = p;
		}
		
		@EventHandler(priority = EventPriority.NORMAL)
		public void onZoneSelection(PlayerInteractEvent e){
			
			Player p = e.getPlayer();
			if(p.getItemInHand().getType().equals(Material.FLINT) && (e.getAction().equals(Action.RIGHT_CLICK_BLOCK) || e.getAction().equals(Action.LEFT_CLICK_BLOCK)) )
			{
				if(this.plugin.MM.isMaxcraftien(p.getName()))
				{
					Location l = e.getClickedBlock().getLocation();
					Maxcraftien m = this.plugin.MM.getMaxcraftien(p);
					
					if(e.getAction().equals(Action.RIGHT_CLICK_BLOCK))
					{
						this.plugin.ZoneManager.selections.get(m).points.clear();
						p.sendMessage(this.plugin.message("Sélection réinitialisée !"));
					}
					else if(e.getAction().equals(Action.LEFT_CLICK_BLOCK))
					{
						if(!this.plugin.ZoneManager.selections.containsKey(m))
						{
							this.plugin.ZoneManager.selections.put(m, new Selection(this.plugin));
						}
						
						this.plugin.ZoneManager.selections.get(m).points.add(l);
						this.plugin.ZoneManager.selections.get(m).world = l.getWorld();
		
						p.sendMessage(this.plugin.message("Position "+this.plugin.ZoneManager.selections.get(m).points.size()+" enregistrée !"));
					}
					
					e.setCancelled(true);
				
				}
			}
			
		}
		
		@EventHandler(priority = EventPriority.NORMAL)
		public void onSignPlaced(SignChangeEvent e)
		{
			Location l = e.getBlock().getLocation();
			Block b = e.getBlock();
			
			if(this.plugin.ZoneManager.getZone(l) != null && (e.getLine(0).equalsIgnoreCase("LOUER")|| e.getLine(0).equalsIgnoreCase("A LOUER")))
				if (e.getLine(1).equalsIgnoreCase("FIN")||e.getLine(1).equalsIgnoreCase("STOP"))
				{
					Zone z = this.plugin.ZoneManager.getZone(l);
					if (z.getBailleur()!=null)
						if (this.plugin.MM.getMaxcraftien(e.getPlayer()).equals(z.getOwner())||e.getPlayer().hasPermission("maxcraft.modo"))
						{
							this.plugin.ZoneManager.getRent(z).remove();
							e.getPlayer().sendMessage("Location arretée");
							e.setCancelled(true);
							return;
						}
					e.getPlayer().sendMessage("Vous ne pouvez arreter cette location!");
					e.setCancelled(true);
					return;
				}
			
			if(this.plugin.ZoneManager.getZone(l) != null && (e.getLine(0).equalsIgnoreCase("VENTE") || e.getLine(0).equalsIgnoreCase("A VENDRE") || e.getLine(0).equalsIgnoreCase("LOUER")|| e.getLine(0).equalsIgnoreCase("A LOUER")))
			{
				//VALIDATION
				
				if(!this.plugin.isInt(e.getLine(1)))
				{
					//pas entier
					e.getPlayer().sendMessage(this.plugin.message("Le prix de votre terrain doit être entier !"));
					return;
				}
				
				int price = Integer.parseInt(e.getLine(1));
				
				if(price <= 0)
				{
					//nÃ©gatif
					e.getPlayer().sendMessage(this.plugin.message("Le prix de votre terrain doit être strictement positif !"));
					return;
				}
				
				Zone zone = this.plugin.ZoneManager.getZone(l);
				
				if(zone.getVente() != null)
				{
					//Deja en vente
					e.getPlayer().sendMessage(this.plugin.message("La zone est déjà en vente !"));
					return;
				}
				
				if(!this.plugin.MM.isMaxcraftien(e.getPlayer().getName()))
				{
					//Pas maxcraftien
					return;
				}
				
				Maxcraftien m = this.plugin.MM.getMaxcraftien(e.getPlayer());
					if(!((zone.getOwner() == m && zone.getBailleur()==null) || zone.canAdmin(e.getPlayer()) ))
					{
						e.getPlayer().sendMessage(this.plugin.message("Vous n'avez pas le droit de vendre ce terrain !"));
						return;
					}
			

				//CREATION
				Vente vente = new Vente(this.plugin, this.plugin.MM.getNextId("vente"), zone, price, b, null, null);
				
				//Compte
				if(! e.getLine(2).isEmpty())
				{
					vente.setAccount(e.getLine(2));
				}
				
				//Si admin
				else if(e.getPlayer().hasPermission("maxcraft.zone.admin"))
				{
					if(zone.hasOwner())
					{
					vente.setAccount(zone.getOwner().getUserName());
					}
					else
					{
						vente.setAccount("maxcraft");
					}
				}
				
				//type de vente
				if (e.getLine(0).equalsIgnoreCase("LOUER")|| e.getLine(0).equalsIgnoreCase("A LOUER"))
					vente.setType("L");
				else if(e.getLine(3).equals("F") || e.getLine(3).equals("A") || e.getLine(3).equals("F+A"))
				{
					vente.setType(e.getLine(3));
				}
				
				vente.create();
				
				e.getPlayer().sendMessage(this.plugin.message("Le terrain <"+ ChatColor.GOLD + zone.getName()+ ChatColor.GRAY+"> a été mis en vente pour "+ ChatColor.RED + this.plugin.MM.economy.format((double) price)+ChatColor.GRAY +"."));
			}
			
			
			
		}
		
		
		@EventHandler(priority = EventPriority.NORMAL)
		public void onSignBreak(BlockBreakEvent e)
		{
			for(Vente vente : this.plugin.ZoneManager.listVentes)
			{
				Block up = vente.getSignBlock().getRelative(BlockFace.UP);
				Block down = vente.getSignBlock().getRelative(BlockFace.DOWN);
				Block north = vente.getSignBlock().getRelative(BlockFace.NORTH);
				Block south = vente.getSignBlock().getRelative(BlockFace.SOUTH);
				Block west = vente.getSignBlock().getRelative(BlockFace.WEST);
				Block east = vente.getSignBlock().getRelative(BlockFace.EAST);
				
				if(e.getBlock().equals(vente.getSignBlock()) || e.getBlock().equals(up) || e.getBlock().equals(down) || e.getBlock().equals(north) || e.getBlock().equals(south) || e.getBlock().equals(west) || e.getBlock().equals(east))
				{		
					e.setCancelled(true);
					return;
				}
			}
		}
		
		
		@EventHandler(priority = EventPriority.NORMAL)
		public void onBuyBySign(PlayerInteractEvent e)
		{
			
			if(e.getAction().equals(Action.LEFT_CLICK_BLOCK))
			{
			for(Vente vente : this.plugin.ZoneManager.listVentes)
			{
				if(e.getClickedBlock().equals(vente.getSignBlock()))
				{
					vente.onClick(e.getPlayer());
					return;
				}
			}
				
				
			}
			
			if(e.getAction().equals(Action.RIGHT_CLICK_BLOCK))
			{
				for(Vente vente : this.plugin.ZoneManager.listVentes)
				{
					if(e.getClickedBlock().equals(vente.getSignBlock()))
					{
						vente.onRightClick(e.getPlayer());
						return;
					}
				}
			}
		}
		
		// SHOP protection
		@EventHandler(priority = EventPriority.NORMAL)
		public void onShopCreated(ShopCreateEvent e)
		{
			Zone z = this.plugin.getZone(e.getLocation());
			if(z == null) e.setCancelled(true);
			if(!z.hasTag("shop")) e.setCancelled(true);
			if(e.isCancelled())
			{
				e.getOwner().getPlayer().sendMessage(this.plugin.message("Vous ne pouvez pas créer de shop sur cette zone !"));
			}
			return;
		}
		
		@EventHandler(priority = EventPriority.NORMAL)
		public void onShopTransaction(ShopTransactionEvent e)
		{
			Zone z = this.plugin.getZone(e.getShop().getChest().getLocation());
			if(z == null) e.setCancelled(true);
			if(!z.hasTag("shop")) e.setCancelled(true);
			if(e.isCancelled())
			{
				e.getPlayer().sendMessage(this.plugin.message("Vous ne pouvez pas utiliser de shop sur cette zone !"));
			}
			return;
		}
}
