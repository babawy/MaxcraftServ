package fr.maxcraft;



import java.awt.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Pig;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.vehicle.VehicleEnterEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;


public class MaxcraftListener implements Listener{

	//VARIABLES
	private MaxcraftManager plugin;
				
			
	//CONSTRUCTOR
	public MaxcraftListener(MaxcraftManager p)
	{
	this.plugin = p;
	}
	
	//METHODES

	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPortal(PlayerMoveEvent e){
		
		Player p = e.getPlayer();
		Block b = e.getTo().getBlock();
		
		for(Portal portal: this.plugin.listPortals)
		{
			//portail1
			if(portal.getLieu1().getLocation().getBlock().equals(b) && portal.isOuvert())
			{
				portal.getLieu2().tpPlayer(p);
					
			}
			if(portal.getLieu2().getLocation().getBlock().equals(b) && portal.isOuvert())
			{
				portal.getLieu1().tpPlayer(p);
			}	
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onStartSession(PlayerJoinEvent e){
		
		Player p = e.getPlayer();
		Session session = new Session(this.plugin, p.getName());
		this.plugin.log("#" +session.id+ " Nouvelle session : " + session.getPlayer());
		this.plugin.getMaxcraftien(p).updatename(p.getName());
		

		
		
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onStopSession(PlayerQuitEvent e){
		
		Player p = e.getPlayer();
		Session s = this.plugin.getSession(p.getName());
		s.remove();
		this.plugin.listSessions.remove(s);
		
		

	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onOreBreak(BlockBreakEvent e){
		
		

		Block b = e.getBlock();
		
			
			if(b.getLocation().getWorld().getName().equals("world"))
			{
				e.setCancelled(true);
				e.getBlock().setType(Material.AIR);
				e.getPlayer().sendMessage(this.plugin.message("Ne pas miner dans cette map, il n\'y a pas de minerais !"));
			}
			
		
	}
	

	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onChatAdmin(AsyncPlayerChatEvent e){
		
		if(! e.getMessage().startsWith("!"))
		{
			return;
		}
		
		e.setCancelled(true);
		
		for(Player p : Bukkit.getServer().getOnlinePlayers())
		{
			String m = e.getMessage();
			if(p.hasPermission("maxcraft.modo") || p.equals(e.getPlayer()))
			{
				p.sendMessage(ChatColor.RED + "[A] " +ChatColor.GRAY+ e.getPlayer().getName()+ " : " +ChatColor.WHITE+   m.substring(1,m.length()));
			}
		}
		
		
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onTravel(PlayerMoveEvent e){
		
		Player p = e.getPlayer();
		
		for(Travel t : this.plugin.listTravels)
		{
			Lieu l = t.getStart(e.getTo());
			if(l != null && t.getStart(e.getFrom()) == null)
			{
				t.travel(p, l);
				return;
			}
		}
	}
	
	

	
	
	
}
