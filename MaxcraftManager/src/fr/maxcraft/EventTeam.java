package fr.maxcraft;

import java.util.ArrayList;

import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;


public class EventTeam {
	private MaxcraftManager plugin;
	
	private String name;
	private ItemStack identity;
	private ArrayList<EventPlayer> players = new ArrayList<EventPlayer>();
	private String letter;
	private Event event;
	private ChatColor color;
	private Team team;
	
	
	public EventTeam(MaxcraftManager plugin, Event event, String name, String letter, ChatColor color)
	{
		this.plugin = plugin;
		this.name = name;
		this.letter = letter;
		this.event = event;
		this.color = color;
		this.identity = this.generateIdentity();
		event.getTeams().add(this);
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ItemStack getIdentity() {
		return identity;
	}

	public void setIdentity(ItemStack identity) {
		this.identity = identity;
	}

	public ArrayList<EventPlayer> getPlayers() {
		return players;
	}

	public void setPlayers(ArrayList<EventPlayer> players) {
		this.players = players;
	}

	public String getLetter() {
		return letter;
	}

	public void setLetter(String letter) {
		this.letter = letter;
	}
	
	
	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public ChatColor getColor() {
		return color;
	}

	public void setColor(ChatColor color) {
		this.color = color;
	}

	public void join(EventPlayer ep)
	{
		
		ep.setBack(ep.getOnlinePlayer().getLocation());
		
		sendMessage(ep.getPlayer().getUserName() + " a rejoint l' équipe "+ this.color + this.name + ChatColor.GRAY + " pour l'event "+ ChatColor.GOLD + this.event.getFullName());
		this.players.add(ep);
		ep.setTeam(this);
		
		if(!this.event.getTeamType().equals("INDIVIDUAL"))
		{
		ep.getOnlinePlayer().sendMessage(this.plugin.messageEvent("Vous avez rejoint "+ this.color + this.name + ChatColor.GRAY +" pour l'event "+ ChatColor.GOLD + this.event.getFullName()));
		}
		else
		{
			ep.getOnlinePlayer().sendMessage(this.plugin.messageEvent("Vous avez rejoint l'event "+ ChatColor.GOLD + this.event.getFullName()));
		}
		
		ep.getOnlinePlayer().getInventory().setHelmet(this.identity);
	}
	
	public void sendMessage(String message)
	{
		for(EventPlayer ep : this.getPlayers())
		{
			if(ep.getOnlinePlayer() != null)
			{
			ep.getPlayer().getPlayer().sendMessage(this.plugin.messageEvent(message));
			}
		}
		
		
	}
	
	public void leave(EventPlayer ep)
	{

		this.players.remove(ep);
		

		sendMessage(ep.getPlayer().getUserName() + " a quitté l' équipe "+ this.color + this.name + ChatColor.GRAY + " de l'event "+ ChatColor.GOLD + this.event.getFullName());
		
		
		if(!this.event.getTeamType().equals("INDIVIDUAL"))
		{
			ep.getOnlinePlayer().sendMessage(this.plugin.messageEvent("Vous avez quitté "+ this.color + this.name + ChatColor.GRAY +" de l'event "+ ChatColor.GOLD + this.event.getFullName()));
		}
		else
		{
			ep.getOnlinePlayer().sendMessage(this.plugin.messageEvent("Vous avez quitté l'event "+ ChatColor.GOLD + this.event.getFullName()));
				
		}
		
		ep.getOnlinePlayer().getInventory().setHelmet(null);
		
		//Remove de l'équipe
		if(!this.event.getTeamType().equals("FIXED"))
		{
			if(this.players.size() == 0)
			{
				this.event.getTeams().remove(this);
			}
		}
		
		//On retire les parrains
		for(EventPlayer tep : this.event.getEventPlayers())
		{
			if(tep.getParain() == ep)
			{
				tep.setParain(null);
			}
		}
		
	}
	
	public String displayPlayers()
	{
		int i = 0;
		String players = "";
		for(EventPlayer ep : this.getPlayers())
		{
			if(i == 0)
			{
				players = ep.getPlayer().getUserName();
			}
			else
			{
				players = players+", "+ep.getPlayer().getUserName();
			}
		i++;
		}
		return players;
	}
	
	public ItemStack generateIdentity()
	{
		switch(this.color)
		{
		case WHITE: return new ItemStack(Material.WOOL, 1, (short) 0);
		case GOLD: return new ItemStack(Material.WOOL, 1, (short) 1);
		case LIGHT_PURPLE: return new ItemStack(Material.WOOL, 1, (short) 2);
		case BLUE: return new ItemStack(Material.WOOL, 1, (short) 3);
		case YELLOW: return new ItemStack(Material.WOOL, 1, (short) 4);
		case GREEN: return new ItemStack(Material.WOOL, 1, (short) 5);
		case RED: return new ItemStack(Material.WOOL, 1, (short) 6);
		case DARK_GRAY: return new ItemStack(Material.WOOL, 1, (short) 7);
		case GRAY: return new ItemStack(Material.WOOL, 1, (short) 8);
		case AQUA: return new ItemStack(Material.WOOL, 1, (short) 9);
		case DARK_PURPLE: return new ItemStack(Material.WOOL, 1, (short) 10);
		case DARK_BLUE: return new ItemStack(Material.WOOL, 1, (short) 11);
		case DARK_GREEN: return new ItemStack(Material.WOOL, 1, (short) 13);
		case DARK_RED: return new ItemStack(Material.WOOL, 1, (short) 14);
		case BLACK: return new ItemStack(Material.WOOL, 1, (short) 15);
		
		default: return null;
		}
	}
	
}
