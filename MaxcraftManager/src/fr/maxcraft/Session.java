package fr.maxcraft;

import java.util.Date;

import org.bukkit.Bukkit;

public class Session {


	//VARIABLES
	MaxcraftManager plugin;
	int id;
	String player;
	Date startDate;
	Date endDate;
	
	//CONTROLLER
	public Session(MaxcraftManager plugin, String player)
	{
		this.plugin = plugin;
		this.id = this.plugin.getNextId("session");
		this.player = player;
		this.startDate = new Date();
		this.endDate = null;
		
		
		this.plugin.listSessions.add(this);
		
		//Insert sq
		
		this.plugin.mysql_update("INSERT INTO `session` (`id`, `uuid`, `player`, `start`, `end`) VALUES (NULL, '"+Bukkit.getPlayer(this.player).getUniqueId().toString().replace("-", "")+"', '"+this.player+"', '"+this.plugin.formatDate(this.startDate)+"', NULL);");
		
	}
	//GETTER SETTER

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPlayer() {
		return player;
	}

	public void setPlayer(String player) {
		this.player = player;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	
	
	//METHODES
	
	public void remove()
	{
		//update sql
		this.endDate = new Date();
		this.plugin.mysql_update("UPDATE `session` SET `end` = '"+this.plugin.formatDate(this.endDate)+"' WHERE `session`.`id` = "+this.id+";");
		this.plugin.log("#" +this.id+ " Fin de session : "+this.player);
	}
	
}
