package fr.maxcraft;

import java.util.Date;

public class Notif {

	//VARIBABLES
	MaxcraftManager plugin;
	String type;
	String content;
	Date date;
	Maxcraftien target;
	boolean view;
	
	//CONTROLLER
	
	public Notif(MaxcraftManager plugin, Maxcraftien target, String type , String content, boolean view)
	{
		this.plugin = plugin;
		this.type = type;
		this.content = content;
		this.target = target;
		this.date = new Date();
		this.view = view;
		
		//Insert sq
		
		this.plugin.mysql_update("INSERT INTO `maxcraftsite`.`notification` (`id`, `user`, `type`, `date`, `content`, `view`) VALUES (NULL, "+this.target.getId()+", '"+this.type+"', '"+this.plugin.formatDate(this.date)+"', '"+this.content+"', "+this.view+");");
	}
	
	//GETTER SETTER

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Maxcraftien getTarget() {
		return target;
	}

	public void setTarget(Maxcraftien target) {
		this.target = target;
	}
	
	
	
	
	//METHODES
	
}
