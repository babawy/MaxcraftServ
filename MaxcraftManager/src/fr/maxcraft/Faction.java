package fr.maxcraft;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.World;

import com.mysql.jdbc.Statement;


public class Faction {

	//VARIABLES
	private MaxcraftManager plugin;
	private int id;
	private String tag;
	private int user;
	
	private int spawn;
	
	private String name;
	private int capital;
	
	//CONSTRUCTEUR
	public Faction(MaxcraftManager plugin, int id, String tag, int owner, String name, int capital, int spawn)
	{
	
		this.plugin = plugin;
		this.plugin.listFactions.add(this);
		this.id = id;
		this.tag = tag;
		this.user = owner;
		this.spawn = spawn;
		this.name = name;
		this.capital = capital;
	}

	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getTag() {
		return tag;
	}


	public void setTag(String tag) {
		this.tag = tag;
	}


	


	public int getUser() {
		return user;
	}


	public void setUser(int user) {
		this.user = user;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getCapital() {
		return capital;
	}


	public void setCapital(int capital) {
		this.capital = capital;
	}
	
	public void saveSpawn(int spawn) {
		this.spawn = spawn;
		
		this.plugin.mysql_update("UPDATE `faction` SET `spawn` = "+ spawn +" WHERE `faction`.`id` = "+this.id+";");
	}

	
	//METHODES

	





	public ArrayList<Maxcraftien> getUsers()
	{
		ArrayList<Maxcraftien> users = new ArrayList<Maxcraftien>();
		for(Maxcraftien m : this.plugin.listMaxcraftiens)
		{
			if(m.getFaction() == this)
			{
				users.add(m);
			}
		}
		return users;
	}
	
	public String getDisplayUsers()
	{
		ArrayList<Maxcraftien> users = this.getUsers();
		
		String list = null;
		int i = 0;
		for(Maxcraftien m : users)
		{
			if(i == 0)
			{
				list = m.getUserName();
			}
			else
			{
			list = list + ", " + m.getUserName();
			}
			i++;
		}
		return list;
		
	}
	
	public Maxcraftien getOwner()
	{
		return this.plugin.getMaxcraftien(this.user);
	}
	
	public void loadFromSql() throws SQLException
	{
		ResultSet result = this.plugin.mysql_query("SELECT * FROM faction WHERE id = "+ this.id, true);
		this.name = result.getString("name");
		this.user = result.getInt("user");
		this.tag = result.getString("tag");
	}
	
	public String getStateWith(Faction f)
	{
		for(FactionState fs: this.plugin.listFactionStates)
		{
			if((fs.getFaction1() == this && fs.getFaction2() == f) || (fs.getFaction2() == this && fs.getFaction1() == f))
			{
				return fs.getState();
			}
		}
		
		return "NEUTRE";
		
	}
	
	public ArrayList<Faction> getEnemyFactions()
	{
		ArrayList<Faction> factions = new ArrayList<Faction>();
		
		for(FactionState fs: this.plugin.listFactionStates)
		{
			if(fs.getFaction1() == this && fs.getState().equals("ENEMY"))
			{
				factions.add(fs.getFaction2());
			}
			if(fs.getFaction2() == this && fs.getState().equals("ENEMY"))
			{
				factions.add(fs.getFaction1());
			}
		}
		return factions;
		
	}
	
	public ArrayList<Faction> getAlliesFactions()
	{
		ArrayList<Faction> factions = new ArrayList<Faction>();
		
		for(FactionState fs: this.plugin.listFactionStates)
		{
			if(fs.getFaction1() == this && fs.getState().equals("FRIEND"))
			{
				factions.add(fs.getFaction2());
			}
			if(fs.getFaction2() == this && fs.getState().equals("FRIEND"))
			{
				factions.add(fs.getFaction1());
			}
		}
		return factions;
		
	}
	
	
	public double getMoney()
	{
		return this.plugin.economy.getBalance(this.tag);
	}
	
	public Lieu getSpawn()
	{
		if(this.haveSpawn())
		{
			return this.plugin.getLieu(this.spawn);
		}
		else
		{
			return null;
		}
	}
	
	public boolean haveSpawn()
	{
		if(this.spawn != 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void setSpawn(Location l)
	{
		if(this.haveSpawn())
		{
			//D�ja un spawn
			this.getSpawn().setLocation(l);
			this.getSpawn().updateInSql();
			
			//Dynmap marker portal
			if(this.getSpawn().isLinkedToPortal())
			{
				this.getSpawn().getPortal().reloadMarker();
			}
		}
		else
		{
			Lieu spawn = new Lieu (this.plugin, this.plugin.getNextId("lieu"), this.getOwner(), l, "Faction " + this.tag, true, "tower", false);
			spawn.saveInSql();
			this.saveSpawn(spawn.getId());
		
		}
			
	}
	
	public void remove()
	{
		for(Maxcraftien m : this.getUsers())
		{
			m.setFaction(null);
		}
		
		for(FactionState fs : this.plugin.listFactionStates)
		{
			if(fs.getFaction1() == this || fs.getFaction2() == this)
			{
				fs.remove();
			}
		}
		this.plugin.listFactions.remove(this);
		
	}
	
}
