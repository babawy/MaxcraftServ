package fr.maxcraft;

import java.sql.ResultSet;
import java.sql.SQLException;



import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import com.mysql.jdbc.Statement;

public class Maxcraftien {

	//VARIABLES
	private String userName;
	private MaxcraftManager plugin;
	private int id;
	private Faction faction;
	
	private Location selector1;
	private Location selector2;

	private boolean actif;
	private int gametime;
	private String uuid;
	
	//CONSTRUCTEUR
	public Maxcraftien(MaxcraftManager plugin, int id, String uuid, String userName, int faction, boolean isActif, int gametime)
	{
		this.plugin = plugin;

		this.userName = userName;
		this.uuid = uuid;
		this.id = id;
		this.actif = isActif;
		this.plugin.listMaxcraftiens.add(this);
		this.faction = plugin.getFaction(faction);
		this.gametime = gametime;
	}
	
	
	public boolean isOnline()
	{
		Collection<? extends Player> playerlist = this.plugin.getServer().getOnlinePlayers();
		for(Player p : playerlist)
		{
			if(p.getName().equals(this.userName)){
				return true;
			}
		}
		return false;
	}
	
	public Player getPlayer()
	{
		Collection<? extends Player> playerlist = this.plugin.getServer().getOnlinePlayers();
		for(Player p : playerlist)
		{
			if(p.getName().equals(this.userName)){
				return p;
			}
		}
		return null;
	}
	
	public void removeMaxcraftien()
	{
		this.plugin.listMaxcraftiens.remove(this);
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	

	public Location getSelector1() {
		return selector1;
	}


	public void setSelector1(Location selector1) {
		this.selector1 = selector1;
	}


	public Location getSelector2() {
		return selector2;
	}


	public void setSelector2(Location selector2) {
		this.selector2 = selector2;
	}


	public Faction getFaction() {
		return faction;
	}


	public void setFaction(Faction faction) {
		this.faction = faction;
	}
	
	public boolean haveFaction()
	{
		if(this.faction != null)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/*
	 * Rechargement depuis SQL
	 */
	public void reloadFromSql() throws SQLException
	{
		ResultSet result = plugin.mysql_query("SELECT * FROM user WHERE id = "+ this.id, true);
		
		this.userName = result.getString("username");
		
	}
	
	public String getChatName()
	{
		if(this.haveFaction())
		{
		return "["+ this.faction.getTag() +"] " + this.userName;
		}
		else
		{
		return this.userName;
		}
	}
	
	public void loadFromSql() throws SQLException
	{
		ResultSet result = this.plugin.mysql_query("SELECT * FROM user WHERE id = "+ this.id, true);
		this.userName = result.getString("username");
		this.uuid = result.getString("uuid");
		this.faction = this.plugin.getFaction(result.getInt("faction"));

	}
	
	public String getStateWith(Maxcraftien m)
	{
		if(m.haveFaction() && this.haveFaction() && m.getFaction() == this.getFaction())
		{
			return "FACTION";
		}
		else if(m.haveFaction() && this.haveFaction())
		{
			return this.faction.getStateWith(m.faction);
		}
		else
		{
			return "NEUTRE";
		}
	}
	
	public String displayFor(Player p)
	{
		
		if(this.plugin.isMaxcraftien(p.getName()))
		{
			Maxcraftien m = this.plugin.getMaxcraftien(p);
			String state = m.getStateWith(this);
			
			if(state.equals("FACTION"))
			{
				return ChatColor.GREEN + this.getChatName();
			}
			else if(state.equals("FRIEND"))
			{
				return ChatColor.DARK_AQUA + this.getChatName();
			}
			else if(state.equals("NEUTRE"))
			{
				return this.getChatName();
			}
			else if(state.equals("ENEMY"))
			{
				return ChatColor.RED + this.getChatName();
			}
			else
			{
				return this.getChatName();
			}

		}
		else
		{
			return ChatColor.WHITE + this.getChatName();
		}
	}
	
	public String tagFor(Player p)
	{
		if(this.plugin.isMaxcraftien(p.getName()))
		{
			Maxcraftien m = this.plugin.getMaxcraftien(p);
			String state = m.getStateWith(this);
			
			if(state.equals("FACTION"))
			{
				return ChatColor.GREEN + this.getUserName();
			}
			else if(state.equals("FRIEND"))
			{
				return ChatColor.DARK_AQUA + this.getUserName();
			}
			else if(state.equals("NEUTRE"))
			{
				return this.getUserName();
			}
			else if(state.equals("ENEMY"))
			{
				return ChatColor.RED + this.getUserName();
			}
			else
			{
				return this.getUserName();
			}
			
			
		}
		else
		{
			return ChatColor.WHITE + this.getUserName();
		}
	}
	
	public boolean isFactionOwner()
	{
		if(this.haveFaction())
		{
			if(this.faction.getOwner() == this)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}


	public boolean isActif() {
		return actif;
	}


	public void setActif(boolean actif) {
		this.actif = actif;
	}


	public int getGametime() {
		return gametime;
	}


	public void setGametime(int gametime) {
		this.gametime = gametime;
	}


	public String getUuid() {
		return uuid;
	}


	public void setUuid(String uuid) {
		this.uuid = uuid;
	}


	public void updatename(String newName) {
		if (!newName.equals(userName)){
			double ex = this.plugin.economy.getBalance(this.userName);
			this.plugin.economy.bankDeposit(newName,ex);
			this.userName = newName;
			this.plugin.mysql_update("UPDATE `user` SET `username` = '"+this.userName+"' WHERE `user`.`id` = "+this.id+";");
			this.plugin.log(userName+" : Pseudo mis a jour!");
		}
	}
	
	
	
}
