package fr.maxcraft;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.dynmap.markers.Marker;
import org.dynmap.markers.PolyLineMarker;

public class Portal {

	
	//VARIABLES
	int id;
	MaxcraftManager plugin;
	Lieu lieu1;
	Lieu lieu2;
	boolean ouvert;
	PolyLineMarker marker;
	//CONSTRUCTOR
	
	public Portal(MaxcraftManager plugin,int id, Lieu lieu1, Lieu lieu2, boolean ouvert)
	{
		this.id = id;
		this.plugin = plugin;
		this.lieu1 = lieu1;
		this.lieu2 = lieu2;
		this.ouvert = ouvert;
		
		this.plugin.listPortals.add(this);
		
		if(ouvert)
		{
			this.createDoor();
		}
		
		this.createMarker();
	}
	
	//GETTER SETTERS

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Lieu getLieu1() {
		return lieu1;
	}

	public void setLieu1(Lieu lieu1) {
		this.lieu1 = lieu1;
	}

	public Lieu getLieu2() {
		return lieu2;
	}

	public void setLieu2(Lieu lieu2) {
		this.lieu2 = lieu2;
	}

	public boolean isOuvert() {
		return ouvert;
	}

	public void setOuvert(boolean ouvert) {
		this.ouvert = ouvert;
	}
	

	//METHODES
	
	public boolean isActivated()
	{
		return true;
	}
	
	public void createDoor()
	{
		
		Material portal = Material.AIR;
		this.lieu1.getLocation().clone().getBlock().setType(portal);
		this.lieu1.getLocation().clone().add(0,1,0).getBlock().setType(portal);
		this.lieu2.getLocation().clone().getBlock().setType(portal);
		this.lieu2.getLocation().clone().add(0,1,0).getBlock().setType(portal);
	}
	
	public void remove()
	{
		this.removeMarker();
		this.plugin.listPortals.remove(this);
		this.plugin.mysql_update("DELETE FROM `portal` WHERE `portal`.`id` = "+this.id+";");
	}
	
	public String getName()
	{
		return this.lieu1.getName() + " <=> " +this.lieu2.getName();
	}
	
	public void saveInSql()
	{
		this.plugin.mysql_update("INSERT INTO `portal` (`id`, `lieu1`, `lieu2`, `ouvert`) VALUES (NULL, '"+ this.lieu1.getId() +"', '"+ this.lieu2.getId() +"', "+ this.ouvert +");");
	}
	
	public void createMarker()
	{
		if(!this.isTransWorld())
		{
		double[] x = new double[2]; 
		double[] y = new double[2]; 
		double[] z = new double[2]; 
		x[0] = (double) this.lieu1.getLocation().getBlockX();
		y[0] = (double) this.lieu1.getLocation().getBlockY();
		z[0] = (double) this.lieu1.getLocation().getBlockZ();
		x[1] = (double) this.lieu2.getLocation().getBlockX();
		y[1] = (double) this.lieu2.getLocation().getBlockY();
		z[1] = (double) this.lieu2.getLocation().getBlockZ();
		
		String id = new Integer(this.id).toString();
		
		this.marker = this.plugin.portalMarkers.createPolyLineMarker(id, this.getName(), true, this.lieu1.getLocation().getWorld().getName(), x, y, z, true);
		this.marker.setLineStyle(3, 0.8,0x00AB3C);
		
		}
	}
	
	public void reloadMarker()
	{
		if(!this.isTransWorld())
		{
		this.marker.deleteMarker();
		this.createMarker();
		}
	}
	
	public void removeMarker()
	{
		if(!this.isTransWorld())
		{
		this.marker.deleteMarker();
		}
	}
	
	public boolean isTransWorld()
	{
		if(this.lieu1.getLocation().getWorld().equals(this.lieu2.getLocation().getWorld()))
		{
			return false;
		}
		else
		{
		return true;
		}
	}
	
}
