package fr.maxcraft;

import org.bukkit.Location;
import org.bukkit.entity.Player;


public class Travel {

	MaxcraftManager plugin;
	Lieu l1;
	Lieu l2;
	Lieu boat;
	int boatRadius;
	int startRadius;
	int travelTime;
	boolean open;
	int id;
	Lieu start;
	
	public Travel(MaxcraftManager plugin, int id, Lieu l1, Lieu l2, Lieu boat, int startRadius, int boatRadius, int travelTime, boolean open)
	{
		this.plugin = plugin;
		this.boatRadius = boatRadius;
		this.startRadius = startRadius;
		this.travelTime = travelTime;
		this.boat = boat;
		this.l1 = l1;
		this.l2 = l2;
		this.open = open;
		this.id = id;
		
		
		this.plugin.listTravels.add(this);
	}
	
	public Lieu getStart(Location l)
	{
		if(l.getWorld().equals(l1.getLocation().getWorld()) && l.distance(l1.location) <= startRadius)
		{
			return l1;
		}
		
		if(l.getWorld().equals(l2.getLocation().getWorld()) && l.distance(l2.location) <= startRadius)
		{
			return l2;
		}
		
		return null;
	}
	
	public Lieu getDestination()
	{
		if(this.start == l1)
		{
			return this.l2;
		}
		
		if(this.start == l2)
		{
			return this.l1;
		}
		
		return null;
	}
	
	public void travel(Player p, Lieu start)
	{
		this.start = start;
		p.sendMessage(this.plugin.message("Patientez 5 secondes ici le temps de larguer les amarres..."));
		//Départ
		new TravelTask(this.plugin, p, start, this.boat ,startRadius, "Profitez du voyage pour admirer la vue ! ("+this.travelTime+" sec)").runTaskLater(this.plugin, 5*20);
		//Arrivée
		new TravelTask(this.plugin, p, this.boat, this.getDestination() ,boatRadius, "Vous pouvez descendre du bateau, allez y avant qu'il ne reparte !").runTaskLater(this.plugin, 5*20+this.travelTime*20);
		
	}

	public Lieu getL1() {
		return l1;
	}

	public void setL1(Lieu l1) {
		this.l1 = l1;
	}

	public Lieu getL2() {
		return l2;
	}

	public void setL2(Lieu l2) {
		this.l2 = l2;
	}

	public Lieu getBoat() {
		return boat;
	}

	public void setBoat(Lieu boat) {
		this.boat = boat;
	}

	public int getBoatRadius() {
		return boatRadius;
	}

	public Lieu getStart() {
		return start;
	}

	public void setStart(Lieu start) {
		this.start = start;
	}

	public void setBoatRadius(int boatRadius) {
		this.boatRadius = boatRadius;
	}

	public int getStartRadius() {
		return startRadius;
	}

	public void setStartRadius(int startRadius) {
		this.startRadius = startRadius;
	}

	public int getTravelTime() {
		return travelTime;
	}

	public void setTravelTime(int travelTime) {
		this.travelTime = travelTime;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}
	
	public void remove()
	{
		this.plugin.listTravels.remove(this);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
