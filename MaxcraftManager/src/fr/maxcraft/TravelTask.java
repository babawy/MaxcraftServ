package fr.maxcraft;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;


public class TravelTask extends BukkitRunnable{

	private MaxcraftManager plugin;

	private Lieu destination;
	private int radius;
	private String message;
	private Player player;
	private Lieu start;
	


	public TravelTask(MaxcraftManager plugin, Player player,  Lieu start, Lieu destination, int radius, String message)
	{
		this.plugin = plugin;
		this.destination = destination;
		this.player = player;
		this.message = message;
		this.radius = radius;
		this.start = start;
		
	}
	
	 public void run() {
	        // What you want to schedule goes here
		 if(!player.getLocation().getBlock().getType().equals(Material.WATER) && player.getLocation().getWorld().equals(start.getLocation().getWorld()) && player.getLocation().distance(start.location) <= radius)
		 {
			
			 Location d;
			 Location a;
			 d = start.location.clone();
			 d.setX(Math.floor(d.getX()));
			 d.setY(Math.floor(d.getY()));
			 d.setZ(Math.floor(d.getZ()));
			 d.setPitch(player.getLocation().getPitch());
			 d.setYaw(player.getLocation().getYaw());
			 d.setWorld(player.getLocation().getWorld());
			 
			 a = destination.location.clone();
			 a.setWorld(d.getWorld());
			 a.setX(Math.floor(a.getX()));
			 a.setY(Math.floor(a.getY()));
			 a.setZ(Math.floor(a.getZ()));
			 a.setPitch(player.getLocation().getPitch());
			 a.setYaw(player.getLocation().getYaw());
			 d.setWorld(player.getLocation().getWorld());
			 
			 Location vecteur = player.getLocation().subtract(d);
			 Location dest = a.add(vecteur);
			 dest.setWorld(this.destination.getLocation().getWorld());
			 player.teleport(dest);
			 
			 player.sendMessage(this.plugin.message(message));
		 }

	    }
}
