package fr.maxcraft;

import java.lang.reflect.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import fr.maxcraft.Event;


public class MaxcraftCommandManager {

	
private MaxcraftManager plugin;
	
	public MaxcraftCommandManager(MaxcraftManager plugin)
	{
		this.plugin = plugin;
	}
	
	public void commandGps(CommandSender sender, String[] args)
	{
		if(args.length >= 1 && !args[0].equals("*"))
		{
			if(! this.plugin.isInt(args[0]))
			{
				sender.sendMessage(this.plugin.message(ChatColor.RED + "Vous devez entrer le numéro de la destination, faites /gps !"));
				return;
			}
			else
			{
				Lieu l = this.plugin.getLieu(Integer.parseInt(args[0]));
				if(l == null || ! l.getOnGps())
				{
					sender.sendMessage(this.plugin.message(ChatColor.RED + "Ce n'est pas une destination, faites /gps !"));	
					return;
				}
				else
				{
					//C'est bien une destination
					Player p = (Player) sender;
					if(! p.getWorld().equals(l.getLocation().getWorld()))
					{
						sender.sendMessage(this.plugin.message(ChatColor.RED + "Cette destination se trouve dans un monde parallèle !"));	
						return;
					}
					int distance = (int) Math.round(p.getLocation().distance(l.getLocation()));
					p.setCompassTarget(l.getLocation());
					sender.sendMessage(this.plugin.message("Votre boussole vous indique désormais <"+ ChatColor.GOLD + l.getName() + ChatColor.GRAY +">, "+ distance +" m è parcourir. " ));	
					return;
					
				}
			}
		}
		else
		{
			// Liste des positions
			sender.sendMessage(this.plugin.message(ChatColor.GREEN + "*** DESTINATIONS ***"));
			sender.sendMessage(this.plugin.message("Faites /gps [numéro de destination] pour que votre boussole l'indique."));
			
			Player p = (Player) sender;
			
			//TRI DES LIEUX
			int distance;
			int min = 20000000;
			Lieu proche = null;
			
			ArrayList<Lieu> listLieux = (ArrayList<Lieu>) this.plugin.listLieux.clone();
			ArrayList<Lieu> parsedLieux = new ArrayList<Lieu>();
			int size = listLieux.size();
			
			for(int i = 0 ; i < size; i++)
			{
			
			for(Lieu l : listLieux)
			{
				if(l.getOnGps() && l.getLocation().getWorld().equals(p.getWorld()))
				{
				
				distance = (int) l.getLocation().distance(p.getLocation());
				
				if(distance < min)
				{
					min = distance;
					proche = l;
				}
				
				}
			}
			
			parsedLieux.add(proche);
			listLieux.remove(proche);
			min = 20000000;
			proche = null;
			
			}
			
			int max = 10;
			
			if(args.length == 1 && args[0].equals("*"))
			{
				max = 1000000;
			}
			int nb = 0;
			
			for(Lieu l : parsedLieux)
			{
				if(l != null && nb < max)
				{
					distance = (int) l.getLocation().distance(p.getLocation());
					sender.sendMessage("- ["+l.getId()+"] "+ ChatColor.GOLD+ l.getName()+ ChatColor.GRAY+ " ("+distance+" m)");
					nb++;
					
				}
			}
			
			//Give de boussole si pas de boussole
			if(!p.getInventory().contains(Material.COMPASS))
			{
				ItemStack compass = new ItemStack(Material.COMPASS, 1);
				ItemMeta meta = compass.getItemMeta().clone();
				meta.setDisplayName("GPS");
				List<String> lore = new ArrayList<String>();
				lore.add("Boussole GPS, faites /gps pour vous diriger.");
				meta.setLore(lore);
				compass.setItemMeta(meta);
				p.getInventory().addItem(compass);
			}
			
			
			return;
		}
	}
	
	public void commandMaxcraft(CommandSender sender, String[] args)
	{
		if(args.length >= 1)
		{
		
			switch(args[0])
			{
			case "travel": case "t": case "voyages": case "voyage":
				this.travel(sender, args);
				return;
			case "portal": case "portail": case "p": case "gate":
				this.portal(sender, args);
				return;
			case "lieu": case "point":
				this.lieu(sender, args);
				return;
			case "list": case "liste":
				this.list(sender, args);
				return;
			case "load": case "l":
				this.load(sender, args);
				return;
			default:
				sender.sendMessage(this.plugin.message("Il manque des paramétres..."));
				return;
		
			}
		}
		
	this.noparameter(sender);
	}
	
	public void commandMourir(CommandSender sender, String[] args)
	{
		Maxcraftien m = this.plugin.getMaxcraftien((Player) sender);
		
		Player p = (Player) sender;
		if(m == null)
		{
		return;
		}
		
		new AutoKillTask(this.plugin, p).runTaskLater(this.plugin, 10*20);
		
		p.sendMessage(this.plugin.message("Vous allez mourir... Patience. (10 secondes)"));
	}

	public void commandEvent(CommandSender sender, String[] args)
	{
		if(args.length == 0)
		{
			Maxcraftien m = this.plugin.getMaxcraftien((Player) sender);
			if(m == null) return;
			Event e = this.plugin.getCurrentEvent();
			
			if(e==null)
			{
				sender.sendMessage(this.plugin.messageEvent("Il n'y a aucun event ouvert."));
				return;
			}
			
			for(Event ev : this.plugin.listEvents)
			{
				this.teamsEvent(sender, ev.getName());
			}
			return;
			
		}
		
		if(args.length >= 1)
		{
		
			switch(args[0])
			{
			case "admin": case "adm": case "a": case "gerer":
				this.adminEvent(sender, args);
				return;
			case "teams": case "team": case "t": case "èquipes": case "equipes": case "groups": case "groupes":
				this.teamsEvent(sender, args[1]);
				return;
			case "join": case "rejoindre": case "j": case "entrer": case "participer": case "jouer": case "concourir":
				this.joinEvent(sender, args);
				return;
			case "quit": case "exit": case "leave":  case "l": case "sortir": case "arreter": case "renoncer": case "camefaitchier": case "partir": case "jevaismangerfinalement":
				this.quitEvent(sender, args);
				return;
			case "m": case "message": case "msg":  case "talk": case "say":
				this.sendMessageEvent(sender, args);
				return;
			case "teleport": case "tp":  case "s":
				this.teleportEvent(sender, args);
				return;
			case "ci": case "clearinventory":  case "clear":
				this.clearInventoryEvent(sender, args);
				return;

			default:
				sender.sendMessage(this.plugin.message("Il manque des paramètres..."));
				return;
		
			}
		}
		
	this.noparameter(sender);
	}
	
	public void commandFaction(CommandSender sender, String[] args)
	{
		if(args.length >= 1)
		{
		
			switch(args[0])
			{
			case "list": case "l": case "liste":
				this.listFactions(sender, args);
				return;
			case "info": case "i": case "infos":
				this.infoFaction(sender, args);
				return;
			case "admin": case "a": case "chef":
				this.adminFaction(sender, args);
				return;
			default:
				sender.sendMessage(this.plugin.message("Il manque des paramètres..."));
				return;
		
			}
		}
		
		this.noparameter(sender);
	}
	



public void portal(CommandSender sender, String[] args)
{
	if(args.length >= 2)
	{
	
		switch(args[1])
		{
		case "add": case "new": case "create": case "define": case "place": case "n": case "link":
			this.addPortal(sender, args);
			return;
		case "remove": case "r": case "rm": case "delete": case "supprimer":
			this.removePortal(sender, args);
			return;
		default:
			this.noparameter(sender);
			return;
	
		}
	}
	
	this.noparameter(sender);
	return;
}

public void travel(CommandSender sender, String[] args)
{
	if(args.length >= 2)
	{
	
		switch(args[1])
		{
		case "add": case "new": case "create": case "define": case "place": case "n": case "link":
			this.addTravel(sender, args);
			return;
		case "remove": case "r": case "rm": case "delete": case "supprimer":
			this.removeTravel(sender, args);
			return;
		default:
			this.noparameter(sender);
			return;
	
		}
	}
	
	this.noparameter(sender);
	return;
}

public void lieu(CommandSender sender, String[] args)
{
	if(args.length >= 2)
	{
	
		switch(args[1])
		{
		case "add": case "new": case "create": case "define": case "place": case "n":
			this.addLieu(sender, args);
			return;
		case "remove": case "r": case "rm": case "delete": case "supprimer":
			this.removeLieu(sender, args);
			return;
		case "deplace": case "d": case "change": case "replace": case "move": case "m":
			this.moveLieu(sender, args);
			return;
		case "info": case "infos": case "i": case "information":
			this.infoLieu(sender, args);
			return;
		case "gps":
			this.gpsLieu(sender, args);
			return;
		default:
			this.noparameter(sender);
			return;
	
		}
	}
	
	this.noparameter(sender);
	return;
}

public void list(CommandSender sender, String[] args)
{
	if(args.length >= 2)
	{
	
		switch(args[1])
		{
		case "faction": case "factions": case "f": case "fact":
			this.listFactions(sender, args);
			return;
		case "m": case "maxcraftien": case "maxcraftiens": case "p": case "player": case "players": case "joueurs":
			this.listMaxcratiens(sender, args);
			return;
		case "lieu": case "lieux": case "points": case "l":
			this.listLieux(sender, args);
			return;
		case "portals": case "portal": case "portails": case "portail":
			this.listPortals(sender, args);		
			return;
		case "travels": case "travel": case "t":
			this.listTravels(sender, args);		
			return;
		case "factionstates": case "fs": case "states": case "war":
			this.listFactionStates(sender, args);		
			return;

		default:
			this.noparameter(sender);
			return;
	
		}
	}
	
	this.noparameter(sender);
	return;
}

public void load(CommandSender sender, String[] args)
{
	if(args.length >= 2)
	{
	
		switch(args[1])
		{
		case "faction": case "factions": case "f": case "fact":
			
			try {
				this.plugin.loadFactionFromSql(Integer.parseInt(args[2]));
			} catch (NumberFormatException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			
			return;
		case "m": case "maxcraftien": case "maxcraftiens": case "p": case "player": case "players": case "joueurs":
			
			try {
				this.plugin.loadMaxcraftienFromSql(Integer.parseInt(args[2]));
			} catch (NumberFormatException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			return;
			
case "factionstate": case "fs": 
			
			try {
				this.plugin.loadFactionStateFromSql(Integer.parseInt(args[2]));
			} catch (NumberFormatException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			return;
			
		default:
			this.noparameter(sender);
			return;
	
		}
	}
	
	this.noparameter(sender);
	return;
}


public void addLieu(CommandSender sender, String[] args)
{
	
	
	String name = this.plugin.multiArgs(args, 2);
	Player p = (Player) sender;
	Maxcraftien m = this.plugin.getMaxcraftien(p);
	
	//Si dèja un lieu
	if(this.plugin.isOnLieu(p.getLocation()) != null)
	{
		p.sendMessage("Il y a déjà un lieu ici !");
		return;
	}
		
	
	Lieu l = new Lieu(this.plugin, this.plugin.getNextId("lieu"), m, p.getLocation(), name, true, "yellowflag", false);
	l.saveInSql();
	p.sendMessage(this.plugin.message("Le lieu a été enregisté : #" + ChatColor.GOLD + l.getId() + " "+ l.getName()));
	return;
}


public void removeLieu(CommandSender sender, String[] args)
{
	Player p = (Player) sender;
	if(args.length == 3)
	{
		Lieu l = this.plugin.getLieu(Integer.parseInt(args[2]));
	}
	else
	{
	if(this.plugin.isOnLieu(p.getLocation()) == null)
	{
		p.sendMessage("Vous n'êtes pas sur un lieu !");
		return;
	}
	}
	
	Lieu l = this.plugin.isOnLieu(p.getLocation());
	
	l.remove();
p.sendMessage(this.plugin.message("Le lieu "+ ChatColor.WHITE + l.getName()+ ChatColor.GRAY +" a été supprimé ainsi que les portails liés."));
return;
}


public void moveLieu(CommandSender sender, String[] args)
{
	Lieu l = this.plugin.getLieu(Integer.parseInt(args[2]));
	Player p = (Player) sender;
	//Portal marker reload
	if(l.isLinkedToPortal())
	{
		l.getPortal().reloadMarker();
	}
	
	l.setLocation(p.getLocation());
	l.updateInSql();
	sender.sendMessage(this.plugin.message("Le lieu " + ChatColor.GOLD + l.getName() + ChatColor.GRAY + " a été déplacé !"));
	return;
}

public void infoLieu(CommandSender sender, String[] args)
{
	Player p = (Player) sender;
	Block b = p.getLocation().getBlock();
	
	if(this.plugin.isOnLieu(p.getLocation()) != null)
	{
		Lieu l = this.plugin.isOnLieu(p.getLocation());
		p.sendMessage(this.plugin.message("** LIEU : "+ ChatColor.GOLD + l.getName()));
		p.sendMessage(this.plugin.message("Numéro : "+ ChatColor.GREEN + l.getId()));
		p.sendMessage(this.plugin.message("Propriétaire : "+ ChatColor.GREEN + l.getOwner().getUserName()));
	}
	else
	{
		sender.sendMessage(this.plugin.message("Il n'y a aucun lieu ici !"));
		return;
	}
	
	return;
}

public void listFactions(CommandSender sender, String[] args)
{
	sender.sendMessage(this.plugin.message("*** LISTE DES FACTIONS ***"));
	for(Faction f : this.plugin.listFactions)
	{
		sender.sendMessage("- "+f.getTag()+" : "+f.getName());
	}
	return;
}

public void infoFaction(CommandSender sender, String[] args)
{
	Faction f = null;
	if(args.length == 1)
	{
		Maxcraftien m = this.plugin.getMaxcraftien((Player) sender);
		if(m != null && m.haveFaction())
		{
			f = m.getFaction();
		}
	}
	else
	{
	f = this.plugin.getFaction(args[1]);
	}
	
	if(f == null)
	{
		this.error(sender);
		return;
	}
	

		sender.sendMessage("");
		sender.sendMessage(this.plugin.message("*** FACTION "+ ChatColor.GOLD + f.getTag()+ ChatColor.GRAY+ " ***"));
		sender.sendMessage(ChatColor.GREEN+ "Tag : " + ChatColor.GRAY + f.getTag());
		sender.sendMessage(ChatColor.GREEN+ "Nom complet : "+ ChatColor.GRAY + f.getName());
		if(f.haveSpawn())
		{
		sender.sendMessage(ChatColor.GREEN+ "Spawn : "+ ChatColor.GRAY + f.getSpawn().getName());
		}
		sender.sendMessage("");
		sender.sendMessage(ChatColor.GREEN+ "Compte de faction : "+ ChatColor.GRAY + this.plugin.economy.format(f.getMoney()));
		sender.sendMessage(ChatColor.GREEN+ "Capital : "+ ChatColor.GRAY + this.plugin.economy.format((double) f.getCapital()));
		sender.sendMessage("");
		sender.sendMessage(ChatColor.GREEN+ "Chef de faction : "+ ChatColor.GRAY + f.getOwner().getUserName());
		sender.sendMessage(ChatColor.GREEN+ "Membres ("+f.getUsers().size()+") : "+ ChatColor.GRAY + f.getDisplayUsers());
		sender.sendMessage("");
		sender.sendMessage(ChatColor.GREEN+ "Factions alliées ("+f.getAlliesFactions().size()+") : "+ ChatColor.GRAY + this.plugin.displayFactionList(f.getAlliesFactions()));
		sender.sendMessage(ChatColor.GREEN+ "Factions enemies ("+f.getEnemyFactions().size()+") : "+ ChatColor.GRAY + this.plugin.displayFactionList(f.getEnemyFactions()));
		
	
	return;
}

public void adminFaction(CommandSender sender, String[] args)
{
	//Pas fait
	this.error(sender);
	return;
}

public void listPortals(CommandSender sender, String[] args)
{
	sender.sendMessage(this.plugin.message("*** LISTE DES PORTAILS ***"));
	
	
	for(Portal p : this.plugin.listPortals)
	{
		sender.sendMessage("- ["+p.getId()+"] "+ p.getLieu1().getName()+" ("+p.getLieu1().getId()+") <=> "+ p.getLieu2().getName() + " ("+p.getLieu2().getId()+")");
	}
	return;
}

public void listTravels(CommandSender sender, String[] args)
{
	sender.sendMessage(this.plugin.message("*** LISTE DES TRAVELS ***"));
	
	
	for(Travel t : this.plugin.listTravels)
	{
		sender.sendMessage("- ["+t.getId()+"] "+ t.getL1().getName()+" ("+t.getL1().getId()+") <=> "+ t.getBoat().getName() + " ("+t.getBoat().getId()+") <=> "+ t.getL2().getName() + " ("+t.getL2().getId()+")");
	}
	return;
}

public void listMaxcratiens(CommandSender sender, String[] args)
{
	sender.sendMessage(this.plugin.message("*** LISTE DES JOUEURS ***"));
	Player p = (Player) sender;
	
	for(Maxcraftien m : this.plugin.listMaxcraftiens)
	{
		sender.sendMessage("- "+ m.displayFor(p));
	}
	return;
}

public void listLieux(CommandSender sender, String[] args)
{
	sender.sendMessage(this.plugin.message("*** LISTE DES LIEUX ***"));
	Player p = (Player) sender;
	
	for(Lieu l : this.plugin.listLieux)
	{
		sender.sendMessage("- ["+l.getId()+"] "+ l.getName());
	}
	return;
}

public void listFactionStates(CommandSender sender, String[] args)
{
	sender.sendMessage(this.plugin.message("*** LIST DES FACTIONSTATES ***"));
	for(FactionState fs : this.plugin.listFactionStates)
	{
		sender.sendMessage("- "+fs.getFaction1().getTag() + " <"+ fs.getState()+"> "+fs.getFaction2().getTag());
	}
}

public void addPortal(CommandSender sender, String[] args)
{

	int l1 = Integer.parseInt(args[2]);
	int l2 = Integer.parseInt(args[3]);
	
	if(this.plugin.getLieu(l1) == null)
	{
		sender.sendMessage(this.plugin.message("Le permier lieu n'existe pas !"));
	}
	else if(this.plugin.getLieu(l2) == null)
	{
		sender.sendMessage(this.plugin.message("Le second lieu n'existe pas !"));
	}
	
	Lieu lieu1 = this.plugin.getLieu(l1);
	Lieu lieu2 = this.plugin.getLieu(l2);
	
	for(Portal p : this.plugin.listPortals)
	{
		if(p.getLieu1().equals(lieu1) || p.getLieu2().equals(lieu2) || p.getLieu1().equals(lieu2) || p.getLieu2().equals(lieu1) )
		{
		
		sender.sendMessage(this.plugin.message("Il existe déja un portail avec l'un de ces lieux !"));
		return;
	
		}
	}
	
	
	Portal p = new Portal(this.plugin, this.plugin.getNextId("portal"), lieu1, lieu2, true);
	p.saveInSql();
	
	sender.sendMessage(this.plugin.message("Portail crée : "+ ChatColor.GOLD + p.getLieu1().getName() + ChatColor.WHITE+ " <-> "+ ChatColor.GOLD+ p.getLieu2().getName() +" "));
	return;
}

public void removePortal(CommandSender sender, String[] args)
{
	int l1 = Integer.parseInt(args[2]);
	int l2 = Integer.parseInt(args[3]);
	
	if(this.plugin.getLieu(l1) == null)
	{
		sender.sendMessage(this.plugin.message("Le permier lieu n'existe pas !"));
	}
	else if(this.plugin.getLieu(l2) == null)
	{
		sender.sendMessage(this.plugin.message("Le second lieu n'existe pas !"));
	}
	
	Lieu lieu1 = this.plugin.getLieu(l1);
	Lieu lieu2 = this.plugin.getLieu(l2);
	
	for(Portal p : this.plugin.listPortals)
	{
		if((p.getLieu1().equals(lieu1) && p.getLieu2().equals(lieu2)) ||( p.getLieu1().equals(lieu2) || p.getLieu2().equals(lieu1)) )
		{
		p.remove();
		sender.sendMessage(this.plugin.message("Le portal a été supprimé."));
		return;
	
		}
	}
	sender.sendMessage(this.plugin.message("Il n'y a aucun portail entre ces deux lieux !"));
	return;
}



public void error(CommandSender sender)
{
	sender.sendMessage(plugin.message("Une erreur s'est produite !"));
}

public void noparameter(CommandSender sender)
{
	sender.sendMessage(plugin.message("Il manque un paramètre !"));
}

public void gpsLieu(CommandSender sender, String[] args)
{
	Lieu l = this.plugin.getLieu(Integer.parseInt(args[2]));
	if(args[3].equalsIgnoreCase("true"))
	{
	l.setOnGps(true);
	}
	else
	{
	l.setOnGps(false);
	}
	
	l.updateInSql();
	
}

public void addTravel(CommandSender sender, String[] args)
{
	if(args.length < 8)
	{
		sender.sendMessage(plugin.message("Il manque un/des paramètre(s) !"));
		return;
	}
	
	Lieu l1 = this.plugin.getLieu(Integer.parseInt(args[2]));
	Lieu boat = this.plugin.getLieu(Integer.parseInt(args[3]));
	Lieu l2 = this.plugin.getLieu(Integer.parseInt(args[4]));
	int startR = Integer.parseInt(args[5]);
	int boatR = Integer.parseInt(args[6]);
	int time = Integer.parseInt(args[7]);
	int id = this.plugin.getNextId("travel");
	
	new Travel(this.plugin, id,l1, l2, boat, startR, boatR, time, true);
	
	//SQL
	this.plugin.mysql_update("INSERT INTO `travel` (`id`, `l1`, `l2`, `boat`, `open`, `startRadius`, `boatRadius`, `travelTime`) "
			+ "VALUES (NULL, '"+l1.getId()+"', '"+l2.getId()+"', '"+boat.getId()+"', 1,  '"+startR+"', '"+boatR+"', '"+time+"');");
	
	sender.sendMessage(plugin.message("Nouveau travel ajouté !"));
	return;
}

public void removeTravel(CommandSender sender, String[] args)
{
	Travel t = this.plugin.getTravel(Integer.parseInt(args[2]));
	
	if(t == null)
	{
		sender.sendMessage(plugin.message("Ce travel n'existe pas !"));
		return;
	}
	
	t.remove();
	this.plugin.mysql_update("DELETE FROM .`travel` WHERE `id` = "+t.getId()+" LIMIT 1;");
	
	sender.sendMessage(plugin.message("Travel supprimè !"));
	return;
}

public void adminEvent(CommandSender sender, String[] args)
{
	if(!sender.hasPermission("maxcraft.admin"))
	{
		return;
	}
	

	
	if(args.length >= 2)
	{
	
		switch(args[1])
		{
		case "new": case "add": case "create": case "n":
			this.newEvent(sender, args);
			return;
		case "remove": case "delete": case "del": case "rem": case "r":
			this.removeEvent(sender, args);
			return;
		case "open": case "ouvert": case "ouvrir": case "o":
			this.openEvent(sender, args);
			return;
		case "close": case "fermer": case "launch": case "start":
			this.closeEvent(sender, args);		
			return;
		default:
			this.noparameter(sender);
			return;
	
		}
	}
	
	this.noparameter(sender);
	return;
}

public void newEvent(CommandSender sender, String[] args)
{

	
	String name = args[2];
	
	Event e = this.plugin.getEvent(name);
	
	if(e != null)
	{
		sender.sendMessage(plugin.message(ChatColor.RED + "Cet event existe déjà !"));
		return;
	}
	
	String type;
	Event event;
	String fullname;
	
	switch(args[3])
	{
	case "individuel": case "i": case "individual": case "solo": case "chacunpoursoidansceputaindemonde":
		type = "INDIVIDUAL";
		int maxPlayer = Integer.parseInt(args[4]);
		fullname = this.plugin.multiArgs(args, 5);
		event = new Event(this.plugin, name, fullname, type);
		event.setNbMaxTeam(maxPlayer);
		event.setNbSlotTeam(1);
		
		
			break;
	case "fixe": case "f": case "fixed": case "defini":
		type = "FIXED";
		int maxPlayerInTeam = Integer.parseInt(args[4]);
		int nbTeam = Integer.parseInt(args[5]);
		fullname = this.plugin.multiArgs(args, 6);
		event = new Event(this.plugin, name, fullname, type);
		event.setNbSlotTeam(maxPlayerInTeam);
		
		for(int i = 0 ; i < nbTeam; i++)
		{
		new EventTeam(this.plugin, event, event.generateName(), event.generateLetter(), event.generateColor());
		}
		
		// CREATION DES N TEAMS
		
			break;
	case "autoteam": case "a": case "auto": case "lesjoueurssedemerdent":
		type = "AUTOTEAM";
		int maxPlayerPerTeam = Integer.parseInt(args[4]);
		int maxTeam = Integer.parseInt(args[5]);
		fullname = this.plugin.multiArgs(args, 6);
		event = new Event(this.plugin, name, fullname, type);
		event.setNbMaxTeam(maxTeam);
		event.setNbSlotTeam(maxPlayerPerTeam);
		
			break;
	default:
		sender.sendMessage(plugin.message(ChatColor.RED + "Le type choisi est invalide, choisir entre individuel, fixe et autoteam !"));
		return;
	}
	
	sender.sendMessage(plugin.message("L'event "+ ChatColor.GOLD + event.getFullName() + ChatColor.GRAY +" à été crée !"));
	return; 
		
	
}
public void removeEvent(CommandSender sender, String[] args)
{
	

	
	Event event = this.plugin.getEvent(args[2]);
	
	if(event == null)
	{
		sender.sendMessage(plugin.message(ChatColor.RED + "Cet event n'existe pas !"));
		return;
	}
	
	this.plugin.listEvents.remove(event);
	sender.sendMessage(plugin.message("L'event "+ ChatColor.GOLD + event.getFullName() + ChatColor.GRAY +" è ètè supprimè !"));
	return; 
	
}

public void openEvent(CommandSender sender, String[] args)
{

	
	Event event = this.plugin.getEvent(args[2]);
	
	if(event == null)
	{
		sender.sendMessage(plugin.message(ChatColor.RED + "Cet event n'existe pas !"));
		return;
	}
	
	if(event.isOpen())
	{
		sender.sendMessage(plugin.message("L'event "+ ChatColor.GOLD + event.getFullName() + ChatColor.GRAY +" est dèjè ouvert !"));
		return; 
	}
	else
	{
		event.setOpen(true);
		sender.sendMessage(plugin.message("Le choix des èquipes de l'event "+ ChatColor.GOLD + event.getFullName() + ChatColor.GRAY +" est ouvert !"));
		return;
		
	}
}

public void closeEvent(CommandSender sender, String[] args)
{

	
	Event event = this.plugin.getEvent(args[2]);
	
	if(event == null)
	{
		sender.sendMessage(plugin.message(ChatColor.RED + "Cet event n'existe pas !"));
		return;
	}
	
	if(!event.isOpen())
	{
		sender.sendMessage(plugin.message("L'event "+ ChatColor.GOLD + event.getFullName() + ChatColor.GRAY +" est dèjè fermè !"));
		return; 
	}
	else
	{
		event.setOpen(false);
		sender.sendMessage(plugin.message("Le choix des èquipes de l'event "+ ChatColor.GOLD + event.getFullName() + ChatColor.GRAY +" est fermè !"));
		return;
		
	}
	
}

public void teamsEvent(CommandSender sender, String eventname)
{
	
	Event event = this.plugin.getEvent(eventname);
	
	if(event == null)
	{
		sender.sendMessage(plugin.message(ChatColor.RED + "Cet event n'existe pas !"));
		return;
	}
	
	sender.sendMessage(ChatColor.BLACK + "***************************************************************");
	
	sender.sendMessage(plugin.messageEvent(""+ ChatColor.GOLD + event.getFullName()));
	
	
	
	if(!event.isOpen())
	{
		sender.sendMessage(plugin.messageEvent(ChatColor.RED + "La formation des èquipes est fermèe !"));
		 
	}
	else
	{
		sender.sendMessage(ChatColor.GREEN + "La formation des èquipes est ouverte !");
	}
	
		sender.sendMessage(ChatColor.BLACK + "***************************************************************");
		sender.sendMessage(ChatColor.GRAY + event.displayTypeDescription());
		sender.sendMessage(ChatColor.BLACK + "***************************************************************");
		
		for(EventTeam team : event.getTeams())
		{
			if(event.getNbSlotTeam() == 1)
			{
				sender.sendMessage("- "+ team.getColor() + "["+team.getLetter()+"] " +ChatColor.DARK_AQUA + team.getName()+ ChatColor.RESET + ChatColor.GRAY +"");
			}
			else if(event.getNbSlotTeam() == -1)
			{
				sender.sendMessage("- "+ ChatColor.WHITE+ "["+team.getLetter()+"] " + team.getColor() + ChatColor.BOLD + team.getName()+ ChatColor.RESET + ChatColor.GRAY +" ["+team.getPlayers().size()+"]");
			}
			else
			{
				sender.sendMessage("- "+ ChatColor.WHITE+ "["+team.getLetter()+"] " + team.getColor() + ChatColor.BOLD + team.getName()+ ChatColor.RESET + ChatColor.GRAY +" ["+team.getPlayers().size()+"/"+event.getNbSlotTeam()+"]");
			}
			
			if(!event.getTeamType().equals("INDIVIDUAL"))
			{
			if(team.getPlayers().isEmpty() )
			{
				sender.sendMessage(ChatColor.GRAY + "   Aucun joueur dans cette èquipe.");
				
			}
			else
			{
				sender.sendMessage(ChatColor.GRAY + "   Membres : "+team.displayPlayers());
			}
			
			
			sender.sendMessage(ChatColor.BLACK + "***************************************************************");
			}
		}
		
		
		sender.sendMessage(plugin.messageEvent("Pour rejoindre l'event : /event join "+event.getName()+""));
	
}

public void joinEvent(CommandSender sender, String[] args)
{
	if(args.length < 2)
	{
		sender.sendMessage(plugin.message(ChatColor.RED + "/event join [event] (team/joueur)"));
		return;
	}
	if(! (sender instanceof Player))
	{
		return;
	}
	
	Maxcraftien m = this.plugin.getMaxcraftien((Player) sender);
	if(m == null)
	{
		sender.sendMessage(plugin.message(ChatColor.RED + "Vous devez Ãªtre joueur pour participer è un event !"));
		return;
	}
	
	if(this.plugin.getEventPlayer(m) != null)
	{
		sender.sendMessage(plugin.message(ChatColor.RED + "Vous devez d'abord quitter l'event dans lequel vous Ãªtes entrè en faisant /event leave."));
		return;
	}
	
Event event = this.plugin.getEvent(args[1]);
	
	if(event == null)
	{
		sender.sendMessage(plugin.message(ChatColor.RED + "Cet event n'existe pas !"));
		return;
	}

	EventPlayer ep = new EventPlayer(this.plugin, null, m, null);
	String who = null;
	if(args.length == 3)
	{
		who = args[2];
	}
	
	event.join(ep, who);
	return;
	
	
}

public void quitEvent(CommandSender sender, String[] args)
{
	if(! (sender instanceof Player))
	{
		return;
	}
	
	Maxcraftien m = this.plugin.getMaxcraftien((Player) sender);
	if(m == null)
	{
		sender.sendMessage(plugin.message(ChatColor.RED + "Vous devez Ãªtre joueur pour participer à un event !"));
		return;
	}
	
	EventPlayer ep = this.plugin.getEventPlayer(m);
	
	if(ep == null)
	{
		sender.sendMessage(plugin.message(ChatColor.RED + "Vous n'êtes pas dans un event !"));
		return;
	}
	
	ep.getTeam().leave(ep);

}

public void sendMessageEvent(CommandSender sender, String[] args)
{
	if(!sender.hasPermission("maxcraft.admin"))
	{
		return;
	}
	
	Event event = null;
	for(Event e : this.plugin.listEvents)
	{
		event = e;
	}
	
	EventTeam t = event.getTeam(args[1]);
	String message;
	
	if(t == null)
	{
		message = plugin.multiArgs(args, 1);
		event.sendMessage(message);
	}
	else
	{
		message = plugin.multiArgs(args, 2);
		t.sendMessage(message);
	}
	
	sender.sendMessage(this.plugin.messageEvent(message));
	return;
}

public void teleportEvent(CommandSender sender, String[] args)
{
	if(!sender.hasPermission("maxcraft.admin"))
	{
		return;
	}
	Player p = (Player) sender;
	Event event = null;
	for(Event e : this.plugin.listEvents)
	{
		event = e;
	}
	
	for(EventPlayer ep : event.getEventPlayers())
	{
		if(ep.getPlayer().isOnline())
		{
			if(args.length == 2 && args[1].equals("back"))
			{
			ep.getOnlinePlayer().teleport(ep.getBack());
			ep.getOnlinePlayer().sendMessage(this.plugin.messageEvent("Vous avez été téléporté à votre position initiale avant l'event <"+ChatColor.GOLD+ ep.getTeam().getEvent().getFullName()  +ChatColor.GRAY+">"));
				
			}
			else
			{
			ep.getOnlinePlayer().teleport(p.getLocation());
			ep.getOnlinePlayer().sendMessage(this.plugin.messageEvent("Vous avez été téléporté pour l'event <"+ChatColor.GOLD+ ep.getTeam().getEvent().getFullName()  +ChatColor.GRAY+">"));
			}
		}
	}
	
}

public void clearInventoryEvent(CommandSender sender, String[] args)
{
	if(!sender.hasPermission("maxcraft.admin"))
	{
		return;
	}
	Player p = (Player) sender;
	Event event = null;
	for(Event e : this.plugin.listEvents)
	{
		event = e;
	}
	
	for(EventPlayer ep : event.getEventPlayers())
	{
		if(ep.getPlayer().isOnline())
		{
		ep.getPlayer().getPlayer().getInventory().clear();
		ep.getPlayer().getPlayer().getInventory().setArmorContents(null);;
		}
	}
	
}









}