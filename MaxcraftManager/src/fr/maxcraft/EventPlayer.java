package fr.maxcraft;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class EventPlayer {
	
private MaxcraftManager plugin;

private Maxcraftien player;
private EventPlayer parain;
private EventTeam team;
private Location back;
	
	public EventPlayer(MaxcraftManager plugin, EventTeam team, Maxcraftien player,EventPlayer parain )
	{
		this.plugin = plugin;
		this.player = player;
		this.parain = parain;
		this.team = team;
	}

	public Maxcraftien getPlayer() {
		return player;
	}

	public void setPlayer(Maxcraftien player) {
		this.player = player;
	}

	public EventPlayer getParain() {
		return parain;
	}

	public void setParain(EventPlayer parain) {
		this.parain = parain;
	}

	public EventTeam getTeam() {
		return team;
	}

	public void setTeam(EventTeam team) {
		this.team = team;
	}
	
	public Player getOnlinePlayer()
	{
		return this.player.getPlayer();
	}
	
	
	
	public Location getBack() {
		return back;
	}

	public void setBack(Location back) {
		this.back = back;
	}

	public void remove()
	{
		if(this.team == null)
		{
			return;
		}
		this.team.getPlayers().remove(this);
		for(EventPlayer ep : this.team.getPlayers())
		{
			if(ep.getParain() == this)
			{
				ep.setParain(null);
			}
		}
	}
	

	
	
}
