package fr.maxcraft;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.ListIterator;

import net.milkbowl.vault.economy.Economy;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.material.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.dynmap.DynmapAPI;
import org.dynmap.markers.MarkerSet;

import com.earth2me.essentials.Essentials;
import com.mysql.jdbc.Statement;

public class MaxcraftManager extends JavaPlugin {

	//VARIABLES
	private String sqlurl;
	private String sqluser;
	private String sqlpasswd;
	
	public static java.sql.Connection sql;
	
	//LISTES
	public static ArrayList<Maxcraftien> listMaxcraftiens = new ArrayList<Maxcraftien>();
	public static ArrayList<Faction> listFactions = new ArrayList<Faction>();
	public static ArrayList<Lieu> listLieux = new ArrayList<Lieu>();
	public static ArrayList<Portal> listPortals = new ArrayList<Portal>();
	public static ArrayList<FactionState> listFactionStates = new ArrayList<FactionState>();
	public static ArrayList<Session> listSessions = new ArrayList<Session>();
	public static ArrayList<Travel> listTravels = new ArrayList<Travel>();
	public static ArrayList<Event> listEvents = new ArrayList<Event>();
	
	//PLUGINS

	public static DynmapAPI Dynmap = null;
	public static Economy economy = null;
	public static MaxcraftZones MZ = null;
	public static Essentials Essentials = null;
	
	//DYNMAP
	MarkerSet portalMarkers;
	MarkerSet lieuMarkers;
	
	//LISTENERS
	private MaxcraftListener ML;
	
	//COMMANDE MANAGER
	public static MaxcraftCommandManager commandManager;
	
	//METHODES
	/*
	 * (non-Javadoc)
	 * @see org.bukkit.plugin.java.JavaPlugin#onEnable()
	 */
	@Override
    public void onEnable(){
		
		//Configuration
	
		saveDefaultConfig();

		//Listeners
		this.ML = new MaxcraftListener(this);
		this.getServer().getPluginManager().registerEvents(this.ML, this);
		
		//Commands
		commandManager = new MaxcraftCommandManager(this);
		
		//SQL
		sqlurl = "jdbc:mysql://"+ getConfig().getString("host") +":3306/" + getConfig().getString("base");
		sqluser = getConfig().getString("user");
		sqlpasswd = getConfig().getString("pass");
		this.connect();
		
		//PLUGINS
		
		this.Dynmap = this.getDynmap();
		this.MZ = this.getMaxcraftZones();
		this.Essentials = this.getEssentials();
		this.setupEconomy();
	
		
		//DYNMAP
		
		this.portalMarkers = this.Dynmap.getMarkerAPI().createMarkerSet("MaxcraftManager Portails", "Portails", null, false);
		this.portalMarkers.setHideByDefault(true);
		this.lieuMarkers = this.Dynmap.getMarkerAPI().createMarkerSet("MaxcraftManager Lieux", "Lieux", null, false);
		
		//SESSIONS CLEAN
		this.mysql_update("DELETE FROM `session` WHERE end IS NULL;");
		
		//ENTITIES
		try {
			this.loadEntities();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log("Plugin charge");
		
		
		
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.bukkit.plugin.java.JavaPlugin#onDisable()
	 */
	
	@Override
    public void onDisable(){
		
		this.closeAllSessions();
		
	}
	
	/*
	 * Connexion MySQL
	 */
	public void connect() {

		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}

		try {

			sql = DriverManager.getConnection(this.sqlurl, this.sqluser, this.sqlpasswd);
			log("Connexion sql etablie !");
		} catch (SQLException e) {
			e.printStackTrace();
			log("Connexion sql echouée !");
		}

	}
	
	/*
	 * Affichage dans console
	 */
	public void log(String message, ChatColor color)
	{
		ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
		console.sendMessage(color + "[MAXCRAFT] "+ ChatColor.WHITE +"[MaxcraftManager] " + message);
		
	}
	public void log(String message)
	{
		ChatColor color = ChatColor.RED;
		log(message, color);
		
	}
	
	
	/*
	 * Requete MySQL
	 */
	public ResultSet mysql_query(String query, boolean singleResult)
	{
		try {
			if(sql.isClosed())
			{
				this.connect();
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			Statement state = (Statement) MaxcraftManager.sql.createStatement();
			ResultSet result = state.executeQuery(query);
	
		if(singleResult)
		{
			result.next();
		}
		return result;
				
		} catch (SQLException e) {
			return null;
		}
		
	}
	
	public void mysql_update(String query)
	{
		try {
			if(sql.isClosed())
			{
				this.connect();
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			Statement state = (Statement) this.sql.createStatement();
		
			state.executeUpdate(query);
			state.close();
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}	
	}
	
	/*
	 * Chargement des entitées au chargement du plugin
	 * Ordre :
	 * 
	 * Faction
	 * FactionState
	 * Maxcraftien
	 * Lieu
	 * Portal
	 * 
	 */
	public void loadEntities() throws SQLException
	{
		
		//SESSIONS DES JOUEURS PRESENTS
		Collection<? extends Player> playerlist = this.getServer().getOnlinePlayers();
		for(Player p : playerlist)
		{
			new Session(this, p.getName());
		}
		
		//FACTIONS
		log("Chargement des factions", ChatColor.BLUE);
		
		ResultSet result = mysql_query("SELECT * FROM faction WHERE removed = 0", false);
		
		while(result.next())
		{
			
			Faction f = new Faction(this, result.getInt("id"), result.getString("tag"), result.getInt("user"), result.getString("name"), result.getInt("capital"), result.getInt("spawn"));
			
		}
		
		//FACTION STATE
		log("Chargement des états de faction", ChatColor.BLUE);
		
		
		result = mysql_query("SELECT * FROM factionstate", false);
		
		while(result.next())
		{
			
			FactionState fs = new FactionState(this, result.getInt("id"), this.getFaction(result.getInt("faction1")),  this.getFaction(result.getInt("faction2")), result.getString("etat"));

		}
		
		//MAXCRAFTIENS
		log("Chargement des Maxcraftiens", ChatColor.BLUE);
		result = mysql_query("SELECT * FROM user", false);
		
		while(result.next())
		{
			Maxcraftien m = new Maxcraftien(this, result.getInt("id"),result.getString("uuid"), result.getString("username"), result.getInt("faction"), result.getBoolean("actif"), result.getInt("gametime"));
		}
		
		//LIEUX
		log("Chargement des Lieux", ChatColor.BLUE);
		result = mysql_query("SELECT * FROM lieu", false);
		
		Location l;
		while(result.next())
		{
			if(this.getServer().getWorld(result.getString("world")) == null) continue;
			l = new Location(this.getServer().getWorld(result.getString("world")), result.getInt("posX"), result.getInt("posY"), result.getInt("posZ"), result.getFloat("posYaw"), result.getFloat("posPitch"));
			Lieu lieu = new Lieu(this, result.getInt("id"), this.getMaxcraftien(result.getInt("owner")), l, result.getString("name"), result.getBoolean("onMap"), result.getString("icon"), result.getBoolean("onGps"));
		}
		
		//PORTAILS
		log("Chargement des Portails", ChatColor.BLUE);
		result = mysql_query("SELECT * FROM portal", false);
		
		while(result.next())
		{
			if(this.getLieu(result.getInt("lieu1")) == null) continue;
			if(this.getLieu(result.getInt("lieu2")) == null) continue;
			
			Portal p = new Portal(this, result.getInt("id"), this.getLieu(result.getInt("lieu1")), this.getLieu(result.getInt("lieu2")), result.getBoolean("ouvert"));
		}
		
		//TRAVELS
		
		log("Chargement des Travels", ChatColor.BLUE);
		result = mysql_query("SELECT * FROM travel", false);
		
		while(result.next())
		{
			Lieu l1 = this.getLieu(result.getInt("l1"));
			Lieu l2 = this.getLieu(result.getInt("l2"));
			Lieu boat = this.getLieu(result.getInt("boat"));
			if(l1 == null || l2 == null || boat == null) continue;
		
			Travel t = new Travel(this, result.getInt("id"),l1, l2, boat, result.getInt("startRadius"), result.getInt("boatRadius"), result.getInt("travelTime"), result.getBoolean("open"));
			
		}
		
		
		
	}
	
	/*
	 * Trouver un Maxcraftien
	 */
	public Maxcraftien getMaxcraftien(Player p)
	{
		for(Maxcraftien m : MaxcraftManager.listMaxcraftiens)
		{
		if(m.getUuid().equalsIgnoreCase(p.getUniqueId().toString().replace("-", ""))){
			return m;
		}
		}
		return null;
	}
	
	public Maxcraftien getMaxcraftien(String username)
	{
		// Attention dangereux ! 
		for(Maxcraftien m : MaxcraftManager.listMaxcraftiens)
		{
		if(m.getUserName().equalsIgnoreCase(username)){
			return m;
		}
		}
		return null;
	}
	
	public Maxcraftien getMaxcraftien(int player)
	{
		for(Maxcraftien m : MaxcraftManager.listMaxcraftiens)
		{
		if(m.getId() == player){
			return m;
		}
		}
		return null;
	}
	/*
	 * Trouver une faction
	 */
	public Faction getFaction(int id) {
		for (Faction faction : MaxcraftManager.listFactions) {
			if (faction.getId() == id) {
				return faction;
			}
		}
		return null;

	}
	
	public FactionState getFactionState(int id) {
		for (FactionState fs: MaxcraftManager.listFactionStates) {
			if (fs.getId() == id) {
				return fs;
			}
		}
		return null;

	}
	
	public Faction getFaction(String tag) {
		for (Faction faction : MaxcraftManager.listFactions) {
			if (faction.getTag().equalsIgnoreCase(tag)) {
				return faction;
			}
		}
		return null;

	}
	
	public Event getEvent(String name) {
		for (Event event : MaxcraftManager.listEvents) {
			if (event.getName().equalsIgnoreCase(name)) {
				return event;
			}
		}
		return null;

	}
	
	/*
	 * Trouver un lieu
	 */
	public Lieu getLieu(int id) {
		for (Lieu lieu : MaxcraftManager.listLieux) {
			if (lieu.getId() == id) {
				return lieu;
			}
		}
		return null;

	}
	
	/*
	 * Trouver un travel
	 */
	public Travel getTravel(int id) {
		for (Travel t : MaxcraftManager.listTravels) {
			if (t.getId() == id) {
				return t;
			}
		}
		return null;

	}
	
	/*
	 * Trouver une session
	 */
	public Session getSession(String player) {
		for (Session s : MaxcraftManager.listSessions) {
			if (s.getPlayer().equals(player)) {
				return s;
			}
		}
		return null;

	}
	
	
	public DynmapAPI getDynmap()
	{
		DynmapAPI dynmap = (DynmapAPI) this.getServer().getPluginManager().getPlugin("dynmap");
        if(dynmap == null) {
        	log("Il manque le plugin Dynmap !");
            
        }
        return dynmap;
	}
	
	public Essentials getEssentials()
	{
		Essentials e = (Essentials) this.getServer().getPluginManager().getPlugin("Essentials");
        if(e == null) {
        	log("Il manque le plugin Essentials !");
            
        }
        return e;
	}
	
	public MaxcraftZones getMaxcraftZones() {
		Plugin plugin = getServer().getPluginManager().getPlugin(
				"MaxcraftZones");

		// WorldGuard may not be loaded
		if (plugin == null || !(plugin instanceof MaxcraftZones)) {
			log("Il manque MaxcraftZones!");
			return null; // Maybe you want throw an exception instead
		}

		return (MaxcraftZones) plugin;
	}
	
	
	
	 private boolean setupEconomy()
	    {
	        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
	        if (economyProvider != null) {
	            economy = economyProvider.getProvider();
	            log("Economie Chargee !");
	        }

	        return (economy != null);
	    }

	
	public boolean isMaxcraftien(String name)
	{
		for(Maxcraftien m : this.listMaxcraftiens)
		{
			if(m.getUserName().equals(name))
			{
				return true;
				
			}
			
		}
		
		return false;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		//MAXCRAFT
		if(cmd.getName().equalsIgnoreCase("maxcraft")){ 
		this.commandManager.commandMaxcraft(sender, args);
		}
		else if(cmd.getName().equalsIgnoreCase("faction")){ 
			this.commandManager.commandFaction(sender, args);
			}
		else if(cmd.getName().equalsIgnoreCase("gps")){ 
			this.commandManager.commandGps(sender, args);
			}
		else if(cmd.getName().equalsIgnoreCase("event")){ 
			this.commandManager.commandEvent(sender, args);
			}
		else if(cmd.getName().equalsIgnoreCase("mourir")){ 
			this.commandManager.commandMourir(sender, args);
			}
		return true;
	}
	
	public String message(String message)
	{
		return ChatColor.RED + "[Maxcraft] " + ChatColor.GRAY + message;
	}
	
	public String messageEvent(String message)
	{
		return ""+ChatColor.GOLD + ChatColor.BOLD +"[Event] " + ChatColor.GRAY + message;
	}
	
	public int getNextId(String table) {
		// Recuperation de l'id

		try {
			Statement state = (Statement) this.sql.createStatement();
			ResultSet result = state.executeQuery("SHOW TABLE STATUS LIKE '" + table + "'");

			result.next();

			return result.getInt(11);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 1;
	}
	
	public Lieu isOnLieu(Location l)
	{
		for(Lieu lieu : this.listLieux)
		{
			if(l.getBlock().equals(lieu.getLocation().getBlock()))
			{
				return lieu;
			}
		}
		return null;
	}
	
	public void loadFactionFromSql(int id) throws SQLException
	{
		if(this.getFaction(id) == null)
		{
		// n'existait pas
		ResultSet result = this.mysql_query("SELECT * FROM faction WHERE id = "+ id, true);
		Faction f = new Faction(this, result.getInt("id"), result.getString("tag"), result.getInt("user"), result.getString("name"), result.getInt("capital"), result.getInt("spawn"));
		log(f.getTag() + " : Faction ajoutée !");
		}
		else
		{
		//existe deja	
			Faction f =this.getFaction(id);
			f.loadFromSql();
			log(f.getTag() + " : Faction mise à jour !");
		}
	}
	
	public void loadMaxcraftienFromSql(int id) throws SQLException
	{
		if(this.getMaxcraftien(id) == null)
		{
			//pas cr��
		ResultSet result = this.mysql_query("SELECT * FROM user WHERE id = "+ id, true);
		Maxcraftien m = new Maxcraftien(this, result.getInt("id"), result.getString("uuid"), result.getString("username"), result.getInt("faction"), result.getBoolean("actif"), result.getInt("gametime"));
		
		log(m.getUserName() + " : Joueur ajouté !");
		}
		else
		{
			Maxcraftien m = this.getMaxcraftien(id);
			m.loadFromSql();
			log(m.getUserName() + " : Joueur mis à jour !");
		}
	}
	
	public void removeFaction(int id)
	{
		Faction f = this.getFaction(id);
		f.remove();
		log(f.getTag() + " : Faction supprimée !");
	}
	
	public void removeFactionStateFromSql(int id)
	{
	
		FactionState fs = this.getFactionState(id);
		this.listFactionStates.remove(fs);
		log("FactionState supprimé !");
	}
	

	public void loadFactionStateFromSql(int id) throws SQLException
	{
	
		ResultSet result = this.mysql_query("SELECT * FROM factionstate WHERE id = "+ id, true);
		FactionState fs = new FactionState(this, result.getInt("id"), this.getFaction(result.getInt("faction1")),  this.getFaction(result.getInt("faction2")), result.getString("etat"));
		
		log("FactionState chargé !");
	}
	
	public String formatDate(Date date)
	{
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if(date != null)
		{
			return df.format(date);
		}
		else
		{
			return "";
		}
	}
	
	public void closeAllSessions()
	{
		Iterator<Session> itr = this.listSessions.iterator();
		while(itr.hasNext())
		{
	
		Session s = (Session) itr.next();
		s.remove();

		}
	}
	
	public String displayFactionList(ArrayList<Faction> list)
	{
		String factions = null;
		int i = 0;
		for(Faction f : list)
		{
			if(i == 0)
			{
				factions  = f.getTag();
			}
			else
			{
			factions  = factions + ", " + f.getTag();
			}
			i++;
		}

		if(factions == null)
		{
			return "Pas de faction";
		}
		else
		{
			return factions;
		}
	}
	public boolean isInt(String string)
	{
		try { 
			int i = Integer.parseInt(string); 
			return true;
			} 
			catch (Exception e) { 
				return false;
			}
	}
	public void giveItems(Player p, ItemStack items)
	{
		while(items.getType().getMaxStackSize() < items.getAmount())
		{
			if(p.getInventory().firstEmpty() != -1)
			{
				p.getInventory().addItem(new ItemStack(items.getType(),items.getType().getMaxStackSize(), items.getData().getData()));
			}
			else
			{
				p.getLocation().getWorld().dropItemNaturally(p.getLocation(), new ItemStack(items.getType(),items.getType().getMaxStackSize(), items.getData().getData()));
			}
			
			items.setAmount(items.getAmount()-items.getType().getMaxStackSize());
		}
		
		if(p.getInventory().firstEmpty() != -1)
		{
			p.getInventory().addItem(items);
		}
		else
		{
			p.getLocation().getWorld().dropItemNaturally(p.getLocation(), items);
		}
	}
	
	public Block getLinkedSign(Block block)
	{
		Block sign;
		Sign rSign;
		if(block.getRelative(BlockFace.UP).getType().equals(Material.SIGN_POST))
		{
			return block.getRelative(BlockFace.UP);
		}
		
		if(block.getRelative(BlockFace.NORTH).getType().equals(Material.WALL_SIGN))
		{
			sign = block.getRelative(BlockFace.NORTH);
			rSign = new Sign(sign.getType(), sign.getData());
			if(sign.getRelative(rSign.getAttachedFace()).equals(block))
			{
				return sign;
			}
		}
		
		if(block.getRelative(BlockFace.WEST).getType().equals(Material.WALL_SIGN))
		{
			sign = block.getRelative(BlockFace.WEST);
			rSign = new Sign(sign.getType(), sign.getData());
			if(sign.getRelative(rSign.getAttachedFace()).equals(block))
			{
				return sign;
			}
		}
		
		if(block.getRelative(BlockFace.EAST).getType().equals(Material.WALL_SIGN))
		{
			sign = block.getRelative(BlockFace.EAST);
			rSign = new Sign(sign.getType(), sign.getData());
			if(sign.getRelative(rSign.getAttachedFace()).equals(block))
			{
				return sign;
			}
		}
		
		if(block.getRelative(BlockFace.SOUTH).getType().equals(Material.WALL_SIGN))
		{
			sign = block.getRelative(BlockFace.SOUTH);
			rSign = new Sign(sign.getType(), sign.getData());
			if(sign.getRelative(rSign.getAttachedFace()).equals(block))
			{
				return sign;
			}
		}
		
		return null;
	}
	
	public void giveNetherPass(Player p)
	{
		ItemStack netherpass = new ItemStack(Material.WRITTEN_BOOK);
		BookMeta book = (BookMeta) netherpass.getItemMeta().clone();
		
		book.setAuthor(ChatColor.MAGIC + "Les phoenix");
		book.setTitle(ChatColor.RED + "Nether Pass");
		book.addPage(ChatColor.RED + "Ce livre est l'unique clé permettant d'entrer dans le nether.                    " + ChatColor.MAGIC + "Les Phoenix");
		book.setDisplayName(ChatColor.RED  + "Nether Pass");
		netherpass.setItemMeta(book);
		p.getInventory().addItem(netherpass);
	}
	
	public String multiArgs(String[] args, int start)
	{
		
		int length = args.length;
		int nlength = length-start;
		String[] nametable = new String[nlength];
		int nid;
		for(int i = start; i < args.length; i++)
		{
			nid = i-start;
			nametable[nid] = args[i];
		}
		
		String name = StringUtils.join(nametable, " ");
		return name;
	}
	
	public EventPlayer getEventPlayer(Maxcraftien m)
	{
		for(Event e : this.listEvents)
		{
			for(EventPlayer ep : e.getEventPlayers())
			{
				if(ep.getPlayer() == m)
				{
					return ep;
				}
			}
		}
		return null;
	}
	
	public Event getCurrentEvent()
	{
		for(Event e : this.listEvents)
		{
			return e;
		}
		return null;
	}
	
	public int ramdomInt(int min, int max)
	{
		return min + (int)(Math.random() * ((max - min) + 1));
	}
	
	
}
