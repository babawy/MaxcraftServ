package fr.maxcraft;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Pig;
import org.bukkit.entity.Player;
import org.bukkit.entity.Vehicle;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.dynmap.markers.Marker;
import org.dynmap.markers.MarkerIcon;

public class Lieu {

	
	//VARIABLES
	MaxcraftManager plugin;
	int id;
	Location location;
	String name;
	boolean onMap;
	Maxcraftien owner;
	Marker marker;
	String icon;
	private Boolean onGps;
	
	//CONSTRUCTOR
	public Lieu(MaxcraftManager plugin, int id, Maxcraftien owner, Location location, String name, boolean onMap, String icon, Boolean onGps)
	{
		this.plugin = plugin;
		this.id = id;
		this.location = location;
		this.name = name;
		this.onMap = onMap;
		this.owner = owner;
		this.icon = icon;
		this.onGps = onGps;
		this.plugin.listLieux.add(this);
		
		//marker
		try {
			this.createMarker();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//GETTERS SETTERS
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
		this.removeMarker();
		try {
			this.createMarker();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(this.isLinkedToPortal())
		{
			Portal p = this.getPortal();
			p.reloadMarker();
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isOnMap() {
		return onMap;
	}

	public void setOnMap(boolean onMap) {
		this.onMap = onMap;
	}

	public Maxcraftien getOwner() {
		return owner;
	}

	public void setOwner(Maxcraftien owner) {
		this.owner = owner;
	}
	
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	
	//METHODES
	



	public void tpPlayer(Player p)
	{
		
		if(this.location.getWorld().getName().equals("nether"))
		{
			
		
		
			ItemStack item = p.getItemInHand();
			
			if(item.getType().equals(Material.WRITTEN_BOOK))
			{
				BookMeta book = (BookMeta) item.getItemMeta();
				if(book.getTitle().equals(ChatColor.RED +"Nether Pass") && book.getAuthor().equals(ChatColor.MAGIC +"Les phoenix"))
				{
					p.sendMessage(this.plugin.message("Bienvenue dans le Nether, ne perdez pas votre Nether Pass !"));
					
				}
				else
				{
					return;
				}
			}
			else
			{
				return;
			}
			
		}
		
		if(!this.isLieuValidForTp())
		{
			return;
		}
		
	
		
		Location tpLoc = this.findRedstoneLamp().getLocation();
		
		//tpLoc.add(0.5, 0, 0.5); //Ajustement au centre du bloc
		tpLoc.add(0.5, 1, 0.5); //Ajustement au centre du bloc
		tpLoc.setPitch(this.location.getPitch());
		tpLoc.setYaw(this.location.getYaw());
		
		if(p.getVehicle() != null && p.getVehicle().getType().equals(EntityType.PIG))
		{
			
			Vehicle pig = (Vehicle) p.getVehicle();
			p.leaveVehicle();
			pig.teleport(tpLoc);
			p.teleport(tpLoc);
			pig.setPassenger(p);
			
		}
		else
		{
		p.teleport(tpLoc);
		}
		
		p.sendMessage(plugin.message("Téléportation vers "+ ChatColor.GOLD+ this.name + ChatColor.GRAY +" ..."));
		tpLoc.getWorld().playSound(tpLoc, Sound.ANVIL_LAND, (float) 0.5, tpLoc.getPitch());
	}
	
	public Boolean getOnGps() {
		return onGps;
	}

	public void setOnGps(Boolean onGps) {
		this.onGps = onGps;
	}

	public void tpAdmin(Player p)
	{
		
		Location tpLoc = this.location.clone().add(1, 10, 1);
		p.teleport(tpLoc);
		p.sendMessage(plugin.message("Téléportation vers "+ ChatColor.GOLD+ this.name + ChatColor.GRAY +" ..."));
	}
	
	public Block findRedstoneLamp()
	{

		Location loc1 = this.getLocation().clone().add(1, -1, 0);
		Location loc2 = this.getLocation().clone().add(0, -1, 1);
		Location loc3 = this.getLocation().clone().add(-1, -1, 0);
		Location loc4 = this.getLocation().clone().add(0, -1, -1);
		
		if(loc1.getBlock().getType().equals(Material.REDSTONE_LAMP_ON))
		{
			return loc1.getBlock();
		}
		if(loc2.getBlock().getType().equals(Material.REDSTONE_LAMP_ON))
		{
			return loc2.getBlock();
		}
		if(loc3.getBlock().getType().equals(Material.REDSTONE_LAMP_ON))
		{
			return loc3.getBlock();
		}
		if(loc4.getBlock().getType().equals(Material.REDSTONE_LAMP_ON))
		{
			return loc4.getBlock();
		}
		
		return null;
	
	}
	
	public Block findEmeraldBlock()
	{
		Block b = this.location.clone().add(0,-1,0).getBlock();
		if(b.getType().equals(Material.EMERALD_BLOCK))
		{
			return b;
		}
		else
		{
			return null;
		}
	}
	
	public boolean isLieuValidForTp()
	{
		if(this.findRedstoneLamp() == null)
		{
			return false;
		}
		
		if(this.findEmeraldBlock() == null)
		{
			return false;
		}
		
		
		
		return true;
		
		
	}
	
	public void saveInSql()
	{
		
		this.plugin.mysql_update(
		"INSERT INTO `lieu` (`id`, `owner`, `posX`, `posY`, `posZ`, `posPitch`, `posYaw`, `world`, `name`, `onMap`, icon, onGps) VALUES (NULL, '"+ this.owner.getId() +"', '"+ this.location.getBlockX() +"', '"+ this.location.getBlockY() +"', '"+ this.location.getBlockZ() +"', '"+ this.location.getPitch() +"', '"+ this.location.getYaw() +"', '"+ this.location.getWorld().getName() +"', '"+ name +"', "+ this.onMap +", '"+this.icon+"', false);");
		
	}
	
	public void updateInSql()
	{
		
		this.plugin.mysql_update("UPDATE `lieu` SET  `world` =  '"+this.location.getWorld().getName()+"', owner = "+ this.owner.getId()+", posX = "+this.location.getBlockX()+", posY = "+this.location.getBlockY()+", posZ = "+this.location.getBlockZ()+", posPitch = "+this.location.getPitch()+", posYaw = "+this.location.getYaw()+", name = '"+this.name+"',`onMap` =  "+this.onMap+", icon = '"+this.icon+"',`onGps` =  "+this.onGps+" WHERE  `lieu`.`id` = "+this.id+";");
	}
	
	public void remove()
	{
	this.removeMarker();
		
	if(this.isLinkedToPortal()){
		Portal p = this.getPortal();
		p.remove();
	}
		this.plugin.listLieux.remove(this);
		this.plugin.mysql_update("DELETE FROM `lieu` WHERE `lieu`.`id` = "+ this.id +";");
		
	}
	
	public Portal getPortal()
	{
		for(Portal p : this.plugin.listPortals)
		{
			if(p.getLieu1() == this || p.getLieu2() == this)
			{
				return p;
			}
		}
		return null;
	}
	
	public boolean isLinkedToPortal()
	{
		if(this.getPortal() == null)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public void createMarker() throws IOException 
	{
		if(this.onMap)
		{
		String id = new Integer(this.id).toString();

		MarkerIcon myicon = this.plugin.Dynmap.getMarkerAPI().getMarkerIcon(this.icon);
		
		
		
		
		this.marker = this.plugin.lieuMarkers.createMarker(id, this.getName(), true, this.getLocation().getWorld().getName(), this.getLocation().getX(), this.getLocation().getY(), this.getLocation().getZ(), myicon, true);
		this.marker.setDescription("Lieu : "+ this.name+" ("+this.id+")");
		this.marker.setLabel(this.getName());
		}
	}
	
	public void removeMarker()
	{
		if(this.onMap)
		{
		this.marker.deleteMarker();
		}
	}
	
	
	
}
