package fr.maxcraft;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;


public class Event {

	private MaxcraftManager plugin;
	private String fullName;
	private String name;
	private String teamType;
	private int nbMaxTeam;
	private int nbSlotTeam;
	private boolean open;
	private ArrayList<EventTeam> teams = new ArrayList<EventTeam>();
	private Scoreboard board;
	private Objective objective;



	public Event(MaxcraftManager plugin, String name, String fullName, String teamType)
	{
		this.plugin = plugin;
	
		this.fullName = fullName;
		this.name = name;
		this.teamType = teamType;
		this.nbMaxTeam = -1;
		this.nbSlotTeam = -1;
		this.plugin.listEvents.add(this);
		this.open = true;

		

	}


	public ArrayList<EventTeam> getTeams() {
		return teams;
	}


	public void setTeams(ArrayList<EventTeam> teams) {
		this.teams = teams;
	}
	
	public String getFullName() {
		return fullName;
	}


	public Objective getObjective() {
		return objective;
	}


	public void setObjective(Objective objective) {
		this.objective = objective;
	}


	public void setFullName(String fullName) {
		this.fullName = fullName;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getTeamType() {
		return teamType;
	}


	public void setTeamType(String teamType) {
		this.teamType = teamType;
	}


	public int getNbMaxTeam() {
		return nbMaxTeam;
	}


	public void setNbMaxTeam(int nbMaxTeam) {
		this.nbMaxTeam = nbMaxTeam;
	}


	public int getNbSlotTeam() {
		return nbSlotTeam;
	}


	public void setNbSlotTeam(int nbSlotTeam) {
		this.nbSlotTeam = nbSlotTeam;
	}


	public boolean isOpen() {
		return open;
	}


	public void setOpen(boolean open) {
		this.open = open;
	}
	
	public Scoreboard getBoard() {
		return board;
	}


	public void setBoard(Scoreboard board) {
		this.board = board;
	}


	public String displayTypeDescription()
	{
		switch(this.teamType)
		{
		case "INDIVIDUAL":
			return "C'est un event individuel, vous ne pouvez pas former d'équipe, rejoignez le simplement !";
		case "FIXED":
			return "Les "+this.teams.size()+" équipes sont définies, rejoignez l'une d'entre elles !";
		case "AUTOTEAM":
			return "Vous pouvez créer votre propre équipe ou en rejoindre une !";
		default:
			return "Erreur";
		}
	}
	
	public EventTeam getTeam(String letter)
	{
	for(EventTeam et : this.teams)
	{
		if(et.getLetter().equalsIgnoreCase(letter))
		{
			return et;
		}
	}
	return null;
	}
	
	public ArrayList<EventPlayer> getEventPlayers()
	{
		ArrayList<EventPlayer> players = new ArrayList<EventPlayer>();
		for(EventTeam et : this.teams)
		{
			for(EventPlayer ep : et.getPlayers())
			{
				players.add(ep);
			}
		}
		
		return players;
	}
	
	public EventPlayer getEventPlayer(String player)
	{
		for(EventPlayer ep : this.getEventPlayers())
		{
			if(ep.getPlayer().getUserName().equals(player))
			{
				return ep;
			}
		}
		return null;
	}
	
	public EventTeam getTeam(EventPlayer eplayer)
	{
		for(EventTeam et : this.teams)
		{
			if(et.getPlayers().contains(eplayer))
			{
				return et;
			}
		}
		return null;
	}
	
	public void sendMessage(String message)
	{
		for(EventPlayer ep : this.getEventPlayers())
		{
			if(ep.getOnlinePlayer() != null)
			{
			ep.getPlayer().getPlayer().sendMessage("" +ChatColor.BOLD + ChatColor.GOLD + "[Event] " + ChatColor.GRAY + message );
			}
		}
		
		
	}
	
	public void join(EventPlayer ep, String who)
	{
		Player p = ep.getOnlinePlayer();
		
		if(!this.open)
		{
			p.sendMessage(this.plugin.messageEvent(ChatColor.RED + "La formation des équipes est fermée !"));
			return;
		}
		
		if(ep.getOnlinePlayer().getInventory().getHelmet() != null)
		{
			p.sendMessage(this.plugin.messageEvent(ChatColor.RED + "Vous devez d'abord retirer votre casque !"));
			return;
		}
		
		EventPlayer parrain;
		if(who != null && !(who.startsWith("new:") && this.teamType.equals("AUTOTEAM")))
		{
		EventTeam et = this.getTeam(who);
		if(et == null)
		{
			parrain = this.getEventPlayer(who);
			if(parrain == null)
			{
				p.sendMessage(plugin.messageEvent(ChatColor.RED + "Ce n'est pas une équipe ni un joueur d'une équipe de cet event !"));
				return;
			}
			
			et = this.getTeam(parrain);
		}
		
		if(this.teamType.equals("INDIVIDUAL"))
		{
			p.sendMessage(plugin.messageEvent(ChatColor.RED + "C'est un event en individuel !"));
			return;
		}
		if(et.getPlayers().size() >= this.nbSlotTeam)
		{
			p.sendMessage(plugin.messageEvent(ChatColor.RED + "Il n'y a plus de place dans cette équipe !"));
			return;
		}
		//On peut join le who
		et.join(ep);
		return;
		}
	
		//Pas de who
		EventTeam et;
		
		switch(this.teamType)
		{
		case "INDIVIDUAL":
			
			if(this.teams.size() >= this.nbMaxTeam)
			{
				p.sendMessage(this.plugin.messageEvent(ChatColor.RED + "Ce event ne peut pas accueillir plus de "+ this.nbMaxTeam +" participants !"));
				return;
			}
			et = new EventTeam(this.plugin, this, ep.getPlayer().getUserName(), this.generateLetter(), this.generateColor());
			et.join(ep);
			return;
			
		case "FIXED":
			
				et = this.emptiestTeam();
				
				if(et == null)
				{
					p.sendMessage(this.plugin.messageEvent(ChatColor.RED + "Il est impossible de rejoindre cet event, aucune équipe n'a été créée."));
					return;
				}
				
				et.join(ep);
				return;
				
		case "AUTOTEAM":
			
				if(who != null && who.startsWith("new:"))
				{
					//Nouvelle équipe
					String teamName = who.substring(4);
					et = new EventTeam(this.plugin, this, teamName, this.generateLetter(), this.generateColor());
					et.join(ep);
					return;
				}
				
				et = this.emptiestTeam();
				
				if(et == null)
				{
					p.sendMessage(this.plugin.messageEvent(ChatColor.RED + "Toutes les équipes sont au complet, pour en créer : /event join "+this.name+" new:NomEquipe"));
					return;
				}
				
				et.join(ep);
				return;
		
		}
	}
	
	
	public String generateLetter()
	{
		
		String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		if(this.teams.size() >= chars.length())
		{
			return "ERROR";
		}
		
		char selected;
		String caractere = null;
		boolean choose = false;
		
		while(!choose)
		{
			int i = (int)Math.floor(Math.random() * chars.length());
			selected = chars.charAt(i);
			caractere = Character.toString(selected); 
			
			if(this.getTeam(caractere) == null)
			{
				choose = true;
			}
			
		}
		
		return caractere;
	}
	
	public ChatColor generateColor()
	{
		ArrayList<ChatColor> colors = new ArrayList<ChatColor>();
		colors.add(ChatColor.AQUA);
		colors.add(ChatColor.BLUE);
		colors.add(ChatColor.DARK_BLUE);
		colors.add(ChatColor.DARK_GRAY);
		colors.add(ChatColor.DARK_GREEN);
		colors.add(ChatColor.DARK_PURPLE);
		colors.add(ChatColor.DARK_RED);
		colors.add(ChatColor.GOLD);
		colors.add(ChatColor.GRAY);
		colors.add(ChatColor.GREEN);
		colors.add(ChatColor.LIGHT_PURPLE);
		colors.add(ChatColor.RED);
		colors.add(ChatColor.YELLOW);
		colors.add(ChatColor.WHITE);
		
		int i = (int)Math.floor(Math.random() * colors.size());
		return colors.get(i);
	}
	
	public String generateName()
	{
		ArrayList<String> names = new ArrayList<String>();
		names.add("Alpha");
		names.add("Bravo");
		names.add("Charlie");
		names.add("Delta");
		names.add("Echo");
		names.add("Foxtrot");
		names.add("Golf");
		names.add("Hotel");
		names.add("India");
		names.add("Juilet");
		names.add("Kilo");
		names.add("Lima");
		names.add("Mike");
		names.add("November");
		names.add("Oscar");
		names.add("Papa");
		names.add("Quebec");
		names.add("Romeo");
		names.add("Sierra");
		names.add("Tango");
		names.add("Uniform");
		names.add("Whiskey");
		names.add("X-Ray");
		names.add("Yankee");
		names.add("Zulu");
		
		int i = (int) Math.floor(Math.random() * names.size());
		return names.get(i);
	}
	
	public EventTeam emptiestTeam()
	{
		int size = 10000;
		EventTeam emptiest = null;
		if(this.teams.size() == 0)
		{
			return null;
		}
		for(EventTeam t : this.teams)
		{
			if(t.getPlayers().size() <= size)
			{
				size = t.getPlayers().size();
				emptiest = t;
				
			}
		}
		
		return emptiest;
	}
	
	public void cleanDeco()
	{
		for(EventPlayer ep : this.getEventPlayers())
		{
			if(!ep.getPlayer().isOnline())
			{
				ep.getTeam().leave(ep);
			}
		}
	}



	
	
	
}

