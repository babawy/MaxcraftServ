package fr.maxcraft;

public class FactionState {

	//VARIABLES
	MaxcraftManager plugin;
	int id;
	Faction faction1;
	Faction faction2;
	String state;
	
	
	//CONSTRUCTOR
	public FactionState(MaxcraftManager plugin, int id, Faction faction1, Faction faction2, String state)
	{
		this.plugin = plugin;
		this.id = id;
		this.faction1 = faction1;
		this.faction2 = faction2;
		this.state = state;
		
		this.plugin.listFactionStates.add(this);
	}
	
	//GETTER SETTER

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public Faction getFaction1() {
		return faction1;
	}


	public void setFaction1(Faction faction1) {
		this.faction1 = faction1;
	}


	public Faction getFaction2() {
		return faction2;
	}


	public void setFaction2(Faction faction2) {
		this.faction2 = faction2;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}
	
	

	//METHODES
	
	public void remove()
	{
		this.plugin.listFactionStates.remove(this);
	}
}
