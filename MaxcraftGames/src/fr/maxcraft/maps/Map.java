package fr.maxcraft.maps;

import java.io.File;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import fr.maxcraft.MaxcraftGames;

public class Map  {
	
	private MaxcraftGames plugin;
	private int id;
	private String name;
	private String worldName;
	private int slot;
	private Location pos1 = null;
	private Location pos2 = null;
	
	public Map(MaxcraftGames plugin, String name, String worldName)
	{
		this.plugin = plugin;
		this.name = name;
		this.worldName = worldName;
		this.slot = 8;
		this.plugin.MapManager.maps.add(this);
	}

	public void remove()
	{
		this.plugin.MapManager.maps.remove(this);
		this.removeFromConfig();
	}


	public File getWorldFile()
	{
		File file = new File(this.plugin.Multiverse.getServerFolder().getPath(), this.worldName);
		return file;
	}
	
	public void saveInConfig()
	{
		ConfigurationSection section = this.plugin.getConfig().createSection("maps."+ this.worldName);
		section.set("name", this.name);
		section.set("world", this.worldName);
		section.set("slot", this.slot);

		
		//Start Sign

		//Pos1
		
		if(this.pos1 != null)
		{
			section.set("pos1Placed", true);
			section.set("pos1.x", this.pos1.getX());
			section.set("pos1.y", this.pos1.getY());
			section.set("pos1.z", this.pos1.getZ());
			
		}
		else
		{
			section.set("pos1Placed", false);
		}
		
		//Pos2
		
		if(this.pos2 != null)
		{
			section.set("pos2Placed", true);
			section.set("pos2.x", this.pos2.getX());
			section.set("pos2.y", this.pos2.getY());
			section.set("pos2.z", this.pos2.getZ());
			
		}
		else
		{
			section.set("pos2Placed", false);
		}

		
		
		this.plugin.saveConfig();
	}
	
	
	public void removeFromConfig()
	{
		this.plugin.getConfig().set("maps."+this.worldName, null);
		this.plugin.saveConfig();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWorldName() {
		return worldName;
	}

	public void setWorldName(String worldName) {
		this.worldName = worldName;
	}

	public int getSlot() {
		return slot;
	}

	public void setSlot(int slot) {
		this.slot = slot;
	}

	public Location getPos1() {
		return pos1;
	}

	public void setPos1(Location pos1) {
		this.pos1 = pos1;
	}

	public Location getPos2() {
		return pos2;
	}

	public void setPos2(Location pos2) {
		this.pos2 = pos2;
	}
	
	
}
