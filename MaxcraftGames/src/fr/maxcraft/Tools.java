package fr.maxcraft;

import org.bukkit.Bukkit;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

public class Tools {

	private MaxcraftGames plugin;
	
	public Tools(MaxcraftGames plugin)
	{
		this.plugin = plugin;
	}
	
	public int ramdomInt(int min, int max)
	{
		return min + (int)(Math.random() * ((max - min) + 1));
	}
	
	public Sign reloadSign(Sign sign)
	{
		
		sign.update();
		sign.getChunk().unload();
		sign.getChunk().load();
		return sign;
	}
	
	public void sendMessagePermissions(String message, String perm)
	{
		for(Player p : Bukkit.getOnlinePlayers())
		{
		if(p.hasPermission(perm))
		{
			p.sendMessage(message);
		}
		}
	}
	
	 public boolean playerHasClearInventory(Player p) {
	        for (int i=0; i<=p.getInventory().getSize()-1; i++) {
	             if (!(p.getInventory().getItem(i) == null)) {
	                 return false;
	             }
	        }
	        
	        if(p.getInventory().getBoots() != null)
	        {
	        	return false;
	        }
	        if(p.getInventory().getChestplate() != null)
	        {
	        	return false;
	        }
	        if(p.getInventory().getLeggings() != null)
	        {
	        	return false;
	        }
	        if(p.getInventory().getHelmet() != null)
	        {
	        	return false;
	        }
	        return true;
	    }
	 
	 public void clearInventory(Player p)
	 {
		 p.getInventory().clear();
		 p.getInventory().setArmorContents(null);
	 }
	
}


