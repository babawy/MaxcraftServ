package fr.maxcraft.managers;

import java.io.File;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import fr.maxcraft.GamePlayer;
import fr.maxcraft.GameTeam;
import fr.maxcraft.MaxcraftGames;
import fr.maxcraft.games.dj.DonjonGame;
import fr.maxcraft.maps.Donjon;
import fr.maxcraft.maps.Map;

public class MapManager {

	private MaxcraftGames plugin;
	public static ArrayList<Map> maps = new ArrayList<Map>();
	
	public MapManager(MaxcraftGames plugin)
	{
		this.plugin = plugin;
		
		//CHARGEMENT DES MAPS DE LA CONFIG
	
				FileConfiguration config = this.plugin.getConfig();
				String path;
				Location pos1;
				Location pos2;
				ConfigurationSection donjons = config.getConfigurationSection("maps");
				
				if(donjons == null)
				{
					return;
				}
				for(String key : donjons.getKeys(false))
				{
					
					pos1 = null;
					pos2 = null;
					
					
					path = "maps."+key+".";
					
					
					Map map = new Map(this.plugin, config.getString(path + "name"), config.getString(path + "world"));
				
			
					if(config.getBoolean(path + "pos1Placed"))
					{
						pos1 = new Location(Bukkit.getWorld(map.getWorldName()), (float) config.getDouble(path + "pos1.x"), (float) config.getDouble(path + "pos1.y"), (float) config.getDouble(path + "pos1.z"));
						map.setPos1(pos1);
					}
					
					if(config.getBoolean(path + "pos2Placed"))
					{
						pos2 = new Location(Bukkit.getWorld(map.getWorldName()), (float) config.getDouble(path + "pos2.x"), (float) config.getDouble(path + "pos2.y"), (float) config.getDouble(path + "pos2.z"));
						map.setPos2(pos2);
					}
					
					
				}
		
	}
	
	public Map getMap(String worldName)
	{
		for(Map map : this.maps)
		{
			if(map.getWorldName().equals(worldName))
			{
				return map;
			}
		}
		return null;
	}

	
	public boolean add(String world, String name)
	{
		if(this.getMap(world) != null)
		{
			return false;
		}
		
		if(!this.plugin.WorldManager.isInFolder(world))
		{
			return false;
		}
		
		Map map = new Map(this.plugin,name, world);
		File worldfile = map.getWorldFile();
		map.saveInConfig();
		return true;
	}
	
	public boolean remove(String map)
	{
		Map dj = this.getMap(map);
		if(dj == null)
		{
			return false;
		}
		
		dj.remove();
		return true;
	}
	
	
	public boolean save(String mapstr)
	{
		
		Map map = this.getMap(mapstr);
		if(map == null)
		{
			return false;
		}
		
		Bukkit.getWorld(map.getWorldName()).save();
		return true;
	}
	
	public boolean edit(Player p, String worldName)
	{
		Map map = this.getMap(worldName);
		
		if(map == null)
		{
			return false;
		}
		
		this.plugin.WorldManager.load(map.getWorldName());
		World djWorld = Bukkit.getWorld(worldName);
		p.teleport(djWorld.getSpawnLocation());
		return true;
		
	}
	
	public boolean set(String worldName, String tag, String value, Player p)
	{
		Map map = this.getMap(worldName);
		
		if(map == null)
		{
			return false;
		}
		
		switch(tag)
		{
		case "pos1":
			map.setPos1(p.getLocation());
			break;
		case "pos2":
			map.setPos2(p.getLocation());
			break;
		default : return false;
			
		}
		
		map.saveInConfig();
		return true;
	}
	
}
