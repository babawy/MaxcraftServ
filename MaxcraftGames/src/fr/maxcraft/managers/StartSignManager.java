package fr.maxcraft.managers;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import fr.maxcraft.MaxcraftGames;
import fr.maxcraft.StartSign;


public class StartSignManager {

	private MaxcraftGames plugin;
	public static ArrayList<StartSign> startSigns = new ArrayList<StartSign>();
	
	public StartSignManager(MaxcraftGames plugin)
	{
		this.plugin = plugin;
		
		//Chargement depuis config
		
				FileConfiguration config = this.plugin.getConfig();
				Location startSign;
				String path;
				
				ConfigurationSection startsignsconf = config.getConfigurationSection("startsigns");
				if(startsignsconf == null) return;
				for(String key : startsignsconf.getKeys(false))
				{
					path = "startsigns."+key+".";
					
					 boolean closed = false;
				      if (config.contains(path + "closed"))
				      {
				        closed = config.getBoolean(path + "closed");
				      }
				      
					startSign = new Location(Bukkit.getWorld(config.getString(path + "world")), config.getInt(path + "x"), config.getInt(path + "y"), config.getInt(path + "z"));
					if (startSign.getWorld()==null)
						continue;
					if((startSign.getBlock().getState() instanceof Sign))
					{
						
					
					
					Sign s = (Sign) startSign.getBlock().getState();
					if(s != null && s instanceof Sign)
					{
					StartSign ss = new StartSign(this.plugin, s, config.getString(path + "gametype"), config.getString(path + "param"), config.getInt(path + "id"), closed);
					ss.loadSign();
					}
					
					}
				}
	}
	
	public StartSign getSign(Block b)
	{
		for(StartSign ss : StartSignManager.startSigns)
		{
			if(ss.getSign().getBlock().equals(b))
			{
				return ss;
			}
		}
		return null;
	}
	
	public void saveInConfig(StartSign s)
	{
		ConfigurationSection section = this.plugin.getConfig().createSection("startsigns."+ s.getId());
		section.set("gametype", s.getGameType());
		section.set("param", s.getParameter());
		section.set("id", s.getId());
		section.set("closed", s.isClosed());
		
		//Start Sign
		section.set("x", s.getSign().getLocation().getX());
		section.set("y", s.getSign().getLocation().getY());
		section.set("z", s.getSign().getLocation().getZ());
		section.set("world", s.getSign().getLocation().getWorld().getName());
		
		this.plugin.saveConfig();
	}
	
	public void removeFromConfig(StartSign s)
	{
		this.plugin.getConfig().set("startsigns."+s.getId(), null);
		this.plugin.saveConfig();
	}
}
