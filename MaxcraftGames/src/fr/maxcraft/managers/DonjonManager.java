package fr.maxcraft.managers;

import java.io.File;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import fr.maxcraft.GamePlayer;
import fr.maxcraft.GameTeam;
import fr.maxcraft.MaxcraftGames;
import fr.maxcraft.games.dj.DonjonGame;
import fr.maxcraft.maps.Donjon;

public class DonjonManager {

	private MaxcraftGames plugin;
	public static ArrayList<Donjon> donjons = new ArrayList<Donjon>();
	
	public DonjonManager(MaxcraftGames plugin)
	{
		this.plugin = plugin;
		
		//CHARGEMENT DES DONJONS DE LA CONFIG
	
				FileConfiguration config = this.plugin.getConfig();
				String path;
				Location pos1;
				Location pos2;
				ConfigurationSection donjons = config.getConfigurationSection("donjons");
				if(donjons == null)
				{
					return;
				}
				for(String key : donjons.getKeys(false))
				{
					
					pos1 = null;
					pos2 = null;
				
					
					path = "donjons."+key+".";
					
					
					Donjon donjon = new Donjon(this.plugin, config.getString(path + "name"), config.getString(path + "world"));
					donjon.setBuild(config.getBoolean(path + "build"));
					donjon.setOpen(config.getBoolean(path + "open"));
					donjon.setLifeAmount(config.getInt(path + "lifeAmount"));
					donjon.setClrInv(config.getBoolean(path + "clrInv"));
					donjon.setSlot(config.getInt(path + "slot"));
					
			
					
					if(config.getBoolean(path + "pos1Placed"))
					{
						pos1 = new Location(Bukkit.getWorld(donjon.getWorldName()), (float) config.getDouble(path + "pos1.x"), (float) config.getDouble(path + "pos1.y"), (float) config.getDouble(path + "pos1.z"));
						donjon.setPos1(pos1);
					}
					
					if(config.getBoolean(path + "pos2Placed"))
					{
						pos2 = new Location(Bukkit.getWorld(donjon.getWorldName()), (float) config.getDouble(path + "pos2.x"), (float) config.getDouble(path + "pos2.y"), (float) config.getDouble(path + "pos2.z"));
						donjon.setPos2(pos2);
					}
				
					
				}
		
	}
	
	public Donjon getDonjon(String worldName)
	{
		for(Donjon j : this.donjons)
		{
			if(j.getWorldName().equals(worldName))
			{
				return j;
			}
		}
		return null;
	}

	
	public boolean add(String world, String name)
	{
		if(this.getDonjon(world) != null)
		{
			return false;
		}
		
		if(!this.plugin.WorldManager.isInFolder(world))
		{
			return false;
		}
		
		Donjon donjon = new Donjon(this.plugin,name, world);
		File worldfile = donjon.getWorldFile();
		donjon.saveInConfig();
		return true;
	}
	
	public boolean remove(String donjon)
	{
		Donjon dj = this.getDonjon(donjon);
		
		if(dj == null)
		{
			return false;
		}
		
		dj.remove();
		return true;
	}
	
	public boolean set(String worldName, String tag, String value, Player p)
	{
		Donjon donjon = this.getDonjon(worldName);
		
		if(donjon == null)
		{
			return false;
		}
		
		switch(tag)
		{
		case "build":
			if(value.equals("true"))
			{
				donjon.setBuild(true);
			}
			if(value.equals("false"))
			{
				donjon.setBuild(false);
			}
			break;
		case "clrinv":
			if(value.equals("true"))
			{
				donjon.setClrInv(true);
			}
			if(value.equals("false"))
			{
				donjon.setClrInv(false);
			}
			break;
		case "slot":
			donjon.setSlot(Integer.parseInt(value));
			break;
		case "life":
			donjon.setLifeAmount(Integer.parseInt(value));
			break;
		case "pos1":
			donjon.setPos1(p.getLocation());
			break;
		case "pos2":
			donjon.setPos2(p.getLocation());
			break;
		default : return false;
			
		}
		
		donjon.saveInConfig();
		return true;
	}
	
	public boolean save(String donjon)
	{
		Donjon d = this.getDonjon(donjon);
		
		if(d == null)
		{
			return false;
		}
		
		Bukkit.getWorld(d.getWorldName()).save();
		return true;
	}
	
	public boolean edit(Player p, String worldName)
	{
		Donjon donjon = this.getDonjon(worldName);
		
		if(donjon == null)
		{
			return false;
		}
		
		this.plugin.WorldManager.load(donjon.getWorldName());
		World djWorld = Bukkit.getWorld(worldName);
		p.teleport(djWorld.getSpawnLocation());
		return true;
		
	}
	
	public boolean test(String dj, Player p)
	{
		Donjon donjon = this.getDonjon(dj);
		if(donjon == null)
		{
			return false;
		}
		
		//Game
		DonjonGame djg = new DonjonGame(this.plugin, dj, true,p);
		
		//Echec du demarrage (impossible de rejoindre ou non trouvé)
				if(this.plugin.getGame(djg.getId()) == null)
				{
					return false;
				}
		
		//Provider
		GameTeam gt = new GameTeam(this.plugin, p.getName(), djg);
		GamePlayer gp = new GamePlayer(this.plugin, p, djg);
		gt.add(gp);
		djg.getTeams().add(gt);
		djg.playerJoin(gp);
		
		
		return true;
	}
}
