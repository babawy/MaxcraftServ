package fr.maxcraft;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

import fr.maxcraft.games.dj.DonjonGame;
import fr.maxcraft.games.rainbowrunner.RainbowRunnerGame;

public class StartSign {

	private MaxcraftGames plugin;
	private Sign sign;
	private String gameType;
	private String parameter;
	private int gameChoiceId;
	private int id;
	private boolean closed;
	
	public StartSign(MaxcraftGames plugin, Sign sign, String gameType, String param, int id, boolean closed)
	{
		this.plugin = plugin;
		this.plugin.StartSignManager.startSigns.add(this);
		this.sign = sign;
		this.gameType = gameType;
		this.gameChoiceId = 0;
		this.parameter = param;
		this.id = id;
		this.closed = closed;
		this.loadSign();
		
		
	}
	
	public void remove()
	{
		this.plugin.StartSignManager.startSigns.remove(this);
		this.plugin.StartSignManager.removeFromConfig(this);
	}

	public Sign getSign() {
		return sign;
	}

	public void setSign(Sign sign) {
		this.sign = sign;
	}

	public String getGameType() {
		return gameType;
	}

	public void setGameType(String gameType) {
		this.gameType = gameType;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
	
	
	public boolean isClosed() {
		return closed;
	}

	public void setClosed(boolean closed) {
		this.closed = closed;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void onClickJoin(Player p)
	{
		Game game;
		
		if(this.gameChoiceId == -1)
		{
			this.sign.getBlock().setType(Material.AIR);
			this.remove();
			p.sendMessage(this.plugin.message("StartSign retiré !"));
			return;
		}
		
		if(this.gameChoiceId == -3)
		{
			if(this.closed) this.closed = false;
			else this.closed = true;
			this.plugin.StartSignManager.saveInConfig(this);
			this.gameChoiceId = 0;
			this.loadSign();
			return;
		}
		
		if(this.gameChoiceId == 0 || this.gameChoiceId == -2) // -2 = DEV MOD
		//NEW 
		{

			if (this.closed)
			{
			        p.sendMessage(this.plugin.message("Ce jeu est fermé pour le moment, repassez plus tard !"));
			        return;
			}
			
		boolean devmod = false;
		if(this.gameChoiceId == -2) devmod = true;
		
		p.sendMessage(this.plugin.message("Chargement du jeu en cours..."));
			
		//Game
		switch(this.gameType)
		{
		case "DJ":
			game = new DonjonGame(this.plugin, this.parameter, devmod, p);
			break;
		case "RR":
			game = new RainbowRunnerGame(this.plugin, this.parameter, devmod);
			break;
		default:
			return;
		}
		}
		else
		{
			//JOIN
			game = this.plugin.getGame(this.gameChoiceId);
		}
		
		//Echec du demarrage (impossible de rejoindre ou non trouvé)
		if(this.plugin.getGame(game.getId()) == null)
		{
			return;
		}
		
		//Provider
		GameTeam gt = new GameTeam(this.plugin, p.getName(), game);
		GamePlayer gp = new GamePlayer(this.plugin, p, game);
		gt.add(gp);
		game.getTeams().add(gt);
		game.playerJoin(gp);
		
		this.gameChoiceId = 0;
		this.loadSign();
	}
	
	public void onClickSwitch(Player p)
	{
		ArrayList<Integer> choices = new ArrayList<Integer>();
		choices.add(0);
		
		if(p.hasPermission("maxcraft.admin.game"))
		{
			choices.add(-1); //Remove
			choices.add(-2); //DevMod
			choices.add(-3); //Close/Open
		}
		
		for(Game g : MaxcraftGames.listGames){
			if(g.getGameType().equals(this.gameType) && g.getParameter().equals(this.parameter) && g.isJoinable(p))
			{
				choices.add(g.getId());
			}
		}
		
		if(!choices.contains(this.gameChoiceId))
		{
			this.gameChoiceId = 0;
			
		}
		else
		{
			if(choices.indexOf(this.gameChoiceId) == choices.size()-1)
			{
				this.gameChoiceId = 0;
			}
			else
			{
			this.gameChoiceId = (int) choices.get(choices.indexOf(this.gameChoiceId) + 1);
			}
		}
		
		this.loadSign();
		return;
	}
	
	
	public void loadSign()
	{
		
		if(this.sign == null)
		{
			return;
		}
		
		this.sign = (Sign) this.sign.getBlock().getState();
		
		if(this.gameChoiceId == 0)
		{
			if(!this.closed)
			{
			sign.setLine(2, ""+ChatColor.GREEN + ChatColor.BOLD + "NOUVEAU");
			}
			else
			{
			sign.setLine(2, ""+ChatColor.DARK_RED + ChatColor.BOLD + "Jeu fermé !");
			}
			sign.setLine(3, "");
		}
		else if(this.gameChoiceId == -1)
		{
			sign.setLine(2, ChatColor.DARK_RED+ ""+ ChatColor.BOLD + "SUPPRIMER");
			sign.setLine(3, "* Admin *");
		}
		else if(this.gameChoiceId == -2)
		{
			sign.setLine(2, ChatColor.AQUA +""+ ChatColor.BOLD + "DEV MOD");
			sign.setLine(3, "* Admin *");
		}
		else if(this.gameChoiceId == -3)
		{
			if(this.closed)
			{
			sign.setLine(2, ChatColor.GOLD +""+ ChatColor.BOLD + "OUVRIR");
			}
			else
			{
				sign.setLine(2, ChatColor.GOLD +""+ ChatColor.BOLD + "FERMER");	
			}
			sign.setLine(3, "* Admin *");
		}
		else
		{
			Game current = this.plugin.getGame(this.gameChoiceId);
			sign.setLine(2, current.getStartSignLines()[0]);
			sign.setLine(3, current.getStartSignLines()[1]);
		}
		
		sign.setLine(1, ChatColor.RED+""+ ChatColor.BOLD+"***************");
		
		this.plugin.tools.reloadSign(this.sign);
	}
		
	
}
