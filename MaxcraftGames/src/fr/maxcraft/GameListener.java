package fr.maxcraft;

import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Painting;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;

public class GameListener implements Listener{

	//VARIABLES
	private MaxcraftGames plugin;
				
			
	//CONSTRUCTOR
	public GameListener(MaxcraftGames p)
	{
	this.plugin = p;
	}
	
	//Empecher le spawn naturel
	@EventHandler(priority = EventPriority.NORMAL)
	public void onCreatureSpawn(CreatureSpawnEvent e){
	
		
		for(Instance i : this.plugin.listInstances)
		{
			if(i.getWorld().equals(e.getLocation().getWorld()))
			{
				i.getGame().onCreatureSpawn(e);
				return;
			}
		}
		
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onBlockBreak(BlockBreakEvent e){
	
		for(GamePlayer gp : this.plugin.getGamePlayers())
		{
			if(gp.getPlayer().equals(e.getPlayer()))
			{
			gp.getGame().onBlockBreak(e);
			}
		}
		
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onBlockPlace(BlockPlaceEvent e){
	
		for(GamePlayer gp : this.plugin.getGamePlayers())
		{
			if(gp.getPlayer().equals(e.getPlayer()))
			{
			gp.getGame().onBlockPlace(e);
			}
		}
		
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onHangingBreakByEntity(HangingBreakByEntityEvent e)
	{
		for(Instance i : this.plugin.listInstances)
		{
			if(i.getWorld().equals(e.getEntity().getLocation().getWorld()))
			{
				i.getGame().onHangingBreakByEntity(e);
				return;
			}
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onCraftItem(CraftItemEvent e)
	{
		for(Instance i : this.plugin.listInstances)
		{
			if(i.getWorld().equals(e.getWhoClicked().getLocation().getWorld()))
			{
				i.getGame().onCraftItem(e);
				return;
			}
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onVehicleDestroy(VehicleDestroyEvent e)
	{
		for(Instance i : this.plugin.listInstances)
		{
			if(i.getWorld().equals(e.getVehicle().getLocation().getWorld()))
			{
				i.getGame().onVehicleDestroy(e);
				return;
			}
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerQuit(PlayerQuitEvent e){
		
		for(GamePlayer gp : this.plugin.getGamePlayers())
		{
			if(gp.getPlayer().equals(e.getPlayer()))
			{
			gp.getGame().onPlayerQuit(e);
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerRespawn(PlayerRespawnEvent e){
		for(GamePlayer gp : this.plugin.getGamePlayers())
		{
			if(gp.getPlayer().equals(e.getPlayer()))
			{
			gp.getGame().onPlayerRespawn(e);
			}
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerTeleport(PlayerTeleportEvent e){
		
		for(GamePlayer gp : this.plugin.getGamePlayers())
		{
			if(gp.getPlayer().equals(e.getPlayer()))
			{
			gp.getGame().onPlayerTeleport(e);
			}
		}
		
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerTeleport(EntityDamageEvent e){
		
		if(!(e.getEntity() instanceof Player))
		{
			return;
		}
		Player p = (Player) e.getEntity();
		
		for(GamePlayer gp : this.plugin.getGamePlayers())
		{
			if(gp.getPlayer().equals(p))
			{
			gp.getGame().onPlayerDamage(e);
			}
		}
		
	}
	
}
