package fr.maxcraft;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;


import com.onarandombox.MultiverseCore.MultiverseCore;
import com.onarandombox.MultiverseCore.api.MultiverseWorld;

import fr.maxcraft.managers.DonjonManager;
import fr.maxcraft.managers.StartSignManager;
import fr.maxcraft.managers.MapManager;
import fr.maxcraft.maps.Donjon;

public class MaxcraftGames extends JavaPlugin {
	
	//PLUGINS
	public static MaxcraftManager MM = null;
	public static MaxcraftZones MZ = null;
	public static MultiverseCore Multiverse = null;
	
	//UTILS
	public FileUtils fileU;

	//LISTES
	public static ArrayList<Instance> listInstances = new ArrayList<Instance>();
	public static ArrayList<Game> listGames = new ArrayList<Game>();

	
	//LISTENERS
	private GameListener GL;
	private InstanceListener IL;
	private LauncherListener LL;
	
	//MANAGERS
	public CommandManager CommandManager = new CommandManager(this);
	public WorldManager WorldManager = new WorldManager(this);
	public Tools tools = new Tools(this);
	public DonjonManager DonjonManager;
	public StartSignManager StartSignManager;
	public MapManager MapManager;
	
	//AUTRE
	public int instanceId = 1;
	public int gameId = 1;

	@Override
	public void onEnable() {
		
		super.onEnable();
		
		//Configuration
		saveDefaultConfig();
				
		//Plugins
		this.MM = this.getMM();
		this.MZ = this.getMZ();
		this.Multiverse = this.getMultiverseCore();
		
		//Listeners register
		this.GL = new GameListener(this);
		this.IL = new InstanceListener(this);
		this.LL = new LauncherListener(this);
		this.getServer().getPluginManager().registerEvents(this.GL, this);
		this.getServer().getPluginManager().registerEvents(this.IL, this);
		this.getServer().getPluginManager().registerEvents(this.LL, this);
		
		//MANAGERS
		this.DonjonManager = new DonjonManager(this);
		this.StartSignManager = new StartSignManager(this);
		this.MapManager = new MapManager(this);
		
		//Suppression des instanceX anciennes
		File file;
		for(MultiverseWorld w : this.Multiverse.getMVWorldManager().getMVWorlds())
		{
			if(w.getName().matches("instance.*"))
			{
				file = new File(this.Multiverse.getServerFolder().getPath(), w.getName());
				this.Multiverse.getMVWorldManager().removeWorldFromConfig(w.getName());
				try {
					this.fileU.deleteDirectory(file);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	@Override
	public void onDisable() {
		
		super.onDisable();
		
		//Suppresion des Game
		for(Game g : this.listGames)
		{
					g.onServerStop();
					g.remove();
		}
	}
	
	/*
	 * Affichage dans console
	 */
	public void log(String message, String title, ChatColor color)
	{
		ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
		console.sendMessage(color +""+ ChatColor.BOLD + "["+title+"] "+ ChatColor.RESET + message);
		
	}
	public void log(String message, String title)
	{
		ChatColor color = ChatColor.RED;
		log(message, title,color);
		
	}
	
	public void log(String message)
	{
		ChatColor color = ChatColor.RED;
		log(message, "Games",color);
		
	}
	
	/*
	 * Messages
	 */
	public String message(String message, String title)
	{
		return ChatColor.GOLD + "["+title+"] " + ChatColor.GRAY + message;
	}
	
	public String message(String message)
	{
		return message(message, "Jeu");
	}
	
	/*
	 * Chargement des plugins
	 */
	public MultiverseCore getMultiverseCore() {
		Plugin plugin = getServer().getPluginManager().getPlugin("Multiverse-Core");
		log("MultiVerse chargé !");
		return (MultiverseCore) plugin;
	}
	
	public MaxcraftManager getMM()
	{
		MaxcraftManager mm = (MaxcraftManager) this.getServer().getPluginManager().getPlugin("MaxcraftManager");
        log("MaxcraftManager chargé !");
		return mm;
	}
	
	public MaxcraftZones getMZ()
	{
		MaxcraftZones mz = (MaxcraftZones) this.getServer().getPluginManager().getPlugin("MaxcraftZones");
        log("MaxcraftZones chargé !");
		return mz;
	}
	
	/*
	 * Commandes
	 */
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		
		
		
		//COMMANDE GENERALE ADMIN
		if(cmd.getName().equalsIgnoreCase("game")){ 
		
			this.CommandManager.command(sender, args);
			return true;
		}
		
		//COMMMANDES DES GAMES
		else if(cmd.getName().equalsIgnoreCase("donjon")){ 
			
			this.CommandManager.gameCommand("dj", sender, args);
			return true;
		}
		return false;
		
	}
	
	/*
	 * Obtenir GamePlayer
	 */
	
	public GamePlayer getGamePlayer(OfflinePlayer p)
	{
		
			for(GamePlayer gp : this.getGamePlayers())
			{
				if(p.equals(gp.getPlayer()))
				{
					return gp;
				}
			}
		return null;
	}
	
	public ArrayList<GamePlayer> getGamePlayers()
	{
		ArrayList<GamePlayer> players = new ArrayList<GamePlayer>();
		
		for(Game g : this.listGames)
		{
			for(GamePlayer gp : g.getPlayers())
			{
				players.add(gp);
			}
		}
		return players;
	}
	
	
	public Game getGame(int id)
	{
		for(Game g : this.listGames)
		{
			if(g.getId() == id)
			{
				return g;
			}
		}
		return null;
	}

}
