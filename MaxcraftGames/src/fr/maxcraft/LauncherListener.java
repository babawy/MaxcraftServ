package fr.maxcraft;


import org.bukkit.GameMode;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import fr.maxcraft.managers.StartSignManager;


public class LauncherListener implements Listener {

	//VARIABLES
		private MaxcraftGames plugin;
					
				
		//CONSTRUCTOR
		public LauncherListener(MaxcraftGames p)
		{
		this.plugin = p;
		}
		
		
		@EventHandler(priority = EventPriority.NORMAL)
		public void onStartSignPlaced(SignChangeEvent e) throws ClassNotFoundException, InstantiationException, IllegalAccessException
		{
			if(!(e.getLine(2).equals("=startsign") || e.getLine(2).equals("=ss")))
			{
				return;
			}
			
			if(!e.getPlayer().hasPermission("maxcraft.admin.game"))
			{
				return;
			}
				
				String datas[] = e.getLine(3).split(":");
				String gameType  = datas[0];
				
				String param;
				if(datas.length >= 2) param = datas[1];
				else param = "noparam";
				
		
				
			switch(gameType)
			{
			case "dj": case "DJ":
				gameType = "DJ";
				break;
			case "rr": case "RR":
				gameType = "RR";
				break;
			default:
				e.getPlayer().sendMessage(this.plugin.message("Le type de jeu est inconnu."));
				return;
			}
			
			StartSign ss = new StartSign(this.plugin, (Sign) e.getBlock().getState(), gameType, param, this.plugin.tools.ramdomInt(1, 1000000000), false);
			this.plugin.StartSignManager.saveInConfig(ss);
			e.getPlayer().sendMessage(this.plugin.message("StartSign placé (type: "+ gameType +",paramètre: "+param+")!"));
			return;
			
		}
		
		@EventHandler(priority = EventPriority.NORMAL)
		public void onStartSignBreak(BlockBreakEvent e)
		{
			StartSign ss = this.plugin.StartSignManager.getSign(e.getBlock());
			
			if(ss != null)
			{
				ss.remove();
				e.getPlayer().sendMessage(this.plugin.message("StartSign retiré !"));
				return;
			}
		}
		
		@EventHandler(priority = EventPriority.NORMAL)
		public void onStartSignClicked(PlayerInteractEvent e)
		{
			
			if(!(e.getAction().equals(Action.LEFT_CLICK_BLOCK) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK))) return;
			StartSign ss = this.plugin.StartSignManager.getSign(e.getClickedBlock());
			
			if(ss == null) return;
			
			if(e.getPlayer().getGameMode().equals(GameMode.CREATIVE))
			{
				e.setCancelled(true);
			}
			
			if(!e.getPlayer().hasPermission("maxcraft.games"))
			{
				e.getPlayer().sendMessage(this.plugin.message("Vous n'avez pas la permissions d'entrer dans un jeu."));
			}
			
			if(e.getAction().equals(Action.LEFT_CLICK_BLOCK)) ss.onClickJoin(e.getPlayer());
			if(e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) 
			{
				ss.onClickSwitch(e.getPlayer());
			}
			return;
			
		}
		
		
}
