package fr.maxcraft;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.maxcraft.managers.DonjonManager;
import fr.maxcraft.managers.MapManager;
import fr.maxcraft.managers.StartSignManager;
import fr.maxcraft.maps.Donjon;

public class CommandManager {
	
	private MaxcraftGames plugin;
	
	public CommandManager(MaxcraftGames plugin)
	{
		this.plugin = plugin;
	}
	
	/*
	 * COMMANDE SPECIFIQUES INTRA-GAME
	 */
	
	public void gameCommand(String type, CommandSender sender, String[] args)
	{

		Player p = (Player) sender;
	
		
		GamePlayer gp = this.plugin.getGamePlayer(p);
		
		if(gp == null )
		{
			sender.sendMessage(this.plugin.message("Vous n'êtes pas en jeu !"));
			return;
		}
		
		gp.getGame().onCommand(gp, args);
	}
	
	/*
	 * COMMANDE GAME
	 */
	
	public void command(CommandSender sender, String[] args)
	{
		
		if(args.length == 0)
		{
			sender.sendMessage(this.plugin.message("Commande incomplète."));
			return;
		}
		switch(args[0])
		{
		case "dj": case "donjon":
			this.dj(sender, args);
			break;
		case "list": case "l":
			this.listGames(sender);
			break;
		case "reload": case "r": case "load":
			this.reload(sender);
			break;
		case "remove": case "rem":
			this.removeGame(sender, args);
			break;
		case "map": case "m":
			this.map(sender, args);
			break;
		}
	}
	
	/*
	 * DONJONS
	 */
	
	public void dj(CommandSender sender, String[] args)
	{
		if(args.length >= 1)
		{
		
		switch(args[1])
		{
		case "create": case "c":
			if(this.plugin.DonjonManager.add(args[2], this.plugin.MM.multiArgs(args, 3)))
			{
				sender.sendMessage(this.plugin.message("Le donjon à été ajouté !"));
			}
			else
			{
				sender.sendMessage(this.plugin.message("Vous ne pouvez pas créer ce donjon !"));
			}
			return;
		case "list": case "l":
			sender.sendMessage(this.plugin.message(" *** LISTE DES DONJONS *** "));
			for(Donjon dj : this.plugin.DonjonManager.donjons)
			{
				sender.sendMessage("- "+dj.getWorldName() +" : " + dj.getName());
			}
			return;
		case "remove": case "r":
			if(!sender.hasPermission("maxcraft.admin.game.dj."+ args[2]))
			{
				sender.sendMessage(this.plugin.message("Vous n'avez pas la permission sur ce donjon !"));
			}
			if(this.plugin.DonjonManager.remove(args[2]))
			{
				sender.sendMessage(this.plugin.message("Le donjon à été supprimé !"));
			}
			else
			{
				sender.sendMessage(this.plugin.message("Vous ne pouvez pas supprimer ce donjon !"));
			}
			return;
			
	
			
		case "edit": case "e":
			if(!sender.hasPermission("maxcraft.admin.game.dj."+ args[2]))
			{
				sender.sendMessage(this.plugin.message("Vous n'avez pas la permission sur ce donjon !"));
			}
			if(this.plugin.DonjonManager.edit((Player) sender, args[2]))
			{
				sender.sendMessage(this.plugin.message("Vous êtes maintenant en mode EDITION !"));
			}
			else
			{
				sender.sendMessage(this.plugin.message("Le mode EDITION de ce donjon est inacessible."));
			}
			return;
			
		case "test": case "t":
			
			if(!sender.hasPermission("maxcraft.admin.game.dj."+ args[2]))
			{
				sender.sendMessage(this.plugin.message("Vous n'avez pas la permission sur ce donjon !"));
			}
			
			if(this.plugin.DonjonManager.test(args[2], (Player) sender))
			{
				sender.sendMessage(this.plugin.message("Vous allez être téléporté à une instance en mode DEV..."));
			}
			else
			{
				sender.sendMessage(this.plugin.message("Impossible, erreur !"));
			}
			return;	
					
		case "save": case "sauvegarde": case "sauver":
			if(!sender.hasPermission("maxcraft.admin.game.dj."+ args[2]))
			{
				sender.sendMessage(this.plugin.message("Vous n'avez pas la permission sur ce donjon !"));
			}
			
			if(this.plugin.DonjonManager.save(args[2]))
			{
				sender.sendMessage(this.plugin.message("Donjon sauvegardé !"));
			}
			else
			{
				sender.sendMessage(this.plugin.message("Sauvegarde de ce donjon impossible !"));
			}
			return;
		
		case "set": case "s":
			
			if(!sender.hasPermission("maxcraft.admin.game."+ args[2]))
			{
				sender.sendMessage(this.plugin.message("Vous n'avez pas la permission sur ce donjon !"));
			}
			
			Player p = (Player) sender;
			String value = null;
			if(args.length == 5)
			{
				value = args[4];
			}
			
			if(this.plugin.DonjonManager.set(args[2], args[3], value, p))
			{
				sender.sendMessage(this.plugin.message("Propriété <"+args[3]+"> modifiée  !"));
			}
			else
			{
				sender.sendMessage(this.plugin.message("Cette propriété ne peut pas être modifiée."));
			}
			return;
		
		}
		
		}
		
		sender.sendMessage(this.plugin.message("Commande non reconnue..."));
	}
	
	public void listGames(CommandSender sender)
	{
		sender.sendMessage(this.plugin.message(" *** LISTE DES GAMES ("+this.plugin.getGamePlayers().size()+" Joueurs) *** "));
		for(Game game : this.plugin.listGames)
		{
			sender.sendMessage("- #"+game.getId()+" ["+game.getGameType()+"] "+ game.getParameter()+ " ("+game.getPlayers().size()+")");
		}
	}
	
	public void removeGame(CommandSender sender, String[] args)
	{
		int id = Integer.parseInt(args[1]);
		Game g = this.plugin.getGame(id);
		
		if(g == null)
		{
			sender.sendMessage(this.plugin.message("Game inconnu !"));
		}
		
		g.onServerStop();
		g.remove();
		
	}
	
	public void reload(CommandSender sender)
	{
		this.plugin.StartSignManager = new StartSignManager(this.plugin);
		this.plugin.DonjonManager = new DonjonManager(this.plugin);
		this.plugin.MapManager = new MapManager(this.plugin);
		
		sender.sendMessage(this.plugin.message("Configuration du plugin rechargée (StartSigns/Donjons/Maps) !"));
	}
	
	
	public void map(CommandSender sender, String[] args)
	{
		Player p = (Player) sender;
		switch(args[1])
		{
		case "set":
			
			String value = null;
			if(args.length == 5)
			{
				value = args[4];
			}
			
			if(this.plugin.MapManager.set(args[2], args[3], value, p))
			{
				sender.sendMessage(this.plugin.message("Position modifiée !"));
			}
			else
			{
				sender.sendMessage(this.plugin.message("Impossible ! Position non modifiée !"));
			}
			break;
		case "create": case "c":
			if(this.plugin.MapManager.add(args[2], this.plugin.MM.multiArgs(args, 3)))
			{
				sender.sendMessage(this.plugin.message("La map à été ajoutée !"));
			}
			else
			{
				sender.sendMessage(this.plugin.message("Vous ne pouvez pas créer cette map !"));
			}
			return;
		case "remove": case "r":
			if(this.plugin.MapManager.remove(args[2]))
			{
				sender.sendMessage(this.plugin.message("La map à été supprimée !"));
			}
			else
			{
				sender.sendMessage(this.plugin.message("Vous ne pouvez pas supprimer cette map !"));
			}
			return;
		}
	}
	
}