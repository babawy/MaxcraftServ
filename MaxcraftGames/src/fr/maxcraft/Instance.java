package fr.maxcraft;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;

import fr.maxcraft.games.dj.signs.CheckpointSign;
import fr.maxcraft.games.dj.signs.ExitSign;
import fr.maxcraft.signs.BlockSign;
import fr.maxcraft.signs.BossSign;
import fr.maxcraft.signs.CaptorSign;
import fr.maxcraft.signs.ChatSign;
import fr.maxcraft.signs.ClearInventorySign;
import fr.maxcraft.signs.EntranceSign;
import fr.maxcraft.signs.MagicSign;
import fr.maxcraft.signs.MarkerSign;
import fr.maxcraft.signs.PlayerSpawnSign;
import fr.maxcraft.signs.PulsorSign;
import fr.maxcraft.signs.RedstoneActivatorSign;
import fr.maxcraft.signs.RedstoneSign;
import fr.maxcraft.signs.SoundSign;
import fr.maxcraft.signs.SpawnerSign;
import fr.maxcraft.signs.StockChestSign;
import fr.maxcraft.signs.StrikeSign;
import fr.maxcraft.signs.TargetChestSign;
import fr.maxcraft.signs.TntSign;
import fr.maxcraft.signs.TpSign;


public class Instance {

	private MaxcraftGames plugin;
	private Game game;
	
	public ArrayList<GamePlayer> players = new ArrayList<GamePlayer>();
	public ArrayList<Object> listMagicSigns = new ArrayList<Object>();
	
	private int id;
	private File instanceFile;
	private File sourceWorldFile;

	private String sourceWorld;
	private boolean multiinv;
	private Location pos1;
	private Location pos2;
	
	
	public Instance(MaxcraftGames plugin, Game game, String sourceWorld, Location l1, Location l2, boolean multiinv)
	{
		this.plugin = plugin;
		this.id = this.plugin.instanceId;
		this.plugin.instanceId++;
		this.multiinv = multiinv;
		this.pos1= l1;
		this.pos2 = l2;
		
		this.game = game;
		this.sourceWorld = sourceWorld;

		
		this.plugin.listInstances.add(this);
		
		this.sourceWorldFile = this.getWorldFile(this.sourceWorld);
		this.instanceFile = new File(this.sourceWorldFile.getParent(), this.getWorldName());
		
		try {
			FileUtils.copyDirectory(this.sourceWorldFile, this.instanceFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		File uidFile = new File(instanceFile, "uid.dat");
        uidFile.delete();
		
		this.plugin.WorldManager.load(this.getWorldName());
		
		//COMPILATION
		this.compile();
		
		
	}
	
	public String getSourceWorld() {
		return sourceWorld;
	}

	public void setSourceWorld(String sourceWorld) {
		this.sourceWorld = sourceWorld;
	}

	public File getWorldFile(String name)
	{
		File file = new File(this.plugin.Multiverse.getServerFolder().getPath(), name);
		return file;
	}
	
	public void remove()
	{
		this.plugin.listInstances.remove(this);
		this.plugin.WorldManager.remove(this.getWorldName());
		
		try {
			this.plugin.fileU.deleteDirectory(this.getInstanceFile());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	public String getWorldName()
	{
		if(this.multiinv)
		{
			int id = this.id + 1000;
			return "instance"+id;
		}
		else
		{
		return "instance"+this.id;
		}
	}
	
	public File getInstanceFile()
	{
		File file = new File(this.plugin.Multiverse.getServerFolder().getPath(), this.getWorldName());
		return file;
	}
	

	public GamePlayer getPlayer(Player p)
	{
		for(GamePlayer gp: this.players)
		{
			if(gp.getPlayer().equals(p))
			{
				return gp;
			}
		}
		return null;
	}
	
	public World getWorld()
	{
		return Bukkit.getWorld(this.getWorldName());
	}
	
	public void compile()
	{
		
		//COMPILATION
		if(this.pos1 == null || this.pos2 == null)
		{
			return;
		}
		
		Block b;
		
		//ORDONNATION DES COORDONN2ES
		int Xmin;
		int Xmax;
		int Ymin;
		int Ymax;
		int Zmin;
		int Zmax;
		
		//X
		if(this.pos1.getBlockX() <= this.pos2.getBlockX())
		{
			Xmin = this.pos1.getBlockX();
			Xmax = this.pos2.getBlockX();
		}
		else
		{
			Xmin = this.pos2.getBlockX();
			Xmax = this.pos1.getBlockX();
		}
		//Y
		if(this.pos1.getBlockY() <= this.pos2.getBlockY())
		{
			Ymin = this.pos1.getBlockY();
			Ymax = this.pos2.getBlockY();
		}
		else
		{
			Ymin = this.pos2.getBlockY();
			Ymax = this.pos1.getBlockY();
		}
		//Z
		if(this.pos1.getBlockZ() <= this.pos2.getBlockZ())
		{
			Zmin = this.pos1.getBlockZ();
			Zmax = this.pos2.getBlockZ();
		}
		else
		{
			Zmin = this.pos2.getBlockZ();
			Zmax = this.pos1.getBlockZ();
		}
		
		//RECUPERATION DES SIGNS
		
		ArrayList<Sign> signs = new ArrayList<Sign>();
		Sign sign;
		for(int x = Xmin; x <= Xmax; x++)
		{
			for(int y = Ymin; y <= Ymax; y++)
			{
				for(int z = Zmin; z <= Zmax; z++)
				{
					
					
					b = this.getWorld().getBlockAt(x, y, z);
					if(b.getType().equals(Material.WALL_SIGN) || b.getType().equals(Material.SIGN_POST))
					{
						sign = (Sign) b.getState();
						//Panneau d'action (magic avec =)
						if(sign.getLine(0).matches("=.*"))
						{
							signs.add(sign);
						}
					}
				}
			}
		}
		
		//SUPPRESSION DES SIGNS
		for(Sign s : signs)
		{
		s.getBlock().setType(Material.AIR);
		}
		for (Entity e : getWorld().getEntities())
			if (e instanceof Item)
				e.remove();

		for(Sign s : signs)
		{
			// Choix de la classe correspondante parmis les classes de sign disponibles pour ce Game
			this.game.compileSign(s, this);
		}
		
		if(this.isDevMod())
		{
			this.plugin.tools.sendMessagePermissions(this.plugin.message("[COMPILATOR] MagicSigns : "+this.listMagicSigns.size()), "maxcraft.admin.game");
		
		}

		
	//ACTIONS POST-COMPILATION
	
	// Chests
	this.fillAllChests();
	
	// Restone Activators
		for(Object o : this.listMagicSigns)
		{
			if(o instanceof RedstoneActivatorSign)
			{
				RedstoneActivatorSign ra = (RedstoneActivatorSign) o;
				ra.getPosition().getBlock().setType(Material.SIGN_POST);
			}
		}
		
	// FIN DE COMPILATION
		
	}
	
	public ArrayList<CaptorSign> findCaptors(String type)
	{
		 ArrayList<CaptorSign> captors = new ArrayList<CaptorSign>();
		 for(Object o : this.listMagicSigns)
		 {
			 if(o instanceof CaptorSign)
			 {
				 CaptorSign captor = (CaptorSign) o;
				 
				 if(!captor.isActivated())
				 {
					captors.add(captor); 
				 }
 
			 }
		 }
		
		return captors;
	}
	
	public void fillAllChests()
	{
		 for(Object o : this.listMagicSigns)
		 {
			 if(o instanceof TargetChestSign)
			 {
				 TargetChestSign cs = (TargetChestSign) o;
				 
				cs.fill(this);
				 
				 
			 }
		 }
	}
	
	public GamePlayer getLeader()
	{
		for(GamePlayer gp : this.players)
		{
			return gp;
		}
		return null;
		
	}
	
	public boolean isDevMod()
	{
		return this.game.isDevMod();
	}
	
	public Location convertLocation(Location loc)
	{
		Location loca = loc.clone();
		loca.setWorld(this.getWorld());
		return loca;
	}
	
	public Location getStartLocation()
	{
		
		//On regarde si il n'y a pas des player spawn de libre
		
		PlayerSpawnSign nextSpawn = null;
		
		for(Object ms : this.listMagicSigns)
		{
			if(ms instanceof PlayerSpawnSign)
			{
				
				PlayerSpawnSign pss = (PlayerSpawnSign) ms;
				if(!pss.getUsed() && (nextSpawn == null || pss.getValue() < nextSpawn.getValue()))
				{
					nextSpawn = pss;
				}
				
				
			}
		}
		
		if(nextSpawn != null)
		{
			nextSpawn.setUsed(true);
			return nextSpawn.getPosition();
		}
		
		//Preference sur panneau a placer
		for(Object ms : this.listMagicSigns)
		{
			if(ms instanceof EntranceSign)
			{
				
				
				return ((MagicSign) ms).getPosition();
			}
		}
		
		Location start;
		if(this.pos1 != null)
		{
			return this.convertLocation(this.pos1);
		}
		else
		{
			return this.getWorld().getSpawnLocation();
		}
		
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public ArrayList<Object> getListMagicSigns() {
		return listMagicSigns;
	}

	public void setListMagicSigns(ArrayList<Object> listMagicSigns) {
		this.listMagicSigns = listMagicSigns;
	}
	
	public void sendAppel(int entry, boolean type, GamePlayer ip)
	{
		this.game.appel(entry, type, ip);
		for(Object ms : this.listMagicSigns)
		{
			if(!(ms instanceof MagicSign))
			{
				return;
			}
			
			MagicSign sign = (MagicSign) ms;

				
					for(int link : sign.getEntries())
					{
						
						if(link == entry)
						{
							if(type == true) //ON
							{
								sign.on(ip);
								if(this.isDevMod())
								{
									ip.getOnlinePlayer().sendMessage("Appel ON réalisé sur : "+sign.getType());
								}
							}
							
							else if(type == false) //OFF
							{
								sign.off(ip);
								
								if(this.isDevMod())
								{
									ip.getOnlinePlayer().sendMessage("Appel OFF réalisé sur : "+sign.getType());
								}
							}
								
							
						}
						
					}
					
		}
		
	}
	
	public GamePlayer getNearestPlayer(Location l)
	{
		GamePlayer nearest = null;
		double distance = 1000000;

		for(GamePlayer gp : this.players)
		{
			if(gp.isOnline())
			{
				if(l.distance(gp.getOnlinePlayer().getLocation()) < distance)
				{
					nearest = gp;
					distance = l.distance(gp.getOnlinePlayer().getLocation());
				}
			}
		}
		return nearest;
	}
	
	public void message(String message)
	{
		for(GamePlayer gp : this.players)
		{
			if(gp.isOnline())
			{
				gp.getOnlinePlayer().sendMessage(this.plugin.message(message));
			}
		}
	}
	
	public Location getMarker(String marker)
	{
		for(Object o : this.listMagicSigns)
		{
			if(o instanceof MarkerSign)
			{
				MarkerSign ms = (MarkerSign) o;
				if(ms.getMarker().equals(marker))
				{
					return ms.getPosition().getBlock().getLocation().clone().add(0.5, 0, 0.5);
				}
			}
		}
		return null;
	}
			
	
}
