package fr.maxcraft.games.dj;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Painting;
import org.bukkit.entity.Player;
import org.bukkit.entity.Vehicle;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;

import fr.maxcraft.Game;
import fr.maxcraft.GamePlayer;
import fr.maxcraft.Instance;
import fr.maxcraft.MaxcraftGames;
import fr.maxcraft.games.dj.signs.CheckpointSign;
import fr.maxcraft.games.dj.signs.ExitSign;
import fr.maxcraft.maps.Donjon;
import fr.maxcraft.signs.EntranceSign;
import fr.maxcraft.signs.MagicSign;
import fr.maxcraft.signs.PlayerSpawnSign;



public class DonjonGame extends Game{

	private Donjon donjon;
	public ArrayList<GamePlayer> loosers = new ArrayList<GamePlayer>();
	
	public DonjonGame(MaxcraftGames plugin, String parameter, boolean devmod, Player p) {
		
		super(plugin, parameter, devmod);
		
		this.donjon = this.plugin.DonjonManager.getDonjon(this.getParameter());
		
		if(this.donjon == null)
		{
			MaxcraftGames.listGames.remove(this);
			return;
		}
		
		if(p != null && !this.plugin.tools.playerHasClearInventory(p) && this.donjon.isClrInv())
		{
			p.sendMessage(this.plugin.message("Tu dois avoir un inventaire vide pour rejoindre ce donjon !"));
			MaxcraftGames.listGames.remove(this);
			return;
		}
		
		
		//Création de l'instance
		this.setInstance(new Instance(this.plugin, this, donjon.getWorldName(), donjon.getPos1(), donjon.getPos2(), false));
		
	}

	/**
	 * @category InOut
	 */
	@Override
	public boolean playerJoin(GamePlayer gp) {
		super.playerJoin(gp);
		
		if(!gp.isOnline())
		{
			return false;
		}
		
		if(!this.canJoin(gp))
		{
			gp.getTeam().remove(gp);
			if(this.getPlayers().size() == 0) this.remove();
			return false;
		}
		
		//Téléportation
		gp.getOnlinePlayer().teleport(this.getStartLocation());
		
		// On ajoute le joueur à l'instance
		this.getInstance().players.add(gp);
		
		//Data
		gp.data.put("lives", this.donjon.getLifeAmount());
		gp.data.put("checkpoint", 0);
		gp.data.put("respawnlocation", this.getStartLocation());
			
		gp.getOnlinePlayer().sendMessage(this.plugin.message("Vous avez rejoint une instance du donjon : "+ ChatColor.GOLD + donjon.getName(), "Donjon"));
		return true;
	}
	
	
	
	/**
	 * @category InOut
	 */
	@Override
	public void playerQuit(GamePlayer gp) {
		this.getInstance().players.remove(gp);
		this.loosers.add(gp);
		super.playerQuit(gp);
	}

	public Location getStartLocation()
	{
		//On regarde si il n'y a pas des player spawn de libre
		
				PlayerSpawnSign nextSpawn = null;
				
				for(Object ms : this.getInstance().listMagicSigns)
				{
					if(ms instanceof PlayerSpawnSign)
					{
						
						PlayerSpawnSign pss = (PlayerSpawnSign) ms;
						if(!pss.getUsed() && (nextSpawn == null || pss.getValue() < nextSpawn.getValue()))
						{
							nextSpawn = pss;
						}
						
						
					}
				}
				
				if(nextSpawn != null)
				{
					nextSpawn.setUsed(true);
					return nextSpawn.getPosition();
				}
				
				//Preference sur panneau a placer
				for(Object ms : this.getInstance().listMagicSigns)
				{
					if(ms instanceof EntranceSign)
					{
						
						
						return ((MagicSign) ms).getPosition();
					}
				}
				
				Location start;
				if(this.donjon.getPos1() != null)
				{
					start = this.donjon.getPos1().clone();
					start.setWorld(this.getInstance().getWorld());
					return start;
				}
				else
				{
					return this.getInstance().getWorld().getSpawnLocation();
				}
	}

	@Override
	public String getGameType() {
		return "DJ";
	}

	@Override
	public void onCommand(GamePlayer gp, String[] args) {
		super.onCommand(gp, args);
		
		switch (args[0])
		{
		case "quit": case "quitter": case "leave": case "stop": case "sortir": case "l":
			this.playerQuit(gp);
			this.plugin.tools.clearInventory(gp.getOnlinePlayer());
			gp.getOnlinePlayer().teleport(gp.getBackLocation());
			gp.getOnlinePlayer().sendMessage(this.plugin.message("Vous avez quitté le donjon !"));
			break;
			
		default:
			gp.getOnlinePlayer().sendMessage(this.plugin.message("Cette commande n'existe pas !"));
			break;
		}
	}
	
	// LISTENERS 
	
	/**
	 * @category Listener
	 */
	@Override
	public void onPlayerTeleport(PlayerTeleportEvent e) {

		GamePlayer gp = this.plugin.getGamePlayer(e.getPlayer());
		if(gp != null && gp.getGame() != this) return;
		
		if(e.getTo().getWorld().equals(this.getInstance().getWorld()))
		{
			return;
		}
		
		this.playerQuit(gp);
		gp.getOnlinePlayer().sendMessage(this.plugin.message("Vous avez quitté le donjon !"));
		
		return;
		
	}
	
	/**
	 * @category Listener
	 */

	@Override
	public void onPlayerRespawn(PlayerRespawnEvent e) {
		
		GamePlayer gp = this.plugin.getGamePlayer(e.getPlayer());
		if(gp != null && gp.getGame() != this) return;
		
		// C'est un joueur d'instance
		
		//Sortie
		if((int) gp.data.get("lives") > 0)
		{
			//Utilisation de vie
		e.setRespawnLocation(this.getNextRespawnLocation(gp));
		 gp.data.put("lives", (int) gp.data.get("lives")-1);
		 gp.getOnlinePlayer().sendMessage(this.plugin.message("Il vous reste "+ gp.data.get("lives")+"/"+this.donjon.getLifeAmount()+" vies sur le donjon : "+ this.donjon.getName()));
		}
		else
		{
		
		
		gp.getGame().playerQuit(gp);
		gp.getOnlinePlayer().sendMessage(this.plugin.message("Vous avez perdu ! N'hésitez pas à recommencer !"));
		this.plugin.tools.clearInventory(gp.getOnlinePlayer());
		e.setRespawnLocation(gp.getBackLocation());
		}
		
		
	}
	
	/**
	 * @category Listener
	 */
	
	@Override
	public void onPlayerQuit(PlayerQuitEvent e) {
		
		GamePlayer gp = this.plugin.getGamePlayer(e.getPlayer());
		if(gp != null && gp.getGame() != this) return;
		
		Player p = e.getPlayer();
		
		
		//Sortie
		this.plugin.tools.clearInventory(p);
		
		p.teleport(gp.getBackLocation());
		
		
		this.playerQuit(gp);
		
	}
	
	

	/**
	 * @category Listener
	 */

	@Override
	public void onCreatureSpawn(CreatureSpawnEvent e) {
		
		if(e.getSpawnReason().equals(SpawnReason.NATURAL))
		{
			e.setCancelled(true);
		}
	}
	
	
	/**
	 * @category Listener
	 */
	@Override
	public void onVehicleDestroy(VehicleDestroyEvent e) {
		
		if((e.getAttacker() instanceof Player))
		{
			e.setCancelled(true);
		}
	}
	
	
	/**
	 * @category Listener
	 */
	@Override
	public void onHangingBreakByEntity(HangingBreakByEntityEvent e) {
		
		if(!(e.getEntity() instanceof ItemFrame || e.getEntity() instanceof Painting))
		{
			return;
		}
		
		if(!(e.getRemover() instanceof Player))
		{
			return;
		}
		
		if(this.donjon.isBuild())
		{
			return;
		}
		
		e.setCancelled(true);
	}

	/**
	 * @category Listener
	 */
	@Override
	public void onCraftItem(CraftItemEvent e) {
		
		if(!(e.getWhoClicked() instanceof Player))
		{
			return;
		}
		if(!(e.getCurrentItem().getType().equals(Material.MINECART) 
				|| e.getCurrentItem().getType().equals(Material.HOPPER_MINECART)
				|| (e.getCurrentItem() instanceof Vehicle)))
		{
			return;
		}
		
		e.setCancelled(true);
			
	}
	
	
	/**
	 * @category Listener
	 */
	@Override
	public void onBlockBreak(BlockBreakEvent e) {
		if(!this.donjon.isBuild())
		{
			e.setCancelled(true);
		}
	}

	/**
	 * @category Listener
	 */
	@Override
	public void onBlockPlace(BlockPlaceEvent e) {
		if(!this.donjon.isBuild())
		{
			e.setCancelled(true);
		}
	}

	public Location getNextRespawnLocation(GamePlayer gp)
	{
		if((int) gp.data.get("checkpoint") == 0)
		{
			return this.getStartLocation();
		}
		else
		{
			return (Location) gp.data.get("respawnlocation");
		}

	}
	
	// Compilator
	
	@Override
	public void compileSign(Sign s, Instance i) {
		
		super.compileSign(s, i);
		
		switch(s.getLine(0))
		{
		/*
		 * Fonctions globale du compilateur (toutes instances)
		 */
	
		case "=exit": case "=sortie": case "=ex": case "=leave":
			i.listMagicSigns.add(new ExitSign(this.plugin, s));
			break;
		case "=check": case "=checkpoint": case "=save":
			i.listMagicSigns.add(new CheckpointSign(this.plugin, s));
			break;
		
		}
	}

	public Donjon getDonjon() {
		return donjon;
	}

	public void setDonjon(Donjon donjon) {
		this.donjon = donjon;
	}


	//StartSign
	@Override
	public String[] getStartSignLines() {
		
		String[] lines = new String[2];
		lines[0] = ChatColor.GREEN+ "" +ChatColor.BOLD +"- "+this.getPlayers().size()+"/"+this.donjon.getSlot()+" -";
		
		String leader = "-";
		
		if(this.getPlayers().size() != 0)
		{
			leader = ChatColor.AQUA+ ""+this.getLeader().getPlayer().getName();
		}
		
		lines[1] = leader;
				
		return lines;
	}
	
	
	@Override
	public boolean canJoin(GamePlayer gp)
	{
		if(this.getPlayers().size() > this.donjon.getSlot())
		{
			gp.getOnlinePlayer().sendMessage(this.plugin.message("Ce donjon est déjà plein !"));
			return false;
		}
		
		for(GamePlayer looser : this.loosers)
		{
			if(looser.getPlayer().getName().equals(gp.getPlayer().getName()))
			{
				gp.getOnlinePlayer().sendMessage(this.plugin.message("Tu as déjà perdu dans cette instance !"));
				return false;
			}
		}
		
		if(!this.plugin.tools.playerHasClearInventory(gp.getOnlinePlayer()) && this.donjon.isClrInv())
		{
			gp.getOnlinePlayer().sendMessage(this.plugin.message("Tu dois avoir un inventaire vide pour rejoindre ce donjon !"));
			return false;
		}
		return true;
	}

	@Override
	public void onServerStop() {
		// TODO Auto-generated method stub
		super.onServerStop();
		for(GamePlayer gp : this.getPlayers())
		{
			if(gp.isOnline())
			{
				this.plugin.tools.clearInventory(gp.getOnlinePlayer());
				gp.getOnlinePlayer().teleport(gp.getBackLocation());
			}
		}
	}
	
	
	

	
}
