package fr.maxcraft.games.dj.signs;


import org.bukkit.block.Sign;

import fr.maxcraft.GamePlayer;
import fr.maxcraft.MaxcraftGames;
import fr.maxcraft.signs.MagicSign;

/*
 * Panneau sortie
 */
public class CheckpointSign extends MagicSign {

	private boolean general;
	private int checkpointValue;
	
	public CheckpointSign(MaxcraftGames plugin, Sign sign) {
		super(plugin, sign);
		//Default 
		this.checkpointValue = 1;
		this.general = false;
		
		if(this.plugin.MM.isInt(sign.getLine(2)))
		{
			this.checkpointValue = Integer.parseInt(sign.getLine(2));
			
		}
		
		if(sign.getLine(3).equals("general"))
		{
			this.general = true;
		}

	}

	@Override
	public String getType() {
		
		return "Checkpoint (PointNb:"+this.checkpointValue+" Value:"+this.isActivated()+")";
	}

	@Override
	public void on(GamePlayer ip) {
		
		if(this.general)
		{
			for(GamePlayer anip : ip.getInstance().players)
			{
				anip.data.put("checkpoint", this.checkpointValue);
				anip.data.put("respawnlocation", this.getPosition());
			}
		}
		else
		{
		if((int) ip.data.get("checkpoint") <= this.checkpointValue)
		{
			ip.data.put("checkpoint", this.checkpointValue);
			ip.data.put("respawnlocation", this.getPosition());
		}
		}

	}

	



}