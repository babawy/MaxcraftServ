package fr.maxcraft.games.dj.signs;


import org.bukkit.block.Sign;

import fr.maxcraft.GamePlayer;
import fr.maxcraft.MaxcraftGames;
import fr.maxcraft.games.dj.DonjonGame;
import fr.maxcraft.signs.MagicSign;

/*
 * Panneau sortie
 */
public class ExitSign extends MagicSign {

	
	public ExitSign(MaxcraftGames plugin, Sign sign) {
		super(plugin, sign);

	}

	@Override
	public String getType() {
		
		return "Exit (Value:"+this.isActivated()+")";
	}

	@Override
	public void on(GamePlayer ip) {
		
		//A faire
		DonjonGame dg = (DonjonGame) ip.getGame();
		ip.getGame().playerQuit(ip);
		ip.getOnlinePlayer().sendMessage(this.plugin.message("Félicitation ! Vous avez terminé le donjon : "+dg.getDonjon().getName()+" !"));
		ip.getOnlinePlayer().teleport(ip.getBackLocation());
		
	}

	



}