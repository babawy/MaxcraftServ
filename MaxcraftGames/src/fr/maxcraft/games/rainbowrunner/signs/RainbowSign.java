package fr.maxcraft.games.rainbowrunner.signs;

import org.bukkit.Material;
import org.bukkit.block.Sign;






import fr.maxcraft.MaxcraftGames;
import fr.maxcraft.signs.MagicSign;

/*
 * Panneau marqueur de position
 */
public class RainbowSign extends MagicSign {

	private int id = 0;
	private int damage = 0;
	private Material material;
	private boolean start;
	
	
	public RainbowSign(MaxcraftGames plugin, Sign sign) {
		super(plugin, sign);
		
		if(sign.getLine(2).equalsIgnoreCase("start"))
		{
			this.start = true;
		}
		else
		{
			this.start = false;
		}
		
		if(start)
		{
		
		String[] params = sign.getLine(3).split(":");

		this.id = Integer.parseInt(params[0]);
		this.damage = 0;
		
		if(params.length == 2)
		{
			this.damage = Integer.parseInt(params[1]);
		}
		
		this.material = Material.getMaterial(this.id);
		
		}
		
		
	}

	public boolean isStart() {
		return start;
	}

	public void setStart(boolean start) {
		this.start = start;
	}

	@Override
	public String getType() {
		
		return "Rainbow ("+this.id+":"+this.damage+")";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}





	
	
	

}
