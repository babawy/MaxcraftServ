package fr.maxcraft.games.rainbowrunner;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Effect;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import fr.maxcraft.FireworkEffectPlayer;
import fr.maxcraft.Game;
import fr.maxcraft.GamePlayer;
import fr.maxcraft.Instance;
import fr.maxcraft.MaxcraftGames;
import fr.maxcraft.games.dj.signs.CheckpointSign;
import fr.maxcraft.games.dj.signs.ExitSign;
import fr.maxcraft.games.rainbowrunner.signs.RainbowSign;
import fr.maxcraft.maps.Map;
import fr.maxcraft.signs.EntranceSign;
import fr.maxcraft.signs.SpawnerSign;

public class RainbowRunnerGame extends Game{
	
	private Map map;
	private boolean launched;
	ArrayList<Rainbow> rainbows;
	public ArrayList<GamePlayer> loosers;
	public ArrayList<GamePlayer> ingame;
	public ArrayList<WaveTask> waves;

	public RainbowRunnerGame(MaxcraftGames plugin, String parameter,boolean devmod) {
		
		super(plugin, parameter, devmod);
		
		this.map = this.plugin.MapManager.getMap(parameter);
		this.launched = false;
		
		if(this.map == null)
		{
			MaxcraftGames.listGames.remove(this);
			return;
		}
		
		//Création de l'instance
		this.setInstance(new Instance(this.plugin, this, map.getWorldName(), map.getPos1(), map.getPos2(), true));
		
		this.initRainbows();
	}

	@Override
	public String getGameType() {
		return "RR";
	}

	@Override
	public boolean playerJoin(GamePlayer gp) {
		
		 this.getInstance().players.add(gp);
		 gp.data.put("points", 0);
		 if(this.launched)
		 {
			 gp.getOnlinePlayer().teleport(this.getInstance().getMarker("spectator")); 
		 }
		 else
		 {
			 gp.getOnlinePlayer().teleport(this.getInstance().getMarker("lobby")); 
		 }
		
		 gp.getOnlinePlayer().sendMessage(this.plugin.message("Bienvenue au Rainbow Runner ! Suivez les instructions pour démarrer !"));
		 return true;
	}

	@Override
	public void playerQuit(GamePlayer gp) {
		
		this.getInstance().players.remove(gp);
		
		if(this.launched)
		{
		if(this.ingame.contains(gp)) this.ingame.remove(gp);
		if(this.loosers.contains(gp)) this.loosers.remove(gp);
		}
		
		gp.getOnlinePlayer().teleport(gp.getBackLocation());
		gp.getOnlinePlayer().sendMessage(this.plugin.message("Au revoir ! Vous pouvez rejouer au Rainbow Runner à tout moment !"));
		super.playerQuit(gp);
	}

	@Override
	public String[] getStartSignLines() {
		 
			String[] lines = new String[2];
			
			ChatColor color = ChatColor.GREEN;
			if(this.launched) color = ChatColor.DARK_RED;
			
			lines[0] = color+ "" +ChatColor.BOLD +"- "+this.getPlayers().size()+"/"+this.map.getSlot()+" -";
			
			String leader = "-";
			
			if(this.getPlayers().size() != 0)
			{
				leader = ChatColor.AQUA+ ""+this.getLeader().getPlayer().getName();
			}
			
			lines[1] = leader;
					
			return lines;
	}

	@Override
	public boolean isJoinable(Player p) {
		return true;
	}

	@Override
	public void appel(int entry, boolean type, GamePlayer gp) {
		
		if(entry == 1 && type)
		{
			//Démarrage
			if(!this.launched)
			{
			this.launchGame();
			}
			else
			{
				this.getInstance().message("Le rainbow Runner est déjà lancé !");
			}
		
		}
		
		if(entry == 2 && type)
		{
			this.playerQuit(gp);
		}
	}
	
	public void launchGame()
	{
		
		//DEAD ?
		for(GamePlayer gp : this.getPlayers())
		{
		if(gp.isOnline())
		{
			if(gp.getOnlinePlayer().isDead())
			{
				this.getInstance().message("Il faut attendre que tous les joueurs aient respawn pour démarrer à nouveau la partie.");
				return;
			}
		}
		}
		
		this.launched = true;
		this.getInstance().message("Le Rainbow Runner est lancé ! Bonne chance !");
		
		this.loosers = new ArrayList<GamePlayer>();
		this.ingame = new ArrayList<GamePlayer>();
		this.ingame.addAll(this.getPlayers());
		
		
		for(GamePlayer gp : this.getPlayers())
		{
			gp.getOnlinePlayer().teleport(this.getInstance().getMarker("game"));
			
			gp.data.put("points", 0);
			gp.data.put("lives", 1);
			gp.getOnlinePlayer().setHealth((double) ((int)gp.data.get("lives")*2));
			gp.getOnlinePlayer().setFoodLevel(4);
		}
		
		this.waveGen();
	}
	
	public void waveGen()
	{
		this.waves = new ArrayList<WaveTask>();
		WaveTask wave;
		//135400 pts max
		for(int i = 0; i<2400; i++)
		{
			
			//END
			if(i == 2399)
			{
				for(Rainbow r :this.rainbows)
				{
				wave = new WaveTask(this.plugin, this,r, 10, 10, 1000, false, true);
				this.waves.add(wave);
				wave.runTaskLater(this.plugin, i*5);
				}
				return;
			}
			
			wave = null;
			int alea = this.plugin.tools.ramdomInt(0, this.rainbows.size()-1);
			if(i < 60)
			{
				
				//DEV
				
		
				
				int launch = this.plugin.tools.ramdomInt(0, 5);
				
				if(launch == 5)
				{
				wave = new WaveTask(this.plugin, this,this.rainbows.get(alea), 5, 5, 50, false, false);
				}
		
			}
			else if(i<120)
			{
			
				int launch = this.plugin.tools.ramdomInt(0, 3);
				
				if(launch == 3)
				{
				wave = new WaveTask(this.plugin, this,this.rainbows.get(alea), 5, 10, 75, false, false);
				}
				
			}
			else if(i<180)
			{
				
				int launch = this.plugin.tools.ramdomInt(0, 3);
				
				if(launch == 3)
				{
				wave = new WaveTask(this.plugin, this,this.rainbows.get(alea), 4, 10,100, false, false);
				}
			}
			else if(i<240)
			{
				int launch = this.plugin.tools.ramdomInt(0, 3);
				
				if(launch == 3)
				{
				wave = new WaveTask(this.plugin, this,this.rainbows.get(alea), 4, 15, 125, false, false);
				}
			}
			else if(i<300)
			{
				int launch = this.plugin.tools.ramdomInt(0, 2);
				
				if(launch == 2)
				{
				wave = new WaveTask(this.plugin, this,this.rainbows.get(alea), 3, 15, 150, false, false);
				}
			}
			else if(i<360)
			{
				int launch = this.plugin.tools.ramdomInt(0, 2);
				
				if(launch == 2)
				{
				wave = new WaveTask(this.plugin, this,this.rainbows.get(alea), 3, 12, 175, false, false);
				}
			}
			else if(i<420)
			{
				int launch = this.plugin.tools.ramdomInt(0, 1);
				
				if(launch == 1)
				{
				wave = new WaveTask(this.plugin, this,this.rainbows.get(alea), 3, 10, 200, false, false);
				}
			}
			else if(i<480)
			{
				int healnb = this.plugin.tools.ramdomInt(0, 50);
			
				boolean heal = false;
				if(healnb == 50)
				{
					heal = true;
				}
				
				int launch = this.plugin.tools.ramdomInt(0, 1);
				
				if(launch == 1)
				{
				wave = new WaveTask(this.plugin, this,this.rainbows.get(alea), 2, 5, 225, heal, false);
				}
			}
			else if(i<540)
			{
				int healnb = this.plugin.tools.ramdomInt(0, 40);
				
				boolean heal = false;
				if(healnb == 40)
				{
					heal = true;
				}
				
				int launch = this.plugin.tools.ramdomInt(0, 1);
				
				if(launch == 1)
				{
				wave = new WaveTask(this.plugin, this,this.rainbows.get(alea), 2, 10, 250, heal, false);
				}
			}
			else if(i<600)
			{
				int healnb = this.plugin.tools.ramdomInt(0, 30);
				
				boolean heal = false;
				if(healnb == 30)
				{
					heal = true;
				}
				
				int launch = this.plugin.tools.ramdomInt(0, 1);
				
				if(launch == 1)
				{
				wave = new WaveTask(this.plugin, this,this.rainbows.get(alea), 1, 3, 275, heal, false);
				}
			}
			else if(i<660)
			{
				int healnb = this.plugin.tools.ramdomInt(0, 30);
				
				boolean heal = false;
				if(healnb == 30)
				{
					heal = true;
				}
				
				int launch = this.plugin.tools.ramdomInt(0, 1);
				
				if(launch == 1)
				{
				wave = new WaveTask(this.plugin, this,this.rainbows.get(alea), 1, 4, 300, heal, false);
				}
			}
			else if(i<780)
			{
				int healnb = this.plugin.tools.ramdomInt(0, 30);
				
				boolean heal = false;
				if(healnb == 30)
				{
					heal = true;
				}
				
				int launch = this.plugin.tools.ramdomInt(0, 1);
				
				if(launch == 1)
				{
				wave = new WaveTask(this.plugin, this,this.rainbows.get(alea), 1, 6, 500, heal, false);
				}
			}
			
			else
			{
				int healnb = this.plugin.tools.ramdomInt(0, 20);
				
				boolean heal = false;
				if(healnb == 30)
				{
					heal = true;
				}
				
				
				wave = new WaveTask(this.plugin, this,this.rainbows.get(alea), 1, 4, 700, heal, false);
				
			}
			
			if(wave != null)
			{
			this.waves.add(wave);
			wave.runTaskLater(this.plugin, i*5);
			}
		}
	}
	
	public void loose(GamePlayer gp)
	{
		
		gp.data.put("lives", (int) gp.data.get("lives") - 1);
		this.getInstance().getWorld().playSound(gp.getOnlinePlayer().getLocation(), Sound.ENDERMAN_TELEPORT, 10, 10);
		
		if((int) gp.data.get("lives") <= 0)
		{
		
		gp.getOnlinePlayer().setHealth((double) 0.0);
		this.getInstance().message(ChatColor.RED + gp.getOnlinePlayer().getName()+ ChatColor.GRAY +" a perdu avec "+ChatColor.GREEN + ChatColor.BOLD + gp.data.get("points")+" points "+ChatColor.RESET + ChatColor.GRAY+"!");
		this.ingame.remove(gp);
		this.loosers.add(gp);
		
		//Fin animation
		
		if(this.ingame.size() == 0)
		{
			this.endGame();
		}
		
		}
		
		else
		{
			this.getInstance().message(ChatColor.RED + gp.getOnlinePlayer().getName()+ ChatColor.GRAY +" a perdu une vie, plus que "+ gp.data.get("lives")+ " vie(s) !");
			gp.getOnlinePlayer().setHealth((double) ((int)gp.data.get("lives")* 2));
		}
		
	}
	
	public void heal(GamePlayer gp)
	{
		
		gp.data.put("lives", (int) gp.data.get("lives")+ 1);
		gp.getOnlinePlayer().setHealth((double) ((int)gp.data.get("lives")*2));
		this.getInstance().getWorld().playSound(gp.getOnlinePlayer().getLocation(), Sound.ANVIL_LAND, 10, 10);
		this.getInstance().message(ChatColor.RED + gp.getOnlinePlayer().getName()+ ChatColor.GRAY +" a gagné une vie !");
	}
	
	public void endGame()
	{
		this.getInstance().message("La partie est terminée !");
		this.getInstance().message(ChatColor.GREEN + "" +ChatColor.BOLD +"LES SCORES");
		
		for(GamePlayer gp : this.loosers)
		{
			this.getInstance().message("- "+ gp.getPlayer().getName()+" => " + ChatColor.GREEN + gp.data.get("points")+" pts");
		}
		
		this.launched = false;
		
		for(WaveTask wt : this.waves)
		{
			wt.cancel();
		}
		
		for(GamePlayer gp : this.getPlayers())
		{
			if(gp.isOnline())
			{
				gp.getOnlinePlayer().teleport(this.getInstance().getMarker("lobby"));
			}
		}
	}

	@Override
	public void onCommand(GamePlayer gp, String[] args) {
		switch(args[0])
		{
		case "leave": case "l": case "q": case "quitter":
			this.playerQuit(gp);
			break;
		}
	}

	@Override
	public void onCreatureSpawn(CreatureSpawnEvent e) {
		// TODO Auto-generated method stub
		e.setCancelled(true);
	}

	@Override
	public void onBlockBreak(BlockBreakEvent e) {
		e.setCancelled(true);
	}

	@Override
	public void onBlockPlace(BlockPlaceEvent e) {
		e.setCancelled(true);
	}

	@Override
	public void onPlayerQuit(PlayerQuitEvent e) {
		
		
		GamePlayer gp = this.plugin.getGamePlayer(e.getPlayer());
		this.playerQuit(gp);
		
	}

	@Override
	public void onPlayerRespawn(PlayerRespawnEvent e) {
		
		if(this.launched)
		{
		e.setRespawnLocation(this.getInstance().getMarker("spectator"));
		}
		else
		{
		e.setRespawnLocation(this.getInstance().getMarker("lobby"));
		}
	}


	@Override
	public void onPlayerDamage(EntityDamageEvent e) {
		e.setCancelled(true);
	}
	
	@Override
	public void compileSign(Sign s, Instance i) {
		
		super.compileSign(s, i);
		
		switch(s.getLine(0))
		{
	
		case "=rainbow": case "=rb":
			i.listMagicSigns.add(new RainbowSign(this.plugin, s));
			break;
		}
	}
	
	public void initRainbows()
	{
		// Recherche
		this.rainbows = new ArrayList<Rainbow>();
		
		for(Object o : this.getInstance().listMagicSigns)
		{
			if(o instanceof RainbowSign)
			{
			
			RainbowSign rs = (RainbowSign) o;
			
			if(rs.isStart())
			{
			
			Block b = rs.getPosition().getBlock();
			ArrayList<Block> rainbow = new ArrayList<Block>();
			
			while(!this.isEndRainbow(b))
			{
			rainbow.add(b);
			b = b.getRelative(rs.getFacing());
			
			if(this.isEndRainbow(b))
			{
			b.setType(rs.getMaterial());
			b.setData((byte) rs.getDamage());
			}
			
			}
			
			this.rainbows.add(new Rainbow(this.plugin, rs.getMaterial(), rs.getDamage(), rainbow));
			
			}
			}
		}
		
		//Placage des blocs
		for(Rainbow rb : this.rainbows)
		{
			rb.init();
		}
	}
	
	public boolean isEndRainbow(Block b)
	{
		for(Object o : this.getInstance().listMagicSigns)
		{
			if((o instanceof RainbowSign))
			{
			
			RainbowSign rs = (RainbowSign) o;
			if(rs.getPosition().getBlock().equals(b))
			{
			if(!rs.isStart())
			{
				return true;
			}
			}
			}
			
			
		}
		
		return false;
	}

	public boolean isLaunched() {
		return launched;
	}

	public void setLaunched(boolean launched) {
		this.launched = launched;
	}
	
	
	
	
}

class Rainbow
{
	public Material material;
	public int damage;
	public ArrayList<Block> blocks;
	
	Rainbow(MaxcraftGames plugin, Material material, int damage, ArrayList<Block> blocks)
	{
	this.material = material;
	this.damage = damage;
	this.blocks = blocks;
	}
	
	public void init()
	{
		for(Block b : this.blocks)
		{
			b = b.getRelative(0, -1, 0);
			b.setType(this.material);
			b.setData((byte) this.damage);
		}
	}
	
	
}

class BlockActionTask extends BukkitRunnable {
	private MaxcraftGames plugin;
	private Block block;
	private boolean direction;
	private Rainbow rainbow;
	private boolean isKilling;
	private RainbowRunnerGame rrg;
	private WaveTask wave;
	
	public BlockActionTask(MaxcraftGames plugin,RainbowRunnerGame rrg, Block b, boolean direction, Rainbow rainbow, boolean isKilling, WaveTask wave)
	{
		this.plugin = plugin;
		this.block = b;
		this.direction = direction;
		this.rainbow = rainbow;
		this.rrg = rrg;
		this.isKilling = isKilling;
		this.wave = wave;

	}
	
	 public void run() {
		 //Kill
		 
		 if(this.isKilling)
		 {
			 for(GamePlayer gp : rrg.getPlayers())
			 {
				 if(rrg.ingame.contains(gp))
				 {
				 if(gp.isOnline())
				 {
					 if(gp.getOnlinePlayer().getLocation().getBlock().equals(this.block))
					 {
						 if(!this.wave.heal)
						 {
							 if(this.wave.autokill)
							 {
							 gp.data.put("lives", (int) 0);
							 }
							 
							 rrg.loose(gp);
						 }
						 else
						 {
							 rrg.heal(gp);
						 }
					 }
				 }
				 }
			 }
		 }
		 
		 if(this.direction == false)
		 {
			 this.block.setType(Material.AIR);
		 }
		 else
		 {
			 if(!this.wave.heal)
			 {
			 this.block.setType(rainbow.material);
			 this.block.setData((byte) rainbow.damage);
			 }
			 else
			 {
				 this.block.setType(Material.GLOWSTONE);
			 }
		 }
		 
	    }

}

class WaveTask extends BukkitRunnable {
	private int speed;
	private Rainbow rainbow;
	private MaxcraftGames plugin;
	private RainbowRunnerGame rrg;
	private int size;
	private int points;
	public boolean heal;
	public boolean autokill;
	
	public WaveTask(MaxcraftGames plugin, RainbowRunnerGame rrg,Rainbow rainbow, int speed, int size, int points, boolean heal, boolean autokill)
	{
		this.plugin = plugin;
		this.rainbow = rainbow;
		this.speed = speed;
		this.rrg = rrg;
		this.size = size;
		this.points = points;
		this.heal = heal;
		this.autokill = autokill;
	}
	
	 public void run() 
	 {
		if(!rrg.isLaunched())
		{
			//Arret des vagues
			return;
		}
		
		for(GamePlayer gp :this.rrg.ingame)
		{
			gp.data.put("points", (int) gp.data.get("points") + this.points);
		}
		
		int time = 0;
		for(Block b : rainbow.blocks)
		{
			boolean killing = false;
			if(rainbow.blocks.get(rainbow.blocks.size()-1).equals(b))
			{
			killing = true;	
			}
			
			new BlockActionTask(this.plugin, rrg,b, true, this.rainbow, killing, this).runTaskLater(this.plugin, time);
			new BlockActionTask(this.plugin, rrg, b, false, this.rainbow, false, this).runTaskLater(this.plugin, time+size);
			time += speed;
		}
	    
	 }

}
