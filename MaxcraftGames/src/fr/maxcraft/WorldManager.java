package fr.maxcraft;


import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.World.Environment;

import com.onarandombox.MultiverseCore.api.MultiverseWorld;

public class WorldManager {

private MaxcraftGames plugin;
	
	public WorldManager(MaxcraftGames plugin)
	{
		this.plugin = plugin;
	}
	
	public boolean isMVLoaded(String world)
	{	
		for(MultiverseWorld MVw : this.plugin.Multiverse.getMVWorldManager().getMVWorlds())
		{
			if(MVw.getName().equals(world))
			{
				return true;
			}
		}
		return false;
	}
	
	public boolean isInFolder(String world)
	{
		File file = new File(this.plugin.Multiverse.getServerFolder().getPath(), world);
		if(file.exists() && file.isDirectory())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean isMVUnloaded(String world)
	{
		for(String w : this.plugin.Multiverse.getMVWorldManager().getUnloadedWorlds())
		{
			if(w.equals(world)) return true;
			
		}
		return false;
	}
	
	public void load(String world)
	{
		if(this.isMVLoaded(world)) return;
		
		if(this.isMVUnloaded(world))
		{
			//LOAD
			this.plugin.Multiverse.getMVWorldManager().loadWorld(world);
			MultiverseWorld mw = this.plugin.Multiverse.getMVWorldManager().getMVWorld(world);
			mw.setDifficulty("NORMAL");
			return;
		}
		if(this.isInFolder(world))
		{
			//ADD
			this.plugin.Multiverse.getMVWorldManager().addWorld(world, Environment.NORMAL, null, null, null, null);
		}
	}
	
	public void unload(String world)
	{
		if(this.isMVLoaded(world))
		{
			this.plugin.Multiverse.getMVWorldManager().unloadWorld(world);
		}
	}
	
	public void remove(String world)
	{
		if(this.isMVLoaded(world))
		{
		this.plugin.Multiverse.getMVWorldManager().removeWorldFromConfig(world);
		}
	}
}
