package fr.maxcraft;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import fr.maxcraft.signs.BossSign;
import fr.maxcraft.signs.CaptorSign;
import fr.maxcraft.signs.RedstoneActivatorSign;


public class InstanceListener implements Listener{

	//VARIABLES
		private MaxcraftGames plugin;
					
				
		//CONSTRUCTOR
		public InstanceListener(MaxcraftGames p)
		{
		this.plugin = p;
		}
		
		@EventHandler(priority = EventPriority.NORMAL)
		public void onCaptor(PlayerMoveEvent e){
		

			Player p = e.getPlayer();
			GamePlayer gp = this.plugin.getGamePlayer(p);
			
			if(gp == null || gp.getGame().getInstance() == null)
			{
				return;
			}
			
			// C'est un joueur d'instance
			
			//Calculs des distances des capteurs on off
			Instance i = gp.getGame().getInstance();
			for(CaptorSign captor : i.findCaptors("ONetOFF"))
			{
				
			
				double distance = captor.getPosition().distance(e.getTo());
				
				
				if(distance <= captor.getRadius())
				{
					
					//Dans le rayon du capteur
					
					//On active le capteur (terminÃ©)
					captor.on(gp);
					
				}
			}

		}
		
		//BOSS
		
		@EventHandler(priority = EventPriority.NORMAL)
		public void onBossDeath(EntityDeathEvent e){
			
		
			Player killer = null;

			Instance instance = null;
			for(Instance i : MaxcraftGames.listInstances)
			{
				if(i.getWorld().equals(e.getEntity().getLocation().getWorld()))
				{
					instance = i;
				}
			}
			
			if(instance == null)
			{
				return;
			}
			
			GamePlayer gp = instance.getLeader();

			
			for(Object o  : instance.getListMagicSigns())
			{
				if(o instanceof BossSign)
				{
					BossSign bs = (BossSign) o;
					if(bs.isActivated() && bs.getBoss().equals(e.getEntity()))
					{
						bs.sendAppel(gp);
					}
				}
			}
			
		}
		
		
		//Activator redstone
				@EventHandler(priority = EventPriority.NORMAL)
				public void onRedstoneActivated(BlockPhysicsEvent e){
					Instance instance = null;
					if(e.getBlock().isBlockIndirectlyPowered()||e.getBlock().isBlockPowered())
					
					
					for(Instance i : this.plugin.listInstances)
					{
						if(i.getWorld().equals(e.getBlock().getLocation().getWorld()))
						{
							instance = i;
						}
					}
					
					if(instance == null)
					{
						return;
					}
					
				
					
					//On est dans une instance
					GamePlayer ip = instance.getNearestPlayer(e.getBlock().getLocation());
					
					
					//
					for(Object o  : instance.getListMagicSigns())
					{
						if(o instanceof RedstoneActivatorSign)
						{
							RedstoneActivatorSign ras = (RedstoneActivatorSign) o;
							Location loclist = e.getBlock().getLocation();
							if(loclist.equals(ras.getPosition()))
							{
								
								if(ras.getTypeActivator().equals("normal") && (e.getBlock().isBlockIndirectlyPowered() || e.getBlock().isBlockPowered()))
								{
								
								ras.sendAppel(ip, "normal");
								ras.setActivated(true);
								}
								
								if(ras.getTypeActivator().equals("switch"))
								{
								if((e.getBlock().isBlockIndirectlyPowered() || e.getBlock().isBlockPowered()))
								{
									if(ras.isActivated())
									{
										return;
									}
									ras.sendAppel(ip, "on");
									ras.setActivated(true);
								}
								if(!(e.getBlock().isBlockIndirectlyPowered() || e.getBlock().isBlockPowered()))
								{
									if(!ras.isActivated())
									{
										return;
									}
									ras.sendAppel(ip, "off");
									ras.setActivated(false);
								}
								}
							}
						}
					}
					
					
				}
}
