package fr.maxcraft.signs;

import org.bukkit.block.Sign;
import org.bukkit.entity.EntityType;

import fr.maxcraft.GamePlayer;
import fr.maxcraft.MaxcraftGames;
import fr.maxcraft.signs.tasks.SpawnTask;

/*
 * Panneau redstone
 */
public class SpawnerSign extends MagicSign {

	private EntityType mob;
	private int frequence;
	private int amount;
	private String option;
	
	
	public SpawnerSign(MaxcraftGames plugin, Sign sign) {
		super(plugin, sign);
		
		//Default
		this.mob = EntityType.ZOMBIE;
		this.frequence = -1;
		this.amount = 1;
		this.option = sign.getLine(2);
		
		
		//Mob type
		switch(this.option)
		{
		case "zombie": case "zomb": case "z": case "zhalloween":
			this.mob = EntityType.ZOMBIE;
			break;
		case "skeleton": case "squelette": case "s":
			this.mob = EntityType.SKELETON;
			break;
		case "bat": case "batman":
			this.mob = EntityType.BAT;
			break;
		case "blaze":
			this.mob = EntityType.BLAZE;
			break;
		case "cavespider":
			this.mob = EntityType.CAVE_SPIDER;
			break;
		case "chicken": case "poulet":
			this.mob = EntityType.CHICKEN;
			break;
		case "cow": case "vache":
			this.mob = EntityType.COW;
			break;
		case "creeper":
			this.mob = EntityType.CREEPER;
			break;
		case "dragon": case "enderdragon":
			this.mob = EntityType.ENDER_DRAGON;
			break;
		case "enderman":
			this.mob = EntityType.ENDERMAN;
			break;
		case "ghast":
			this.mob = EntityType.GHAST;
			break;
		case "giant":
			this.mob = EntityType.GIANT;
			break;
		case "horse":
			this.mob = EntityType.HORSE;
			break;
		case "golem": case "irongolem":
			this.mob = EntityType.IRON_GOLEM;
			break;
		case "magmacube": case "magma":
			this.mob = EntityType.MAGMA_CUBE;
			break;
		case "mushroom": case "mushroomcow":
			this.mob = EntityType.MUSHROOM_COW;
			break;
	   case "ocelelot":
			this.mob = EntityType.OCELOT;
			break;
		case "pig":
			this.mob = EntityType.PIG;
			break;
		case "pigzombie":
			this.mob = EntityType.PIG_ZOMBIE;
			break;
		case "sheep":
			this.mob = EntityType.SHEEP;
			break;
		case "silverfish":
			this.mob = EntityType.SILVERFISH;
			break;
		case "slime":
			this.mob = EntityType.SLIME;
			break;
		case "spider":
			this.mob = EntityType.SPIDER;
			break;
		case "squid":
			this.mob = EntityType.SQUID;
			break;
		case "villager":
			this.mob = EntityType.VILLAGER;
			break;
		case "witch":
			this.mob = EntityType.WITCH;
			break;
		case "wolf":
			this.mob = EntityType.WOLF;
			break;
		}
		
		//Params
	String[] params = sign.getLine(3).split(" ");
		
		for(String param : params)
		{
			String[] values = param.split(":");
			
			switch(values[0])
			{
			case "f": case "F": 
				if(this.plugin.MM.isInt(values[1]))
				{
					this.frequence= Integer.parseInt(values[1]);
				}
				break;
			case "a": case "nb": case "n": case "amount":
				if(this.plugin.MM.isInt(values[1]))
				{
					this.amount = Integer.parseInt(values[1]);
				}
				break;
			}
		}
		
	}

	@Override
	public String getType() {
		
		return "Spawner (Value:"+this.isActivated()+" Mob:"+this.mob.getEntityClass().getName()+" Amount:"+this.amount+" Frequence:"+this.frequence+")";
	}

	@Override
	public void on(GamePlayer ip) {
		
		//Spawner
		for(int i =0; i<this.amount; i++)
		{

			if(this.frequence == -1)
			{
				new SpawnTask(this.plugin, this).runTaskLater(this.plugin, 0);
			}
			else
			{
				new SpawnTask(this.plugin, this).runTaskLater(this.plugin, this.frequence*20*i);
			}
		}
		
	}

	public EntityType getMob() {
		return mob;
	}

	public void setMob(EntityType mob) {
		this.mob = mob;
	}

	public int getFrequence() {
		return frequence;
	}

	public void setFrequence(int frequence) {
		this.frequence = frequence;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}





}
