package fr.maxcraft.signs;

import org.bukkit.block.Sign;

import fr.maxcraft.GamePlayer;
import fr.maxcraft.MaxcraftGames;

/*
 * Panneau de message dans chat
 */
public class ChatSign extends MagicSign {

	private String text;
	
	public ChatSign(MaxcraftGames plugin, Sign sign) {
		super(plugin, sign);
		
		this.text = sign.getLine(2) + " "+ sign.getLine(3);
		
	}

	@Override
	public String getType() {
		
		return "Chat (Value:"+this.isActivated()+" Text:"+this.text+")";
	}

	@Override
	public void on(GamePlayer ip) {
		
		
		for(GamePlayer oneip : ip.getInstance().players)
		{
			if(oneip.isOnline())
			{
				oneip.getOnlinePlayer().sendMessage(this.plugin.message(text));
			}
		}
		
	}

	

	

	

}