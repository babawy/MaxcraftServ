package fr.maxcraft.signs;

import org.bukkit.block.Sign;

import fr.maxcraft.Instance;
import fr.maxcraft.GamePlayer;
import fr.maxcraft.MaxcraftGames;
import fr.maxcraft.signs.tasks.ActivateTask;


public class RedstoneActivatorSign extends MagicSign{

	private int linkON;
	private int linkOFF;
	private String typeActivator;
	
	public RedstoneActivatorSign(MaxcraftGames plugin, Sign sign) {
		super(plugin, sign);
		
		//Default
		this.linkON = 0;
		this.linkOFF = 0;
		this.typeActivator = "normal";
		
		//Configuration
		
		// Sorties OFF et ON
		
		String[] params = sign.getLine(2).split(" ");
		
		for(String param : params)
		{
			String[] values = param.split(":");
			
			switch(values[0])
			{
			case "ON": case "on": case "1":
				if(this.plugin.MM.isInt(values[1]))
				{
					this.linkON = Integer.parseInt(values[1]);
				}
				break;
			case "OFF": case "off": case "0":
				if(this.plugin.MM.isInt(values[1]))
				{
					this.linkOFF = Integer.parseInt(values[1]);
				}
				break;
			}
		}
		
		//Parametres
		params = sign.getLine(3).split(" ");
		
		for(String param : params)
		{
			String[] values = param.split(":");
		
			switch(values[0])
			{
			//Type
			case "type": case "t":
				if(values[1].equalsIgnoreCase("switch"))
				{
					this.typeActivator = "switch";
				}
				break;
			}
		
		}
	}
	
	public void sendAppel(GamePlayer ip, String type)
	{
		if(this.typeActivator.equals("normal") && this.isActivated())
		{
			return;
		}
		
		if(this.typeActivator.equals("normal"))
		{
			if(this.linkON != 0)
			{
				new ActivateTask(this.plugin, ip, ip.getInstance(), true, this.linkON).runTaskLater(this.plugin, 0);
			}
		
			if(this.linkOFF != 0)
			{
				new ActivateTask(this.plugin, ip, ip.getInstance(), false, this.linkOFF).runTaskLater(this.plugin, 0);
			}
		}
		
		else if(this.typeActivator.equals("switch"))
		{
			if(type.equals("on") && this.linkON != 0)
			{
				new ActivateTask(this.plugin, ip, ip.getInstance(), true, this.linkON).runTaskLater(this.plugin, 0);
			}
			
			if(type.equals("off") && this.linkOFF != 0)
			{
				new ActivateTask(this.plugin, ip, ip.getInstance(), false, this.linkOFF).runTaskLater(this.plugin, 0);
			}
		}
		
		
	}

	public int getLinkON() {
		return linkON;
	}

	public void setLinkON(int linkON) {
		this.linkON = linkON;
	}

	public int getLinkOFF() {
		return linkOFF;
	}

	public void setLinkOFF(int linkOFF) {
		this.linkOFF = linkOFF;
	}

	public String getTypeActivator() {
		return typeActivator;
	}

	public void setTypeActivator(String typeActivator) {
		this.typeActivator = typeActivator;
	}

	@Override
	public String getType() {
		return "Redstone Activator (Value:"+this.isActivated()+" Type:"+this.typeActivator+")";
	}
	

	

}
