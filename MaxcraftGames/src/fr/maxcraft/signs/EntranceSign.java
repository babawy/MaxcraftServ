package fr.maxcraft.signs;

import org.bukkit.block.Sign;

import fr.maxcraft.MaxcraftGames;

/*
 * Panneau d'entrée du donjon, unique.
 */
public class EntranceSign extends MagicSign {

	
	public EntranceSign(MaxcraftGames plugin, Sign sign) {
		super(plugin, sign);
		
	}

	@Override
	public String getType() {
		
		return "Entrance";
	}

	

}
