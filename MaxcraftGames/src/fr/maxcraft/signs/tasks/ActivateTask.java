package fr.maxcraft.signs.tasks;

import org.bukkit.scheduler.BukkitRunnable;

import fr.maxcraft.GamePlayer;
import fr.maxcraft.Instance;
import fr.maxcraft.MaxcraftGames;

public class ActivateTask extends BukkitRunnable{

	private MaxcraftGames plugin;
	private GamePlayer ip;
	private Instance instance; 
	boolean type;
	int entry;
	
	
	public ActivateTask(MaxcraftGames plugin,GamePlayer ip, Instance instance, boolean type, int entry)
	{
		this.plugin = plugin;
		this.instance = instance;
		this.type = type;
		this.entry = entry;

		this.ip = ip;
	}
	
	 	public void run() 
	 	{
			this.instance.sendAppel(entry, type, ip);
	    }
}
