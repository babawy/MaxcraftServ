package fr.maxcraft.signs.tasks;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Zombie;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import fr.maxcraft.MaxcraftGames;
import fr.maxcraft.signs.SpawnerSign;

public class SpawnTask extends BukkitRunnable{

	
	private MaxcraftGames plugin;
	private SpawnerSign spawner;
	
	public SpawnTask(MaxcraftGames plugin, SpawnerSign spawner)
	{
		this.plugin = plugin;
		this.spawner = spawner;
	}
	
	 public void run() {
	        // What you want to schedule goes here
		 Entity entity = this.spawner.getPosition().getWorld().spawnEntity(this.spawner.getPosition(), this.spawner.getMob());
		 
		 if(this.spawner.getMob().equals(EntityType.SKELETON))
		 {
			 LivingEntity le = (LivingEntity) entity;
			 le.getEquipment().setItemInHand(new ItemStack(Material.BOW));
		 }
		 
		 if(this.spawner.getOption().equals("zhalloween"))
		 {
			 Zombie zombie = (Zombie) entity;
			 zombie.getEquipment().setHelmet(new ItemStack(Material.PUMPKIN));
		 }
		 
	    }

}
