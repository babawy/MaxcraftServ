package fr.maxcraft.signs;

import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import fr.maxcraft.MaxcraftGames;

public class StockChestSign extends MagicSign{

	private String target;
	private Chest chest = null;
	private int proba;
	
	public StockChestSign(MaxcraftGames plugin, Sign sign) {
		super(plugin, sign);
		
		//Default
		this.proba = 100;

		this.target = sign.getLine(1);
		
	//Proba
		if(this.plugin.MM.isInt(sign.getLine(2)))
		{
			this.proba = Integer.parseInt(sign.getLine(2));
		}
		
		//Recherche du coffre
		
			org.bukkit.material.Sign s = (org.bukkit.material.Sign) sign.getData();
			Block chestBlock = sign.getBlock().getRelative(s.getAttachedFace());
			
			try
			{
				this.chest = (Chest) chestBlock.getState();
			}
			catch(Exception e)
			{
			}

	
				
				

	}

	@Override
	public String getType() {
		
		if(this.chest != null)
		{
			return "StockChest (Chest:"+this.chest.toString()+", Name:"+this.target+")";
		}
		return "StockChest (Chest:AUCUN, Name:"+this.target+")";
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Chest getChest() {
		return chest;
	}

	public void setChest(Chest chest) {
		this.chest = chest;
	}

	public int getProba() {
		return proba;
	}

	public void setProba(int proba) {
		this.proba = proba;
	}
	


}