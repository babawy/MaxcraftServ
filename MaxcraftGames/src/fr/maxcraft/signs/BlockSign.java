package fr.maxcraft.signs;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Sign;

import fr.maxcraft.GamePlayer;
import fr.maxcraft.MaxcraftGames;

public class BlockSign extends MagicSign{
	
	private int id1 = 0;
	private int damage1 = 0;
	private int id2 =0;
	private int damage2 = 0;
	private Material m1;
	private Material m2;

	public BlockSign(MaxcraftGames plugin, Sign sign) {
		super(plugin, sign);
		
		//blocks
		try
		{
			
		String[] params = sign.getLine(2).split(" ");
		String[] values1 = params[0].split(":");
		String[] values2 = params[1].split(":");
		
		this.id1 = Integer.parseInt(values1[0]);
		if(values1.length == 2)
		{
		this.damage1 = Integer.parseInt(values1[1]);
		}
		this.id2 = Integer.parseInt(values2[0]);
		if(values2.length == 2)
		{
		this.damage2 = Integer.parseInt(values2[1]);
		}
		
		this.m1 = Material.getMaterial(id1);
		this.m2 = Material.getMaterial(id2);
		
		//coords
		
		String[] coords = sign.getLine(3).split(" ");
		if(coords.length == 3)
		{
			Location coord = this.getPosition().clone();
			coord.setX(Integer.parseInt(coords[0]));
			coord.setY(Integer.parseInt(coords[1]));
			coord.setZ(Integer.parseInt(coords[2]));
			this.setPosition(coord);
		}
		
		}
		
		catch(Exception e)
		{
			this.m1 = Material.AIR;
			this.m2 = Material.AIR;
		
		}
		
	}

	@Override
	public String getType() {
		
		return "Block (Value:"+this.isActivated()+")";
	}

	@Override
	public void on(GamePlayer ip) {
		
		this.getPosition().getBlock().setType(m1);
		this.getPosition().getBlock().setData((byte) damage1);
		super.on(ip);
	}

	@Override
	public void off(GamePlayer ip) {
		
		this.getPosition().getBlock().setType(m2);
		this.getPosition().getBlock().setData((byte) damage2);
		super.off(ip);
	}

	public int getId1() {
		return id1;
	}

	public void setId1(int id1) {
		this.id1 = id1;
	}

	public int getDamage1() {
		return damage1;
	}

	public void setDamage1(int damage1) {
		this.damage1 = damage1;
	}

	public int getId2() {
		return id2;
	}

	public void setId2(int id2) {
		this.id2 = id2;
	}

	public int getDamage2() {
		return damage2;
	}

	public void setDamage2(int damage2) {
		this.damage2 = damage2;
	}
	
	
}
