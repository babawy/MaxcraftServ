package fr.maxcraft.signs;

import java.util.ArrayList;

import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.inventory.ItemStack;
import fr.maxcraft.Instance;
import fr.maxcraft.MaxcraftGames;

public class TargetChestSign extends MagicSign{

	private String target;
	private Chest chest = null;
	
	public TargetChestSign(MaxcraftGames plugin, Sign sign) {
		super(plugin, sign);

		this.target = sign.getLine(1);
		
	
			org.bukkit.material.Sign s = (org.bukkit.material.Sign) sign.getData();
			Block chestBlock = sign.getBlock().getRelative(s.getAttachedFace());
			
			try
			{
				this.chest = (Chest) chestBlock.getState();
			}
			catch(Exception e)
			{
			}

	
				
				

	}

	@Override
	public String getType() {
		
		if(this.chest != null)
		{
			return "TargetChest (Chest:"+this.chest.toString()+", Name:"+this.target+")";
		}
		return "TargetChest (Chest:AUCUN, Name:"+this.target+")";
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Chest getChest() {
		return chest;
	}

	public void setChest(Chest chest) {
		this.chest = chest;
	}
	
	public ArrayList<StockChestSign> stockSigns(Instance i)
	{
	 ArrayList<StockChestSign> stocks = new ArrayList<StockChestSign>();
	 
	 for(Object o : i.getListMagicSigns())
		{
			if(o instanceof StockChestSign)
			{
				StockChestSign ss = (StockChestSign) o;
				if(ss.getTarget().equals(this.target))
				{
					stocks.add(ss);
				}
			}
		}
	 
	 return stocks;
	}
	
	public void fill(Instance i)
	{
		
		
		if(this.chest == null)
		{
			return;
		}
		
		
		StockChestSign selectionStock = null;
		ArrayList<StockChestSign> stocks = this.stockSigns(i);
		//Selection du coffre au hasard
		
		int sommeProba = 0;
		for(StockChestSign ss : stocks)
		{
			sommeProba += ss.getProba();
		}
		
		
		
		int tirage = this.plugin.tools.ramdomInt(1, sommeProba);
		int n = 0;
		
		
		
		for(StockChestSign ss : stocks)
		{
			n += ss.getProba();
			if(tirage <= n)
			{
				selectionStock = ss;
				break;
			}
		}
		if(selectionStock == null)
		{
			return;
		}
		
	
		
		ItemStack[] contents = selectionStock.getChest().getBlockInventory().getContents().clone();
		this.chest.getBlockInventory().setContents(contents);
	}
	


}
