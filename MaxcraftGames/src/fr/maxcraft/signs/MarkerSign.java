package fr.maxcraft.signs;

import org.bukkit.block.Sign;

import fr.maxcraft.MaxcraftGames;

/*
 * Panneau marqueur de position
 */
public class MarkerSign extends MagicSign {

	private String marker;
	public MarkerSign(MaxcraftGames plugin, Sign sign) {
		super(plugin, sign);
		
		this.marker = sign.getLine(2);
	}

	@Override
	public String getType() {
		
		return "Marker (type: "+this.marker+")";
	}

	public String getMarker() {
		return marker;
	}

	public void setMarker(String marker) {
		this.marker = marker;
	}

	
	
	

}
