package fr.maxcraft.signs;


import org.bukkit.block.Sign;

import fr.maxcraft.MaxcraftGames;

/*
 * Panneau d'entrée du donjon, unique.
 */
public class PlayerSpawnSign extends MagicSign {

	private int value;
	private Boolean used;
	public PlayerSpawnSign(MaxcraftGames plugin, Sign sign) {
		super(plugin, sign);
		
		if(this.plugin.MM.isInt(sign.getLine(2)))
		{
			this.value = Integer.parseInt(sign.getLine(2));
		}
		
		this.used =false;
		
	}

	@Override
	public String getType() {
		
		return "Playerspawn";
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public Boolean getUsed() {
		return used;
	}

	public void setUsed(Boolean used) {
		this.used = used;
	}



	
	

}
