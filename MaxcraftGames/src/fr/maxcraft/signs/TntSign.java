package fr.maxcraft.signs;

import org.bukkit.block.Sign;
import fr.maxcraft.GamePlayer;
import fr.maxcraft.MaxcraftGames;

/*
 * Panneau explosion
 */
public class TntSign extends MagicSign {

	private float radius;
	
	public TntSign(MaxcraftGames plugin, Sign sign) {
		super(plugin, sign);

		try { 
			this.radius = Float.parseFloat(sign.getLine(2));
		} 
		catch (Exception e) { 
			this.radius = 3;
		}
	}

	@Override
	public String getType() {
		
		return "Explosion (Value:"+this.isActivated()+" Radius:"+this.radius+")";
	}

	@Override
	public void on(GamePlayer ip) {
		this.getPosition().getWorld().createExplosion(this.getPosition(), this.radius);
		
	}





}