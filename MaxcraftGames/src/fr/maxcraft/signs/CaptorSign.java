package fr.maxcraft.signs;

import org.bukkit.Location;
import org.bukkit.block.Sign;

import fr.maxcraft.GamePlayer;
import fr.maxcraft.Instance;
import fr.maxcraft.GamePlayer;
import fr.maxcraft.MaxcraftGames;
import fr.maxcraft.signs.tasks.ActivateTask;


public class CaptorSign extends MagicSign {

	
	private int timer;
	private double radius;
	private int linkON;
	private int linkOFF;
	
	public CaptorSign(MaxcraftGames plugin, Sign sign) {
		super(plugin, sign);
		
		//Par default
		this.setActivated(false);
		
		this.radius = (float) 5.0;
		this.linkON = 0;
		this.linkOFF = 0;
		this.timer = 0;
		
		//Configuration
		
		// Sorties OFF et ON
		
		String[] params = sign.getLine(2).split(" ");
		
		for(String param : params)
		{
			String[] values = param.split(":");
			
			switch(values[0])
			{
			case "ON": case "on": case "1":
				if(this.plugin.MM.isInt(values[1]))
				{
					this.linkON = Integer.parseInt(values[1]);
				}
				break;
			case "OFF": case "off": case "0":
				if(this.plugin.MM.isInt(values[1]))
				{
					this.linkOFF = Integer.parseInt(values[1]);
				}
				break;
			}
		}
	

	
		//Parametres
		params = sign.getLine(3).split(" ");
		
		for(String param : params)
		{
			String[] values = param.split(":");
		
			switch(values[0])
			{
			//Rayon
			case "r":
				try
				{
					this.radius = Double.parseDouble(values[1]);
				}
				catch(Exception e)
				{
					
				}
					
				
				break;
				//Timer
			case "t":
				if(this.plugin.MM.isInt(values[1]))
				{
					this.timer = Integer.parseInt(values[1]);
				}
				break;
				//Value ON OFF
			case "v":
				if(values[1].equalsIgnoreCase("on"))
				{
					this.setActivated(true);
				}
				break;
			}
		}
	}

	@Override
	public void on(GamePlayer ip)
	{
		
		if(this.isActivated())
		{
			return;
		}
		
		this.setActivated(true);
		
		
		//Activation
		if(this.linkON != 0)
		{
		new ActivateTask(this.plugin, ip, ip.getInstance(), true, this.linkON).runTaskLater(this.plugin, this.getTimer()*20);
		}
		
		if(this.linkOFF != 0)
		{
		new ActivateTask(this.plugin, ip, ip.getInstance(), false, this.linkOFF).runTaskLater(this.plugin, this.getTimer()*20);
		}
		
		
		if(ip.getInstance().isDevMod())
		{
			if(ip.isOnline())
			{
				ip.getOnlinePlayer().sendMessage("Vous avez activé le capteur !");
			}
		}
	
	}

	@Override
	public void off(GamePlayer ip)
	{
		if(!this.isActivated())
		{
			return;
		}
		
		this.setActivated(false);

		
		if(ip.getInstance().isDevMod())
		{
			if(ip.isOnline())
			{
				ip.getOnlinePlayer().sendMessage("Vous avez reinitialisé le capteur (OFF) !");
			}
		
		}
		
		
		
	}
	
	public void sendAppel(GamePlayer ip)
	{
		
		Instance i = ip.getInstance();
		for(Object ms : i.getListMagicSigns())
		{
			if(ms instanceof MagicSign)
			{
				MagicSign sign = (MagicSign) ms;
				
					//State différent,on cherche les entrées
				
					for(int link : sign.getEntries())
					{
						if(link == this.linkON)
						{
							//Sign lié ! On execute !

								sign.on(ip);
								if(i.isDevMod())
								{
									if(ip.isOnline())
									{
										ip.getOnlinePlayer().sendMessage("Appel ON réalisé sur : "+sign.getType());
									}
								
								}

						}
						
						if(link == this.linkOFF)
						{
							//Sign lié ! On execute !
								sign.off(ip);
								if(i.isDevMod())
								{
									if(ip.isOnline())
									{
										ip.getOnlinePlayer().sendMessage("Vous avez activé le capteur !");
									}
								}
						}
					}
					

			}
		}
	
		
	}
	
	

	public int getTimer() {
		return timer;
	}

	public void setTimer(int timer) {
		this.timer = timer;
	}
	public int getLinkON() {
		return linkON;
	}

	public void setLinkON(int linkON) {
		this.linkON = linkON;
	}

	public int getLinkOFF() {
		return linkOFF;
	}

	public void setLinkOFF(int linkOFF) {
		this.linkOFF = linkOFF;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	@Override
	public String getType() {
		
		return "Captor (Sortie ON:"+this.linkON+" Sortie OFF:"+this.linkOFF+" Entrées:"+this.getEntries().toString()+" Radius:"+this.radius+" Time:"+this.timer+" Valeur:"+this.isActivated();
	}

	
}
