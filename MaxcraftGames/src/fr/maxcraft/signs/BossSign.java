package fr.maxcraft.signs;

import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Spider;
import org.bukkit.entity.Zombie;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import fr.maxcraft.Instance;
import fr.maxcraft.GamePlayer;
import fr.maxcraft.MaxcraftGames;
import fr.maxcraft.signs.tasks.ActivateTask;

public class BossSign extends MagicSign {

	private int linkON;
	private int linkOFF;
	private String bossParam;
	private LivingEntity boss = null;
	
	public BossSign(MaxcraftGames plugin, Sign sign) {
		super(plugin, sign);
		
		this.linkOFF = 0;
		this.linkON = 0;
		
	// Sorties OFF et ON
		
		String[] params = sign.getLine(2).split(" ");
		
		for(String param : params)
		{
			String[] values = param.split(":");
			
			switch(values[0])
			{
			case "ON": case "on": case "1":
				if(this.plugin.MM.isInt(values[1]))
				{
					this.linkON = Integer.parseInt(values[1]);
				}
				break;
			case "OFF": case "off": case "0":
				if(this.plugin.MM.isInt(values[1]))
				{
					this.linkOFF = Integer.parseInt(values[1]);
				}
				break;
			}
		}
	
	//BOSS
		this.bossParam = sign.getLine(3);
	}

	@Override
	public void on(GamePlayer ip) {
		
	
    switch(this.bossParam)
    {
    case "gardienZombie":

    	this.boss = (LivingEntity) ip.getInstance().getWorld().spawnEntity(this.getPosition(), EntityType.ZOMBIE);
    	
    	//Configuration du boss
    
    	this.boss.setCustomName("Gardien Zombie");
    	this.boss.setCustomNameVisible(true);
    	this.boss.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20000,1));
    	
    	this.boss.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
    	this.boss.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
    	this.boss.getEquipment().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
    	this.boss.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
    	this.boss.getEquipment().setItemInHand(new ItemStack(Material.DIAMOND_SWORD));
    	
    	break;
    	
    case "templeZombie":

    	this.boss = (LivingEntity) ip.getInstance().getWorld().spawnEntity(this.getPosition(), EntityType.GIANT);
    	
    	//Configuration du boss
    
    	this.boss.setCustomName("Gardien Zombie");
    	this.boss.setCustomNameVisible(true);
    	this.boss.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20000,1));
    	
    	this.boss.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
    	this.boss.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
    	this.boss.getEquipment().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
    	this.boss.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
    	this.boss.getEquipment().setItemInHand(new ItemStack(Material.DIAMOND_SWORD));
    	
    	break;
    	
    case "netherguardian":

    	this.boss = (Creature) ip.getInstance().getWorld().spawnEntity(this.getPosition(), EntityType.WITHER);
    	
    	//Configuration du boss
    	this.boss.setCustomName("Gardien du Nether");
    	this.boss.setCustomNameVisible(true);
    	this.boss.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20000,1));

    	break;
    case "garu":
    	
    	Spider spider = (Spider) ip.getInstance().getWorld().spawnEntity(this.getPosition(), EntityType.SPIDER);
    	this.boss = (Skeleton) ip.getInstance().getWorld().spawnEntity(this.getPosition(), EntityType.SKELETON);
    	this.boss.setCustomName("Garu");
    	this.boss.setCustomNameVisible(true);
    	
    	this.boss.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20000,1));
    	this.boss.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 20000,1));
    	this.boss.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20000,1));
    	
    	this.boss.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
    	this.boss.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
    	this.boss.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
    	this.boss.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
    	this.boss.getEquipment().setItemInHand(new ItemStack(Material.IRON_SWORD));
    	
    	spider.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20000,2));
    	spider.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20000,2));
    	spider.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 20000,0));
    	
    	spider.setPassenger(boss);
    	
    	break;
    	
    case "mrhalloween":

    	this.boss = (Creature) ip.getInstance().getWorld().spawnEntity(this.getPosition(), EntityType.ZOMBIE);
    	
    	//Configuration du boss
    	this.boss.setCustomName("Mr Halloween");
    	this.boss.setCustomNameVisible(true);
    	this.boss.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 200000,0));
    	this.boss.setMaxHealth(50.00);
    	this.boss.setHealth(50.00);
    	Zombie zombie = (Zombie) this.boss;
    	zombie.getEquipment().setHelmet(new ItemStack(Material.PUMPKIN));
    	break;
    	
    	
    	default:
    		this.boss = (LivingEntity) ip.getInstance().getWorld().spawnEntity(this.getPosition(), EntityType.ZOMBIE);
    		this.boss.setCustomName("Boss non défini");
        	this.boss.setCustomNameVisible(true);
    		break;
        case "rat":
        	
        	this.boss = (Creature) ip.getInstance().getWorld().spawnEntity(this.getPosition(), EntityType.SILVERFISH);
        	this.boss.setCustomName("Rat");
        	this.boss.setCustomNameVisible(true);
        	
        	this.boss.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 20000,1));
        	this.boss.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20000,1));
        	this.boss.setMaxHealth(25);
        	this.boss.setHealth(25);
        	
        	break;

        case "ratboss":
        	
        	this.boss = (Spider) ip.getInstance().getWorld().spawnEntity(this.getPosition(), EntityType.SPIDER);
        	this.boss.setCustomName("Demon des egouts");
        	this.boss.setCustomNameVisible(true);
        	this.boss.setMaxHealth(100);
        	this.boss.setHealth(100);
        	this.boss.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 20000,10));
        	
        	break;
        	
    }
		
		super.on(ip);
	}

	@Override
	public String getType() {
		return "Boss (Etat:"+this.isActivated()+")";
	}
	
	
	public void sendAppel(GamePlayer ip)
	{
		if(this.linkON != 0)
		{
		new ActivateTask(this.plugin, ip, ip.getInstance(), true, this.linkON).runTaskLater(this.plugin, 0);
		}
		
		if(this.linkOFF != 0)
		{
		new ActivateTask(this.plugin, ip, ip.getInstance(), false, this.linkOFF).runTaskLater(this.plugin, 0);
		}
		
	}

	public int getLinkON() {
		return linkON;
	}

	public void setLinkON(int linkON) {
		this.linkON = linkON;
	}

	public int getLinkOFF() {
		return linkOFF;
	}

	public void setLinkOFF(int linkOFF) {
		this.linkOFF = linkOFF;
	}

	public LivingEntity getBoss() {
		return boss;
	}

	public void setBoss(LivingEntity boss) {
		this.boss = boss;
	}
	
	
	
}
