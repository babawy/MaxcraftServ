package fr.maxcraft.signs;

import org.bukkit.Sound;
import org.bukkit.block.Sign;
import fr.maxcraft.GamePlayer;
import fr.maxcraft.MaxcraftGames;


	/*
	 * Panneau explosion
	 */
	public class SoundSign extends MagicSign {

		private Sound sound;
		private float volume;
		
		public SoundSign(MaxcraftGames plugin, Sign sign) {
			super(plugin, sign);
			
			//Type
			try
			{
			this.sound = Sound.valueOf(sign.getLine(2));
			}
			catch(Exception e)
			{
				this.sound = Sound.ANVIL_BREAK;
			}
			
			

			//Volume
			try { 
				this.volume = Float.parseFloat(sign.getLine(3));
			} 
			catch (Exception e) { 
				this.volume = 1;
			}
		
		}

		@Override
		public String getType() {
			
			return "Sound (Value:"+this.isActivated()+" Sound:"+ this.sound.name()+")";
		}

		@Override
		public void on(GamePlayer ip) {
			this.getPosition().getWorld().playSound(this.getPosition(), this.sound, 1, 0);
			
		}



	
}
