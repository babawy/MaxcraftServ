package fr.maxcraft.signs;

import org.bukkit.block.Sign;
import fr.maxcraft.GamePlayer;
import fr.maxcraft.MaxcraftGames;

/*
 * Panneau strike
 */
public class StrikeSign extends MagicSign {

	
	public StrikeSign(MaxcraftGames plugin, Sign sign) {
		super(plugin, sign);

	}

	@Override
	public String getType() {
		
		return "Strike (Value:"+this.isActivated()+")";
	}

	@Override
	public void on(GamePlayer ip) {
		this.getPosition().getWorld().strikeLightning(this.getPosition());
	}

	



}