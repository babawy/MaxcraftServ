package fr.maxcraft.signs;

import org.bukkit.Material;
import org.bukkit.block.Sign;
import fr.maxcraft.GamePlayer;
import fr.maxcraft.MaxcraftGames;

/*
 * Panneau redstone
 */
public class RedstoneSign extends MagicSign {

	
	public RedstoneSign(MaxcraftGames plugin, Sign sign) {
		super(plugin, sign);
		
		switch(sign.getLine(2))
		{
		case "ON": case "on": case "1":
			this.on(null);
			break;
		}
	}

	@Override
	public String getType() {
		
		return "Redstone (Value:"+this.isActivated()+")";
	}

	@Override
	public void on(GamePlayer ip) {
		
		this.getPosition().getBlock().setType(Material.REDSTONE_BLOCK);
		super.on(ip);
	}

	@Override
	public void off(GamePlayer ip) {
		
		this.getPosition().getBlock().setType(Material.AIR);
		super.off(ip);
	}



}