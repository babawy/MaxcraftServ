package fr.maxcraft.signs;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.util.Vector;

import fr.maxcraft.GamePlayer;
import fr.maxcraft.MaxcraftGames;

public class MagicSign implements Cloneable {

	protected static MaxcraftGames plugin;
	
	private Location position;
	private boolean activated;
	private Sign sign;
	private ArrayList<Integer> entries= new ArrayList<Integer>();;

	

	
	public MagicSign(MaxcraftGames plugin, Sign sign)
	{
		this.plugin = plugin;
		this.activated = false;
		this.sign = sign;
		this.position = sign.getLocation();
		
		//Recherche des enties
		String entrieslist = this.sign.getLine(1);
		
		for(String entry : entrieslist.split(" "))
		{
			if(this.plugin.MM.isInt(entry))
			{
				this.entries.add( Integer.parseInt(entry));
		
			}
			
		}
		
	}
	

	public void on(GamePlayer ip)
	{
		
		this.activated = true;
	}


	public void off(GamePlayer ip)
	{
		this.activated = false;
	}

	public ArrayList<Integer> getEntries() {
		return entries;
	}


	public void setPosition(Location position) {
		this.position = position;
	}


	public void setEntries(ArrayList<Integer> entries) {
		this.entries = entries;
	}


	

	public Location getPosition() {
		return position;
	}




	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	

	public String getType()
	{
		return "Aucun";
	}
	
	public BlockFace getFacing()
	{
		org.bukkit.material.Sign s = (org.bukkit.material.Sign) this.sign.getData();
		return s.getFacing();
	}
	
	
	
}
