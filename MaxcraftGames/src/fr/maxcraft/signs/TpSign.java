package fr.maxcraft.signs;

import org.bukkit.block.Sign;

import fr.maxcraft.GamePlayer;
import fr.maxcraft.MaxcraftGames;

public class TpSign extends MagicSign{

	private boolean general;
	public TpSign(MaxcraftGames plugin, Sign sign) {
		super(plugin, sign);
		
		this.general = false;
		if(sign.getLine(2).equals("general"))
		{
			this.general = true;
		}
		
	}

	

	@Override
	public String getType() {
		
		return "Teleport (Value:"+this.isActivated()+")";
	}

	@Override
	public void on(GamePlayer ip) {
		if(!this.general)
		{	
			if(ip.isOnline())
			{
			ip.getOnlinePlayer().teleport(this.getPosition().add(0.5, 0, 0.5));
			}
		}
		else
		{
			for(GamePlayer anip : ip.getInstance().players)
			{
				if(anip.isOnline())
				{
					anip.getOnlinePlayer().teleport(this.getPosition().add(0.5, 0, 0.5));
				}
			}
		}
	}
}
