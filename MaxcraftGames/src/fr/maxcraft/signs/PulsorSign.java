package fr.maxcraft.signs;

import org.bukkit.Material;
import org.bukkit.block.Sign;

import fr.maxcraft.GamePlayer;
import fr.maxcraft.MaxcraftGames;
import fr.maxcraft.signs.tasks.PulsorTask;


public class PulsorSign extends MagicSign {

	private int frequence;
	private boolean pulsorValue;
	
	
	public PulsorSign(MaxcraftGames  plugin, Sign sign) {
		super(plugin, sign);
		
		this.pulsorValue = false;
		
		try { 
			this.frequence = Integer.parseInt(sign.getLine(2));
		} 
		catch (Exception e) { 
			this.frequence = 1;
		}
		
	}

	@Override
	public void on(GamePlayer ip) {
		super.on(ip);
		this.change();
	}

	@Override
	public void off(GamePlayer ip) {
		
		super.off(ip);
	}

	@Override
	public String getType() {
		return "Pulsor";
	}
	
	public void change()
	{

	 if(this.pulsorValue)
	 {
		 this.getPosition().getBlock().setType(Material.AIR);
		 this.pulsorValue  = false;
	 }
	 else
	 {
		 this.getPosition().getBlock().setType(Material.REDSTONE_BLOCK);
		 this.pulsorValue  = true;
	 }
	 
	 if(this.isActivated())
	 {
	 new PulsorTask(this.plugin, this).runTaskLater(this.plugin, this.frequence);
	 }
	 
	}
	

}
