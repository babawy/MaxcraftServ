package fr.maxcraft.signs;

import org.bukkit.block.Sign;

import fr.maxcraft.GamePlayer;
import fr.maxcraft.MaxcraftGames;

public class ClearInventorySign extends MagicSign {

	private boolean general;
	public ClearInventorySign(MaxcraftGames plugin, Sign sign) {
		super(plugin, sign);

		this.general = false;
		if(sign.getLine(2).equals("general"))
		{
			this.general = true;
		}
	}

	@Override
	public String getType() {
		
		return "ClearInventory (Value:"+this.isActivated()+")";
	}

	@Override
	public void on(GamePlayer ip) {
		if(!this.general)
		{
			if(ip.isOnline())
			{
			ip.getOnlinePlayer().getInventory().clear();
			ip.getOnlinePlayer().getInventory().setBoots(null);
			ip.getOnlinePlayer().getInventory().setLeggings(null);;
			ip.getOnlinePlayer().getInventory().setChestplate(null);;
			ip.getOnlinePlayer().getInventory().setHelmet(null);
			}
		}
		else
		{
			for(GamePlayer anip : ip.getInstance().players)
			{
				if(anip.isOnline())
				{
				anip.getOnlinePlayer().getInventory().clear();
				anip.getOnlinePlayer().getInventory().setBoots(null);
				anip.getOnlinePlayer().getInventory().setLeggings(null);;
				anip.getOnlinePlayer().getInventory().setChestplate(null);;
				anip.getOnlinePlayer().getInventory().setHelmet(null);
				}
				
			}
		}
	}
}
