package fr.maxcraft.signs;

import org.bukkit.block.Sign;

import fr.maxcraft.GamePlayer;
import fr.maxcraft.MaxcraftGames;

public class KillSign extends MagicSign{

	private boolean general;
	public KillSign(MaxcraftGames plugin, Sign sign) {
		super(plugin, sign);
		
		this.general = false;
		if(sign.getLine(2).equals("general"))
		{
			this.general = true;
		}
		
	}

	

	@Override
	public String getType() {
		
		return "Kill (Value:"+this.isActivated()+")";
	}

	@Override
	public void on(GamePlayer ip) {
		if(!this.general)
		{	
			if(ip.isOnline())
			{
			ip.getOnlinePlayer().setHealth((double) 0.0);
			}
		}
		else
		{
			for(GamePlayer anip : ip.getInstance().players)
			{
				if(anip.isOnline())
				{
					anip.getOnlinePlayer().setHealth((double) 0.0);
				}
			}
		}
	}
}
