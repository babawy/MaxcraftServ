package fr.maxcraft;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.block.Sign;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;

import fr.maxcraft.games.dj.signs.CheckpointSign;
import fr.maxcraft.games.dj.signs.ExitSign;
import fr.maxcraft.signs.BlockSign;
import fr.maxcraft.signs.BossSign;
import fr.maxcraft.signs.CaptorSign;
import fr.maxcraft.signs.ChatSign;
import fr.maxcraft.signs.ClearInventorySign;
import fr.maxcraft.signs.EntranceSign;
import fr.maxcraft.signs.KillSign;
import fr.maxcraft.signs.MarkerSign;
import fr.maxcraft.signs.PlayerSpawnSign;
import fr.maxcraft.signs.PulsorSign;
import fr.maxcraft.signs.RedstoneActivatorSign;
import fr.maxcraft.signs.RedstoneSign;
import fr.maxcraft.signs.SoundSign;
import fr.maxcraft.signs.SpawnerSign;
import fr.maxcraft.signs.StockChestSign;
import fr.maxcraft.signs.StrikeSign;
import fr.maxcraft.signs.TargetChestSign;
import fr.maxcraft.signs.TntSign;
import fr.maxcraft.signs.TpSign;
import fr.maxcraft.signs.tasks.ActivateTask;


public class Game {

	private int id;
	public MaxcraftGames plugin;
	private boolean devmod;
	private Instance instance;
	private ArrayList<GameTeam> teams = new ArrayList<GameTeam>();
	private String parameter;
	
	public Game(MaxcraftGames plugin, String parameter, boolean devmod)
	{
		this.plugin = plugin;
		this.devmod = devmod;
		this.parameter = parameter;
		if(parameter == null) this.parameter = "noparam";
		
		this.id = this.plugin.gameId;
		this.plugin.gameId++;
		this.plugin.listGames.add(this);
	}
	
	public boolean isDevMod()
	{
		return this.devmod;
	}
	
	public void compileSign(Sign s, Instance i)
	{
		switch(s.getLine(0))
		{
		/*
		 * Fonctions globale du compilateur (toutes instances)
		 */
		
		case "=captor": case "=c": case "=detector": case "=activator":
			i.listMagicSigns.add(new CaptorSign(this.plugin, s));
			break;
		case "=redstone": case "=rs":
			i.listMagicSigns.add(new RedstoneSign(this.plugin, s));
			break;
		case "=spawner": case "=s": case "=spawn": case "=mob":
			i.listMagicSigns.add(new SpawnerSign(this.plugin, s));
			break;
		case "=message": case "=chat":
			i.listMagicSigns.add(new ChatSign(this.plugin, s));
			break;
		case "=tnt": case "=explosion":
			i.listMagicSigns.add(new TntSign(this.plugin, s));
			break;
		case "=sound": 
			i.listMagicSigns.add(new SoundSign(this.plugin, s));
			break;
		case "=chest": case "=coffre":
			i.listMagicSigns.add(new TargetChestSign(this.plugin, s));
			break;
		case "=stock":
			i.listMagicSigns.add(new StockChestSign(this.plugin, s));
			break;
		case "=boss":
			i.listMagicSigns.add(new BossSign(this.plugin, s));
			break;
		case "=redactivator": case "=ra":
			i.listMagicSigns.add(new RedstoneActivatorSign(this.plugin, s));
			break;
		case "=pulsor": case "=pulse":
			i.listMagicSigns.add(new PulsorSign(this.plugin, s));
			break;
		case "=strike": case "=str":
			i.listMagicSigns.add(new StrikeSign(this.plugin, s));
			break;
		case "=tp": case "=teleport": case "=teleporter":
			i.listMagicSigns.add(new TpSign(this.plugin, s));
			break;
		case "=clear": case "=clrinv": case "=ci": case "=clearinventory":
			i.listMagicSigns.add(new ClearInventorySign(this.plugin, s));
			break;
		case "=block": case "=b":
			i.listMagicSigns.add(new BlockSign(this.plugin, s));
			break;
		case "=entrance":
			i.listMagicSigns.add(new EntranceSign(this.plugin, s));
			break;
		case "=ps": case "=playerspawn":
			i.listMagicSigns.add(new PlayerSpawnSign(this.plugin, s));
			break;
		case "=kill":
			i.listMagicSigns.add(new KillSign(this.plugin, s));
			break;
		case "=marker":
			i.listMagicSigns.add(new MarkerSign(this.plugin, s));
			break;
		}
	}
	
	public String getGameType()
	{
		return "DEFAULT";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Instance getInstance() {
		return instance;
	}

	public void setInstance(Instance instance) {
		this.instance = instance;
	}

	public ArrayList<GameTeam> getTeams() {
		return teams;
	}

	public void setTeams(ArrayList<GameTeam> teams) {
		this.teams = teams;
	}
	
	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public ArrayList<GamePlayer> getPlayers()
	{
		ArrayList<GamePlayer> players = new ArrayList<GamePlayer>();
		for(GameTeam gt : this.teams)
		{
			players.addAll(gt.players);
		}
		return players;
	}
	
	public void onCommand(GamePlayer gp, String[] args)
	{
		/*
		 * Commandes spécifiques au Game
		 */
		
		switch(args[0])
		{
		case "send":
			if(!gp.getOnlinePlayer().hasPermission("maxcraft.admin.games"))
			{
				return;
			}
			
			if(this.instance == null)
			{
				return;
			}
			
			boolean type = true;
			String params[] = args[1].split(":");
			
			int entry = Integer.parseInt(params[1]);
			
			if(params[0].equalsIgnoreCase("ON"))
			{
				new ActivateTask(this.plugin, gp, this.instance, true, entry).runTask(plugin);
			}
			if(params[0].equalsIgnoreCase("OFF"))
			{
				new ActivateTask(this.plugin, gp, this.instance, false, entry).runTask(plugin);
			}
			break;
		}
		
		
		
	}
	
	
	/*
	 * Appelé quand un joueur join le Game
	 */
	public boolean playerJoin(GamePlayer gp)
	{
		return true;
	}
	
	/*
	 * Appelé quand un joueur quitte le Game
	 */
	public void playerQuit(GamePlayer gp)
	{
		GameTeam gt = gp.getTeam();
		gt.remove(gp);
		
		if(gt.players.size() == 0)
		{
			this.teams.remove(gt);
		}
		
		if(this.teams.size() == 0)
		{
			this.remove();
			return;
		}
		if(this.getPlayers().size() == 0)
		{
			this.remove();
			return;
		}
		
		
	}

	/*
	 * Renvoi les 2 lignes à afficher sur les startSigns
	 */
	public String[] getStartSignLines()
	{
		String[] lines = new String[2];
		lines[0] = this.getPlayers().size()+" joueurs";
		
		String leader = "-";
		
		if(this.getPlayers().size() != 0)
		{
			leader = this.getLeader().getPlayer().getName();
		}
		
		lines[1] = leader;
				
		return lines;
	}
	
	//StartSign
	public boolean isJoinable(Player p)
	{
		return true;
	}
	
	
	public GamePlayer getLeader()
	{
		for(GamePlayer p : this.getPlayers())
		{
			return p;
		}
		return null;
		
	}

	public void remove()
	{
		if(this.instance != null)
		{
		this.instance.remove();
		}
		
		this.plugin.listGames.remove(this);
	}
	
	//Listeners
	public void onCreatureSpawn(CreatureSpawnEvent e)
	{
		// TODO Auto-generated method stub
	}

	public void onBlockBreak(BlockBreakEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void onHangingBreakByEntity(HangingBreakByEntityEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void onCraftItem(CraftItemEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void onVehicleDestroy(VehicleDestroyEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void onBlockPlace(BlockPlaceEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void onPlayerQuit(PlayerQuitEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void onPlayerRespawn(PlayerRespawnEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void onPlayerTeleport(PlayerTeleportEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	public void onPlayerDamage(EntityDamageEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	public void onServerStop() {
		// TODO Auto-generated method stub
		for(GamePlayer gp : this.getPlayers())
		{
		this.playerQuit(gp);
		}
		
	}
	
	public boolean canJoin(GamePlayer gp)
	{
		return true;
	}
	
	public void appel(int entry, boolean type, GamePlayer gp)
	{
		return;
	}
	
	
}
