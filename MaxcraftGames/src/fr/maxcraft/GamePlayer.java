package fr.maxcraft;

import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

public class GamePlayer {

	//VARIABLES
	private MaxcraftGames plugin;
	public HashMap<String, Object> data = new HashMap<String, Object>();
	private OfflinePlayer player;
	private Game game;
	private GameTeam team;
	private Location backLocation;
					
	//CONSTRUCTOR
	public GamePlayer(MaxcraftGames p, OfflinePlayer player, Game game)
	{
		
	this.plugin = p;
	this.player = player;
	this.game = game;
	this.team = null;
	this.backLocation = this.getOnlinePlayer().getLocation();
	}

	public OfflinePlayer getPlayer() {
		return player;
	}

	public void setPlayer(OfflinePlayer player) {
		this.player = player;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}
	
	public Player getOnlinePlayer()
	{
		return this.player.getPlayer();
	}
	
	public boolean isOnline()
	{
		return this.player.isOnline();
	}

	public Instance getInstance() {
		return game.getInstance();
	}



	public GameTeam getTeam() {
		return team;
	}

	public void setTeam(GameTeam team) {
		this.team = team;
	}

	public Location getBackLocation() {
		return backLocation;
	}

	public void setBackLocation(Location backLocation) {
		this.backLocation = backLocation;
	}
	
	

}
