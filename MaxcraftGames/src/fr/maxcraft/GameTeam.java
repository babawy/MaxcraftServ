package fr.maxcraft;

import java.util.ArrayList;
import java.util.HashMap;

public class GameTeam {
	
	//VARIABLES
	private MaxcraftGames plugin;
	public ArrayList<GamePlayer> players = new ArrayList<GamePlayer>();
	public HashMap<String, Object> data = new HashMap<String, Object>();
	private Game game;
					
	//CONSTRUCTOR
	public GameTeam(MaxcraftGames p, String name, Game game)
	{
		
	this.plugin = p;
	this.game = game;
	this.data.put("name", name);
	}
	
	public GamePlayer getLeader()
	{
		for(GamePlayer gp : this.players)
		{
			return gp;
		}
		return null;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}
	
	public void add(GamePlayer gp)
	{
		this.players.add(gp);
		gp.setTeam(this);
	}
	
	public void remove(GamePlayer gp)
	{
		this.players.remove(gp);
		gp.setTeam(null);
	}
	
	public void remove()
	{
		this.game.getTeams().remove(this);
	}
}
